import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:flutter/material.dart';

class RowFilter extends StatelessWidget {
  final String title;
  final String value;
  final Function()? onPress;
  final IconData icon;

  const RowFilter(
      {Key? key,
      required this.title,
      required this.value,
      this.onPress,
      required this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Align(
          alignment: Alignment.topLeft,
          child: Text(
            title,
            style: const TextStyle(fontSize: 12, color: textColor),
          ),
        ),
        const SizedBox(
          height: 4,
        ),
        Container(
          height: 36,
          child: TextButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
              foregroundColor: MaterialStateProperty.all<Color>(Colors.black),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  side: BorderSide(color: Color(0xFFE0E0E0)),
                ),
              ),
            ),
            onPressed: onPress,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 9,
                  child: Container(
                    child: Text(
                      value,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: textColor),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Icon(
                    icon,
                    color: Color(0xFFBBC2C6),
                    size: 16,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
