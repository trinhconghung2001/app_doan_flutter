import 'package:flutter/material.dart';

class BottomNavigator extends StatelessWidget {
  final Widget widget;
  const BottomNavigator({super.key, required this.widget});

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 64,
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 2,
            offset: const Offset(0, 2), // changes position of shadow
          ),
        ]),
        child: widget);
  }
}
