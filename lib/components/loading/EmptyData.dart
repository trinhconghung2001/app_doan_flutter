import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../untils/AppLocalizations.dart';

class EmptyData extends StatelessWidget {
  final String? msg;
  final double? scale;

  const EmptyData({
    Key? key,
    this.msg, this.scale = 0.2,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double heigh = MediaQuery.of(context).size.height;
    return Center(
      child: Column(
        children: [
          SizedBox(height: heigh * 0.05),
          SvgPicture.asset(
            "assets/icons/emptydata/EmptyData.svg",
            width: heigh * scale!,
            fit: BoxFit.cover,
          ),
          SizedBox(height: heigh * 0.03),
          Text(
            msg ??
                AppLocalizations.of(context)?.translate('empty_data_msg') ??
                "",
            style: new TextStyle(
              fontSize: 16,
            ),
          ),
        ],
      ),
    );
  }
}
