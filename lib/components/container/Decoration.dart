import 'package:flutter/material.dart';

BoxDecoration BoxDecorationContainer({Color? color}) {
  return BoxDecoration(
    color: color ?? Colors.white,
    borderRadius: BorderRadius.circular(8),
    boxShadow: [
      BoxShadow(
        color: Colors.grey.withOpacity(0.5),
        spreadRadius: 2,
        blurRadius: 2,
        offset: const Offset(0, 2), // changes position of shadow
      ),
    ],
  );
}
