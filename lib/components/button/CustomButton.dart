import 'package:flutter/material.dart';

import '../../config/styles/CustomColor.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final IconData icon;
  final bool iconDisplay;
  final double fontSize, iconSize, spaceIconText, height;
  final Function()? press;
  final Color background, textColor, iconColor;
  const CustomButton({
    Key? key,
    required this.text,
    required this.icon,
    this.iconDisplay = true,
    this.fontSize = 14,
    this.iconSize = 14,
    this.iconColor = Colors.white,
    this.spaceIconText = 4,
    required this.height,
    this.press,
    this.background = primaryColor,
    this.textColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: height,
      child: Center(
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(background),
            foregroundColor: MaterialStatePropertyAll(textColor),
            side:
                const MaterialStatePropertyAll(BorderSide(color: primaryColor)),
            shape: MaterialStatePropertyAll(
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8))),
          ),
          onPressed: press,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Visibility(
                visible: iconDisplay,
                child: Icon(
                  icon,
                  color: iconColor,
                  size: iconSize,
                ),
              ),
              SizedBox(
                width: spaceIconText,
              ),
              Text(
                text,
                textAlign: TextAlign.center,
                style: TextStyle(color: textColor, fontSize: fontSize),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
