import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/config/styles/Dimens.dart';
import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget {
  final String text;
  final double width;
  final double height;
  final Function()? press;
  final Color color, textColor;
  const PrimaryButton({
    Key? key,
    required this.text,
    this.press,
    this.color = buttonTextColor,
    this.textColor = primaryColor,
    required this.width,
    required this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(17, 0, 18, 0),
      width: width,
      height: height,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(defaultButtonRadius),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            backgroundColor: color,
            textStyle: const TextStyle(
                fontWeight: FontWeight.bold, fontSize: defaultButtonText),
          ),
          onPressed: press,
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: textColor,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
