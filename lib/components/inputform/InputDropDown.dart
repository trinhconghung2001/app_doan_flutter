import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:flutter/material.dart';

import '../../untils/AppLocalizations.dart';

class InputDropDown extends StatelessWidget {
  final String title;
  final String value;
  final int maxLines;
  final Function()? onPress;
  final IconData icon;
  final bool showIcon;
  final bool? checkRangBuoc;
  final bool? isError;
  final bool? offEdit;

  const InputDropDown(
      {Key? key,
      required this.title,
      required this.value,
      this.maxLines = 1,
      this.onPress,
      this.isError = false,
      required this.icon,
      this.offEdit = false,
      this.checkRangBuoc = false,
      this.showIcon = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Row(
          children: <Widget>[
            Expanded(
              flex: 4,
              child: Align(
                  alignment: Alignment.topLeft,
                  child: checkRangBuoc == true
                      ? RichText(
                          text: TextSpan(children: [
                          TextSpan(
                            text: title,
                            style: const TextStyle(
                              color: textColor,
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          const TextSpan(
                            text: ' *',
                            style: TextStyle(color: warningColor),
                          )
                        ]))
                      : Text(
                          title,
                          style: const TextStyle(
                            color: textColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          ),
                          // textAlign: TextAlign.left,
                        )),
            ),
            Expanded(
              flex: 6,
              child: Container(
                child: TextButton(
                  style: ButtonStyle(
                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.black),
                    backgroundColor: offEdit == true
                        ? MaterialStatePropertyAll(huyColor)
                        : MaterialStatePropertyAll(Colors.white),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(4.0),
                        side: BorderSide(
                            color: isError! ? warningColor : Color(0xFFE0E0E0)),
                      ),
                    ),
                  ),
                  onPressed: onPress,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 9,
                        child: Container(
                          child: Text(
                            value,
                            maxLines: maxLines,
                            textAlign: TextAlign.left,
                            style: const TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: textColor),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Visibility(
                          visible: showIcon,
                          child: Icon(
                            icon,
                            color: Color(0xFFBBC2C6),
                            size: 16,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
        Visibility(
          visible: isError!,
          child: Row(
            children: [
              Expanded(flex: 4, child: SizedBox()),
              Expanded(
                  flex: 6,
                  child: Container(
                      margin: EdgeInsets.fromLTRB(8, 0, 0, 0),
                      child: Text(
                        AppLocalizations.of(context)!
                            .translate('error_textfield'),
                        style: TextStyle(fontSize: 12, color: Colors.red),
                      ))),
            ],
          ),
        ),
      ],
    );
  }
}
