import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:flutter/material.dart';

class InputText extends StatelessWidget {
  final String? labelText;
  final String? hintText;
  final int maxLines;
  final TextEditingController? textController;
  final FocusNode? focusNode;
  final Color focusedBorderColor;
  final Function(String)? onChanged;

  const InputText(
      {Key? key,
      this.labelText,
      this.hintText,
      this.onChanged,
      this.maxLines = 1,
      this.textController,
      this.focusNode,
      this.focusedBorderColor = primaryColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      maxLines: maxLines,
      cursorColor: secondColor,
      style: TextStyle(
        color: buttonTextColor,
      ),
      controller: textController,
      focusNode: focusNode,
      onChanged: onChanged,
      decoration: InputDecoration(
        labelText: labelText,
        labelStyle: TextStyle(
          color: buttonTextColor,
          fontSize: 16,
        ),
        helperStyle: TextStyle(
          color: Color(0xFFFDFDFD),
          fontSize: 16,
        ),
        hintText: hintText,
        hintStyle: TextStyle(
          color: Colors.white70,
          fontSize: 16,
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: focusedBorderColor,
          ),
        ),
      ),
    );
  }
}
