import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/config/styles/Dimens.dart';
import 'package:flutter/material.dart';

class RowTextView extends StatelessWidget {
  final String title;
  final String content;
  final int flexItemLeft;
  final int flexItemRight;
  final String? iconButtonUrl;
  final Color? color1;
  final Color? color2;
  final double? paddingBottom;

  RowTextView({
    Key? key,
    required this.title,
    required this.content,
    required this.flexItemLeft,
    required this.flexItemRight,
    this.iconButtonUrl,
    this.color1,
    this.color2, this.paddingBottom = defaultPadding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: paddingBottom!),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: flexItemLeft,
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                title,
                style: TextStyle(
                  color: color1 ?? textColor,
                  fontSize: 14,
                ),
                textAlign: TextAlign.left,
              ),
            ),
          ),
          Expanded(
            flex: flexItemRight,
            child: Text(
              content,
              style: TextStyle(
                color: color2 ?? textColor,
                fontSize: 14,
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ],
      ),
    );
  }
}
