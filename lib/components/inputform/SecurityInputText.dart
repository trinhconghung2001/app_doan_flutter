import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/config/styles/Dimens.dart';
import 'package:flutter/material.dart';

class SecurityInputText extends StatelessWidget {
  final String? hintText;
  final Color? colorText;
  final String? labelText;
  final String? value;
  final Color focusedBorderColor;
  final bool isHidePassword;
  final Function(String)? onChanged;
  final Function()? onClickIcon;
  const SecurityInputText(
      {Key? key,
      this.hintText,
      this.labelText,
      this.value,
      this.isHidePassword = true,
      this.onChanged,
      this.onClickIcon,
      this.focusedBorderColor = primaryColor,
      this.colorText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.fromLTRB(19.52, 16, 31.34, 0),
        child: Container(
          child: TextFormField(
              cursorColor: secondColor,
              style: TextStyle(
                color: colorText ?? Color(0xFFFDFDFD),
              ),
              initialValue: value,
              obscureText: isHidePassword,
              onChanged: onChanged,
              decoration: InputDecoration(
                labelText: labelText,
                labelStyle: TextStyle(
                  color: colorText ?? Color(0xFFFDFDFD),
                  fontSize: 16,
                ),
                helperStyle: TextStyle(
                  color: colorText ?? Color(0xFFFDFDFD),
                  fontSize: 16,
                ),
                hintText: hintText,
                hintStyle: TextStyle(
                  color: Colors.white70,
                  fontSize: 16,
                ),
                suffixIcon: IconButton(
                  onPressed: onClickIcon,
                  icon: Icon(
                    isHidePassword ? Icons.visibility_off : Icons.visibility,
                    color: colorText ?? Colors.white,
                    size: defaultIcon,
                  ),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: colorText ?? Colors.white,
                  ),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: focusedBorderColor,
                  ),
                ),
              )),
        ));
  }
}
