import 'package:app_doan_flutter/untils/AppLocalizations.dart';
import 'package:flutter/material.dart';

import '../../config/styles/CustomColor.dart';
import '../../config/styles/Dimens.dart';

class InputTextField extends StatelessWidget {
  final String title;
  final TextEditingController? textController;
  final bool? isShowIcon;
  final Icon? icon;
  final bool? isNumberKeyboard;
  final Function(String)? onChanged;
  final bool show;
  final bool read;
  final Function()? onTap;
  final bool? checkRangBuoc;
  final FocusNode? focusNode;
  final VoidCallback? onPress;
  final Color? backgroudText;
  final bool? isError;
  final String? hintText;

  InputTextField(
      {Key? key,
      required this.title,
      this.textController,
      this.isShowIcon,
      this.onChanged,
      this.isNumberKeyboard = false,
      this.show = true,
      this.read = false,
      this.onTap,
      this.checkRangBuoc,
      this.focusNode,
      this.onPress,
      this.icon,
      this.hintText,
      this.backgroudText,
      this.isError = false});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(children: <Widget>[
          Expanded(
            flex: 4,
            child: Align(
                alignment: Alignment.topLeft,
                child: checkRangBuoc == true
                    ? RichText(
                        text: TextSpan(children: [
                        TextSpan(
                          text: title,
                          style: const TextStyle(
                            color: textColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          ),
                          // textAlign: TextAlign.left,
                        ),
                        const TextSpan(
                          text: ' *',
                          style: TextStyle(color: warningColor),
                        )
                      ]))
                    : RichText(
                        text: TextSpan(children: [
                        TextSpan(
                          text: title,
                          style: const TextStyle(
                            color: textColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          ),
                          // textAlign: TextAlign.left,
                        ),
                      ]))),
          ),
          Expanded(
              flex: 6,
              child: Container(
                height: 36,
                child: TextFormField(
                  maxLines: 1,
                  keyboardType: isNumberKeyboard!
                      ? TextInputType.number
                      : TextInputType.text,
                  style: const TextStyle(
                    color: textColor,
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                  ),
                  controller: textController,
                  onChanged: onChanged,
                  focusNode: focusNode,
                  onTap: onTap,
                  decoration: InputDecoration(
                    focusedBorder: const OutlineInputBorder(
                        borderSide:
                            BorderSide(color: primaryColor, width: 2.0)),
                    contentPadding: const EdgeInsets.only(
                        left: defaultTextInsideBoxPadding, right: 0),
                    fillColor: backgroudText ?? Colors.white,
                    filled: true,
                    hintText: hintText,
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: isError! ? warningColor : Color(0xFFE0E0E0),
                          width: 1.0),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                    suffixIcon: Visibility(
                      // visible: isShowIcon ?? false,
                      visible: textController?.text != '' || isShowIcon == true
                          ? true
                          : false,
                      child: GestureDetector(
                        onTap: onPress,
                        child: icon ??
                            const Icon(
                              Icons.cancel_outlined,
                              size: 14,
                            ),
                      ),
                    ),
                    suffixIconConstraints: const BoxConstraints(
                      minWidth: 30,
                      minHeight: 25,
                    ),
                  ),
                  cursorColor: primaryColor,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  showCursor: show,
                  readOnly: read,
                ),
              ))
        ]),
        Visibility(
          visible: isError!,
          child: Row(
            children: [
              const Expanded(flex: 4, child: SizedBox()),
              Expanded(
                  flex: 6,
                  child: Container(
                      margin: EdgeInsets.fromLTRB(8, 0, 0, 0),
                      child: Text(
                        AppLocalizations.of(context)!
                            .translate('error_textfield'),
                        style: TextStyle(fontSize: 12, color: Colors.red),
                      ))),
            ],
          ),
        ),
      ],
    );
  }
}
