import 'package:app_doan_flutter/untils/AppLocalizations.dart';
import 'package:flutter/material.dart';

import '../../config/styles/CustomColor.dart';
import '../../config/styles/Dimens.dart';

class InputPopup extends StatelessWidget {
  final String? title;
  final String? hintText;
  final String? buttonText;
  final String? value;
  final Function(String)? onChanged;
  final Function()? onPress;

  const InputPopup({
    Key? key,
    this.title,
    this.hintText,
    this.buttonText,
    this.value,
    this.onChanged,
    this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
      child: Container(
        width: MediaQuery.of(context).size.width / 1.2,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.all(defaultPadding),
              child: Text(
                title ?? AppLocalizations.of(context)!.translate('content_note'),
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
              child: TextFormField(
                initialValue: value,
                onChanged: onChanged,
                keyboardType: TextInputType.multiline,
                maxLines: null,
                decoration: InputDecoration(
                  hintText: hintText ?? AppLocalizations.of(context)!.translate('hint_content_note'),
                  contentPadding: const EdgeInsets.only(left: 15),
                  border: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(8.0),
                    borderSide: new BorderSide(),
                  ),
                ),
              ),
            ),
            Padding(
                padding: const EdgeInsets.all(defaultPadding),
                child: SizedBox(
                  width: double.infinity,
                  child: TextButton(
                      style: ButtonStyle(
                          foregroundColor:
                              MaterialStateProperty.all<Color>(Colors.white),
                          backgroundColor:
                              MaterialStateProperty.all<Color>(primaryColor),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                      side: BorderSide(color: primaryColor)))),
                      onPressed: onPress,
                      child: Text(buttonText ?? AppLocalizations.of(context)!.translate('button_close'))),
                ))
          ],
        ),
      ),
    );
  }
}
