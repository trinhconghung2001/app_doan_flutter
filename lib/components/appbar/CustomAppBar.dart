import 'package:flutter/material.dart';

import '../../config/styles/CustomColor.dart';

class CustomAppBar extends StatelessWidget {
  final String text;
  final bool check;
  final bool checkIconBack;
  final Icon? icon;
  final VoidCallback? onPress;
  final VoidCallback? onTap;
  const CustomAppBar({
    Key? key,
    required this.text,
    required this.check,
    this.onPress,
    this.checkIconBack = false,
    this.onTap, this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(text),
      centerTitle: true,
      actions: check == true
          ? <Widget>[
              Builder(
                builder: (context) {
                  return GestureDetector(
                    onTap: onTap,
                    child: SizedBox(
                      width: 50,
                      height: 50,
                      child: icon ??
                      Icon(
                        Icons.control_point_outlined,
                        // size: 14,
                      ),
                    ),
                  );
                },
              )
            ]
          : null,
      backgroundColor: primaryColor,
      automaticallyImplyLeading: !checkIconBack,
      leading: checkIconBack == false
          ? GestureDetector(
              onTap: onPress,
              child: const SizedBox(
                width: 50,
                height: 50,
                child: Icon(
                  Icons.arrow_back_ios,
                  // size: 16,
                ),
              ),
            )
          : null,
    );
  }
}
