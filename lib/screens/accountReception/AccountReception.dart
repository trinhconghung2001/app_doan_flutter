import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../components/appbar/CustomAppBar.dart';
import '../../components/container/Decoration.dart';
import '../../components/inputform/RowTextView.dart';
import '../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../components/loading/EmptyData.dart';
import '../../untils/AppLocalizations.dart';
import '../../untils/FutureManageDoctor.dart';
import '../../untils/StorePreferences.dart';
import '../manage/data/Doctor.dart';
import 'EditManageReception.dart';

class AccountReception extends StatefulWidget {
  final String idSend;
  const AccountReception({super.key, required this.idSend});

  @override
  State<AccountReception> createState() => _AccountReceptionState();
}

class _AccountReceptionState extends State<AccountReception> {
  late Future<void> _loadDetailDoctor;
  Doctor bacSi = Doctor.defaultContructor();
  @override
  void initState() {
    _loadDetailDoctor = loadDetail();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDetailDoctor = loadDetail();
    });
  }

  @override
  Widget build(BuildContext context) {
    print(widget.idSend);
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: CustomAppBar(
          text: AppLocalizations.of(context)!.translate("app_bar_detail_file"),
          check: true,
          onPress: () {
            Navigator.pop(context);
          },
          checkIconBack: true,
          icon: Icon(Icons.edit_document),
          onTap: () {
            Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            EditManageReception(bacSi: bacSi)))
                .then((value) => loadScreen());
          },
        ),
      ),
      body: SingleChildScrollView(
          child: FutureBuilder(
        future: _loadDetailDoctor,
        builder: ((context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return CircularProgressIndicatorWidget();
          } else if (snapshot.hasError) {
            return EmptyData(
                msg: AppLocalizations.of(context)!.translate('error_data_msg'));
          } else {
            return Column(
              children: [
                const SizedBox(
                  height: 16,
                ),
                bacSi.imgURL != ''
                    ? ClipOval(
                        child: CachedNetworkImage(
                        imageUrl: bacSi.imgURL,
                        fit: BoxFit.fill,
                        placeholder: (context, url) => const CircleAvatar(
                          radius: 40,
                          backgroundImage:
                              AssetImage('assets/images/account.jpg'),
                        ),
                        errorWidget: (context, url, error) =>
                            const CircleAvatar(
                          radius: 40,
                          backgroundImage:
                              AssetImage('assets/images/account.jpg'),
                        ),
                        width: MediaQuery.of(context).size.width * 0.6,
                        height: MediaQuery.of(context).size.height * 0.29,
                      ))
                    : ClipOval(
                        child: Image.asset(
                          'assets/images/account.jpg',
                          fit: BoxFit.fill,
                          width: MediaQuery.of(context).size.width * 0.6,
                          height: MediaQuery.of(context).size.height * 0.29,
                        ),
                      ),
                Container(
                  decoration: BoxDecorationContainer(),
                  padding: const EdgeInsets.only(
                    top: 16,
                    left: 16,
                    right: 16,
                  ),
                  margin: const EdgeInsets.only(
                      top: 8, left: 16, right: 16, bottom: 0),
                  child: Column(
                    children: [
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('title_name_doctor'),
                          content: bacSi.ten,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                      RowTextView(
                          title:
                              AppLocalizations.of(context)!.translate('email'),
                          content: bacSi.email,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('account_title_sex'),
                          content: bacSi.gioiTinh,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('account_title_nation'),
                          content: bacSi.danToc,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('account_title_date'),
                          content: bacSi.ngaySinh,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('doctor_title_role'),
                          content: bacSi.role,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('account_title_province'),
                          content: bacSi.tinhThanhPho,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('account_title_district'),
                          content: bacSi.quanHuyen,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('account_title_coummune'),
                          content: bacSi.phuongXa,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('account_title_number_house'),
                          content: bacSi.soNha,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('phone_number'),
                          content: bacSi.sdt,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                    ],
                  ),
                ),
              ],
            );
          }
        }),
      )),
    );
  }

  Future<void> loadDetail() async {
    List<Doctor> listDoctor =
        await readAllDoctor_IdHosital(StorePreferences.getIdHospital());
    for (Doctor e in listDoctor) {
      if (e.id == widget.idSend) {
        bacSi = e;
        break;
      }
    }
  }
}
