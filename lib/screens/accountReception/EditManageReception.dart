import 'dart:io';

import 'package:app_doan_flutter/screens/manage/data/Doctor.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/InputDropDown.dart';
import '../../../components/inputform/InputTextField.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../config/styles/KeyValue.dart';
import '../../../untils/AppLocalizations.dart';
import '../../untils/FuturePicture.dart';
import '../category/ListCategory.dart';

class EditManageReception extends StatefulWidget {
  final Doctor bacSi;
  const EditManageReception({super.key, required this.bacSi});

  @override
  State<EditManageReception> createState() => _EditManageReceptionState();
}

class _EditManageReceptionState extends State<EditManageReception> {
  final FocusNode _focusTen = FocusNode();
  final FocusNode _focusSoNha = FocusNode();
  final FocusNode _focusSDT = FocusNode();

  String textGioiTinh = '';
  String textDanToc = '';
  String textTinhThanhPho = '';
  String textQuanHuyen = '';
  String textPhuongXa = '';
  String textNgaySinh = '';
  String textViTri = '';
  String textEmail = '';
  String urlImg = '';

  bool validate = false;
  DateTime dateTime = DateTime.now();

  @override
  void initState() {
    textTen.text = widget.bacSi.ten;
    textDanToc = widget.bacSi.danToc;
    textGioiTinh = widget.bacSi.gioiTinh;
    textTinhThanhPho = widget.bacSi.tinhThanhPho;
    textQuanHuyen = widget.bacSi.quanHuyen;
    textPhuongXa = widget.bacSi.phuongXa;
    textNgaySinh = widget.bacSi.ngaySinh;
    textViTri = widget.bacSi.role;
    textSDT.text = widget.bacSi.sdt;
    textEmail = widget.bacSi.email;
    textSoNha.text = widget.bacSi.soNha;
    urlImg = widget.bacSi.imgURL;
    super.initState();
    _focusTen.addListener(_onFocusChange);
    _focusSoNha.addListener(_onFocusChange);
    _focusSDT.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    setState(() {});
  }

  @override
  void dispose() {
    _focusTen.dispose();
    _focusSoNha.dispose();
    _focusSDT.dispose();

    super.dispose();
  }

  final TextEditingController textTen = TextEditingController();
  final TextEditingController textSoNha = TextEditingController();
  final TextEditingController textSDT = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text:
                  AppLocalizations.of(context)!.translate("app_bar_edit_file"),
              check: false,
              onPress: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: Scaffold(
            body: SingleChildScrollView(
              child: Column(
                children: [
                  const SizedBox(
                    height: 16,
                  ),
                  Stack(children: [
                    urlImg == widget.bacSi.imgURL
                        ? ClipOval(
                            child: CachedNetworkImage(
                            imageUrl: urlImg,
                            fit: BoxFit.fill,
                            placeholder: (context, url) => const CircleAvatar(
                              radius: 40,
                              backgroundImage:
                                  AssetImage('assets/images/account.jpg'),
                            ),
                            errorWidget: (context, url, error) =>
                                const CircleAvatar(
                              radius: 40,
                              backgroundImage:
                                  AssetImage('assets/images/account.jpg'),
                            ),
                            width: MediaQuery.of(context).size.width * 0.6,
                            height: MediaQuery.of(context).size.height * 0.29,
                          ))
                        : ClipOval(
                            child: Image.file(
                              File(urlImg),
                              fit: BoxFit.cover,
                              width: MediaQuery.of(context).size.width * 0.6,
                              height: MediaQuery.of(context).size.height * 0.29,
                            ),
                          ),
                    Positioned(
                        bottom: 0,
                        right: 10,
                        child: MaterialButton(
                          elevation: 1,
                          onPressed: () {
                            showBottom(context, () async {
                              final ImagePicker picker = ImagePicker();
                              final XFile? image = await picker.pickImage(
                                  source: ImageSource.gallery);
                              if (image != null) {
                                print(
                                    'Image path: ${image.path} -- Mimetype: ${image.mimeType}');
                                setState(() {
                                  urlImg = image.path;
                                });
                                Navigator.pop(context);
                              }
                            }, () async {
                              final ImagePicker picker = ImagePicker();
                              final XFile? image = await picker.pickImage(
                                  source: ImageSource.camera);
                              if (image != null) {
                                print(
                                    'Image path: ${image.path} -- Mimetype: ${image.mimeType}');
                                setState(() {
                                  urlImg = image.path;
                                });
                                Navigator.pop(context);
                              }
                            });
                          },
                          child: Icon(
                            Icons.camera_alt,
                            color: backgroudSearch,
                            size: 30,
                          ),
                        ))
                  ]),
                  Container(
                    decoration: BoxDecorationContainer(),
                    padding: const EdgeInsets.only(
                        top: 16, left: 16, right: 16, bottom: 16),
                    margin: const EdgeInsets.only(
                        top: 8, left: 16, right: 16, bottom: 0),
                    child: Column(
                      children: [
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("title_name_doctor"),
                          checkRangBuoc: true,
                          focusNode: _focusTen,
                          textController: textTen,
                          onPress: () {
                            setState(() {
                              textTen.clear();
                            });
                          },
                          isError: validate && textTen.text.isEmpty,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                          onPress: () {},
                          value: textEmail,
                          title:
                              AppLocalizations.of(context)!.translate("email"),
                          checkRangBuoc: false,
                          showIcon: false,
                          offEdit: true,
                          icon: Icons.arrow_drop_down,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultGioiTinh,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_sex'));
                            },
                            value: textGioiTinh,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_sex"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textGioiTinh.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultDanToc,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_nation'));
                            },
                            value: textDanToc,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_nation"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textDanToc.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              _show();
                            },
                            value: textNgaySinh,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_date"),
                            checkRangBuoc: true,
                            icon: Icons.calendar_month,
                            isError: validate && textNgaySinh.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                          value: textViTri,
                          title: AppLocalizations.of(context)!
                              .translate('doctor_title_role'),
                          checkRangBuoc: false,
                          showIcon: false,
                          offEdit: true,
                          icon: Icons.arrow_drop_down,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultTinh,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_province'));
                            },
                            value: textTinhThanhPho,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_province"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textTinhThanhPho.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultHuyen,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_district'));
                            },
                            value: textQuanHuyen,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_district"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textQuanHuyen.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultXa,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_commune'));
                            },
                            value: textPhuongXa,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_coummune"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textPhuongXa.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("account_title_number_house"),
                          checkRangBuoc: false,
                          focusNode: _focusSoNha,
                          textController: textSoNha,
                          onPress: () {
                            setState(() {
                              textSoNha.clear();
                            });
                          },
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("phone_number"),
                          checkRangBuoc: true,
                          focusNode: _focusSDT,
                          textController: textSDT,
                          onPress: () {
                            setState(() {
                              textSDT.clear();
                            });
                          },
                          isError: validate && textSDT.text.isEmpty,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            bottomNavigationBar: BottomNavigator(
              widget: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: 16,
                  ),
                  Expanded(
                      child: CustomButton(
                    text: AppLocalizations.of(context)!
                        .translate("button_cancel"),
                    height: 32,
                    icon: Icons.cancel_outlined,
                    iconColor: primaryColor,
                    textColor: primaryColor,
                    background: backgroundScreenColor,
                    press: () {
                      Navigator.pop(context);
                    },
                  )),
                  Container(
                    width: 8,
                  ),
                  Expanded(
                      child: CustomButton(
                    text: AppLocalizations.of(context)!
                        .translate("button_update"),
                    icon: Icons.save_alt,
                    height: 32,
                    press: () {
                      checkValidate();
                      if (validate == false) {
                        if (isPhoneNumberValid(textSDT.text) == true) {
                          editDoctor(widget.bacSi.id);
                        } else {
                          showDialog(
                              context: context,
                              builder: ((context) {
                                return ErrorPopup(
                                  message: AppLocalizations.of(context)!
                                      .translate('phone_number_error_format'),
                                  buttonText: 'OK',
                                  onPress: () {
                                    Navigator.pop(context);
                                  },
                                );
                              }));
                        }
                      }
                    },
                  )),
                  Container(
                    width: 16,
                  ),
                ],
              ),
            ),
          )),
    );
  }

  void guiNhanDrop(String loai, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      if (value != null) {
        if (loai == defaultGioiTinh) {
          setState(() {
            textGioiTinh = value;
          });
        } else if (loai == defaultDanToc) {
          setState(() {
            textDanToc = value;
          });
        } else if (loai == defaultTinh) {
          setState(() {
            textTinhThanhPho = value;
          });
        } else if (loai == defaultHuyen) {
          setState(() {
            textQuanHuyen = value;
          });
        } else if (loai == defaultXa) {
          setState(() {
            textPhuongXa = value;
          });
        }
      }
    });
  }

  void _show() async {
    final DateTime? result = await showDatePicker(
      context: context,
      initialDate: dateTime,
      firstDate: DateTime(1600),
      lastDate: DateTime(2025),
      locale: const Locale('vi'),
    );
    if (result != null) {
      setState(() {
        dateTime = result;
        textNgaySinh = '${formatDate(dateTime, [dd, '-', mm, '-', yyyy])}';
      });
    }
  }

  void checkValidate() {
    if (textGioiTinh.isNotEmpty &&
        textDanToc.isNotEmpty &&
        textTinhThanhPho.isNotEmpty &&
        textQuanHuyen.isNotEmpty &&
        textPhuongXa.isNotEmpty &&
        textTen.text.isNotEmpty &&
        textSDT.text.isNotEmpty) {
      setState(() {
        validate = false;
      });
    } else {
      setState(() {
        validate = true;
      });
    }
  }

  bool isPhoneNumberValid(String phoneNumber) {
    // Biểu thức chính quy kiểm tra định dạng số điện thoại cơ bản
    final phoneRegExp = RegExp(
      r'^(\d{10,12})$',
    );
    return phoneRegExp.hasMatch(phoneNumber);
  }

  Future<void> editDoctor(String idDoctor) async {
    String url = '';
      if (urlImg.isNotEmpty) {
        url = await updateProfilePicture(
            StorePreferences.getIdUser(), File(urlImg));
      }
    Doctor bacSiNew = Doctor(
        id: widget.bacSi.id,
        idBenhVien: StorePreferences.getIdHospital(),
        idPhong: widget.bacSi.idPhong,
        idKhoa: widget.bacSi.idKhoa,
        role: widget.bacSi.role,
        ten: textTen.text,
        email: widget.bacSi.email,
        sdt: textSDT.text,
        ngaySinh: textNgaySinh,
        gioiTinh: textGioiTinh,
        danToc: textDanToc,
        tinhThanhPho: textTinhThanhPho,
        quanHuyen: textQuanHuyen,
        phuongXa: textPhuongXa,
        imgURL: url,
        online: widget.bacSi.online,
        hoatDongCuoi: widget.bacSi.hoatDongCuoi,
        pushToken: widget.bacSi.pushToken,
        soNha: textSoNha.text);
    CollectionReference collectionReference =
        FirebaseFirestore.instance.collection('Doctors');
    DocumentReference documentReference =
        await collectionReference.doc(widget.bacSi.id);
    try {
      documentReference.set(bacSiNew.toMap()).then((value) {
        ToastCommon.showToast(AppLocalizations.of(context)!
            .translate('toast_edit_file_family_success'));
        Navigator.pop(context);
      });
    } catch (e) {
      showDialog(
          context: context,
          builder: (context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_edit_file_family_faild'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          });
    }
  }
}
