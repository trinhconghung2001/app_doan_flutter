import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/category/ProvinceViewModel.dart';
import 'package:app_doan_flutter/screens/home/suggestVaccine/Vaccine.dart';
import 'package:app_doan_flutter/screens/manage/data/Department.dart';
import 'package:app_doan_flutter/screens/manage/data/DepartmentAdvise.dart';
import 'package:app_doan_flutter/screens/manage/data/Hospital.dart';
import 'package:app_doan_flutter/screens/manage/data/Room.dart';
import 'package:app_doan_flutter/untils/FutureCalendarRoom.dart';
import 'package:app_doan_flutter/untils/FutureHospital.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../../../../components/appbar/CustomAppBar.dart';
import '../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../components/loading/EmptyData.dart';
import '../../config/styles/Dimens.dart';
import '../../untils/AppLocalizations.dart';
import '../../untils/FutureRoom.dart';
import '../../untils/FutureUser.dart';
import '../login/data/FamilyMember.dart';
import '../login/data/User.dart';

class ListIdCategory extends StatefulWidget {
  final String loai;
  final String? titleAppbar;
  final String? idNguoiThan;

  ListIdCategory(
      {Key? key, required this.loai, this.titleAppbar, this.idNguoiThan})
      : super(key: key);

  @override
  State<ListIdCategory> createState() => _ListIdCategoryState();
}

class _ListIdCategoryState extends State<ListIdCategory> {
  ProvinceViewModel viewModel = new ProvinceViewModel();
  //list trung gian
  List<String> list = [];
  List<String> listSearch = [];
  List<String> listId = [];
  List<String> listIdSearch = [];
  //list luu id Khoa
  bool checkDanhMuc = true;

  FocusNode focusNode = FocusNode();
  TextEditingController textEditingController = TextEditingController();
  late Future<void> _loadDataFuture;
  @override
  void initState() {
    if (widget.loai == defaultHoSo) {
      _loadDataFuture = loadMemberFamily();
    } else if (widget.loai == defaultBenhVien) {
      _loadDataFuture = loadHospital();
    } else if (widget.loai == defaultPhongTiem) {
      _loadDataFuture = loadPhongTiem(StorePreferences.getIdHospital());
    } else if (widget.loai == defaultGoiVaccine) {
      _loadDataFuture =
          loadGoiVaccine(StorePreferences.getIdUser(), widget.idNguoiThan!);
    } else if (widget.loai == defaultKhoa) {
      _loadDataFuture = loadKhoa();
    } else if (widget.loai == defaultKhoaTuVan) {
      _loadDataFuture = loadKhoaTuVan();
    } else if (widget.loai == defaultPhongKham) {
      _loadDataFuture = loadPhong(StorePreferences.getIdDepartment());
    } else if (widget.loai == key_role_doctor_examination) {
      _loadDataFuture = loadAddPhongKham();
    } else if (widget.loai == key_role_doctor_vaccination) {
      _loadDataFuture = loadAddPhongTiem();
    } else if (widget.loai == key_role_doctor_advise) {
      _loadDataFuture = loadAddPhongTuVan();
    }
    super.initState();
  }

  @override
  void dispose() {
    focusNode.dispose();
    textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          // backgroundColor: Colors.grey,
          appBar: PreferredSize(
            preferredSize: const Size.fromHeight(50),
            child: CustomAppBar(
                text:
                    "${AppLocalizations.of(context)!.translate('category')} ${widget.titleAppbar}",
                check: false,
                onPress: () {
                  Navigator.pop(context);
                }),
          ),
          body: Container(
            margin: const EdgeInsets.only(top: 16),
            child: Column(
              children: [
                Visibility(
                  visible: checkDanhMuc == false ? true : false,
                  child: Container(
                    margin:
                        const EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    height: 50,
                    child: TextField(
                        maxLines: 1,
                        style: const TextStyle(
                          color: Color(0xFF444444),
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                        ),
                        onChanged: (value) {
                          searchList(value);
                        },
                        focusNode: focusNode,
                        controller: textEditingController,
                        decoration: InputDecoration(
                          hintText: AppLocalizations.of(context)!
                              .translate('hint_value_find'),
                          contentPadding: const EdgeInsets.only(
                              left: defaultTextInsideBoxPadding, right: 0),
                          fillColor: Colors.white,
                          enabledBorder: const OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(0xFFE0E0E0), width: 1.0),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(4.0),
                          ),
                          suffixIcon: Visibility(
                            // visible: isShowIcon ?? false,
                            visible: focusNode.hasFocus == false ||
                                    textEditingController.text.isNotEmpty
                                ? true
                                : false,
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  textEditingController.clear;
                                  searchList('');
                                });
                              },
                              child: const Icon(
                                Icons.cancel_outlined,
                                size: 14,
                              ),
                            ),
                          ),
                          prefixIcon: const Icon(
                            Icons.search,
                            size: 14,
                          ),
                          prefixIconConstraints: const BoxConstraints(
                            minWidth: 30,
                            minHeight: 25,
                          ),
                          suffixIconConstraints: const BoxConstraints(
                            minWidth: 30,
                            minHeight: 25,
                          ),
                        )),
                  ),
                ),
                // SizedBox(
                //   height: 8,
                // ),
                FutureBuilder(
                  future: _loadDataFuture,
                  builder: ((context, AsyncSnapshot snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return CircularProgressIndicatorWidget();
                    } else if (snapshot.hasError) {
                      return EmptyData(
                          msg: AppLocalizations.of(context)!
                              .translate('error_data_msg'));
                    } else {
                      if (listSearch.length > 0) {
                        return Expanded(
                          child: ListView.builder(
                              itemBuilder: (BuildContext context, int index) {
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context, [
                                      listSearch[index].toString(),
                                      listIdSearch[index].toString()
                                    ]);
                                    print(listId[index].toString());
                                  },
                                  child: Column(
                                    children: [
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        margin: const EdgeInsets.fromLTRB(
                                            16, 0, 16, 0),
                                        height: 50,
                                        // color: Colors.white,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                const BorderRadius.all(
                                                    Radius.circular(5)),
                                            color: const Color.fromRGBO(
                                                255, 255, 255, 1),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.black
                                                    .withOpacity(0.2),
                                                spreadRadius: 1,
                                                blurRadius: 2,
                                                offset: Offset(0, 1),
                                              ),
                                            ]),
                                        // margin: EdgeInsets.all(10),
                                        child: Padding(
                                          padding: const EdgeInsets.all(16.0),
                                          child: Text(
                                            '${index + 1}. ${listSearch[index]}',
                                            style:
                                                const TextStyle(fontSize: 14),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 16,
                                      ),
                                    ],
                                  ),
                                );
                              },
                              itemCount: listSearch.length),
                        );
                      } else {
                        // Không có dữ liệu
                        return CircularProgressIndicatorWidget();
                      }
                    }
                  }),
                ),
              ],
            ),
          )),
    );
  }

  void searchList(String value) {
    setState(() {
      textEditingController.text = value;
      listIdSearch = [];
      listSearch = [];
      for (int i = 0; i < list.length; i++) {
        if (list[i].toLowerCase().contains(value.trim().toLowerCase()) ==
            true) {
          listSearch.add(list[i]);
          listIdSearch.add(listId[i]);
        }
      }
      listSearch = list
          .where(
              (item) => item.toLowerCase().contains(value.trim().toLowerCase()))
          .toList();
    });
  }

  Future<void> loadMemberFamily() async {
    List<String> listHoSo = ['Bản thân'];
    List<String> listIdHoSo = [StorePreferences.getIdUser()];
    List<FamilyMember> listNguoiThan =
        await readListNguoiThan(StorePreferences.getIdUser());
    //reset lai list nguoi than
    for (int i = 0; i < listNguoiThan.length; i++) {
      listHoSo.add(listNguoiThan[i].moiQH);
      listIdHoSo.add(listNguoiThan[i].id);
    }
    setState(() {
      list = listHoSo;
      listSearch = list;
      listId = listIdHoSo;
      listIdSearch = listId;
    });
  }

  Future<void> loadHospital() async {
    List<String> listNameHospital = [];
    List<String> listIdHospital = [];
    List<Hospital> listHospital = await readAllHospital();
    for (Hospital i in listHospital) {
      listNameHospital.add(i.ten);
      listIdHospital.add(i.id);
    }
    setState(() {
      list = listNameHospital;
      listSearch = listNameHospital;
      listId = listIdHospital;
      listIdSearch = listId;
    });
  }

  Future<void> loadPhongTiem(String idBenhVien) async {
    // List<Room> listPhongTiem =
    //     await readAllRoomVaccine_ByIdHospital(idBenhVien);
    // List<String> listTenPhongTiem = [];
    // List<String> listIdPhongTiem = [];
    // listPhongTiem.forEach((element) {
    //   listTenPhongTiem.add(element.tenPhong);
    //   listIdPhongTiem.add(element.id);
    // });
    // setState(() {
    //   list = listTenPhongTiem;
    //   listId = listIdPhongTiem;
    //   listSearch = list;
    //   listIdSearch = listId;
    // });
  }

  Future<void> loadGoiVaccine(String idUser, String idNguoiThan) async {
    if (idNguoiThan == idUser) {
      Users banThan = await readBanThan(idUser);
      DateTime ngaySinh = DateFormat('dd-MM-yyyy').parse(banThan.ngaySinh);
      DateTime ngayHienTai = DateTime.now();
      //tinh su khac biet giữa ngày hiện tại và ngày sinh
      Duration difference = ngayHienTai.difference(ngaySinh);
      int tuoi = (difference.inDays / 365).floor();
      String gioiTinh = banThan.gioiTinh;
      List<Vaccine> listVaccine = [];
      List<String> listIdVaccine = [];
      List<String> listGoiVaccine = [];
      listVaccine = await loadData(tuoi, gioiTinh);
      for (Vaccine i in listVaccine) {
        listGoiVaccine.add(i.ten);
        listIdVaccine.add(i.id);
      }
      setState(() {
        list = listGoiVaccine;
        listId = listIdVaccine;
        listSearch = list;
        listIdSearch = listId;
        checkDanhMuc = false;
      });
    } else {
      List<FamilyMember> listNguoiThan = await readListNguoiThan(idUser);
      FamilyMember nguoiThan = FamilyMember.defaultContructor();
      for (FamilyMember i in listNguoiThan) {
        if (i.id == idNguoiThan) {
          nguoiThan = i;
          break;
        }
      }
      DateTime ngaySinh = DateFormat('dd-MM-yyyy').parse(nguoiThan.ngaySinh);
      DateTime ngayHienTai = DateTime.now();
      //tinh su khac biet giữa ngày hiện tại và ngày sinh
      Duration difference = ngayHienTai.difference(ngaySinh);
      int tuoi = (difference.inDays / 365).floor();
      List<Vaccine> listVaccine = [];
      listVaccine = await loadData(tuoi, nguoiThan.gioiTinh);
      print(listVaccine.length);
      List<String> listGoiVaccine = [];
      List<String> listIdVaccine = [];
      for (Vaccine i in listVaccine) {
        listGoiVaccine.add(i.ten);
        listIdVaccine.add(i.id);
      }
      setState(() {
        list = listGoiVaccine;
        listId = listIdVaccine;
        listSearch = list;
        listIdSearch = listId;
        checkDanhMuc = false;
      });
    }
  }

  Future<void> loadKhoa() async {
    Hospital hospital = await readIDHospital(StorePreferences.getIdHospital());
    List<Department> listKhoa = hospital.khoa;
    List<String> nameDepartment = [];
    List<String> idDepartment = [];
    listKhoa.forEach((e) {
      nameDepartment.add(e.ten);
      idDepartment.add(e.id);
    });
    setState(() {
      list = nameDepartment;
      listSearch = list;
      listId = idDepartment;
      listIdSearch = listId;
      checkDanhMuc = false;
    });
  }

  Future<void> loadKhoaTuVan() async {
    Hospital hospital = await readIDHospital(StorePreferences.getIdHospital());
    List<DepartmentAdvise> listKhoaTuVan = hospital.khoaTuVan;
    List<String> nameDepartment = [];
    List<String> idDepartment = [];
    listKhoaTuVan.forEach((e) {
      nameDepartment.add(e.ten);
      idDepartment.add(e.id);
    });
    setState(() {
      list = nameDepartment;
      listSearch = list;
      listId = idDepartment;
      listIdSearch = listId;
      checkDanhMuc = false;
    });
  }

  Future<void> loadPhong(String idKhoa) async {
    Hospital hospital = await readIDHospital(StorePreferences.getIdHospital());
    List<Department> listKhoa = hospital.khoa;
    List<String> namePhong = [];
    List<String> idPhong = [];
    List<Room> listPhong = [];
    for (Department e in listKhoa) {
      if (e.id == idKhoa) {
        listPhong = e.phongKham;
        break;
      }
    }
    for (int i = 0; i < listPhong.length; i++) {
      await checkDate(StorePreferences.getDate(), StorePreferences.getTime(),
          listPhong[i].id);
      bool checkCalendar = await readCalendarRoom(StorePreferences.getDate(),
          StorePreferences.getTime(), listPhong[i].id);
      if (checkCalendar == true) {
        namePhong.add(listPhong[i].tenPhong);
        idPhong.add(listPhong[i].id);
      }
    }
    setState(() {
      list = namePhong;
      listSearch = list;
      listId = idPhong;
      listIdSearch = listId;
      checkDanhMuc = false;
    });
  }

  Future<List<Vaccine>> loadData(int tuoi, String gioiTinh) async {
    print('Vao loadData');
    DocumentReference documentReference = FirebaseFirestore.instance
        .collection("Categorys")
        .doc('8nDu9e5HN5HkdyhmO4Lz');
    DocumentSnapshot documentSnapshot = await documentReference.get();
    if (documentSnapshot.exists) {
      Map<String, dynamic> dataCategory =
          documentSnapshot.data() as Map<String, dynamic>;
      List<Map<String, dynamic>> result =
          List<Map<String, dynamic>>.from(dataCategory['goiVaccine'] as List);
      List<Vaccine> list = [];
      List<Vaccine> listVaccine = [];
      list = result.map((e) => Vaccine.fromMap(e)).toList();
      if (tuoi >= 18 && tuoi <= 70 && gioiTinh == 'Nam') {
        list.forEach((element) {
          List<dynamic> doiTuong = element.doiTuong;
          doiTuong.forEach((e) {
            if (e == key_adults) {
              listVaccine.add(element);
            }
          });
        });
        return listVaccine;
      } else if (tuoi >= 18 && tuoi <= 70 && gioiTinh == 'Nữ') {
        list.forEach((element) {
          List<dynamic> doiTuong = element.doiTuong;
          for (var e in doiTuong) {
            if (e == key_adults || e == key_woment_1 || e == key_woment_2) {
              listVaccine.add(element);
              break;
            }
          }
        });
        return listVaccine;
      } else if (tuoi > 70) {
        list.forEach((element) {
          List<dynamic> doiTuong = element.doiTuong;
          for (var e in doiTuong) {
            if (e == key_old_person || e == key_old_person_2) {
              listVaccine.add(element);
              break;
            }
          }
        });
        return listVaccine;
      }
    }
    return [];
  }

  Future<void> loadAddPhongTiem() async {
    // List<Room> listPhongTiem =
    //     await readAllRoomVaccine_ByIdHospital(StorePreferences.getIdHospital());
    // List<String> listTenPhongTiem = [];
    // List<String> listIdPhongTiem = [];
    // listPhongTiem.forEach((element) {
    //   if (element.idBacSi.isEmpty) {
    //     listTenPhongTiem.add(element.tenPhong);
    //     listIdPhongTiem.add(element.id);
    //   }
    // });
    // setState(() {
    //   list = listTenPhongTiem;
    //   listId = listIdPhongTiem;
    //   listSearch = list;
    //   listIdSearch = listId;
    // });
  }

  Future<void> loadAddPhongKham() async {
    List<Room> listRoom = await readAllRoomExamination_ByIdHospital_ByIdKhoa(
        StorePreferences.getIdDepartment(), StorePreferences.getIdHospital());
    List<String> listTenPhong = [];
    List<String> listIdPhong = [];
    for (Room i in listRoom) {
      if (i.idBacSi.isEmpty) {
        listTenPhong.add(i.tenPhong);
        listIdPhong.add(i.id);
      }
    }
    setState(() {
      list = listTenPhong;
      listId = listIdPhong;
      listSearch = list;
      listIdSearch = listId;
    });
  }

  Future<void> loadAddPhongTuVan() async {
    List<Room> listPhongTuVan = await readAllRoomAdvise_ByIdHospital_ByIdKhoa(
        StorePreferences.getIdDepartment(), StorePreferences.getIdHospital());
    List<String> listTenPhongTuVan = [];
    List<String> listIdPhongTuVan = [];
    listPhongTuVan.forEach((element) {
      if (element.idBacSi.isEmpty) {
        listTenPhongTuVan.add(element.tenPhong);
        listIdPhongTuVan.add(element.id);
      }
    });
    setState(() {
      list = listTenPhongTuVan;
      listId = listIdPhongTuVan;
      listSearch = list;
      listIdSearch = listId;
    });
  }
}
