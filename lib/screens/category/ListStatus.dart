import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/untils/AppLocalizations.dart';
import 'package:flutter/material.dart';

import '../../../../../../components/appbar/CustomAppBar.dart';

class ListStatus extends StatelessWidget {
  final String loai;
  final String? titleAppbar;
  const ListStatus({super.key, required this.loai, this.titleAppbar});

  @override
  Widget build(BuildContext context) {
    List<String> listStatusMedication = [
      AppLocalizations.of(context)!.translate('category_select_all'),
      status_medication_1,
      status_medication_2,
      status_medication_3
    ];
    List<String> listStatusBook = [
      AppLocalizations.of(context)!.translate('category_select_all'),
      status_1,
      status_2,
      status_3,
      status_4
    ];
    List<String> listRole = [
      AppLocalizations.of(context)!.translate('category_select_all'),
      key_role_doctor_advise,
      key_role_doctor_examination,
      // key_role_doctor_vaccination
    ];
    List<String> listSex = [
      AppLocalizations.of(context)!.translate('category_select_all'),
      'Nam',
      'Nữ'
    ];
    List<String> listStatusResult = [
      AppLocalizations.of(context)!.translate('category_select_all'),
      status_result_1,
      status_result_2,
    ];
    List<String> list = [];
    if (loai == defaultStatusMedication) {
      list = listStatusMedication;
    } else if (loai == defaultStatusBook) {
      list = listStatusBook;
    } else if (loai == key_role_reception) {
      list = listRole;
    } else if (loai == defaultGioiTinh) {
      list = listSex;
    } else if (loai == defaultStatusResult) {
      list = listStatusResult;
    }
    return Scaffold(
        // backgroundColor: Colors.grey,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(50),
          child: CustomAppBar(
              text: "${AppLocalizations.of(context)!.translate('category')} ${titleAppbar}",
              check: false,
              onPress: () {
                Navigator.pop(context);
              }),
        ),
        body: Container(
            margin: const EdgeInsets.only(top: 16),
            child: Expanded(
              child: ListView.builder(
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.pop(context, list[index].toString());
                      },
                      child: Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                            height: 50,
                            // color: Colors.white,
                            decoration: BoxDecoration(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(5)),
                                color: const Color.fromRGBO(255, 255, 255, 1),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black.withOpacity(0.2),
                                    spreadRadius: 1,
                                    blurRadius: 2,
                                    offset: Offset(0, 1),
                                  ),
                                ]),
                            // margin: EdgeInsets.all(10),
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Text(
                                list[index],
                                style: const TextStyle(fontSize: 14),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                        ],
                      ),
                    );
                  },
                  itemCount: list.length),
            )));
  }
}
