import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/category/ProvinceViewModel.dart';
import 'package:app_doan_flutter/screens/category/data/model/Province.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../../../../../components/appbar/CustomAppBar.dart';
import '../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../components/loading/EmptyData.dart';
import '../../config/styles/Dimens.dart';
import '../../untils/AppLocalizations.dart';

class ListCategory extends StatefulWidget {
  final String loai;
  final String? titleAppbar;
  final String? moiQH;

  ListCategory({Key? key, required this.loai, this.titleAppbar, this.moiQH})
      : super(key: key);

  @override
  State<ListCategory> createState() => _ListCategoryState();
}

class _ListCategoryState extends State<ListCategory> {
  ProvinceViewModel viewModel = new ProvinceViewModel();
  //list trung gian
  List<String> listStatusMedication = [
    status_medication_1,
    status_medication_2,
    status_medication_3
  ];
  List<String> listStatusBook = [status_confirm_1, status_confirm_2];
  List<String> listIntensity = [
    key_intensity_few,
    key_intensity_little,
    key_intensity_medium,
    key_intensity_high,
    key_intensity_very_high
  ];
  List<String> listUnit = [unit_gram,unit_teaspoon, unit_tablespoon, unit_cup];
  List<String> list = [];
  List<String> listSearch = [];
  //list luu id Khoa
  bool checkDanhMuc = true;

  FocusNode focusNode = FocusNode();
  TextEditingController textEditingController = TextEditingController();
  late Future<void> _loadDataFuture;
  @override
  void initState() {
    if (widget.loai == defaultTinh) {
      _loadDataFuture = loadProvince();
    } else if (widget.loai == defaultHuyen) {
      _loadDataFuture = loadDistrict();
    } else if (widget.loai == defaultXa) {
      _loadDataFuture = loadWard();
    } else {
      _loadDataFuture = loadDataCategory();
    }
    super.initState();
  }

  @override
  void dispose() {
    focusNode.dispose();
    textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          // backgroundColor: Colors.grey,
          appBar: PreferredSize(
            preferredSize: const Size.fromHeight(50),
            child: CustomAppBar(
                text: "${AppLocalizations.of(context)!.translate('category')} ${widget.titleAppbar}",
                check: false,
                onPress: () {
                  Navigator.pop(context);
                }),
          ),
          body: Container(
            margin: const EdgeInsets.only(top: 16),
            child: Column(
              children: [
                Visibility(
                  visible: checkDanhMuc == false ? true : false,
                  child: Container(
                    margin:
                        const EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    height: 50,
                    child: TextField(
                        maxLines: 1,
                        style: const TextStyle(
                          color: Color(0xFF444444),
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                        ),
                        onChanged: (value) {
                          searchList(value);
                        },
                        focusNode: focusNode,
                        controller: textEditingController,
                        decoration: InputDecoration(
                          hintText: AppLocalizations.of(context)!.translate('hint_value_find'),
                          contentPadding: const EdgeInsets.only(
                              left: defaultTextInsideBoxPadding, right: 0),
                          fillColor: Colors.white,
                          enabledBorder: const OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(0xFFE0E0E0), width: 1.0),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(4.0),
                          ),
                          suffixIcon: Visibility(
                            // visible: isShowIcon ?? false,
                            visible: focusNode.hasFocus == false ||
                                    textEditingController.text.isNotEmpty
                                ? true
                                : false,
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  textEditingController.clear;
                                  searchList('');
                                });
                              },
                              child: const Icon(
                                Icons.cancel_outlined,
                                size: 14,
                              ),
                            ),
                          ),
                          prefixIcon: const Icon(
                            Icons.search,
                            size: 14,
                          ),
                          prefixIconConstraints: const BoxConstraints(
                            minWidth: 30,
                            minHeight: 25,
                          ),
                          suffixIconConstraints: const BoxConstraints(
                            minWidth: 30,
                            minHeight: 25,
                          ),
                        )),
                  ),
                ),
                // SizedBox(
                //   height: 8,
                // ),
                FutureBuilder(
                  future: _loadDataFuture,
                  builder: ((context, AsyncSnapshot snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return CircularProgressIndicatorWidget();
                    } else if (snapshot.hasError) {
                      return EmptyData(
                          msg: AppLocalizations.of(context)!
                              .translate('error_data_msg'));
                    } else {
                      if (listSearch.length > 0) {
                        return Expanded(
                          child: ListView.builder(
                              itemBuilder: (BuildContext context, int index) {
                                return GestureDetector(
                                  onTap: () {
                                    if (widget.loai == defaultTinh) {
                                      StorePreferences.setKeyProvince(
                                          listSearch[index].toString());
                                    } else if (widget.loai == defaultHuyen) {
                                      StorePreferences.setKeyDistricts(
                                          listSearch[index].toString());
                                    }
                                    Navigator.pop(
                                        context, listSearch[index].toString());
                                  },
                                  child: Column(
                                    children: [
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        margin: const EdgeInsets.fromLTRB(
                                            16, 0, 16, 0),
                                        height: 50,
                                        // color: Colors.white,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                const BorderRadius.all(
                                                    Radius.circular(5)),
                                            color: const Color.fromRGBO(
                                                255, 255, 255, 1),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.black
                                                    .withOpacity(0.2),
                                                spreadRadius: 1,
                                                blurRadius: 2,
                                                offset: Offset(0, 1),
                                              ),
                                            ]),
                                        // margin: EdgeInsets.all(10),
                                        child: Padding(
                                          padding: const EdgeInsets.all(16.0),
                                          child: Text(
                                            '${index + 1}. ${listSearch[index]}',
                                            style:
                                                const TextStyle(fontSize: 14),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 16,
                                      ),
                                    ],
                                  ),
                                );
                              },
                              itemCount: listSearch.length),
                        );
                      } else {
                        // Không có dữ liệu
                        return CircularProgressIndicatorWidget();
                      }
                    }
                  }),
                ),
              ],
            ),
          )),
    );
  }

  Future<void> loadDataCategory() async {
    if (widget.loai == defaultStatusMedication) {
      listSearch = listStatusMedication;
    } else if (widget.loai == defaultStatusBook) {
      listSearch = listStatusBook;
    } else if (widget.loai == defaultIntensity) {
      listSearch = listIntensity;
    } else if (widget.loai == defaultUnit) {
      listSearch = listUnit;
    } else {
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('Categorys')
          .doc('8nDu9e5HN5HkdyhmO4Lz');
      DocumentSnapshot documentSnapshot = await documentReference.get();
      if (documentSnapshot.exists) {
        Map<String, dynamic> dataCategory =
            documentSnapshot.data() as Map<String, dynamic>;
        if (widget.loai == defaultBenhVien) {
          list = List<String>.from(dataCategory['benhVien'] as List);
          listSearch = list;
          setState(() {
            checkDanhMuc = false;
          });
        } else if (widget.loai == defaultGioTuVan) {
          listSearch = List<String>.from(dataCategory['gioTuVan'] as List);
        } else if (widget.loai == defaultGioKham) {
          listSearch = List<String>.from(dataCategory['gioKham'] as List);
        } else if (widget.loai == defaultLoaiKham) {
          listSearch = List<String>.from(dataCategory['loaiKham'] as List);
        } else if (widget.loai == defaultGioTiem) {
          listSearch = List<String>.from(dataCategory['gioTiem'] as List);
        } else if (widget.loai == defaultQuanHe) {
          listSearch = List<String>.from(dataCategory['moiQH'] as List);
        } else if (widget.loai == defaultGioiTinh) {
          listSearch = List<String>.from(dataCategory['gioiTinh'] as List);
        } else if (widget.loai == defaultCachDung) {
          listSearch = List<String>.from(dataCategory['cachDung'] as List);
        } else if (widget.loai == defaultDanToc) {
          list = List<String>.from(dataCategory['danToc'] as List);
          listSearch = list;
          setState(() {
            checkDanhMuc = false;
          });
        }
      } else {
        list = [];
      }
    }
  }

  Future<void> loadProvince() async {
    try {
      viewModel.getAllProvince();
      viewModel.getprovice.listen((data) {
        List<String> listProvice = [];
        for (Province i in data) {
          listProvice.add(i.name);
        }
        setState(() {
          list = listProvice;
          listSearch = list;
          checkDanhMuc = false;
        });
      });
    } catch (e) {
      print('ListCategory -> Loi: ${e.toString()}');
    }
  }

  Future<void> loadDistrict() async {
    try {
      viewModel.getAllDistrict(StorePreferences.getKeyProvince());
      viewModel.getdistrict.listen((data) {
        List<String> listDistrict = [];
        for (District i in data) {
          listDistrict.add(i.name);
        }
        setState(() {
          list = listDistrict;
          listSearch = list;
          checkDanhMuc = false;
        });
      });
    } catch (e) {
      print('ListCategory -> Loi: ${e.toString()}');
    }
  }

  Future<void> loadWard() async {
    try {
      viewModel.getAllWard(StorePreferences.getKeyProvince(),
          StorePreferences.getKeyDistricts());
      viewModel.getward.listen((data) {
        List<String> listWard = [];
        for (Ward i in data) {
          listWard.add(i.name);
        }
        setState(() {
          list = listWard;
          listSearch = list;
          checkDanhMuc = false;
        });
      });
    } catch (e) {
      print('ListCategory -> Loi: ${e.toString()}');
    }
  }

  void searchList(String value) {
    setState(() {
      textEditingController.text = value;
      listSearch = list
          .where(
              (item) => item.toLowerCase().contains(value.trim().toLowerCase()))
          .toList();
    });
  }
}
