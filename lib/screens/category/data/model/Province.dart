// To parse this JSON data, do
//
//     final province = provinceFromJson(jsonString);

import 'dart:convert';

List<Province> provinceFromJson(String str) =>
    List<Province>.from(json.decode(str).map((x) => Province.fromJson(x)));

String provinceToJson(List<Province> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Province {
  String name;
  int code;
  String divisionType;
  String codename;
  int phoneCode;
  List<District> districts;

  Province({
    required this.name,
    required this.code,
    required this.divisionType,
    required this.codename,
    required this.phoneCode,
    required this.districts,
  });

  factory Province.fromJson(Map<String, dynamic> json) => Province(
        name: json["name"] ?? '',
        code: json["code"] ?? 0,
        divisionType: json["division_type"] ?? '',
        codename: json["codename"] ?? 0,
        phoneCode: json["phone_code"] ?? '',
        districts: List<District>.from(
            (json["districts"] ?? []).map((x) => District.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "code": code,
        "division_type": divisionType,
        "codename": codename,
        "phone_code": phoneCode,
        "districts": List<dynamic>.from(districts.map((x) => x.toJson())),
      };
}

class District {
  String name;
  int code;
  String divisionType;
  String codename;
  String shortCodeName;
  List<Ward> wards;

  District({
    required this.name,
    required this.code,
    required this.divisionType,
    required this.codename,
    required this.shortCodeName,
    required this.wards,
  });

  factory District.fromJson(Map<String, dynamic> json) => District(
        name: json["name"] ?? '',
        code: json["code"] ?? 0,
        divisionType: json["division_type"] ?? '',
        codename: json["codename"] ?? '', 
        shortCodeName: json["short_codename"] ?? '',
        wards: List<Ward>.from(json["wards"].map((x) => Ward.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "code": code,
        "division_type": divisionType,
        "codename": codename,
        "short_codename": shortCodeName,
        "wards": List<dynamic>.from(wards.map((x) => x.toJson())),
      };
}

class Ward {
  String name;
  int code;
  String codename;
  String divisionType;
  String shortCodename;

  Ward({
    required this.name,
    required this.code,
    required this.codename,
    required this.divisionType,
    required this.shortCodename,
  });

  factory Ward.fromJson(Map<String, dynamic> json) => Ward(
        name: json["name"],
        code: json["code"],
        codename: json["codename"],
        divisionType: json["division_type"],
        shortCodename: json["short_codename"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "code": code,
        "codename": codename,
        "division_type": divisionType,
        "short_codename": shortCodename,
      };
}
