import 'package:app_doan_flutter/screens/category/data/model/Province.dart';
import 'package:app_doan_flutter/screens/category/data/remote/ProvinceApi.dart';

import 'ProvinceRespository.dart';

class ProvinceRespositoryImpl implements ProvinceRespository {
  late ProvinceApi provinceApi;
  ProvinceRespositoryImpl(ProvinceApi provinceApi) {
    this.provinceApi = provinceApi;
  }
  @override
  Future<List<District>> getDistrict(String province) {
    // TODO: implement getDistrict
    return provinceApi.getDistrict(province);
  }

  @override
  Future<List<Province>> getProvince() {
    // TODO: implement getProvince
    return provinceApi.getProvince();
  }

  @override
  Future<List<Ward>> getWard(String province, String district) {
    // TODO: implement getWard
    return provinceApi.getWard(province, district);
  }
}
