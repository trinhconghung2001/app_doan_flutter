import 'dart:async';
import 'dart:convert';

import 'package:app_doan_flutter/screens/category/data/model/Province.dart';
import 'package:app_doan_flutter/screens/category/data/remote/ProvinceApi.dart';
import 'package:http/http.dart' as http;

class ProvinceApiImpl implements ProvinceApi {
  @override
  Future<List<District>> getDistrict(String province) async {
    String apiUrl = 'https://provinces.open-api.vn/api/?';
    String param = 'depth=3';
    try {
      final uri = Uri.parse(apiUrl + param);
      var headers = {'Content-Type': 'application/json'};
      final response = await http.get(uri, headers: headers).timeout(
        Duration(seconds: 20),
        onTimeout: () {
          throw TimeoutException('The request timed out.');
        },
      );
      print('=====================\nvao 2');
      if (response.statusCode == 200 || response.statusCode == 400) {
        print('Co vaof');
        var jsonList = json.decode(utf8.decode(response.bodyBytes));
        print(jsonList.runtimeType);

        List<Province> listProvince = [];
        List<District> listDistrict = [];
        for (int i = 0; i < jsonList.length; i++) {
          listProvince.add(Province.fromJson(jsonList[i]));
        }
        for (Province i in listProvince) {
          if (i.name == province) {
            listDistrict = i.districts;
            break;
          }
        }
        // jsonList.map((json) => Province.fromJson(json)).toList();
        return listDistrict;
      } else {
        throw Exception("Error Code: ${response.statusCode}");
      }
    } catch (e) {
      print('==================\nloi:' + e.toString());
      throw Exception("There was a problem with the connection");
    }
  }

  @override
  Future<List<Province>> getProvince() async {
    String apiUrl = 'https://provinces.open-api.vn/api/?';
    String param = 'depth=3';
    try {
      final uri = Uri.parse(apiUrl + param);
      var headers = {'Content-Type': 'application/json'};
      final response = await http.get(uri, headers: headers).timeout(
        Duration(seconds: 20),
        onTimeout: () {
          return http.Response('Error', 500);
        },
      );
      print('=====================\nvao 2');
      if (response.statusCode == 200 || response.statusCode == 400) {
        print('Co vaof');
        var jsonList = json.decode(utf8.decode(response.bodyBytes));
        print(jsonList.runtimeType);

        List<Province> listProvince = [];
        for (int i = 0; i < jsonList.length; i++) {
          listProvince.add(Province.fromJson(jsonList[i]));
        }
        // jsonList.map((json) => Province.fromJson(json)).toList();
        return listProvince;
      } else {
        throw Exception("Error Code: ${response.statusCode}");
      }
    } catch (e) {
      print('==================\nloi:' + e.toString());
      throw Exception("There was a problem with the connection");
    }
  }

  @override
  Future<List<Ward>> getWard(String province, String district) async {
    String apiUrl = 'https://provinces.open-api.vn/api/?';
    String param = 'depth=3';
    try {
      final uri = Uri.parse(apiUrl + param);
      var headers = {'Content-Type': 'application/json'};
      final response = await http.get(uri, headers: headers).timeout(
        Duration(seconds: 20),
        onTimeout: () {
          return http.Response('Error', 500);
        },
      );
      print('=====================\nvao 2');
      if (response.statusCode == 200 || response.statusCode == 400) {
        print('Co vaof');
        var jsonList = json.decode(utf8.decode(response.bodyBytes));
        print(jsonList.runtimeType);

        List<Province> listProvince = [];
        List<District> listDistrict = [];
        List<Ward> listWard = [];
        for (int i = 0; i < jsonList.length; i++) {
          listProvince.add(Province.fromJson(jsonList[i]));
        }
        for (Province i in listProvince) {
          if (i.name == province) {
            listDistrict = i.districts;
            break;
          }
        }
        for (District i in listDistrict) {
          if (i.name == district) {
            listWard = i.wards;
          }
        }
        return listWard;
      } else {
        throw Exception("Error Code: ${response.statusCode}");
      }
    } catch (e) {
      print('==================\nloi:' + e.toString());
      throw Exception("There was a problem with the connection");
    }
  }
}
