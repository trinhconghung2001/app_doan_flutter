import 'package:app_doan_flutter/screens/category/data/model/Province.dart';

abstract class ProvinceApi {
  Future<List<Province>> getProvince();
  Future<List<District>> getDistrict(String province);
  Future<List<Ward>> getWard(String province, String district);
}
