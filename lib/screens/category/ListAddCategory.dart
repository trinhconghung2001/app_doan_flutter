import 'package:app_doan_flutter/components/popup/InputPopup.dart';
import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/untils/AppLocalizations.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../../../../components/appbar/CustomAppBar.dart';
import '../../components/bottomnavigator/BottomNavigator.dart';
import '../../components/button/CustomButton.dart';

// ignore: must_be_immutable
class ListAddCategory extends StatefulWidget {
  List<String> list;
  final String type;
  final String? titleAppbar;

  ListAddCategory(
      {Key? key, this.titleAppbar, required this.list, required this.type})
      : super(key: key);

  @override
  State<ListAddCategory> createState() => _ListAddCategoryState();
}

class _ListAddCategoryState extends State<ListAddCategory> {
  List<String> listCategory = [];
  List<bool> listIsCheck = [];
  String textTrieuChung = '';
  String textThuoc = '';
  TimeOfDay selectedTime = TimeOfDay.now();
  @override
  void initState() {
    if (widget.type == defaultGioThongBao) {
      listCategory =
          (StorePreferences.getListTime() + widget.list).toSet().toList();
    } else if (widget.type == defaultTrieuChung) {
      listCategory =
          (StorePreferences.getListTrieuChung() + widget.list).toSet().toList();
    } else if (widget.type == defaultTypeMedication) {
      listCategory =
          (StorePreferences.getListThuoc() + widget.list).toSet().toList();
    } else if (widget.type == defaultTypeMedication2) {
      listCategory = StorePreferences.getListThuoc();
    }

    print(StorePreferences.getListTrieuChung());
    print(StorePreferences.getListTime());
    listIsCheck = List.generate(listCategory.length, (index) => false);
    if (widget.list.isNotEmpty) {
      for (int i = 0; i < listCategory.length; i++) {
        if (widget.list.contains(listCategory[i])) {
          listIsCheck[i] = true;
        }
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(50),
          child: CustomAppBar(
              text: "${AppLocalizations.of(context)!.translate('category')} ${widget.titleAppbar}",
              check: false,
              onPress: () {
                List<String> result = [];
                for (int i = 0; i < listCategory.length; i++) {
                  if (listIsCheck[i] == true) {
                    result.add(listCategory[i]);
                  }
                }
                Navigator.pop(context, result);
              }),
        ),
        body: Scaffold(
          body: Container(
            margin: const EdgeInsets.all(16),
            child: ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        listIsCheck[index] = !listIsCheck[index];
                      });
                    },
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          height: 50,
                          // color: Colors.white,
                          decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(5)),
                              color: const Color.fromRGBO(255, 255, 255, 1),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black.withOpacity(0.2),
                                  spreadRadius: 1,
                                  blurRadius: 2,
                                  offset: Offset(0, 1),
                                ),
                              ]),
                          // margin: EdgeInsets.all(10),
                          child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  listCategory[index],
                                  style: const TextStyle(fontSize: 14),
                                ),
                                Container(
                                  child: listIsCheck[index] == false
                                      ? const Icon(
                                          Icons.check_circle_outlined,
                                          size: 18,
                                          color: Color(0xFFBBC2C6),
                                        )
                                      : const Icon(
                                          Icons.check_circle,
                                          size: 18,
                                          color: primaryColor,
                                        ),
                                )
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                      ],
                    ),
                  );
                },
                itemCount: listCategory.length),
          ),
          bottomNavigationBar: BottomNavigator(widget: bottomButton()),
        ));
  }

  void showTime() async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );

    if (picked != null) {
      setState(() {
        selectedTime = picked;
        listCategory.add(convertTime(selectedTime));
        listIsCheck.add(false);
        StorePreferences.setListTime(listCategory);
      });
    }
  }

  String convertTime(TimeOfDay timeOfDay) {
    final now = DateTime.now();
    final DateTime time = DateTime(
        now.year, now.month, now.day, timeOfDay.hour, timeOfDay.minute);
    final format = DateFormat.jm(); // Formats time as "10:30 PM"
    return format.format(time);
  }

  void themTrieuChung() {
    setState(() {
      listCategory.add(textTrieuChung);
      listIsCheck.add(false);
      StorePreferences.setListTrieuChung(listCategory);
    });
    Navigator.pop(context);
  }

  // void themThuoc() {
  //   setState(() {
  //     listCategory.add(textThuoc);
  //     listIsCheck.add(false);
  //     StorePreferences.setListThuoc(listCategory);
  //   });
  //   Navigator.pop(context);
  // }

  String readTitle() {
    if (widget.type == defaultGioThongBao) {
      return AppLocalizations.of(context)!.translate('button_add_time');
    } else if (widget.type == defaultTrieuChung) {
      return AppLocalizations.of(context)!.translate('button_add_symptom');
    }
    // else if (widget.type == defaultTypeMedication) {
    //   return AppLocalizations.of(context)!.translate('button_add_medication');
    // }
    return '';
  }

  Widget bottomButton() {
    if (widget.type == defaultTypeMedication ||
        widget.type == defaultTypeMedication2) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            width: 16,
          ),
          Expanded(
              child: CustomButton(
            text: AppLocalizations.of(context)!.translate('button_success'),
            icon: Icons.save,
            height: 32,
            press: () {
              List<String> result = [];
              for (int i = 0; i < listCategory.length; i++) {
                if (listIsCheck[i] == true) {
                  result.add(listCategory[i]);
                }
              }
              Navigator.pop(context, result);
            },
          )),
          Container(
            width: 16,
          ),
        ],
      );
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            width: 16,
          ),
          Expanded(
              child: CustomButton(
            text: readTitle(),
            icon: Icons.add,
            height: 32,
            press: () {
              if (widget.type == defaultGioThongBao) {
                showTime();
              } else if (widget.type == defaultTrieuChung) {
                showDialog(
                    context: context,
                    builder: ((context) {
                      return InputPopup(
                        onChanged: (value) {
                          setState(() {
                            textTrieuChung = value;
                          });
                        },
                        onPress: () {
                          if (textTrieuChung.trim().isNotEmpty) {
                            themTrieuChung();
                          }
                        },
                        title: AppLocalizations.of(context)!
                            .translate('button_add_symptom'),
                        hintText: AppLocalizations.of(context)!
                            .translate('hint_button_add_symptom'),
                        buttonText: 'OK',
                      );
                    }));
              }
              // else if (widget.type == defaultTypeMedication) {
              //   showDialog(
              //       context: context,
              //       builder: ((context) {
              //         return InputPopup(
              //           onChanged: (value) {
              //             setState(() {
              //               textThuoc = value;
              //             });
              //           },
              //           onPress: () {
              //             if (textThuoc.trim().isNotEmpty) {
              //               themThuoc();
              //             }
              //           },
              //           title: AppLocalizations.of(context)!
              //               .translate('button_add_medication'),
              //           hintText: AppLocalizations.of(context)!
              //               .translate('hint_button_add_medication'),
              //           buttonText: 'OK',
              //         );
              //       }));
              // }
            },
          )),
          const SizedBox(
            width: 8,
          ),
          Expanded(
              child: CustomButton(
            text: AppLocalizations.of(context)!.translate('button_success'),
            icon: Icons.save,
            height: 32,
            press: () {
              List<String> result = [];
              for (int i = 0; i < listCategory.length; i++) {
                if (listIsCheck[i] == true) {
                  result.add(listCategory[i]);
                }
              }
              Navigator.pop(context, result);
            },
          )),
          Container(
            width: 16,
          ),
        ],
      );
    }
  }
}
