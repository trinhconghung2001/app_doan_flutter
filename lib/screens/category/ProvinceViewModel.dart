import 'package:app_doan_flutter/screens/category/data/model/Province.dart';
import 'package:app_doan_flutter/screens/category/data/remote/ProvinceApiImpl.dart';
import 'package:app_doan_flutter/screens/category/data/repository/ProvinceRespositoryImpl.dart';
import 'package:rxdart/rxdart.dart';

class ProvinceViewModel {
  var provinceSubject = PublishSubject<List<Province>>();
  Stream<List<Province>> get getprovice => provinceSubject.stream;
  ProvinceRespositoryImpl impl = ProvinceRespositoryImpl(ProvinceApiImpl());

  void getAllProvince() async {
    try {
      provinceSubject = PublishSubject<List<Province>>();
      provinceSubject.sink.add(await impl.getProvince());
    } catch (e) {
      await Future.delayed(Duration(microseconds: 500));
      provinceSubject.sink.addError(e);
    }
  }

  var districtSubject = PublishSubject<List<District>>();
  Stream<List<District>> get getdistrict => districtSubject.stream;

  void getAllDistrict(String province) async {
    try {
      districtSubject = PublishSubject<List<District>>();
      districtSubject.sink.add(await impl.getDistrict(province));
    } catch (e) {
      await Future.delayed(Duration(microseconds: 500));
      districtSubject.sink.addError(e);
    }
  }

  var wardSubject = PublishSubject<List<Ward>>();
  Stream<List<Ward>> get getward => wardSubject.stream;


  void getAllWard(String provice, String district) async {
    try {
      wardSubject = PublishSubject<List<Ward>>();
      wardSubject.sink.add(await impl.getWard(provice,district));
    } catch (e) {
      await Future.delayed(Duration(microseconds: 500));
      wardSubject.sink.addError(e);
    }
  }
}
