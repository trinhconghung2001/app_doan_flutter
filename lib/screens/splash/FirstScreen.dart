import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/screens/login/LoginDoctor.dart';
import 'package:app_doan_flutter/screens/login/LoginReception.dart';
import 'package:app_doan_flutter/screens/login/LoginScreen.dart';
import 'package:app_doan_flutter/screens/splash/components/Body.dart';
import 'package:app_doan_flutter/untils/AppLocalizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../config/styles/Dimens.dart';

class FirstScreen extends StatelessWidget {
  const FirstScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: LayoutBuilder(
          builder: (context, constraints) {
            double deviceHeight = MediaQuery.of(context).size.height;
            double sizedBoxHeight = 0;
            if (deviceHeight - 350 < 340)
              sizedBoxHeight = deviceHeight -
                  350; // 350 là chiều cao tối thiểu của khung đăng nhập
            else
              sizedBoxHeight = deviceHeight *
                  0.45; // 340 là chiều cao tối thiểu cho tiêu đề và ảnh
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                    minWidth: constraints.maxWidth,
                    minHeight: constraints.maxHeight),
                child: IntrinsicHeight(
                  child: Stack(
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(16, 53, 16, 16),
                            child: Row(
                              children: [
                                SvgPicture.asset(
                                  'assets/images/logo_app.svg',
                                  width: 57,
                                  height: 57,
                                ),
                                Container(
                                  padding:
                                      const EdgeInsets.fromLTRB(12, 0, 0, 0),
                                  child: Column(
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.fromLTRB(
                                            0, 0, 0, 8),
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .translate('name_app'),
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 20,
                                            color: Color(0xFF1C4983),
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                              mainAxisAlignment: MainAxisAlignment.center,
                            ),
                          ),
                          Padding(
                            child: SvgPicture.asset(
                              'assets/images/hospital_login.svg',
                              width: 270.03, // 270.03
                              height: 183, // 183
                            ),
                            padding: EdgeInsets.fromLTRB(0, 0, 0, 17),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          SizedBox(
                            height: sizedBoxHeight,
                          ),
                          Expanded(
                            child: Container(
                              constraints: const BoxConstraints(
                                minHeight: 350,
                              ),
                              decoration: const BoxDecoration(
                                color: primaryColor,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(30),
                                    topRight: Radius.circular(30)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Color(0x26006A67),
                                    offset: Offset(0, -4),
                                    blurRadius: 6,
                                  ),
                                ],
                              ),
                              child: Stack(
                                children: [
                                  Positioned(
                                    top: -3,
                                    right: 38,
                                    child: Container(
                                        width: 130,
                                        height: 130,
                                        decoration: const BoxDecoration(
                                          color: Color(0x0DFFFFFF),
                                          shape: BoxShape.circle,
                                        )),
                                  ),
                                  Positioned(
                                    top: 31,
                                    right: -106,
                                    child: Container(
                                        width: 212,
                                        height: 212,
                                        decoration: const BoxDecoration(
                                          color: Color(0x0DFFFFFF),
                                          shape: BoxShape.circle,
                                        )),
                                  ),
                                  Positioned(
                                    left: -105,
                                    bottom: -53,
                                    child: Container(
                                        width: 272,
                                        height: 272,
                                        decoration: const BoxDecoration(
                                          color: Color(0x0DFFFFFF),
                                          shape: BoxShape.circle,
                                        )),
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.only(
                                        top: defaultButtonHeight),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Text(
                                          AppLocalizations.of(context)!
                                              .translate(
                                                  'splash_select_employer'),
                                          textAlign: TextAlign.center,
                                          style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20,
                                              color: Color(0xFFFDFDFD)),
                                        ),
                                        Container(
                                          // color: Colors.amber,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.9,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceAround,
                                                children: [
                                                  Expanded(
                                                    child: Container(),
                                                    flex: 1,
                                                  ),
                                                  Expanded(
                                                    child: Body(
                                                      svgPicture: SvgPicture.asset(
                                                          'assets/icons/splash/Reception.svg'),
                                                      text: AppLocalizations.of(
                                                              context)!
                                                          .translate(
                                                              'splash_select_reception'),
                                                      onPress: () {
                                                        Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        LoginReception()));
                                                      },
                                                    ),
                                                    flex: 3,
                                                  ),
                                                  Expanded(
                                                    child: Body(
                                                      svgPicture: SvgPicture.asset(
                                                          'assets/icons/splash/User.svg'),
                                                      text: AppLocalizations.of(
                                                              context)!
                                                          .translate(
                                                              'splash_select_user'),
                                                      onPress: () {
                                                        Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        LoginScreen()));
                                                      },
                                                    ),
                                                    flex: 3,
                                                  ),
                                                  Expanded(
                                                    child: Container(),
                                                    flex: 1,
                                                  ),
                                                ],
                                              ),
                                              // Container(
                                              //   height: 32,
                                              // ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceAround,
                                                children: [
                                                  Expanded(
                                                    child: Container(),
                                                    flex: 1,
                                                  ),
                                                  Expanded(
                                                    child: Body(
                                                      svgPicture: SvgPicture.asset(
                                                          'assets/icons/splash/Doctor.svg'),
                                                      text: AppLocalizations.of(
                                                              context)!
                                                          .translate(
                                                              'splash_select_doctor'),
                                                      onPress: () {
                                                        Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        LoginDoctor()));
                                                      },
                                                    ),
                                                    flex: 3,
                                                  ),
                                                  Expanded(
                                                    child: Body(
                                                      // svgPicture: SvgPicture.asset(
                                                      //     'assets/icons/splash/User.svg'),
                                                      text: "",
                                                    ),
                                                    flex: 3,
                                                  ),
                                                  Expanded(
                                                    child: Container(),
                                                    flex: 1,
                                                  ),
                                                ],
                                              ),
                                              Container(
                                                height: 20,
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
