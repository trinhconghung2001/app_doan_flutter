import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Body extends StatelessWidget {
  final SvgPicture? svgPicture;
  final String? text;
  final Function()? onPress;
  const Body({super.key, this.svgPicture, this.text, this.onPress});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Color(0xFF1C4983),
            borderRadius: BorderRadius.circular(18),
            boxShadow: [
              BoxShadow(
                color: Colors.white.withOpacity(0.2),
                spreadRadius: 2,
                blurRadius: 2,
                offset: const Offset(2, 2), // changes position of shadow
              ),
            ],
          ),
          // decoration: BoxDecorationContainer(color: Color(0xFF1C4983)),
          margin: const EdgeInsets.only(top: 8, left: 16, right: 16),
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
          child: Column(
            children: [
              Container(
                height: 50,
                child: svgPicture ??
                    SvgPicture.asset(
                      'assets/images/logo_app.svg',
                    ),
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                child: Text(
                  text!,
                  style: TextStyle(color: Colors.white),
                ),
              )
            ],
          )),
    );
  }
}
