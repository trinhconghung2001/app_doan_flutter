import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/screens/account/edit_new/AddAccount.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/screens/login/data/User.dart';
import 'package:flutter/material.dart';

import '../../components/appbar/CustomAppBar.dart';
import '../../components/loading/EmptyData.dart';
import '../../config/styles/CustomColor.dart';
import '../../untils/AppLocalizations.dart';
import '../../untils/FutureUser.dart';
import '../../untils/StorePreferences.dart';
import 'components/Body.dart';
import 'detail/DetailAccount.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({super.key});

  @override
  State<AccountScreen> createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  late Future<void> _loadDataFuture;
  List<FamilyMember> listNguoiThan = [];
  Map<String, dynamic> banThan = {};
  @override
  void initState() {
    _loadDataFuture = loadData();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDataFuture = loadData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: CustomAppBar(
          text: AppLocalizations.of(context)!.translate("app_bar_manage_file"),
          check: true,
          checkIconBack: true,
          onTap: () {
            Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AddAccount()))
                .then((value) => loadScreen());
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 8, bottom: 8, right: 8, left: 8),
        child: ListView.builder(
          itemCount: 4,
          itemBuilder: (context, index) {
            if (index == 0) {
              return Text(
                AppLocalizations.of(context)!
                    .translate('acount_screen_individuals'),
                style: const TextStyle(
                  color: primaryColor,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.left,
              );
            } else if (index == 1) {
              return FutureBuilder(
                  future: _loadDataFuture,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return CircularProgressIndicatorWidget();
                    } else if (snapshot.hasError) {
                      return CircularProgressIndicatorWidget();
                    } else {
                      return Body(
                        imgUrl: banThan['imgURL'],
                        hoTen: banThan['ten'],
                        gioiTinh: banThan['gioiTinh'],
                        ngaySinh: banThan['ngaySinh'],
                        onPress: () {
                          StorePreferences.setBHYT_User(banThan['soTheBHYT']);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => DetailAccount(
                                        idSend: StorePreferences.getIdUser(),
                                        checkType: true,
                                      ))).then((value) => loadScreen());
                        },
                      );
                    }
                  });
            } else if (index == 2) {
              return Container(
                margin: const EdgeInsets.only(top: 8),
                child: Text(
                  AppLocalizations.of(context)!
                      .translate('acount_screen_relatives'),
                  style: const TextStyle(
                    color: primaryColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.left,
                ),
              );
            } else if (index == 3) {
              return FutureBuilder(
                  future: _loadDataFuture,
                  builder: (context, AsyncSnapshot snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return CircularProgressIndicatorWidget();
                    } else if (snapshot.hasError) {
                      return EmptyData(
                          msg: AppLocalizations.of(context)!
                              .translate('error_data_msg'));
                    } else {
                      if (listNguoiThan.length > 0) {
                        return Container(
                          height: MediaQuery.of(context).size.height * 0.92,
                          child: ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: listNguoiThan.length,
                              itemBuilder: (BuildContext context, index) {
                                return Body(
                                  imgUrl: listNguoiThan[index].imgURL,
                                  hoTen: listNguoiThan[index].ten,
                                  gioiTinh: listNguoiThan[index].gioiTinh,
                                  ngaySinh: listNguoiThan[index].ngaySinh,
                                  onPress: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => DetailAccount(
                                                  idSend:
                                                      listNguoiThan[index].id,
                                                  checkType: false,
                                                ))).then(
                                        (value) => loadScreen());
                                  },
                                );
                              }),
                        );
                      } else {
                        return EmptyData();
                      }
                    }
                  });
            }
            return null;
          },
        ),
      ),
    );
  }

  Future<void> loadData() async {
    Users users = await readBanThan(StorePreferences.getIdUser());
    banThan = users.toMap();
    listNguoiThan = await readListNguoiThan(StorePreferences.getIdUser());
  }
}
