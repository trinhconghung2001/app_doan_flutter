import 'package:app_doan_flutter/untils/AppLocalizations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../config/styles/CustomColor.dart';

class Body extends StatelessWidget {
  final String imgUrl;
  final String hoTen;
  final String gioiTinh;
  final String ngaySinh;
  final VoidCallback onPress;
  const Body(
      {super.key,
      required this.hoTen,
      required this.gioiTinh,
      required this.ngaySinh,
      required this.onPress,
      required this.imgUrl});

  @override
  Widget build(BuildContext context) {
    // double height_screen = MediaQuery.of(context).size.height;
    // bool checkGioiTinh = false;
    // if (gioiTinh == 'Nam') {
    //   checkGioiTinh = true;
    // }
    return GestureDetector(
      onTap: onPress,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 1,
              offset: const Offset(1, 1), // changes position of shadow
            ),
          ],
        ),
        padding: const EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 8),
        margin: const EdgeInsets.only(top: 8, bottom: 0),
        child: Row(
          children: [
            Expanded(
              child: imgUrl != ''
                  ? ClipOval(
                      child: CachedNetworkImage(
                        width: 60,
                        height: 100,
                        imageUrl: imgUrl,
                        fit: BoxFit.fill,
                        placeholder: (context, url) => const CircleAvatar(
                          radius: 40,
                          backgroundImage:
                              AssetImage('assets/images/account.jpg'),
                        ),
                        errorWidget: (context, url, error) =>
                            const CircleAvatar(
                          radius: 40,
                          backgroundImage:
                              AssetImage('assets/images/account.jpg'),
                        ),
                      ),
                    )
                  : ClipOval(
                      child: Image.asset(
                        'assets/images/account.jpg',
                        fit: BoxFit.fill,
                        width: 60,
                        height: 100,
                      ),
                    ),
              flex: 2,
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              flex: 5,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 4,
                  ),
                  Text(
                    hoTen,
                    style: const TextStyle(
                        color: textColor,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Text(
                    AppLocalizations.of(context)!
                            .translate('account_title_sex') +
                        ': ' +
                        gioiTinh,
                    style: const TextStyle(
                      color: textColor,
                      fontSize: 14,
                    ),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Text(
                    AppLocalizations.of(context)!
                            .translate('account_title_date') +
                        ': ' +
                        ngaySinh,
                    style: const TextStyle(
                      color: textColor,
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
