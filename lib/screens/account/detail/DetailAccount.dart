import 'package:app_doan_flutter/components/inputform/RowTextView.dart';
import 'package:app_doan_flutter/screens/home/bookExamination/data/Examination.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/untils/FutureExamination.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureUser.dart';
import '../../../untils/StorePreferences.dart';
import '../edit_new/EditAccount.dart';

class DetailAccount extends StatefulWidget {
  final bool checkType;
  final String idSend;
  const DetailAccount(
      {super.key, required this.checkType, required this.idSend});

  @override
  State<DetailAccount> createState() => _DetailAccountState();
}

class _DetailAccountState extends State<DetailAccount> {
  late Future<void> _loadDetailFuture;
  FamilyMember nguoiThan = FamilyMember.defaultContructor();
  @override
  void initState() {
    _loadDetailFuture = loadDetail();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDetailFuture = loadDetail();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: AppLocalizations.of(context)!
                  .translate("app_bar_detail_file"),
              check: false,
              onPress: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: Scaffold(
            body: SingleChildScrollView(
                padding: EdgeInsets.only(top: 16),
                child: FutureBuilder(
                    future: _loadDetailFuture,
                    builder: (context, AsyncSnapshot snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return CircularProgressIndicatorWidget();
                      } else if (snapshot.hasError) {
                        return EmptyData(
                            msg: AppLocalizations.of(context)!
                                .translate('error_data_msg'));
                      } else {
                        print(nguoiThan.imgURL);
                        //Ban than
                        // if (widget.idSend == StorePreferences.getIdUser()) {
                        return Column(
                          children: [
                            nguoiThan.imgURL != ''
                                ? ClipOval(
                                    child: CachedNetworkImage(
                                    imageUrl: nguoiThan.imgURL,
                                    fit: BoxFit.fill,
                                    placeholder: (context, url) =>
                                        const CircleAvatar(
                                      radius: 40,
                                      backgroundImage: AssetImage(
                                          'assets/images/account.jpg'),
                                    ),
                                    errorWidget: (context, url, error) =>
                                        const CircleAvatar(
                                      radius: 40,
                                      backgroundImage: AssetImage(
                                          'assets/images/account.jpg'),
                                    ),
                                    width:
                                        MediaQuery.of(context).size.width * 0.6,
                                    height: MediaQuery.of(context).size.height *
                                        0.29,
                                  ))
                                : ClipOval(
                                    child: Image.asset(
                                      'assets/images/account.jpg',
                                      fit: BoxFit.fill,
                                      width: MediaQuery.of(context).size.width *
                                          0.6,
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.29,
                                    ),
                                  ),
                            Container(
                              decoration: BoxDecorationContainer(),
                              padding: const EdgeInsets.only(
                                top: 16,
                                left: 16,
                                right: 16,
                              ),
                              margin: const EdgeInsets.only(
                                  top: 8, left: 16, right: 16, bottom: 0),
                              child: Column(
                                children: [
                                  RowTextView(
                                      title: AppLocalizations.of(context)!
                                          .translate(
                                              'account_title_relationship'),
                                      content: nguoiThan.moiQH,
                                      flexItemLeft: 2,
                                      flexItemRight: 3),
                                  RowTextView(
                                      title: AppLocalizations.of(context)!
                                          .translate(
                                              'account_title_number_bhyt'),
                                      content: nguoiThan.soTheBHYT,
                                      flexItemLeft: 2,
                                      flexItemRight: 3),
                                  RowTextView(
                                      title: AppLocalizations.of(context)!
                                          .translate('account_title_name'),
                                      content: nguoiThan.ten,
                                      flexItemLeft: 2,
                                      flexItemRight: 3),
                                  RowTextView(
                                      title: AppLocalizations.of(context)!
                                          .translate('account_title_date'),
                                      content: nguoiThan.ngaySinh,
                                      flexItemLeft: 2,
                                      flexItemRight: 3),
                                  RowTextView(
                                      title: AppLocalizations.of(context)!
                                          .translate('account_title_job'),
                                      content: nguoiThan.ngheNghiep,
                                      flexItemLeft: 2,
                                      flexItemRight: 3),
                                  RowTextView(
                                      title: AppLocalizations.of(context)!
                                          .translate('account_title_sex'),
                                      content: nguoiThan.gioiTinh,
                                      flexItemLeft: 2,
                                      flexItemRight: 3),
                                  RowTextView(
                                      title: AppLocalizations.of(context)!
                                          .translate('account_title_nation'),
                                      content: nguoiThan.danToc,
                                      flexItemLeft: 2,
                                      flexItemRight: 3),
                                  RowTextView(
                                      title: AppLocalizations.of(context)!
                                          .translate('account_title_province'),
                                      content: nguoiThan.tinhThanhPho,
                                      flexItemLeft: 2,
                                      flexItemRight: 3),
                                  RowTextView(
                                      title: AppLocalizations.of(context)!
                                          .translate('account_title_district'),
                                      content: nguoiThan.quanHuyen,
                                      flexItemLeft: 2,
                                      flexItemRight: 3),
                                  RowTextView(
                                      title: AppLocalizations.of(context)!
                                          .translate('account_title_coummune'),
                                      content: nguoiThan.phuongXa,
                                      flexItemLeft: 2,
                                      flexItemRight: 3),
                                  RowTextView(
                                      title: AppLocalizations.of(context)!
                                          .translate(
                                              'account_title_number_house'),
                                      content: nguoiThan.soNha,
                                      flexItemLeft: 2,
                                      flexItemRight: 3),
                                  RowTextView(
                                      title: AppLocalizations.of(context)!
                                          .translate('phone_number'),
                                      content: nguoiThan.sdt,
                                      flexItemLeft: 2,
                                      flexItemRight: 3),
                                ],
                              ),
                            ),
                          ],
                        );
                      }
                    })),
            bottomNavigationBar: BottomNavigator(
              widget: widget.checkType == false
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: 16,
                        ),
                        Expanded(
                            child: CustomButton(
                          text: AppLocalizations.of(context)!
                              .translate("button_delete"),
                          height: 32,
                          icon: Icons.cancel_outlined,
                          iconColor: primaryColor,
                          textColor: primaryColor,
                          background: backgroundScreenColor,
                          press: () {
                            removeDetail();
                          },
                        )),
                        Container(
                          width: 8,
                        ),
                        Expanded(
                            child: CustomButton(
                          text: AppLocalizations.of(context)!
                              .translate("button_edit"),
                          icon: Icons.edit_square,
                          height: 32,
                          press: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => EditAccount(
                                          checkType: false,
                                          nguoiThan: nguoiThan,
                                        ))).then((value) {
                              loadScreen();
                            });
                          },
                        )),
                        Container(
                          width: 16,
                        ),
                      ],
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: 16,
                        ),
                        Expanded(
                            child: CustomButton(
                                text: AppLocalizations.of(context)!
                                    .translate("button_edit"),
                                icon: Icons.edit_square,
                                height: 32,
                                press: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => EditAccount(
                                                checkType: true,
                                                nguoiThan: nguoiThan,
                                              ))).then((value) {
                                    loadScreen();
                                  });
                                })),
                        Container(
                          width: 16,
                        ),
                      ],
                    ),
            ),
          )),
    );
  }

  Future<void> loadDetail() async {
    nguoiThan =
        await readIDBenhNhan(StorePreferences.getIdUser(), widget.idSend);
  }

  Future<void> removeDetail() async {
    try {
      List<FamilyMember> listFamily =
          await readListNguoiThan(StorePreferences.getIdUser());
      List<Map<String, dynamic>> listNguoiThan = [];
      listFamily.forEach((element) {
        listNguoiThan.add(element.toMap());
      });
      List<Examination> listExamination_Family =
          await readAllExamination_ByIdBenhNhan(
              StorePreferences.getIdUser(), widget.idSend);
      if (listExamination_Family.length == 0) {
        for (Map<String, dynamic> i in listNguoiThan) {
          if (i['id'] == widget.idSend) {
            listNguoiThan.remove(i);
            break;
          }
          FirebaseFirestore.instance
              .collection('Users')
              .doc(StorePreferences.getIdUser())
              .update({'listNguoiThan': listNguoiThan}).then((value) {
            ToastCommon.showToast(AppLocalizations.of(context)!
                .translate('toast_remove_account_succes'));
            Navigator.pop(context);
          });
        }
      } else {
        showDialog(
            context: context,
            builder: (context) {
              return ErrorPopup(
                message: AppLocalizations.of(context)!
                    .translate('error_remove_accout_exits'),
                buttonText: 'OK',
                onPress: () {
                  Navigator.pop(context);
                },
              );
            });
      }
    } on Exception catch (e) {
      print(e);
      // ignore: use_build_context_synchronously
      showDialog(
          context: context,
          builder: (context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('toast_remove_account_faild'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          });
    }
  }
}
