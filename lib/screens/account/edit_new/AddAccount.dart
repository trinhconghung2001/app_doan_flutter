import 'dart:io';

import 'package:app_doan_flutter/components/bottomnavigator/BottomNavigator.dart';
import 'package:app_doan_flutter/components/popup/ErrorPopup.dart';
import 'package:app_doan_flutter/components/toast/ToastCommon.dart';
import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/InputDropDown.dart';
import '../../../components/inputform/InputTextField.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FuturePicture.dart';
import '../../../untils/FutureUser.dart';
import '../../category/ListCategory.dart';

class AddAccount extends StatefulWidget {
  const AddAccount({
    super.key,
  });

  @override
  State<AddAccount> createState() => _AddAccountState();
}

class _AddAccountState extends State<AddAccount> {
  String? _image;
  final FocusNode _focusSoTheBHYT = FocusNode();
  final FocusNode _focusTen = FocusNode();
  final FocusNode _focusNgheNghiep = FocusNode();
  final FocusNode _focusSoNha = FocusNode();
  final FocusNode _focusSDT = FocusNode();

  String textMoiQH = '';
  String textGioiTinh = '';
  String textDanToc = '';
  String textTinhThanhPho = '';
  String textQuanHuyen = '';
  String textPhuongXa = '';
  String textNgaySinh = '';

  bool validate = false;
  DateTime dateTime = DateTime.now();
  final TextEditingController textSoBHYT = TextEditingController();
  final TextEditingController textHoTen = TextEditingController();
  final TextEditingController textNgheNghiep = TextEditingController();
  final TextEditingController textSoNha = TextEditingController();
  final TextEditingController textSoDienThoai = TextEditingController();

  @override
  void initState() {
    super.initState();
    _focusSoTheBHYT.addListener(_onFocusChange);
    _focusTen.addListener(_onFocusChange);
    _focusNgheNghiep.addListener(_onFocusChange);
    _focusSoNha.addListener(_onFocusChange);
    _focusSDT.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    setState(() {});
  }

  @override
  void dispose() {
    _focusSoTheBHYT.dispose();
    _focusTen.dispose();
    _focusNgheNghiep.dispose();
    _focusSoNha.dispose();
    _focusSDT.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: AppLocalizations.of(context)!.translate("app_bar_add_file"),
              check: false,
              onPress: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: Scaffold(
            body: SingleChildScrollView(
              child: Column(
                children: [
                  const SizedBox(
                    height: 16,
                  ),
                  Stack(
                    children: [
                      _image != null
                          ? ClipOval(
                              child: Image.file(
                                File(_image!),
                                fit: BoxFit.cover,
                                width: MediaQuery.of(context).size.width * 0.6,
                                height:
                                    MediaQuery.of(context).size.height * 0.29,
                              ),
                            )
                          : ClipOval(
                              child: Image.asset(
                                'assets/images/account.jpg',
                                fit: BoxFit.cover,
                                width: MediaQuery.of(context).size.width * 0.6,
                                height:
                                    MediaQuery.of(context).size.height * 0.29,
                              ),
                            ),
                      Positioned(
                          bottom: 0,
                          right: 10,
                          child: MaterialButton(
                            elevation: 1,
                            onPressed: () {
                              showBottom(context, () async {
                                final ImagePicker picker = ImagePicker();
                                final XFile? image = await picker.pickImage(
                                    source: ImageSource.gallery);
                                if (image != null) {
                                  print(
                                      'Image path: ${image.path} -- Mimetype: ${image.mimeType}');
                                  setState(() {
                                    _image = image.path;
                                  });
                                  Navigator.pop(context);
                                }
                              }, () async {
                                final ImagePicker picker = ImagePicker();
                                final XFile? image = await picker.pickImage(
                                    source: ImageSource.camera);
                                if (image != null) {
                                  print(
                                      'Image path: ${image.path} -- Mimetype: ${image.mimeType}');
                                  setState(() {
                                    _image = image.path;
                                  });
                                  Navigator.pop(context);
                                }
                              });
                            },
                            child: Icon(
                              Icons.camera_alt,
                              color: backgroudSearch,
                              size: 30,
                            ),
                          ))
                    ],
                  ),
                  Container(
                    decoration: BoxDecorationContainer(),
                    padding: const EdgeInsets.only(
                        top: 16, left: 16, right: 16, bottom: 16),
                    margin: const EdgeInsets.only(
                        top: 8, left: 16, right: 16, bottom: 16),
                    child: Column(
                      children: [
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultQuanHe,
                                  AppLocalizations.of(context)!.translate(
                                      'category_title_relationship'));
                            },
                            value: textMoiQH,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_relationship"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textMoiQH.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("account_title_number_bhyt"),
                          checkRangBuoc: true,
                          focusNode: _focusSoTheBHYT,
                          textController: textSoBHYT,
                          onPress: () {
                            setState(() {
                              textSoBHYT.clear();
                            });
                          },
                          isError: validate && textSoBHYT.text.isEmpty,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("account_title_name"),
                          checkRangBuoc: true,
                          focusNode: _focusTen,
                          textController: textHoTen,
                          onPress: () {
                            setState(() {
                              textHoTen.clear();
                            });
                          },
                          isError: validate && textHoTen.text.isEmpty,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              _show();
                            },
                            value: textNgaySinh,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_date"),
                            checkRangBuoc: true,
                            icon: Icons.calendar_month,
                            isError: validate && textNgaySinh.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("account_title_job"),
                          checkRangBuoc: true,
                          focusNode: _focusNgheNghiep,
                          textController: textNgheNghiep,
                          onPress: () {
                            setState(() {
                              textNgheNghiep.clear();
                            });
                          },
                          isError: validate && textNgheNghiep.text.isEmpty,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultGioiTinh,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_sex'));
                            },
                            value: textGioiTinh,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_sex"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textGioiTinh.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultDanToc,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_nation'));
                            },
                            value: textDanToc,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_nation"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textDanToc.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultTinh,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_province'));
                            },
                            value: textTinhThanhPho,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_province"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textTinhThanhPho.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultHuyen,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_district'));
                            },
                            value: textQuanHuyen,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_district"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textQuanHuyen.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultXa,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_commune'));
                            },
                            value: textPhuongXa,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_coummune"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textPhuongXa.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("account_title_number_house"),
                          checkRangBuoc: false,
                          focusNode: _focusSoNha,
                          textController: textSoNha,
                          onPress: () {
                            setState(() {
                              textSoNha.clear();
                            });
                          },
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("phone_number"),
                          checkRangBuoc: true,
                          focusNode: _focusSDT,
                          textController: textSoDienThoai,
                          onPress: () {
                            setState(() {
                              textSoDienThoai.clear();
                            });
                          },
                          isError: validate && textSoDienThoai.text.isEmpty,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            bottomNavigationBar: BottomNavigator(
              widget: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: 16,
                  ),
                  Expanded(
                      child: CustomButton(
                    text: AppLocalizations.of(context)!
                        .translate("button_cancel"),
                    height: 32,
                    icon: Icons.cancel_outlined,
                    iconColor: primaryColor,
                    textColor: primaryColor,
                    background: backgroundScreenColor,
                    press: () {
                      Navigator.pop(context);
                    },
                  )),
                  Container(
                    width: 8,
                  ),
                  Expanded(
                      child: CustomButton(
                    text: AppLocalizations.of(context)!
                        .translate("button_update"),
                    icon: Icons.save_alt,
                    height: 32,
                    press: () {
                      checkValidate();
                      print(StorePreferences.getBHYT_User().trim());
                      print(textSoBHYT.text.trim());
                      if (validate == false) {
                        if (isPhoneNumberValid(textSoDienThoai.text) == true) {
                          if (textSoBHYT.text.trim() !=
                              StorePreferences.getBHYT_User().trim()) {
                            addUser();
                          } else {
                            showDialog(
                                context: context,
                                builder: ((context) {
                                  return ErrorPopup(
                                    message: AppLocalizations.of(context)!
                                        .translate('error_bhyt_duplicate'),
                                    buttonText: 'OK',
                                    onPress: () {
                                      Navigator.pop(context);
                                    },
                                  );
                                }));
                          }
                        } else {
                          showDialog(
                              context: context,
                              builder: ((context) {
                                return ErrorPopup(
                                  message: AppLocalizations.of(context)!
                                      .translate('phone_number_error_format'),
                                  buttonText: 'OK',
                                  onPress: () {
                                    Navigator.pop(context);
                                  },
                                );
                              }));
                        }
                      }
                    },
                  )),
                  Container(
                    width: 16,
                  ),
                ],
              ),
            ),
          )),
    );
  }

  void guiNhanDrop(String loai, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      if (value != null) {
        if (loai == defaultQuanHe) {
          setState(() {
            textMoiQH = value;
          });
        } else if (loai == defaultGioiTinh) {
          setState(() {
            textGioiTinh = value;
          });
        } else if (loai == defaultDanToc) {
          setState(() {
            textDanToc = value;
          });
        } else if (loai == defaultTinh) {
          setState(() {
            textTinhThanhPho = value;
          });
        } else if (loai == defaultHuyen) {
          setState(() {
            textQuanHuyen = value;
          });
        } else if (loai == defaultXa) {
          setState(() {
            textPhuongXa = value;
          });
        }
      }
    });
  }

  void _show() async {
    final DateTime? result = await showDatePicker(
      context: context,
      initialDate: dateTime,
      firstDate: DateTime(1600),
      lastDate: DateTime(2025),
      locale: const Locale('vi'),
    );
    if (result != null) {
      setState(() {
        dateTime = result;
        textNgaySinh = '${formatDate(dateTime, [dd, '-', mm, '-', yyyy])}';
      });
    }
  }

  bool isPhoneNumberValid(String phoneNumber) {
    // Biểu thức chính quy kiểm tra định dạng số điện thoại cơ bản
    final phoneRegExp = RegExp(
      r'^(\d{10,12})$',
    );
    return phoneRegExp.hasMatch(phoneNumber);
  }

  void checkValidate() {
    if (textMoiQH.isNotEmpty &&
        textGioiTinh.isNotEmpty &&
        textDanToc.isNotEmpty &&
        textTinhThanhPho.isNotEmpty &&
        textQuanHuyen.isNotEmpty &&
        textPhuongXa.isNotEmpty &&
        textSoBHYT.text.isNotEmpty &&
        textHoTen.text.isNotEmpty &&
        textNgheNghiep.text.isNotEmpty &&
        textSoDienThoai.text.isNotEmpty) {
      setState(() {
        validate = false;
      });
    } else {
      setState(() {
        validate = true;
      });
    }
  }

  Future<void> addUser() async {
    try {
      List<FamilyMember> list =
          await readListNguoiThan(StorePreferences.getIdUser());
      String idNew = DateTime.now().toString();
      String urlImg = '';
      if (urlImg.isNotEmpty) {
        urlImg = await updateProfilePicture(
            StorePreferences.getIdUser(), File(urlImg));
      }
      FamilyMember thanhVien = FamilyMember(
        id: idNew,
        imgURL: urlImg,
        idUser: StorePreferences.getIdUser(),
        moiQH: textMoiQH,
        ten: textHoTen.text,
        sdt: textSoDienThoai.text,
        soTheBHYT: textSoBHYT.text,
        ngaySinh: textNgaySinh,
        ngheNghiep: textNgheNghiep.text,
        gioiTinh: textGioiTinh,
        danToc: textDanToc,
        tinhThanhPho: textTinhThanhPho,
        quanHuyen: textQuanHuyen,
        phuongXa: textPhuongXa,
        soNha: textSoNha.text,
      );
      if (list.any((person) =>
              person.moiQH == thanhVien.moiQH &&
              person.soTheBHYT == thanhVien.soTheBHYT &&
              person.ten == thanhVien.ten &&
              person.ngaySinh == thanhVien.ngaySinh &&
              person.ngheNghiep == thanhVien.ngheNghiep &&
              person.gioiTinh == thanhVien.gioiTinh &&
              person.danToc == thanhVien.danToc &&
              person.tinhThanhPho == thanhVien.tinhThanhPho &&
              person.quanHuyen == thanhVien.quanHuyen &&
              person.phuongXa == thanhVien.phuongXa &&
              person.soNha == thanhVien.soNha &&
              person.sdt == thanhVien.sdt) ==
          false) {
        if (list.any((person) => person.soTheBHYT == thanhVien.soTheBHYT) ==
            false) {
          FirebaseFirestore.instance
              .collection('Users')
              .doc(StorePreferences.getIdUser())
              .update({
            'listNguoiThan': FieldValue.arrayUnion([thanhVien.toMap()])
          }).then((value) {
            ToastCommon.showToast(AppLocalizations.of(context)!
                .translate('toast_add_file_success'));
            Navigator.pop(context);
          });
        } else {
          showDialog(
              context: context,
              builder: (context) {
                return ErrorPopup(
                  message: AppLocalizations.of(context)!
                      .translate('error_bhyt_duplicate'),
                  buttonText: 'OK',
                  onPress: () {
                    Navigator.pop(context);
                  },
                );
              });
        }
      } else {
        showDialog(
            context: context,
            builder: (context) {
              return ErrorPopup(
                message: AppLocalizations.of(context)!
                    .translate('error_file_duplicate'),
                buttonText: 'OK',
                onPress: () {
                  Navigator.pop(context);
                },
              );
            });
      }
    } on Exception catch (e) {
      print(e);
      // ignore: use_build_context_synchronously
      showDialog(
          context: context,
          builder: (context) {
            return ErrorPopup(
              message:
                  AppLocalizations.of(context)!.translate('error_add_file'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          });
    }
  }
}
