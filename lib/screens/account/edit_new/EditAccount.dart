import 'dart:io';

import 'package:app_doan_flutter/components/bottomnavigator/BottomNavigator.dart';
import 'package:app_doan_flutter/components/popup/ErrorPopup.dart';
import 'package:app_doan_flutter/components/toast/ToastCommon.dart';
import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/screens/login/data/User.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/InputDropDown.dart';
import '../../../components/inputform/InputTextField.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FuturePicture.dart';
import '../../../untils/FutureUser.dart';
import '../../category/ListCategory.dart';

class EditAccount extends StatefulWidget {
  final bool checkType;
  final FamilyMember nguoiThan;
  const EditAccount({
    super.key,
    required this.checkType,
    required this.nguoiThan,
  });

  @override
  State<EditAccount> createState() => _EditAccountState();
}

class _EditAccountState extends State<EditAccount> {
  final FocusNode _focusSoTheBHYT = FocusNode();
  final FocusNode _focusTen = FocusNode();
  final FocusNode _focusNgheNghiep = FocusNode();
  final FocusNode _focusSoNha = FocusNode();
  final FocusNode _focusSDT = FocusNode();

  String textMoiQH = '';
  String textGioiTinh = '';
  String textDanToc = '';
  String textTinhThanhPho = '';
  String textQuanHuyen = '';
  String textPhuongXa = '';
  String textNgaySinh = '';
  String urlImg = '';

  bool validate = false;
  DateTime dateTime = DateTime.now();
  final TextEditingController textSoTheBHYT = TextEditingController();
  final TextEditingController textTen = TextEditingController();
  final TextEditingController textNgheNghiep = TextEditingController();
  final TextEditingController textSoNha = TextEditingController();
  final TextEditingController textSDT = TextEditingController();

  @override
  void initState() {
    textMoiQH = widget.nguoiThan.moiQH;
    textGioiTinh = widget.nguoiThan.gioiTinh;
    textDanToc = widget.nguoiThan.danToc;
    textTinhThanhPho = widget.nguoiThan.tinhThanhPho;
    textQuanHuyen = widget.nguoiThan.quanHuyen;
    textPhuongXa = widget.nguoiThan.phuongXa;
    textNgaySinh = widget.nguoiThan.ngaySinh;
    textSoTheBHYT.text = widget.nguoiThan.soTheBHYT;
    textTen.text = widget.nguoiThan.ten;
    textNgheNghiep.text = widget.nguoiThan.ngheNghiep;
    textSoNha.text = widget.nguoiThan.soNha;
    textSDT.text = widget.nguoiThan.sdt;
    urlImg = widget.nguoiThan.imgURL;
    super.initState();
    _focusSoTheBHYT.addListener(_onFocusChange);
    _focusTen.addListener(_onFocusChange);
    _focusNgheNghiep.addListener(_onFocusChange);
    _focusSoNha.addListener(_onFocusChange);
    _focusSDT.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    setState(() {});
  }

  @override
  void dispose() {
    _focusSoTheBHYT.dispose();
    _focusTen.dispose();
    _focusNgheNghiep.dispose();
    _focusSoNha.dispose();
    _focusSDT.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: widget.checkType == true
                  ? AppLocalizations.of(context)!
                      .translate('app_bar_update_file')
                  : AppLocalizations.of(context)!
                      .translate("app_bar_edit_file"),
              check: false,
              onPress: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: Scaffold(
            body: SingleChildScrollView(
              child: Column(
                children: [
                  const SizedBox(
                    height: 16,
                  ),
                  Stack(
                    children: [
                      urlImg == widget.nguoiThan.imgURL
                          ? ClipOval(
                              child: CachedNetworkImage(
                              imageUrl: urlImg,
                              fit: BoxFit.fill,
                              placeholder: (context, url) => const CircleAvatar(
                                radius: 40,
                                backgroundImage:
                                    AssetImage('assets/images/account.jpg'),
                              ),
                              errorWidget: (context, url, error) =>
                                  const CircleAvatar(
                                radius: 40,
                                backgroundImage:
                                    AssetImage('assets/images/account.jpg'),
                              ),
                              width: MediaQuery.of(context).size.width * 0.6,
                              height: MediaQuery.of(context).size.height * 0.29,
                            ))
                          : ClipOval(
                              child: Image.file(
                                File(urlImg),
                                fit: BoxFit.cover,
                                width: MediaQuery.of(context).size.width * 0.6,
                                height:
                                    MediaQuery.of(context).size.height * 0.29,
                              ),
                            ),
                      Positioned(
                          bottom: 0,
                          right: 10,
                          child: MaterialButton(
                            elevation: 1,
                            onPressed: () {
                              showBottom(context, () async {
                                final ImagePicker picker = ImagePicker();
                                final XFile? image = await picker.pickImage(
                                    source: ImageSource.gallery);
                                if (image != null) {
                                  print(
                                      'Image path: ${image.path} -- Mimetype: ${image.mimeType}');
                                  setState(() {
                                    urlImg = image.path;
                                  });
                                  Navigator.pop(context);
                                }
                              }, () async {
                                final ImagePicker picker = ImagePicker();
                                final XFile? image = await picker.pickImage(
                                    source: ImageSource.camera);
                                if (image != null) {
                                  print(
                                      'Image path: ${image.path} -- Mimetype: ${image.mimeType}');
                                  setState(() {
                                    urlImg = image.path;
                                  });
                                  Navigator.pop(context);
                                }
                              });
                            },
                            child: Icon(
                              Icons.camera_alt,
                              color: backgroudSearch,
                              size: 30,
                            ),
                          ))
                    ],
                  ),
                  Container(
                    decoration: BoxDecorationContainer(),
                    padding: const EdgeInsets.only(
                        top: 16, left: 16, right: 16, bottom: 16),
                    margin: const EdgeInsets.only(
                        top: 8, left: 16, right: 16, bottom: 16),
                    child: Column(
                      children: [
                        InputDropDown(
                            onPress: () {
                              if (widget.checkType == false) {
                                guiNhanDrop(
                                    defaultQuanHe,
                                    AppLocalizations.of(context)!.translate(
                                        'category_title_relationship'));
                              }
                            },
                            value: textMoiQH,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_relationship"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textMoiQH.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("account_title_number_bhyt"),
                          checkRangBuoc: true,
                          focusNode: _focusSoTheBHYT,
                          textController: textSoTheBHYT,
                          onPress: () {
                            setState(() {
                              textSoTheBHYT.clear();
                            });
                          },
                          isError: validate && textSoTheBHYT.text.isEmpty,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("account_title_name"),
                          checkRangBuoc: true,
                          focusNode: _focusTen,
                          textController: textTen,
                          onPress: () {
                            setState(() {
                              textTen.clear();
                            });
                          },
                          isError: validate && textTen.text.isEmpty,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              _show();
                            },
                            value: textNgaySinh,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_date"),
                            checkRangBuoc: true,
                            icon: Icons.calendar_month,
                            isError: validate && textNgaySinh.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("account_title_job"),
                          checkRangBuoc: true,
                          focusNode: _focusNgheNghiep,
                          textController: textNgheNghiep,
                          onPress: () {
                            setState(() {
                              textNgheNghiep.clear();
                            });
                          },
                          isError: validate && textNgheNghiep.text.isEmpty,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultGioiTinh,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_sex'));
                            },
                            value: textGioiTinh,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_sex"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textGioiTinh.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultDanToc,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_nation'));
                            },
                            value: textDanToc,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_nation"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textDanToc.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultTinh,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_province'));
                            },
                            value: textTinhThanhPho,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_province"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textTinhThanhPho.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultHuyen,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_district'));
                            },
                            value: textQuanHuyen,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_district"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textQuanHuyen.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultXa,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_commune'));
                            },
                            value: textPhuongXa,
                            title: AppLocalizations.of(context)!
                                .translate("account_title_coummune"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textPhuongXa.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("account_title_number_house"),
                          checkRangBuoc: false,
                          focusNode: _focusSoNha,
                          textController: textSoNha,
                          onPress: () {
                            setState(() {
                              textSoNha.clear();
                            });
                          },
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("phone_number"),
                          checkRangBuoc: true,
                          focusNode: _focusSDT,
                          textController: textSDT,
                          onPress: () {
                            setState(() {
                              textSDT.clear();
                            });
                          },
                          isError: validate && textSDT.text.isEmpty,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            bottomNavigationBar: BottomNavigator(
              widget: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: 16,
                  ),
                  Expanded(
                      child: CustomButton(
                    text: AppLocalizations.of(context)!
                        .translate("button_cancel"),
                    height: 32,
                    icon: Icons.cancel_outlined,
                    iconColor: primaryColor,
                    textColor: primaryColor,
                    background: backgroundScreenColor,
                    press: () {
                      Navigator.pop(context);
                    },
                  )),
                  Container(
                    width: 8,
                  ),
                  Expanded(
                      child: CustomButton(
                    text: AppLocalizations.of(context)!
                        .translate("button_update"),
                    icon: Icons.save_alt,
                    height: 32,
                    press: () {
                      checkValidate();
                      if (validate == false) {
                        if (isPhoneNumberValid(textSDT.text) == true) {
                          editUser();
                        } else {
                          showDialog(
                              context: context,
                              builder: ((context) {
                                return ErrorPopup(
                                  message: AppLocalizations.of(context)!
                                      .translate('phone_number_error_format'),
                                  buttonText: 'OK',
                                  onPress: () {
                                    Navigator.pop(context);
                                  },
                                );
                              }));
                        }
                      }
                    },
                  )),
                  Container(
                    width: 16,
                  ),
                ],
              ),
            ),
          )),
    );
  }

  void guiNhanDrop(String loai, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      if (value != null) {
        if (loai == defaultQuanHe) {
          setState(() {
            textMoiQH = value;
          });
        } else if (loai == defaultGioiTinh) {
          setState(() {
            textGioiTinh = value;
          });
        } else if (loai == defaultDanToc) {
          setState(() {
            textDanToc = value;
          });
        } else if (loai == defaultTinh) {
          setState(() {
            textTinhThanhPho = value;
          });
        } else if (loai == defaultHuyen) {
          setState(() {
            textQuanHuyen = value;
          });
        } else if (loai == defaultXa) {
          setState(() {
            textPhuongXa = value;
          });
        }
      }
    });
  }

  void _show() async {
    final DateTime? result = await showDatePicker(
      context: context,
      initialDate: dateTime,
      firstDate: DateTime(1600),
      lastDate: DateTime(2025),
      locale: const Locale('vi'),
    );
    if (result != null) {
      setState(() {
        dateTime = result;
        textNgaySinh = '${formatDate(dateTime, [dd, '-', mm, '-', yyyy])}';
      });
    }
  }

  void checkValidate() {
    if (textMoiQH.isNotEmpty &&
        textGioiTinh.isNotEmpty &&
        textDanToc.isNotEmpty &&
        textTinhThanhPho.isNotEmpty &&
        textQuanHuyen.isNotEmpty &&
        textPhuongXa.isNotEmpty &&
        textSoTheBHYT.text.isNotEmpty &&
        textTen.text.isNotEmpty &&
        textNgheNghiep.text.isNotEmpty &&
        textSDT.text.isNotEmpty) {
      setState(() {
        validate = false;
      });
    } else {
      setState(() {
        validate = true;
      });
    }
  }

  bool isPhoneNumberValid(String phoneNumber) {
    // Biểu thức chính quy kiểm tra định dạng số điện thoại cơ bản
    final phoneRegExp = RegExp(
      r'^(\d{10,12})$',
    );
    return phoneRegExp.hasMatch(phoneNumber);
  }

  Future<void> editUser() async {
    try {
      List<FamilyMember> listNguoiThan =
          await readListNguoiThan(StorePreferences.getIdUser());
      String url = '';
      if (urlImg.isNotEmpty) {
        url = await updateProfilePicture(
            StorePreferences.getIdUser(), File(urlImg));
      }
      //nếu là sửa bản thân
      if (widget.checkType == true) {
        Users banThanMoi = Users(
            id: StorePreferences.getIdUser(),
            moiQH: textMoiQH,
            imgURL: url,
            ten: textTen.text,
            email: StorePreferences.getEmail(),
            sdt: textSDT.text,
            soTheBHYT: textSoTheBHYT.text,
            ngaySinh: textNgaySinh,
            ngheNghiep: textNgheNghiep.text,
            gioiTinh: textGioiTinh,
            danToc: textDanToc,
            tinhThanhPho: textTinhThanhPho,
            quanHuyen: textQuanHuyen,
            phuongXa: textPhuongXa,
            soNha: textSoNha.text,
            listMember: listNguoiThan,
            hoatDongCuoi: '',
            pushToken: '',
            online: false,
            idUser: StorePreferences.getIdUser());
        if (listNguoiThan
                .any((person) => person.soTheBHYT == textSoTheBHYT.text) ==
            false) {
          FirebaseFirestore.instance
              .collection('Users')
              .doc(StorePreferences.getIdUser())
              .update(banThanMoi.toMap())
              .then((value) {
            StorePreferences.setBHYT_User(textSoTheBHYT.text);
            ToastCommon.showToast(AppLocalizations.of(context)!
                .translate('toast_edit_file_success'));
            Navigator.pop(context);
          }).catchError((e) {
            ToastCommon.showToast(AppLocalizations.of(context)!
                .translate('error_edit_file_faild'));
            Navigator.pop(context);
          });
        } else {
          showDialog(
              context: context,
              builder: ((context) {
                return ErrorPopup(
                  message: AppLocalizations.of(context)!
                      .translate('error_bhyt_duplicate'),
                  buttonText: 'OK',
                  onPress: () {
                    Navigator.pop(context);
                  },
                );
              }));
        }
      } else {
        String url = '';
        if (url.isNotEmpty) {
          url = await updateProfilePicture(widget.nguoiThan.id, File(urlImg));
        }
        // nếu là sửa người thân
        FamilyMember nguoiThanMoi = FamilyMember(
          id: widget.nguoiThan.id,
          idUser: StorePreferences.getIdUser(),
          imgURL: url,
          moiQH: textMoiQH,
          ten: textTen.text,
          sdt: textSDT.text,
          soTheBHYT: textSoTheBHYT.text,
          ngaySinh: textNgaySinh,
          ngheNghiep: textNgheNghiep.text,
          gioiTinh: textGioiTinh,
          danToc: textDanToc,
          tinhThanhPho: textTinhThanhPho,
          quanHuyen: textQuanHuyen,
          phuongXa: textPhuongXa,
          soNha: textSoNha.text,
        );
        for (FamilyMember i in listNguoiThan) {
          if (i.id == widget.nguoiThan.id) {
            listNguoiThan.remove(i);
            break;
          }
        }
        print(listNguoiThan.length);
        if (listNguoiThan.any((person) =>
                person.moiQH == nguoiThanMoi.moiQH &&
                person.soTheBHYT == nguoiThanMoi.soTheBHYT &&
                person.ten == nguoiThanMoi.ten &&
                person.ngaySinh == nguoiThanMoi.ngaySinh &&
                person.ngheNghiep == nguoiThanMoi.ngheNghiep &&
                person.gioiTinh == nguoiThanMoi.gioiTinh &&
                person.danToc == nguoiThanMoi.danToc &&
                person.tinhThanhPho == nguoiThanMoi.tinhThanhPho &&
                person.quanHuyen == nguoiThanMoi.quanHuyen &&
                person.phuongXa == nguoiThanMoi.phuongXa &&
                person.soNha == nguoiThanMoi.soNha &&
                person.sdt == nguoiThanMoi.sdt) ==
            false) {
          if (listNguoiThan.any(
                      (person) => person.soTheBHYT == nguoiThanMoi.soTheBHYT) ==
                  false &&
              nguoiThanMoi.soTheBHYT != StorePreferences.getBHYT_User()) {
            listNguoiThan.add(nguoiThanMoi);
            FirebaseFirestore.instance
                .collection('Users')
                .doc(StorePreferences.getIdUser())
                .update({
              'listNguoiThan': listNguoiThan.map((e) => e.toMap()).toList()
            }).then((value) {
              ToastCommon.showToast(AppLocalizations.of(context)!
                  .translate('toast_edit_file_family_success'));
              Navigator.pop(context);
            }).catchError((e) {
              ToastCommon.showToast(AppLocalizations.of(context)!
                  .translate('error_edit_file_family_faild'));
            });
          } else {
            showDialog(
                context: context,
                builder: (context) {
                  return ErrorPopup(
                    message: AppLocalizations.of(context)!
                        .translate('error_bhyt_duplicate'),
                    buttonText: 'OK',
                    onPress: () {
                      Navigator.pop(context);
                    },
                  );
                });
          }
        } else {
          showDialog(
              context: context,
              builder: (context) {
                return ErrorPopup(
                  message: AppLocalizations.of(context)!
                      .translate('error_file_duplicate'),
                  buttonText: 'OK',
                  onPress: () {
                    Navigator.pop(context);
                  },
                );
              });
        }
      }
    } on Exception catch (e) {
      print(e);
      // ignore: use_build_context_synchronously
      showDialog(
          context: context,
          builder: (context) {
            return ErrorPopup(
              message:
                  AppLocalizations.of(context)!.translate('error_add_file'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          });
    }
  }
}
