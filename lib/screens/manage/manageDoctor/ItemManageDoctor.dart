import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:flutter/material.dart';

import '../../../components/inputform/RowTextView.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../config/styles/Dimens.dart';
import '../../../item/ItemInformation.dart';
import '../../../untils/AppLocalizations.dart';

class ItemManageDoctor extends StatelessWidget {
  final String ten;
  final String tenBenhVien;
  final String gioiTinh;
  final String viTri;
  final String sdt;

  const ItemManageDoctor(
      {super.key,
      required this.ten,
      required this.tenBenhVien,
      required this.gioiTinh,
      required this.viTri,
      required this.sdt});

  @override
  Widget build(BuildContext context) {
    Color? titleColor;
    if (viTri == key_role_doctor_examination) {
      titleColor = primaryColor;
    } else if (viTri == key_role_doctor_vaccination) {
      titleColor = backgroudSearch;
    } else if (viTri == key_role_doctor_advise) {
      titleColor = Color(0xFF1C4983);
    }
    return ItemInformation(
      widgetBottom: Column(
        children: [
          RowTextView(
            title: AppLocalizations.of(context)!.translate('select_hospital'),
            content: tenBenhVien,
            flexItemLeft: 2,
            flexItemRight: 4,
          ),
          RowTextView(
            title: AppLocalizations.of(context)!.translate('account_title_sex'),
            content: gioiTinh,
            flexItemLeft: 2,
            flexItemRight: 4,
          ),
          RowTextView(
            title: AppLocalizations.of(context)!.translate('doctor_title_role'),
            content: viTri,
            flexItemLeft: 2,
            flexItemRight: 4,
            paddingBottom: defaultPaddingMin,
          ),
        ],
      ),
      widgetTop: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Text(
              ten,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
      titleColor: titleColor,
    );
  }
}
