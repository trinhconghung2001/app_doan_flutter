import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/home/bookExamination/data/Examination.dart';
import 'package:app_doan_flutter/screens/manage/data/Department.dart';
import 'package:app_doan_flutter/screens/manage/data/DepartmentAdvise.dart';
import 'package:app_doan_flutter/screens/manage/data/Doctor.dart';
import 'package:app_doan_flutter/screens/manage/data/Hospital.dart';
import 'package:app_doan_flutter/screens/manage/data/Room.dart';
import 'package:app_doan_flutter/screens/manage/manageDoctor/EditManageDoctor.dart';
import 'package:app_doan_flutter/untils/FutureAdvise.dart';
import 'package:app_doan_flutter/untils/FutureHospital.dart';
import 'package:app_doan_flutter/untils/FutureManageDoctor.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/RowTextView.dart';
import '../../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureDepartment.dart';
import '../../../untils/FutureExamination.dart';

class DetailManageDoctor extends StatefulWidget {
  final String idSend;
  const DetailManageDoctor({super.key, required this.idSend});

  @override
  State<DetailManageDoctor> createState() => _DetailManageDoctorState();
}

class _DetailManageDoctorState extends State<DetailManageDoctor> {
  late Future<void> _loadDetailDoctor;
  Doctor bacSi = Doctor.defaultContructor();
  Room phong = Room.defaultContructor();

  @override
  void initState() {
    _loadDetailDoctor = loadDetail();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDetailDoctor = loadDetail();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: AppLocalizations.of(context)!
                .translate("app_bar_detail_doctor"),
            check: false,
            onPress: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Scaffold(
          body: SingleChildScrollView(
              child: FutureBuilder(
            future: _loadDetailDoctor,
            builder: ((context, AsyncSnapshot snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return CircularProgressIndicatorWidget();
              } else if (snapshot.hasError) {
                return EmptyData(
                    msg: AppLocalizations.of(context)!
                        .translate('error_data_msg'));
              } else {
                return Column(
                  children: [
                    const SizedBox(
                      height: 16,
                    ),
                    bacSi.imgURL != ''
                        ? ClipOval(
                            child: CachedNetworkImage(
                            imageUrl: bacSi.imgURL,
                            fit: BoxFit.fill,
                            placeholder: (context, url) => const CircleAvatar(
                              radius: 40,
                              backgroundImage:
                                  AssetImage('assets/images/account.jpg'),
                            ),
                            errorWidget: (context, url, error) =>
                                const CircleAvatar(
                              radius: 40,
                              backgroundImage:
                                  AssetImage('assets/images/account.jpg'),
                            ),
                            width: MediaQuery.of(context).size.width * 0.6,
                            height: MediaQuery.of(context).size.height * 0.29,
                          ))
                        : ClipOval(
                            child: Image.asset(
                              'assets/images/account.jpg',
                              fit: BoxFit.fill,
                              width: MediaQuery.of(context).size.width * 0.6,
                              height: MediaQuery.of(context).size.height * 0.29,
                            ),
                          ),
                    Container(
                      decoration: BoxDecorationContainer(),
                      padding: const EdgeInsets.only(
                        top: 16,
                        left: 16,
                        right: 16,
                      ),
                      margin: const EdgeInsets.only(
                          top: 8, left: 16, right: 16, bottom: 0),
                      child: Column(
                        children: [
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('title_name_doctor'),
                              content: bacSi.ten,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('email'),
                              content: bacSi.email,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('title_name_room_examination'),
                              content: phong.tenPhong,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('account_title_sex'),
                              content: bacSi.gioiTinh,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('account_title_nation'),
                              content: bacSi.danToc,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('account_title_date'),
                              content: bacSi.ngaySinh,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('doctor_title_role'),
                              content: bacSi.role,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('account_title_province'),
                              content: bacSi.tinhThanhPho,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('account_title_district'),
                              content: bacSi.quanHuyen,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('account_title_coummune'),
                              content: bacSi.phuongXa,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('account_title_number_house'),
                              content: bacSi.soNha,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('phone_number'),
                              content: bacSi.sdt,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                        ],
                      ),
                    ),
                  ],
                );
              }
            }),
          )),
          bottomNavigationBar: BottomNavigator(
              widget: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                width: 16,
              ),
              Expanded(
                  child: CustomButton(
                text: AppLocalizations.of(context)!.translate("button_delete"),
                height: 32,
                icon: Icons.cancel_outlined,
                iconColor: primaryColor,
                textColor: primaryColor,
                background: backgroundScreenColor,
                press: () {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return ErrorPopup(
                          message: AppLocalizations.of(context)!
                              .translate('popup_remove_account'),
                          buttonText: AppLocalizations.of(context)!
                              .translate('popup_button_confirm'),
                          onPress: () {
                            deleteDoctor();
                          },
                        );
                      });
                },
              )),
              Container(
                width: 8,
              ),
              Expanded(
                  child: CustomButton(
                text: AppLocalizations.of(context)!.translate("button_edit"),
                icon: Icons.edit_square,
                height: 32,
                press: () {
                  Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  EditManageDoctor(bacSi: bacSi, phong: phong)))
                      .then((value) => loadScreen());
                },
              )),
              Container(
                width: 16,
              ),
            ],
          )),
        ));
  }

  Future<void> loadDetail() async {
    List<Doctor> listDoctor =
        await readAllDoctor_IdHosital(StorePreferences.getIdHospital());
    for (Doctor e in listDoctor) {
      if (e.id == widget.idSend) {
        bacSi = e;
        break;
      }
    }
    String idPhong = bacSi.idPhong;
    String viTri = bacSi.role;
    if (viTri == key_role_doctor_examination) {
      List<Department> listPhong = await readAllDepartment_ByIdHospital(
          StorePreferences.getIdHospital());
      for (Department i in listPhong) {
        List<Room> phongKham = i.phongKham;
        for (Room e in phongKham) {
          if (e.id == idPhong) {
            phong = e;
            break;
          }
        }
      }
    } else if (viTri == key_role_doctor_vaccination) {
      // Hospital hospital =
      //     await readIDHospital(StorePreferences.getIdHospital());
      // List<Room> listPhong = hospital.phongTiem;
      // for (Room i in listPhong) {
      //   if (i.id == idPhong) {
      //     phong = i;
      //     break;
      //   }
      // }
    } else if (viTri == key_role_doctor_advise) {
      List<DepartmentAdvise> listPhongTuVan =
          await readAllDepartmentAdvise_ByIdHospital(
              StorePreferences.getIdHospital());
      for (DepartmentAdvise i in listPhongTuVan) {
        List<Room> phongTuVan = i.phongTuVan;
        for (Room e in phongTuVan) {
          if (e.id == idPhong) {
            phong = e;
            break;
          }
        }
      }
    }
  }

  Future<void> deleteDoctor() async {
    String viTri = bacSi.role;
    List<Examination> listExamination = [];
    // List<Vaccination> listVaccination = [];
    DateTime dateTime = DateFormat('dd-MM-yyyy').parse(bacSi.ngaySinh);
    UserCredential userCredential =
        await FirebaseAuth.instance.signInWithEmailAndPassword(
      email: bacSi.email,
      password: '${formatDate(dateTime, [dd, mm, yyyy])}',
    );
    User user = userCredential.user!;
    // Lấy đối tượng User từ UserCredential
    if (viTri == key_role_doctor_examination) {
      List<Examination> result = await readAllExamination_byIDHosital(
          StorePreferences.getIdHospital());
      result.forEach((element) {
        if (element.idPhong == bacSi.idPhong) {
          listExamination.add(element);
        }
      });
      if (listExamination.length != 0) {
        ToastCommon.showToast(AppLocalizations.of(context)!
            .translate('error_remove_doctor_exits'));
        Navigator.pop(context);
      } else {
        removeAccout(user, bacSi.id, bacSi.role, bacSi.idKhoa, bacSi.idPhong)
            .then((value) {
          ToastCommon.showToast(AppLocalizations.of(context)!
              .translate('toast_remove_doctor_success'));
          Navigator.pop(context);
        });
      }
    } else if (viTri == key_role_doctor_vaccination) {
      // List<Vaccination> result = await readAllVaccination_byIDHosital(
      //     StorePreferences.getIdHospital());
      // result.forEach((element) {
      //   if (element.idPhongTiem == bacSi.idPhong) {
      //     listVaccination.add(element);
      //   }
      // });
      // if (listVaccination.length != 0) {
      //   ToastCommon.showToast(AppLocalizations.of(context)!
      //       .translate('error_remove_doctor_exits'));
      //   Navigator.pop(context);
      // } else {
      //   removeAccout(user, bacSi.id, bacSi.role, bacSi.idKhoa, bacSi.idPhong)
      //       .then((value) {
      //     ToastCommon.showToast(AppLocalizations.of(context)!
      //         .translate('toast_remove_doctor_success'));
      //     Navigator.pop(context);
      //   });
      // }
    } else if (viTri == key_role_doctor_advise) {
      bool check = await checkCalendarAdvise(bacSi.id);
      if (check == true) {
        ToastCommon.showToast(AppLocalizations.of(context)!
            .translate('error_remove_doctor_exits'));
        Navigator.pop(context);
      } else {
        removeAccout(user, bacSi.id, bacSi.role, bacSi.idKhoa, bacSi.idPhong)
            .then((value) {
          ToastCommon.showToast(AppLocalizations.of(context)!
              .translate('toast_remove_doctor_success'));
          Navigator.pop(context);
        });
      }
    }
  }

  Future<void> removeAccout(User user, String idDoctor, String role,
      String idKhoa, String idPhong) async {
    await user.delete();
    // Xóa tài khoản thành công
    await FirebaseFirestore.instance
        .collection('Doctors')
        .doc(idDoctor)
        .delete();
    Hospital benhVien = await readIDHospital(StorePreferences.getIdHospital());
    List<Department> listKhoa = benhVien.khoa;
    // List<Room> listPhongTiem = benhVien.phongTiem;
    List<DepartmentAdvise> listKhoaTuVan = benhVien.khoaTuVan;
    if (role == key_role_doctor_examination) {
      Department khoa = Department.defaultContructor();
      for (Department e in listKhoa) {
        if (e.id == idKhoa) {
          khoa = e;
          break;
        }
      }
      List<Room> listPhongKham = khoa.phongKham;
      for (int i = 0; i < listPhongKham.length; i++) {
        if (listPhongKham[i].id == idPhong) {
          listPhongKham[i].idBacSi = '';
          break;
        }
      }
    } else if (role == key_role_doctor_vaccination) {
      // for (int i = 0; i < listPhongTiem.length; i++) {
      //   if (listPhongTiem[i].id == idPhong) {
      //     listPhongTiem[i].idBacSi = '';
      //     break;
      //   }
      // }
    } else if (role == key_role_doctor_advise) {
      DepartmentAdvise khoaTuVan = DepartmentAdvise.defaultContructor();
      for (DepartmentAdvise e in listKhoaTuVan) {
        if (e.id == idKhoa) {
          khoaTuVan = e;
          break;
        }
      }
      List<Room> listPhongTuVan = khoaTuVan.phongTuVan;
      for (int i = 0; i < listPhongTuVan.length; i++) {
        if (listPhongTuVan[i].id == idPhong) {
          listPhongTuVan[i].idBacSi = '';
          break;
        }
      }
    }
    Hospital benhVienNew = Hospital(
        id: benhVien.id,
        ten: benhVien.ten,
        khoa: listKhoa,
        lichKham: benhVien.lichKham,
        // lichTuVan: benhVien.lichTuVan,
        // lichTiem: benhVien.lichTiem,
        // phongTiem: listPhongTiem,
        khoaTuVan: listKhoaTuVan);
    CollectionReference collection =
        FirebaseFirestore.instance.collection('Hospital');
    DocumentReference document =
        await collection.doc(StorePreferences.getIdHospital());
    await document.set(benhVienNew.toMap());
  }
}
