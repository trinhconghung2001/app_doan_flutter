import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/category/ListStatus.dart';
import 'package:app_doan_flutter/screens/manage/data/Doctor.dart';
import 'package:app_doan_flutter/screens/manage/data/Hospital.dart';
import 'package:app_doan_flutter/screens/manage/manageDoctor/AddManageDoctor.dart';
import 'package:app_doan_flutter/screens/manage/manageDoctor/DetailManageDoctor.dart';
import 'package:app_doan_flutter/screens/manage/manageDoctor/ItemManageDoctor.dart';
import 'package:app_doan_flutter/untils/FutureHospital.dart';
import 'package:app_doan_flutter/untils/FutureManageDoctor.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/filtersearch/RowFilter.dart';
import '../../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../config/styles/Dimens.dart';
import '../../../untils/AppLocalizations.dart';

class ManageDoctor extends StatefulWidget {
  const ManageDoctor({super.key});

  @override
  State<ManageDoctor> createState() => _ManageDoctorState();
}

class _ManageDoctorState extends State<ManageDoctor> {
  late Future<void> _loadDataDoctor;
  List<Doctor> listDoctor = [];
  List<Hospital> listHospital = [];
  String textViTri = '';
  String textGioiTinh = '';

  @override
  void initState() {
    _loadDataDoctor = loadData();
    super.initState();
  }

  void loadScreen() {
    print('Vao load');
    setState(() {
      _loadDataDoctor = loadData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: AppLocalizations.of(context)!
                .translate("app_bar_manage_doctor"),
            check: true,
            onPress: () {
              Navigator.pop(context);
            },
            checkIconBack: false,
            icon: Icon(Icons.sync),
            onTap: () {
              loadScreen();
            },
          ),
        ),
        body: ListView.builder(
            itemCount: 2,
            itemBuilder: (BuildContext context, index) {
              if (index == 0) {
                return FilterManageDoctor(
                  dataSearch: searchData,
                  textViTri: textViTri,
                  textGioiTinh: textGioiTinh,
                  requestReload: loadScreen,
                );
              } else {
                return FutureBuilder(
                    future: _loadDataDoctor,
                    builder: ((context, AsyncSnapshot snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return CircularProgressIndicatorWidget();
                      } else if (snapshot.hasError) {
                        return EmptyData(
                            msg: AppLocalizations.of(context)!
                                .translate('error_data_msg'));
                      } else {
                        if (listDoctor.length > 0) {
                          return Container(
                              margin: const EdgeInsets.only(top: 8),
                              child: ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: listDoctor.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetailManageDoctor(
                                                        idSend:
                                                            listDoctor[index]
                                                                .id))).then(
                                            (value) => loadScreen());
                                      },
                                      child: ItemManageDoctor(
                                          ten: listDoctor[index].ten,
                                          tenBenhVien: listHospital[index].ten,
                                          gioiTinh: listDoctor[index].gioiTinh,
                                          viTri: listDoctor[index].role,
                                          sdt: listDoctor[index].sdt));
                                },
                              ));
                        } else {
                          // Không có dữ liệu
                          return EmptyData();
                        }
                      }
                    }));
              }
            }),
      ),
    );
  }

  void searchData(List<String> list) {
    setState(() {
      _loadDataDoctor = loadDataSearch(list);
      textGioiTinh = list[1];
      textViTri = list[0];
    });
  }

  Future<void> loadDataSearch(List<String> list) async {
    listDoctor = [];
    List<Doctor> result =
        await readAllDoctor_IdHosital(StorePreferences.getIdHospital());
    List<Doctor> listResult = [];
    result.forEach((e) {
      if (e.role != key_role_reception) {
        listResult.add(e);
      }
    });
    if (list[0] == 'Tất cả') {
      if (list[1] == 'Tất cả') {
        listDoctor = listResult;
      } else {
        listResult.forEach((e) {
          if (e.gioiTinh == list[1]) {
            listDoctor.add(e);
          }
        });
      }
    } else {
      if (list[1] == 'Tất cả') {
        listResult.forEach((e) {
          if (e.role == list[0]) {
            listDoctor.add(e);
          }
        });
      } else {
        listResult.forEach((e) {
          if (e.gioiTinh == list[1] && e.role == list[0]) {
            listDoctor.add(e);
          }
        });
      }
    }
  }

  Future<void> loadData() async {
    textGioiTinh = 'Tất cả';
    textViTri = 'Tất cả';
    print(StorePreferences.getIdHospital());
    List<Doctor> list =
        await readAllDoctor_IdHosital(StorePreferences.getIdHospital());
    listDoctor = [];
    listHospital = [];
    list.forEach((e) {
      if (e.role != key_role_reception) {
        listDoctor.add(e);
      }
    });
    for (Doctor i in listDoctor) {
      Hospital hospital = Hospital.defaultContructor();
      hospital = await readIDHospital(i.idBenhVien);
      listHospital.add(hospital);
    }
  }
}

// ignore: must_be_immutable
class FilterManageDoctor extends StatefulWidget {
  final void Function(List<String> list) dataSearch;
  final void Function() requestReload;
  String textViTri;
  String textGioiTinh;
  FilterManageDoctor({
    super.key,
    required this.dataSearch,
    required this.textViTri,
    required this.textGioiTinh,
    required this.requestReload,
    // required this.dataSearch
  });

  @override
  State<FilterManageDoctor> createState() => _FilterManageDoctorState();
}

class _FilterManageDoctorState extends State<FilterManageDoctor> {
  DateTimeRange? dateTimeRange;
  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecorationContainer(color: filterColor),
        margin: const EdgeInsets.only(top: 8, left: 16, right: 16),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(children: [
              Expanded(
                flex: 2,
                child: RowFilter(
                  title: AppLocalizations.of(context)!
                      .translate('doctor_title_role'),
                  value: widget.textViTri,
                  icon: Icons.arrow_drop_down,
                  onPress: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ListStatus(
                                  loai: key_role_reception,
                                  titleAppbar: AppLocalizations.of(context)!
                                      .translate('category_title_role'),
                                ))).then((value) {
                      if (value != '') {
                        setState(() {
                          widget.textViTri = value;
                        });
                      }
                    });
                  },
                ),
              ),
              const SizedBox(
                width: 8,
              ),
              Expanded(
                  flex: 2,
                  child: RowFilter(
                    title: AppLocalizations.of(context)!
                        .translate('account_title_sex'),
                    value: widget.textGioiTinh,
                    icon: Icons.arrow_drop_down,
                    onPress: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ListStatus(
                                    loai: defaultGioiTinh,
                                    titleAppbar: AppLocalizations.of(context)!
                                        .translate('category_title_status'),
                                  ))).then((value) {
                        if (value != '') {
                          setState(() {
                            widget.textGioiTinh = value;
                          });
                        }
                      });
                    },
                  )),
            ]),
            const SizedBox(height: 8),
            Row(
              children: <Widget>[
                Expanded(
                    flex: 2,
                    child: Column(
                      children: [
                        const Padding(
                          padding: EdgeInsets.only(
                            left: 8,
                            right: 8,
                          ),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              '',
                              style: TextStyle(fontSize: 12, color: textColor),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        CustomButton(
                            text: AppLocalizations.of(context)!
                                .translate('button_add_doctor'),
                            icon: Icons.add_circle_outline_outlined,
                            press: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return Dialog(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(8.0)),
                                        child: Container(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              5,
                                          padding:
                                              EdgeInsets.all(defaultPadding),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              1.2,
                                          child: Column(
                                            children: [
                                              Expanded(
                                                  child: CustomButton(
                                                text: AppLocalizations.of(
                                                        context)!
                                                    .translate(
                                                        "button_add_doctor_examination"),
                                                height: 32,
                                                icon: Icons.cancel_outlined,
                                                iconDisplay: false,
                                                iconColor: primaryColor,
                                                textColor: primaryColor,
                                                background:
                                                    backgroundScreenColor,
                                                press: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              AddManageDoctor(
                                                                typeDoctor:
                                                                    key_role_doctor_examination,
                                                              ))).then((value) {
                                                    Navigator.pop(context);
                                                    widget.requestReload();
                                                  });
                                                },
                                              )),
                                              Expanded(
                                                  child: CustomButton(
                                                text: AppLocalizations.of(
                                                        context)!
                                                    .translate(
                                                        "button_add_doctor_advise"),
                                                height: 32,
                                                icon: Icons.cancel_outlined,
                                                iconDisplay: false,
                                                iconColor: primaryColor,
                                                textColor: primaryColor,
                                                background:
                                                    backgroundScreenColor,
                                                press: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              AddManageDoctor(
                                                                typeDoctor:
                                                                    key_role_doctor_advise,
                                                              ))).then((value) {
                                                    Navigator.pop(context);
                                                    widget.requestReload();
                                                  });
                                                },
                                              )),
                                              // Expanded(
                                              //     child: CustomButton(
                                              //   text: AppLocalizations.of(
                                              //           context)!
                                              //       .translate(
                                              //           "button_add_doctor_vaccination"),
                                              //   height: 32,
                                              //   icon: Icons.cancel_outlined,
                                              //   iconDisplay: false,
                                              //   iconColor: primaryColor,
                                              //   textColor: primaryColor,
                                              //   background:
                                              //       backgroundScreenColor,
                                              //   press: () {
                                              //     Navigator.push(
                                              //         context,
                                              //         MaterialPageRoute(
                                              //             builder: (context) =>
                                              //                 AddManageDoctor(
                                              //                   typeDoctor:
                                              //                       key_role_doctor_vaccination,
                                              //                 ))).then((value) {
                                              //       Navigator.pop(context);
                                              //       widget.requestReload();
                                              //     });
                                              //   },
                                              // )),
                                            ],
                                          ),
                                        ));
                                  });
                            },
                            height: 36),
                      ],
                    )),
                const SizedBox(
                  width: 8,
                ),
                Expanded(
                    flex: 2,
                    child: Column(
                      children: [
                        const Padding(
                          padding: EdgeInsets.only(
                            left: 8,
                            right: 8,
                          ),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              '',
                              style: TextStyle(fontSize: 12, color: textColor),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        CustomButton(
                            text: AppLocalizations.of(context)!
                                .translate('button_search'),
                            icon: Icons.search,
                            press: () {
                              widget.dataSearch(
                                  [widget.textViTri, widget.textGioiTinh]);
                            },
                            height: 36),
                      ],
                    )),
              ],
            ),
          ],
        ));
  }
}
