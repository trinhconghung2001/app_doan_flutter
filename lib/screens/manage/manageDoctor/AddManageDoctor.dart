import 'package:app_doan_flutter/screens/manage/data/Department.dart';
import 'package:app_doan_flutter/screens/manage/data/DepartmentAdvise.dart';
import 'package:app_doan_flutter/screens/manage/data/Hospital.dart';
import 'package:app_doan_flutter/screens/manage/data/Room.dart';
import 'package:app_doan_flutter/untils/FutureHospital.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/InputDropDown.dart';
import '../../../components/inputform/InputTextField.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../config/styles/KeyValue.dart';
import '../../../untils/AppLocalizations.dart';
import '../../category/ListCategory.dart';
import '../../category/ListIdCategory.dart';
import '../data/Doctor.dart';

class AddManageDoctor extends StatefulWidget {
  final String typeDoctor;
  const AddManageDoctor({super.key, required this.typeDoctor});

  @override
  State<AddManageDoctor> createState() => _AddManageDoctorState();
}

class _AddManageDoctorState extends State<AddManageDoctor> {
  final FocusNode _focusTen = FocusNode();
  final FocusNode _focusSoNha = FocusNode();
  final FocusNode _focusSDT = FocusNode();
  final FocusNode _focusEmail = FocusNode();

  String textGioiTinh = '';
  String textDanToc = '';
  String textTinhThanhPho = '';
  String textQuanHuyen = '';
  String textPhuongXa = '';
  String textNgaySinh = '';
  String textTenPhong = '';
  String idPhong = '';
  String textTenKhoa = '';

  bool validate = false;
  DateTime dateTime = DateTime.now();

  @override
  void initState() {
    super.initState();
    _focusTen.addListener(_onFocusChange);
    _focusSoNha.addListener(_onFocusChange);
    _focusSDT.addListener(_onFocusChange);
    _focusEmail.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    setState(() {});
  }

  @override
  void dispose() {
    _focusTen.dispose();
    _focusSoNha.dispose();
    _focusSDT.dispose();
    _focusEmail.dispose();

    super.dispose();
  }

  final TextEditingController textTen = TextEditingController();
  final TextEditingController textSoNha = TextEditingController();
  final TextEditingController textSDT = TextEditingController();
  final TextEditingController textEmail = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: AppLocalizations.of(context)!.translate("app_bar_add_file"),
              check: false,
              onPress: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: Scaffold(
            body: SingleChildScrollView(
              child: Container(
                decoration: BoxDecorationContainer(),
                padding: const EdgeInsets.only(
                    top: 16, left: 16, right: 16, bottom: 16),
                margin: const EdgeInsets.only(
                    top: 8, left: 16, right: 16, bottom: 0),
                child: Column(children: [
                  InputTextField(
                    title: AppLocalizations.of(context)!
                        .translate("title_name_doctor"),
                    checkRangBuoc: true,
                    focusNode: _focusTen,
                    textController: textTen,
                    onPress: () {
                      setState(() {
                        textTen.clear();
                      });
                    },
                    isError: validate && textTen.text.isEmpty,
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  InputTextField(
                    title: AppLocalizations.of(context)!.translate("email"),
                    checkRangBuoc: true,
                    focusNode: _focusEmail,
                    textController: textEmail,
                    onPress: () {
                      setState(() {
                        textEmail.clear();
                      });
                    },
                    isError: validate && textEmail.text.isEmpty,
                  ),
                  Visibility(
                    visible: widget.typeDoctor != key_role_doctor_vaccination,
                    child: SizedBox(
                      height: 16,
                    ),
                  ),
                  Visibility(
                    visible: widget.typeDoctor == key_role_doctor_examination,
                    child: InputDropDown(
                        onPress: () {
                          guiNhanIDDrop(
                              defaultKhoa,
                              AppLocalizations.of(context)!
                                  .translate('title_appbar_department'));
                        },
                        value: textTenKhoa,
                        title: AppLocalizations.of(context)!
                            .translate("category_title_department"),
                        checkRangBuoc: true,
                        icon: Icons.arrow_drop_down,
                        isError: validate && textTenKhoa.isEmpty),
                  ),
                  Visibility(
                    visible: widget.typeDoctor == key_role_doctor_advise,
                    child: InputDropDown(
                        onPress: () {
                          guiNhanIDDrop(
                              defaultKhoaTuVan,
                              AppLocalizations.of(context)!
                                  .translate('title_appbar_departmentAdvise'));
                        },
                        value: textTenKhoa,
                        title: AppLocalizations.of(context)!
                            .translate("category_title_departmentAdvise"),
                        checkRangBuoc: true,
                        icon: Icons.arrow_drop_down,
                        isError: validate && textTenKhoa.isEmpty),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  InputDropDown(
                      onPress: () {
                        if (widget.typeDoctor == key_role_doctor_examination) {
                          guiNhanIDDrop(
                              key_role_doctor_examination,
                              AppLocalizations.of(context)!
                                  .translate('title_appbar_room_examination'));
                        } else if (widget.typeDoctor ==
                            key_role_doctor_vaccination) {
                          guiNhanIDDrop(
                              key_role_doctor_vaccination,
                              AppLocalizations.of(context)!
                                  .translate('title_appbar_room_vaccination'));
                        } else if (widget.typeDoctor ==
                            key_role_doctor_advise) {
                          guiNhanIDDrop(
                              key_role_doctor_advise,
                              AppLocalizations.of(context)!
                                  .translate('title_appbar_room_advise'));
                        }
                      },
                      value: textTenPhong,
                      title: AppLocalizations.of(context)!
                          .translate("title_name_room_examination"),
                      checkRangBuoc: true,
                      icon: Icons.arrow_drop_down,
                      isError: validate && textTenPhong.isEmpty),
                  const SizedBox(
                    height: 16,
                  ),
                  InputDropDown(
                      onPress: () {
                        guiNhanDrop(
                            defaultGioiTinh,
                            AppLocalizations.of(context)!
                                .translate('category_title_sex'));
                      },
                      value: textGioiTinh,
                      title: AppLocalizations.of(context)!
                          .translate("account_title_sex"),
                      checkRangBuoc: true,
                      icon: Icons.arrow_drop_down,
                      isError: validate && textGioiTinh.isEmpty),
                  const SizedBox(
                    height: 16,
                  ),
                  InputDropDown(
                      onPress: () {
                        guiNhanDrop(
                            defaultDanToc,
                            AppLocalizations.of(context)!
                                .translate('category_title_nation'));
                      },
                      value: textDanToc,
                      title: AppLocalizations.of(context)!
                          .translate("account_title_nation"),
                      checkRangBuoc: true,
                      icon: Icons.arrow_drop_down,
                      isError: validate && textDanToc.isEmpty),
                  const SizedBox(
                    height: 16,
                  ),
                  InputDropDown(
                      onPress: () {
                        _show();
                      },
                      value: textNgaySinh,
                      title: AppLocalizations.of(context)!
                          .translate("account_title_date"),
                      checkRangBuoc: true,
                      icon: Icons.calendar_month,
                      isError: validate && textNgaySinh.isEmpty),
                  const SizedBox(
                    height: 16,
                  ),
                  InputDropDown(
                      onPress: () {
                        guiNhanDrop(
                            defaultTinh,
                            AppLocalizations.of(context)!
                                .translate('category_title_province'));
                      },
                      value: textTinhThanhPho,
                      title: AppLocalizations.of(context)!
                          .translate("account_title_province"),
                      checkRangBuoc: true,
                      icon: Icons.arrow_drop_down,
                      isError: validate && textTinhThanhPho.isEmpty),
                  const SizedBox(
                    height: 16,
                  ),
                  InputDropDown(
                      onPress: () {
                        guiNhanDrop(
                            defaultHuyen,
                            AppLocalizations.of(context)!
                                .translate('category_title_district'));
                      },
                      value: textQuanHuyen,
                      title: AppLocalizations.of(context)!
                          .translate("account_title_district"),
                      checkRangBuoc: true,
                      icon: Icons.arrow_drop_down,
                      isError: validate && textQuanHuyen.isEmpty),
                  const SizedBox(
                    height: 16,
                  ),
                  InputDropDown(
                      onPress: () {
                        guiNhanDrop(
                            defaultXa,
                            AppLocalizations.of(context)!
                                .translate('category_title_commune'));
                      },
                      value: textPhuongXa,
                      title: AppLocalizations.of(context)!
                          .translate("account_title_coummune"),
                      checkRangBuoc: true,
                      icon: Icons.arrow_drop_down,
                      isError: validate && textPhuongXa.isEmpty),
                  const SizedBox(
                    height: 16,
                  ),
                  InputTextField(
                    title: AppLocalizations.of(context)!
                        .translate("account_title_number_house"),
                    checkRangBuoc: false,
                    focusNode: _focusSoNha,
                    textController: textSoNha,
                    onPress: () {
                      setState(() {
                        textSoNha.clear();
                      });
                    },
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  InputTextField(
                    title:
                        AppLocalizations.of(context)!.translate("phone_number"),
                    checkRangBuoc: true,
                    focusNode: _focusSDT,
                    textController: textSDT,
                    onPress: () {
                      setState(() {
                        textSDT.clear();
                      });
                    },
                    isError: validate && textSDT.text.isEmpty,
                  ),
                ]),
              ),
            ),
          ),
          bottomNavigationBar: BottomNavigator(
            widget: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  width: 16,
                ),
                Expanded(
                    child: CustomButton(
                  text:
                      AppLocalizations.of(context)!.translate("button_cancel"),
                  height: 32,
                  icon: Icons.cancel_outlined,
                  iconColor: primaryColor,
                  textColor: primaryColor,
                  background: backgroundScreenColor,
                  press: () {
                    Navigator.pop(context);
                  },
                )),
                Container(
                  width: 8,
                ),
                Expanded(
                    child: CustomButton(
                  text:
                      AppLocalizations.of(context)!.translate("button_update"),
                  icon: Icons.save_alt,
                  height: 32,
                  press: () {
                    checkValidate();
                    if (validate == false) {
                      if (isPhoneNumberValid(textSDT.text) == true) {
                        if (isEmailValid(textEmail.text) == true) {
                          addDoctor();
                        } else {
                          showDialog(
                              context: context,
                              builder: ((context) {
                                return ErrorPopup(
                                  message: AppLocalizations.of(context)!
                                      .translate('email_error_format'),
                                  buttonText: 'OK',
                                  onPress: () {
                                    Navigator.pop(context);
                                  },
                                );
                              }));
                        }
                      } else {
                        showDialog(
                            context: context,
                            builder: ((context) {
                              return ErrorPopup(
                                message: AppLocalizations.of(context)!
                                    .translate('phone_number_error_format'),
                                buttonText: 'OK',
                                onPress: () {
                                  Navigator.pop(context);
                                },
                              );
                            }));
                      }
                    }
                  },
                )),
                Container(
                  width: 16,
                ),
              ],
            ),
          ),
        ));
  }

  void guiNhanIDDrop(String loai, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListIdCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      if (value != null) {
        if (loai == defaultKhoa) {
          setState(() {
            textTenKhoa = value[0];
            StorePreferences.setIdDepartment(value[1]);
          });
        } else if (loai == defaultKhoaTuVan) {
          setState(() {
            textTenKhoa = value[0];
            StorePreferences.setIdDepartment(value[1]);
          });
        } else {
          setState(() {
            textTenPhong = value[0];
            idPhong = value[1];
          });
        }
      }
    });
  }

  void guiNhanDrop(String loai, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      if (value != null) {
        if (loai == defaultGioiTinh) {
          setState(() {
            textGioiTinh = value;
          });
        } else if (loai == defaultDanToc) {
          setState(() {
            textDanToc = value;
          });
        } else if (loai == defaultTinh) {
          setState(() {
            textTinhThanhPho = value;
          });
        } else if (loai == defaultHuyen) {
          setState(() {
            textQuanHuyen = value;
          });
        } else if (loai == defaultXa) {
          setState(() {
            textPhuongXa = value;
          });
        }
      }
    });
  }

  void _show() async {
    final DateTime? result = await showDatePicker(
      context: context,
      initialDate: dateTime,
      firstDate: DateTime(1600),
      lastDate: DateTime(2025),
      locale: const Locale('vi'),
    );
    if (result != null) {
      setState(() {
        dateTime = result;
        textNgaySinh = '${formatDate(dateTime, [dd, '-', mm, '-', yyyy])}';
      });
    }
  }

  void checkValidate() {
    if (textGioiTinh.isNotEmpty &&
        textDanToc.isNotEmpty &&
        textTinhThanhPho.isNotEmpty &&
        textQuanHuyen.isNotEmpty &&
        textPhuongXa.isNotEmpty &&
        textTen.text.isNotEmpty &&
        textEmail.text.isNotEmpty &&
        textTenPhong.isNotEmpty &&
        textSDT.text.isNotEmpty) {
      setState(() {
        validate = false;
      });
    } else {
      setState(() {
        validate = true;
      });
    }
  }

  bool isPhoneNumberValid(String phoneNumber) {
    // Biểu thức chính quy kiểm tra định dạng số điện thoại cơ bản
    final phoneRegExp = RegExp(
      r'^(\d{10,12})$',
    );
    return phoneRegExp.hasMatch(phoneNumber);
  }

  bool isEmailValid(String email) {
    // Biểu thức chính quy kiểm tra định dạng email
    final emailRegExp = RegExp(
      r'^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$',
    );
    return emailRegExp.hasMatch(email);
  }

  Future<void> addDoctor() async {
    try {
      UserCredential user = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(
              email: textEmail.text,
              password: '${formatDate(dateTime, [dd, mm, yyyy])}');
      // Đăng ký thành công
      String idUserRegistry = user.user!.uid;
      Doctor bacSiNew = Doctor(
          id: idUserRegistry,
          email: textEmail.text,
          idBenhVien: StorePreferences.getIdHospital(),
          idPhong: idPhong,
          idKhoa: StorePreferences.getIdDepartment(),
          role: widget.typeDoctor,
          ten: textTen.text,
          sdt: textSDT.text,
          ngaySinh: textNgaySinh,
          gioiTinh: textGioiTinh,
          danToc: textDanToc,
          tinhThanhPho: textTinhThanhPho,
          quanHuyen: textQuanHuyen,
          phuongXa: textPhuongXa,
          online: false,
          hoatDongCuoi: '',
          imgURL: '',
          pushToken: '',
          soNha: textSoNha.text);
      CollectionReference collectionReference =
          FirebaseFirestore.instance.collection('Doctors');
      DocumentReference documentReference =
          await collectionReference.doc(idUserRegistry);
      documentReference.set(bacSiNew.toMap());
      Hospital benhVien =
          await readIDHospital(StorePreferences.getIdHospital());
      List<Department> listKhoa = benhVien.khoa;
      // List<Room> listPhongTiem = benhVien.phongTiem;
      List<DepartmentAdvise> listKhoaTuVan = benhVien.khoaTuVan;
      if (widget.typeDoctor == key_role_doctor_examination) {
        Department khoa = Department.defaultContructor();
        for (Department e in listKhoa) {
          if (e.id == StorePreferences.getIdDepartment()) {
            khoa = e;
            break;
          }
        }
        List<Room> listPhongKham = khoa.phongKham;
        Room phongMoi =
            Room(id: idPhong, tenPhong: textTenPhong, idBacSi: idUserRegistry);
        for (int i = 0; i < listPhongKham.length; i++) {
          if (listPhongKham[i].id == idPhong) {
            listPhongKham[i] = phongMoi;
            break;
          }
        }
      } else if (widget.typeDoctor == key_role_doctor_vaccination) {
        // Room phongMoi =
        //     Room(id: idPhong, tenPhong: textTenPhong, idBacSi: idUserRegistry);
        // for (int i = 0; i < listPhongTiem.length; i++) {
        //   if (listPhongTiem[i].id == idPhong) {
        //     listPhongTiem[i] = phongMoi;
        //     break;
        //   }
        // }
      } else if (widget.typeDoctor == key_role_doctor_advise) {
        DepartmentAdvise khoaTuVan = DepartmentAdvise.defaultContructor();
        for (DepartmentAdvise e in listKhoaTuVan) {
          if (e.id == StorePreferences.getIdDepartment()) {
            khoaTuVan = e;
            break;
          }
        }
        List<Room> listPhongTuVan = khoaTuVan.phongTuVan;
        Room phongMoi =
            Room(id: idPhong, tenPhong: textTenPhong, idBacSi: idUserRegistry);
        for (int i = 0; i < listPhongTuVan.length; i++) {
          if (listPhongTuVan[i].id == idPhong) {
            listPhongTuVan[i] = phongMoi;
            break;
          }
        }
      }
      Hospital benhVienNew = Hospital(
          id: benhVien.id,
          ten: benhVien.ten,
          khoa: listKhoa,
          lichKham: benhVien.lichKham,
          // lichTuVan: benhVien.lichTuVan,
          // lichTiem: benhVien.lichTiem,
          // phongTiem: listPhongTiem,
          khoaTuVan: listKhoaTuVan);
      CollectionReference collection =
          FirebaseFirestore.instance.collection('Hospital');
      DocumentReference document =
          await collection.doc(StorePreferences.getIdHospital());
      await document.set(benhVienNew.toMap());
      ToastCommon.showToast(
          AppLocalizations.of(context)!.translate('toast_registry_success'));
      Navigator.pop(context);
    } catch (e) {
      showDialog(
        context: context,
        builder: (context) {
          return ErrorPopup(
            message: AppLocalizations.of(context)!.translate('error_add_file'),
            buttonText: 'OK',
            onPress: () {
              Navigator.pop(context);
            },
          );
        },
      );
    }
  }
}
