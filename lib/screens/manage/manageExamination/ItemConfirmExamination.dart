import 'package:flutter/material.dart';

import '../../../components/inputform/RowTextView.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../config/styles/Dimens.dart';
import '../../../config/styles/KeyValue.dart';
import '../../../item/ItemInformation.dart';
import '../../../untils/AppLocalizations.dart';

class ItemConfirmExamination extends StatelessWidget {
  final String trangThai;
  final String benhNhan;
  final String noiDungKham;
  final String thoiGian;
  const ItemConfirmExamination(
      {super.key,
      required this.trangThai,
      required this.benhNhan,
      required this.noiDungKham,
      required this.thoiGian});

  @override
  Widget build(BuildContext context) {
    Color? titleColor;
    if (trangThai == status_1) {
      titleColor = primaryColor;
    } else if (trangThai == status_2) {
      titleColor = backgroudSearch;
    } else if (trangThai == status_3) {
      titleColor = huyColor;
    } else if (trangThai == status_4) {
      titleColor = statusColor;
    } else if (trangThai == status_5) {
      titleColor = warningColor;
    }
    return ItemInformation(
      widgetBottom: Column(
        children: [
          RowTextView(
            title: AppLocalizations.of(context)!.translate('title_patient'),
            content: benhNhan,
            flexItemLeft: 2,
            flexItemRight: 4,
          ),
          RowTextView(
            title:
                AppLocalizations.of(context)!.translate('content_examination'),
            content: noiDungKham,
            flexItemLeft: 2,
            flexItemRight: 4,
          ),
          RowTextView(
            title: AppLocalizations.of(context)!
                .translate('select_day_book_examination'),
            content: thoiGian,
            flexItemLeft: 2,
            flexItemRight: 4,
            paddingBottom: defaultPaddingMin,
          ),
        ],
      ),
      widgetTop: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Text(
              trangThai,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
      titleColor: titleColor,
    );
  }
}
