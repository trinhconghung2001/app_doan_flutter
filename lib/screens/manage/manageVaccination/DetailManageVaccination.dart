import 'package:app_doan_flutter/screens/home/bookVaccine/data/Vaccination.dart';
import 'package:app_doan_flutter/screens/home/suggestVaccine/Vaccine.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/screens/manage/data/Doctor.dart';
import 'package:app_doan_flutter/screens/manage/data/Room.dart';
import 'package:app_doan_flutter/untils/FutureManageDoctor.dart';
import 'package:app_doan_flutter/untils/FutureRoom.dart';
import 'package:app_doan_flutter/untils/FutureVaccination.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/RowTextView.dart';
import '../../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../config/styles/KeyValue.dart';
import '../../../untils/AppLocalizations.dart';

// ignore: must_be_immutable
class DetailManageVaccination extends StatefulWidget {
  Vaccination lichTiem;
  FamilyMember benhNhan;
  DetailManageVaccination(
      {super.key, required this.lichTiem, required this.benhNhan});

  @override
  State<DetailManageVaccination> createState() =>
      _DetailManageVaccinationState();
}

class _DetailManageVaccinationState extends State<DetailManageVaccination> {
  Room phongTiem = Room.defaultContructor();
  Doctor bacSi = Doctor.defaultContructor();
  Vaccine goiVaccine = Vaccine.defaultContructor();
  late Future<void> _loadDetailVaccination;
  @override
  void initState() {
    _loadDetailVaccination = loadDetail();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: CustomAppBar(
          text: AppLocalizations.of(context)!
              .translate("app_bar_detail_vaccination"),
          check: false,
          onPress: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Scaffold(
          body: SingleChildScrollView(
              child: FutureBuilder(
        future: _loadDetailVaccination,
        builder: ((context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return CircularProgressIndicatorWidget();
          } else if (snapshot.hasError) {
            return EmptyData(
                msg: AppLocalizations.of(context)!.translate('error_data_msg'));
          } else {
            return Column(
              children: [
                Container(
                  decoration: BoxDecorationContainer(),
                  padding: const EdgeInsets.only(
                    top: 16,
                    left: 16,
                    right: 16,
                  ),
                  margin: const EdgeInsets.only(
                      top: 8, left: 16, right: 16, bottom: 0),
                  child: RowTextView(
                    title:
                        AppLocalizations.of(context)!.translate('title_status'),
                    content: widget.lichTiem.trangThai,
                    flexItemLeft: 2,
                    flexItemRight: 3,
                    color2: colorTrangThai(widget.lichTiem.trangThai),
                  ),
                ),
                Container(
                  decoration: BoxDecorationContainer(),
                  padding: const EdgeInsets.only(
                    top: 16,
                    left: 16,
                    right: 16,
                  ),
                  margin: const EdgeInsets.only(
                      top: 8, left: 16, right: 16, bottom: 0),
                  child: Column(
                    children: [
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('title_patient'),
                          content: widget.benhNhan.ten,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('category_title_doctor_vaccine'),
                          content: bacSi.ten,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('category_title_department_vaccine'),
                          content: phongTiem.tenPhong,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('select_day_vaccination'),
                          content: widget.lichTiem.ngayTiem,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('select_time_vaccination'),
                          content: widget.lichTiem.gioTiem,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('select_vaccination_package'),
                          content: goiVaccine.ten,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('content_note'),
                          content: widget.lichTiem.ghiChu,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('select_anamnesis'),
                          content: widget.lichTiem.tienSuBenh,
                          flexItemLeft: 2,
                          flexItemRight: 3),
                    ],
                  ),
                ),
              ],
            );
          }
        }),
      ))),
      bottomNavigationBar: BottomNavigator(
        widget: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              width: 16,
            ),
            Expanded(
                child: CustomButton(
              text: 'OK',
              iconDisplay: false,
              icon: Icons.edit_square,
              height: 32,
              press: () {
                Navigator.pop(context);
              },
            )),
            Container(
              width: 16,
            ),
          ],
        ),
      ),
    );
  }

  Color colorTrangThai(String trangThai) {
    if (trangThai == status_1) {
      return primaryColor;
    } else if (trangThai == status_2) {
      return backgroudSearch;
    } else if (trangThai == status_3) {
      return warningColor;
    }
    return textColor;
  }

  Future<void> loadDetail() async {
    phongTiem = await readIDPhongTiem(widget.lichTiem.idPhongTiem);
    goiVaccine = await readIDGoiVaccine(widget.lichTiem.idVaccine);
    bacSi = await readIDDoctor(phongTiem.idBacSi);
  }
}
