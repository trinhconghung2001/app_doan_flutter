class Doctor {
  String id;
  String idBenhVien;
  String idPhong;
  String idKhoa;
  String role;
  String ten;
  String email;
  String sdt;
  String ngaySinh;
  String gioiTinh;
  String danToc;
  String tinhThanhPho;
  String quanHuyen;
  String phuongXa;
  String soNha;
  String imgURL;
  final bool online;
  final String hoatDongCuoi;
  String pushToken;

  factory Doctor.defaultContructor() {
    return Doctor(
        id: '',
        idBenhVien: '',
        idPhong: '',
        idKhoa: '',
        role: '',
        ten: '',
        email: '',
        sdt: '',
        ngaySinh: '',
        gioiTinh: '',
        danToc: '',
        tinhThanhPho: '',
        quanHuyen: '',
        phuongXa: '',
        imgURL: '',
        online: false,
        hoatDongCuoi: '',
        pushToken: '',
        soNha: '');
  }

  Doctor({
    required this.id,
    required this.idBenhVien,
    required this.idPhong,
    required this.idKhoa,
    required this.role,
    required this.ten,
    required this.email,
    required this.sdt,
    required this.ngaySinh,
    required this.gioiTinh,
    required this.danToc,
    required this.tinhThanhPho,
    required this.quanHuyen,
    required this.phuongXa,
    required this.soNha,
    required this.imgURL,
    required this.online,
    required this.hoatDongCuoi,
    required this.pushToken,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'ten': ten,
      'email': email,
      'idPhong': idPhong,
      'idKhoa': idKhoa,
      'idBenhVien': idBenhVien,
      'role': role,
      'sdt': sdt,
      'ngaySinh': ngaySinh,
      'gioiTinh': gioiTinh,
      'danToc': danToc,
      'tinhThanhPho': tinhThanhPho,
      'quanHuyen': quanHuyen,
      'phuongXa': phuongXa,
      'soNha': soNha,
      'imgURL': imgURL,
      'online': online,
      'hoatDongCuoi': hoatDongCuoi,
      'pushToken': pushToken
    };
  }

  factory Doctor.fromMap(Map<String, dynamic> map) {
    return Doctor(
        id: map['id'],
        idBenhVien: map['idBenhVien'],
        idPhong: map['idPhong'],
        idKhoa: map['idKhoa'],
        role: map['role'],
        ten: map['ten'],
        email: map['email'],
        sdt: map['sdt'],
        ngaySinh: map['ngaySinh'],
        gioiTinh: map['gioiTinh'],
        danToc: map['danToc'],
        tinhThanhPho: map['tinhThanhPho'],
        quanHuyen: map['quanHuyen'],
        phuongXa: map['phuongXa'],
        soNha: map['soNha'],
        online: map['online'],
        hoatDongCuoi: map['hoatDongCuoi'],
        pushToken: map['pushToken'],
        imgURL: map['imgURL']);
  }
}
