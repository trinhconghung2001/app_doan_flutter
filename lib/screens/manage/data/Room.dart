class Room {
  final String id;
  final String tenPhong;
  String idBacSi;

  Room({required this.id, required this.tenPhong, required this.idBacSi});

  factory Room.defaultContructor() {
    return Room(id:'', tenPhong: '', idBacSi: '');
  }

  Map<String, dynamic> toMap() {
    return {'id': id, 'tenPhong': tenPhong, 'idBacSi': idBacSi};
  }

  factory Room.fromMap(Map<String, dynamic> map) {
    return Room(id:map['id'], tenPhong: map['tenPhong'], idBacSi: map['idBacSi']);
  }
}
