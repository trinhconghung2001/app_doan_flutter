import 'package:app_doan_flutter/screens/manage/data/Department.dart';
import 'package:app_doan_flutter/screens/manage/data/DepartmentAdvise.dart';

class Hospital {
  final String id;
  final String ten;
  final List<Department> khoa;
  // final List<Map<String,dynamic>> lichTiem;
  final List<Map<String, dynamic>> lichKham;
  // final List<Map<String,dynamic>> lichTuVan;
  // final List<Room> phongTiem;
  final List<DepartmentAdvise> khoaTuVan;

  Hospital({
    required this.id,
    required this.ten,
    required this.khoa,
    required this.lichKham,
    // required this.lichTuVan,
    // required this.lichTiem,
    // required this.phongTiem,
    required this.khoaTuVan,
  });

  factory Hospital.defaultContructor() {
    return Hospital(
        id: '',
        ten: '',
        khoa: [],
        lichKham: [],
        // lichTuVan: [],
        // lichTiem: [],
        // phongTiem: [],
        khoaTuVan: []);
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'ten': ten,
      'khoa': khoa.map((e) => e.toMap()).toList(),
      'lichKham': lichKham.map((e) => e).toList(),
      // 'lichTuVan': lichTuVan.map((e) => e).toList(),
      // 'lichTiem': lichTiem.map((e) => e).toList(),
      // 'phongTiem': phongTiem.map((e) => e.toMap()).toList(),
      'khoaTuVan': khoaTuVan.map((e) => e.toMap()).toList(),
    };
  }

  factory Hospital.fromMap(Map<String, dynamic> map) {
    final List<dynamic> itemKhoa = map['khoa'];
    // final List<dynamic> itemLichTiem = map['lichTiem'];
    final List<dynamic> itemLichKham = map['lichKham'];
    // final List<dynamic> itemLichTuVan = map['lichTuVan'];
    // final List<dynamic> itemPhongTiem = map['phongTiem'];
    final List<dynamic> itemKhoaTuVan = map['khoaTuVan'];
    return Hospital(
        id: map['id'],
        ten: map['ten'],
        khoa: itemKhoa.map((e) => Department.fromMap(e)).toList(),
        lichKham: itemLichKham
            .where((item) => item is Map<String, dynamic>)
            .map((item) => item as Map<String, dynamic>)
            .toList(),
        // lichTuVan: itemLichTuVan.where((item) => item is Map<String, dynamic>).map((item) => item as Map<String, dynamic>).toList(),
        // lichTiem: itemLichTiem.where((item) => item is Map<String, dynamic>).map((item) => item as Map<String, dynamic>).toList(),
        // phongTiem: itemPhongTiem.map((e) => Room.fromMap(e)).toList(),
        khoaTuVan:
            itemKhoaTuVan.map((e) => DepartmentAdvise.fromMap(e)).toList());
  }
}
