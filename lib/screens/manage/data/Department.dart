import 'package:app_doan_flutter/screens/manage/data/Room.dart';
class Department {
  final String id;
  final List<Room> phongKham;
  final String ten;

  factory Department.defaultContructor() {
    return Department(id: '', phongKham: [], ten: '');
  }
  Department({
    required this.id,
    required this.phongKham,
    required this.ten,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'phongKham': phongKham.map((e) => e.toMap()).toList(),
      'ten': ten
    };
  }

  factory Department.fromMap(Map<String, dynamic> map) {
    final List<dynamic> memberList = map['phongKham'];
    final List<Room> members =
        memberList.map(((e) => Room.fromMap(e))).toList();
    return Department(
        id: map['id'], phongKham: members, ten: map['ten']);
  }
}
