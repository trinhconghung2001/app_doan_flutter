import 'package:app_doan_flutter/screens/manage/data/Room.dart';

class DepartmentAdvise {
  final String id;
  final List<Room> phongTuVan;
  final String ten;
  final String moTa;

  factory DepartmentAdvise.defaultContructor() {
    return DepartmentAdvise(id: '', phongTuVan: [], ten: '',moTa: '');
  }
  DepartmentAdvise({
    required this.id,
    required this.phongTuVan,
    required this.ten,
    required this.moTa
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'phongTuVan': phongTuVan.map((e) => e.toMap()).toList(),
      'ten': ten,
      'moTa': moTa
    };
  }

  factory DepartmentAdvise.fromMap(Map<String, dynamic> map) {
    final List<dynamic> memberList = map['phongTuVan'];
    final List<Room> members =
        memberList.map(((e) => Room.fromMap(e))).toList();
    return DepartmentAdvise(
        id: map['id'], phongTuVan: members, ten: map['ten'],moTa: map['moTa']);
  }
}
