import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Body extends StatelessWidget {
  final SvgPicture svgPicture;
  final String title;
  final VoidCallback onClick;
  const Body(
      {super.key,
      required this.svgPicture,
      required this.title,
      required this.onClick});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onClick,
      child: Column(
        children: [
          Container(width: 50, height: 50, child: svgPicture),
          Container(
            // width: 60,
            // height: 50,
            child: Align(
              alignment: Alignment.topCenter,
              child: Text(
                title,
                style: const TextStyle(fontSize: 14, color: textColor),
                textAlign: TextAlign.center,
              ),
            ),
          )
        ],
      ),
    );
  }
}
