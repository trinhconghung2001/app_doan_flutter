import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/category/ListIdCategory.dart';
import 'package:app_doan_flutter/screens/home/suggestVaccine/ItemSugggesVaccine.dart';
import 'package:app_doan_flutter/screens/home/suggestVaccine/Vaccine.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/screens/login/data/User.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/filtersearch/RowFilter.dart';
import '../../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureUser.dart';
import '../../../untils/StorePreferences.dart';
import 'DetailSuggestVaccine.dart';

class SuggestVaccine extends StatefulWidget {
  const SuggestVaccine({super.key});

  @override
  State<SuggestVaccine> createState() => _SuggestVaccineState();
}

class _SuggestVaccineState extends State<SuggestVaccine> {
  int tuoi = 0;
  late Future<void> _loadDataVaccine;
  List<Vaccine> listVaccine = [];
  String textQH = '';
  String idNguoiThan = '';

  @override
  void initState() {
    // _loadDataVaccine = loadAllData();
    _loadDataVaccine = loadAge(StorePreferences.getIdUser());
    textQH = "Bản thân";
    idNguoiThan = StorePreferences.getIdUser();
    super.initState();
  }

  void loadScreen(String id) {
    setState(() {
      _loadDataVaccine = loadAge(id);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: AppLocalizations.of(context)!
                .translate('app_bar_suggest_vaccin'),
            check: false,
            onPress: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: ListView.builder(
            itemCount: 2,
            itemBuilder: (BuildContext context, index) {
              if (index == 0) {
                return Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecorationContainer(color: filterColor),
                    margin: const EdgeInsets.only(top: 8, left: 16, right: 16),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 16),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: <Widget>[
                            Expanded(
                                flex: 2,
                                child: RowFilter(
                                  title: AppLocalizations.of(context)!
                                      .translate("account_title_relationship"),
                                  value: textQH,
                                  icon: Icons.arrow_drop_down,
                                  onPress: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                ListIdCategory(
                                                  loai: defaultHoSo,
                                                  titleAppbar: AppLocalizations
                                                          .of(context)!
                                                      .translate(
                                                          'category_title_relationship'),
                                                ))).then((value) {
                                      setState(() {
                                        if (value != null) {
                                          idNguoiThan = value[1];
                                          textQH = value[0];
                                        }
                                      });
                                    });
                                  },
                                )),
                            const SizedBox(
                              width: 8,
                            ),
                            Expanded(
                                flex: 2,
                                child: Column(
                                  children: [
                                    const Padding(
                                      padding: EdgeInsets.only(
                                        left: 8,
                                        right: 8,
                                      ),
                                      child: Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          '',
                                          style: TextStyle(
                                              fontSize: 12, color: textColor),
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 4,
                                    ),
                                    CustomButton(
                                        text: AppLocalizations.of(context)!
                                            .translate('button_search'),
                                        icon: Icons.search,
                                        press: () {
                                          loadScreen(idNguoiThan);
                                        },
                                        height: 36),
                                  ],
                                )),
                          ],
                        ),
                      ],
                    ));
              } else {
                return FutureBuilder(
                    future: _loadDataVaccine,
                    builder: ((context, AsyncSnapshot snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return CircularProgressIndicatorWidget();
                      } else if (snapshot.hasError) {
                        return EmptyData(
                            msg: AppLocalizations.of(context)!
                                .translate('error_data_msg'));
                      } else {
                        if (listVaccine.length > 0) {
                          return Container(
                              margin: const EdgeInsets.only(top: 8),
                              child: ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: listVaccine.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetailSuggestVaccine(
                                                        idSend:
                                                            listVaccine[index]
                                                                .id,
                                                        idNguoiDat:
                                                            idNguoiThan)));
                                      },
                                      child: ItemSugggesVaccine(
                                          ten: listVaccine[index].ten,
                                          nguonGoc: listVaccine[index].nguonGoc,
                                          phongBenh:
                                              listVaccine[index].phongBenh,
                                          duongTiem:
                                              listVaccine[index].duongTiem));
                                },
                              ));
                        } else {
                          // Không có dữ liệu
                          return EmptyData();
                        }
                      }
                    }));
              }
            }));
  }

  Future<void> loadAge(String id) async {
    if (id == StorePreferences.getIdUser()) {
      Users banThan = await readBanThan(id);
      DateTime ngaySinh = DateFormat('dd-MM-yyyy').parse(banThan.ngaySinh);
      DateTime ngayHienTai = DateTime.now();
      //tinh su khac biet giữa ngày hiện tại và ngày sinh
      Duration difference = ngayHienTai.difference(ngaySinh);
      int tuoi = (difference.inDays / 365).floor();
      String gioiTinh = banThan.gioiTinh;
      listVaccine = [];
      listVaccine = await loadData(tuoi, gioiTinh);
    } else {
      List<FamilyMember> listNguoiThan =
          await readListNguoiThan(StorePreferences.getIdUser());
      for (int i = 0; i < listNguoiThan.length; i++) {
        if (listNguoiThan[i].id == id) {
          FamilyMember nguoiDat = listNguoiThan[i];
          DateTime ngaySinh = DateFormat('dd-MM-yyyy').parse(nguoiDat.ngaySinh);
          DateTime ngayHienTai = DateTime.now();
          //tinh su khac biet giữa ngày hiện tại và ngày sinh
          Duration difference = ngayHienTai.difference(ngaySinh);
          int tuoi = (difference.inDays / 365).floor();
          print(tuoi);
          listVaccine = [];
          listVaccine = await loadData(tuoi, nguoiDat.gioiTinh);
          break;
        }
      }
    }
  }

  Future<List<Vaccine>> loadData(int tuoi, String gioiTinh) async {
    print('Vao loadData');
    DocumentReference documentReference = FirebaseFirestore.instance
        .collection("Categorys")
        .doc('8nDu9e5HN5HkdyhmO4Lz');
    DocumentSnapshot documentSnapshot = await documentReference.get();
    if (documentSnapshot.exists) {
      Map<String, dynamic> dataCategory =
          documentSnapshot.data() as Map<String, dynamic>;
      List<Map<String, dynamic>> result =
          List<Map<String, dynamic>>.from(dataCategory['goiVaccine'] as List);
      List<Vaccine> list = [];
      listVaccine = [];
      list = result.map((e) => Vaccine.fromMap(e)).toList();
      if (tuoi >= 18 && tuoi <= 70 && gioiTinh == 'Nam') {
        list.forEach((element) {
          List<dynamic> doiTuong = element.doiTuong;
          doiTuong.forEach((e) {
            if (e == key_adults) {
              listVaccine.add(element);
            }
          });
        });
        return listVaccine;
      } else if (tuoi >= 18 && tuoi <= 70 && gioiTinh == 'Nữ') {
        list.forEach((element) {
          List<dynamic> doiTuong = element.doiTuong;
          for (var e in doiTuong) {
            if (e == key_adults || e == key_woment_1 || e == key_woment_2) {
              listVaccine.add(element);
              break;
            }
          }
        });
        return listVaccine;
      } else if (tuoi > 70) {
        list.forEach((element) {
          List<dynamic> doiTuong = element.doiTuong;
          for (var e in doiTuong) {
            if (e == key_old_person || e == key_old_person_2) {
              listVaccine.add(element);
              break;
            }
          }
        });
        return listVaccine;
      }
    }
    return [];
  }
}
