import 'package:app_doan_flutter/untils/AppLocalizations.dart';
import 'package:flutter/material.dart';

import '../../../components/inputform/RowTextView.dart';
import '../../../config/styles/Dimens.dart';
import '../../../item/ItemInformation.dart';

class ItemSugggesVaccine extends StatelessWidget {
  final String ten;
  final String nguonGoc;
  final String phongBenh;
  final String duongTiem;
  const ItemSugggesVaccine({super.key, required this.ten, required this.nguonGoc, required this.phongBenh, required this.duongTiem});

  @override
  Widget build(BuildContext context) {
    return ItemInformation(
      widgetBottom: Column(
        children: [
          RowTextView(
            title: AppLocalizations.of(context)!.translate('category_the_origin'),
            content: nguonGoc,
            flexItemLeft: 2,
            flexItemRight: 4,
          ),
          RowTextView(
            title: AppLocalizations.of(context)!.translate('category_disease_prevention'),
            content: phongBenh,
            flexItemLeft: 2,
            flexItemRight: 4,
          ),
          RowTextView(
            title: AppLocalizations.of(context)!.translate('category_injection_router'),
            content: duongTiem,
            flexItemLeft: 2,
            flexItemRight: 4,
            paddingBottom: defaultPaddingMin,
          ),
        ],
      ),
      widgetTop: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Text(
              ten,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }
}
