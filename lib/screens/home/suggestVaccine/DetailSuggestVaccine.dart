import 'package:app_doan_flutter/screens/home/bookVaccine/BookVaccine.dart';
import 'package:app_doan_flutter/screens/home/suggestVaccine/Vaccine.dart';
import 'package:app_doan_flutter/untils/FutureVaccination.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/RowTextView.dart';
import '../../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../untils/AppLocalizations.dart';

class DetailSuggestVaccine extends StatefulWidget {
  final String idSend;
  final String idNguoiDat;
  const DetailSuggestVaccine(
      {super.key, required this.idSend, required this.idNguoiDat});

  @override
  State<DetailSuggestVaccine> createState() => _DetailSuggestVaccineState();
}

class _DetailSuggestVaccineState extends State<DetailSuggestVaccine> {
  Vaccine detailVaccine = Vaccine.defaultContructor();
  late Future<void> _loadDetailVaccine;
  String doiTuong = '';
  @override
  void initState() {
    _loadDetailVaccine = loadDetail();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: AppLocalizations.of(context)!
                .translate("app_bar_detail_vaccine"),
            check: false,
            onPress: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Scaffold(
          body: SingleChildScrollView(
              child: FutureBuilder(
            future: _loadDetailVaccine,
            builder: (context, AsyncSnapshot snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return CircularProgressIndicatorWidget();
              } else if (snapshot.hasError) {
                return EmptyData(
                    msg: AppLocalizations.of(context)!
                        .translate('error_data_msg'));
              } else {
                return Container(
                  decoration: BoxDecorationContainer(),
                  padding: const EdgeInsets.only(
                    top: 16,
                    left: 16,
                    right: 16,
                  ),
                  margin: const EdgeInsets.only(
                      top: 8, left: 16, right: 16, bottom: 0),
                  child: Column(
                    children: [
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('category_name_vaccine'),
                          content: detailVaccine.ten,
                          flexItemLeft: 2,
                          flexItemRight: 4),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('category_the_origin'),
                          content: detailVaccine.nguonGoc,
                          flexItemLeft: 2,
                          flexItemRight: 4),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('category_disease_prevention'),
                          content: detailVaccine.phongBenh,
                          flexItemLeft: 2,
                          flexItemRight: 4),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('category_injection_router'),
                          content: detailVaccine.duongTiem,
                          flexItemLeft: 2,
                          flexItemRight: 4),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('category_number_injection'),
                          content: detailVaccine.soMui,
                          flexItemLeft: 2,
                          flexItemRight: 4),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('category_person_vaccine'),
                          content: doiTuong,
                          flexItemLeft: 2,
                          flexItemRight: 4),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('category_contraindicated'),
                          content: detailVaccine.chongChiDinh,
                          flexItemLeft: 2,
                          flexItemRight: 4),
                      RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('category_side_effects'),
                          content: detailVaccine.tacDungPhu,
                          flexItemLeft: 2,
                          flexItemRight: 4),
                    ],
                  ),
                );
              }
            },
          )),
          bottomNavigationBar: BottomNavigator(
              widget: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                width: 16,
              ),
              Expanded(
                  child: CustomButton(
                text: AppLocalizations.of(context)!.translate("button_cancel"),
                height: 32,
                icon: Icons.cancel_outlined,
                iconColor: primaryColor,
                textColor: primaryColor,
                background: backgroundScreenColor,
                press: () {
                  Navigator.pop(context);
                },
              )),
              Container(
                width: 8,
              ),
              Expanded(
                  child: CustomButton(
                text: AppLocalizations.of(context)!.translate("button_book"),
                icon: Icons.edit_square,
                height: 32,
                press: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => BookVaccine(
                                vaccine: detailVaccine,
                                idNguoiDat: widget.idNguoiDat,
                              )));
                },
              )),
              Container(
                width: 16,
              ),
            ],
          )),
        ));
  }

  Future<void> loadDetail() async {
    detailVaccine = await readIDGoiVaccine(widget.idSend);
    for (int i = 0; i < detailVaccine.doiTuong.length; i++) {
      doiTuong += detailVaccine.doiTuong[i];
      if (i < detailVaccine.doiTuong.length - 1) {
        doiTuong += ', ';
      }
    }
  }
}
