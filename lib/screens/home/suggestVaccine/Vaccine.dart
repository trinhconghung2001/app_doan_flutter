class Vaccine {
  final String chongChiDinh;
  final List<dynamic> doiTuong;
  final String duongTiem;
  final String id;
  final String nguonGoc;
  final String phongBenh;
  final String soMui;
  final String tacDungPhu;
  final String ten;

  Vaccine({
    required this.chongChiDinh,
    required this.doiTuong,
    required this.duongTiem,
    required this.id,
    required this.nguonGoc,
    required this.phongBenh,
    required this.soMui,
    required this.tacDungPhu,
    required this.ten,
  });

  factory Vaccine.defaultContructor() {
    return Vaccine(
        chongChiDinh: '',
        doiTuong: [],
        duongTiem: '',
        id: '',
        nguonGoc: '',
        phongBenh: '',
        soMui: '',
        tacDungPhu: '',
        ten: '');
  }

  Map<String, dynamic> toMap() {
    return {
      'chongChiDinh': chongChiDinh,
      'doiTuong': doiTuong,
      'duongTiem': duongTiem,
      'id': id,
      'nguonGoc': nguonGoc,
      'phongBenh': phongBenh,
      'soMui': soMui,
      'tacDungPhu': tacDungPhu,
      'ten': ten
    };
  }

  factory Vaccine.fromMap(Map<String, dynamic> map) {
    return Vaccine(
        chongChiDinh: map['chongChiDinh'],
        doiTuong: map['doiTuong'].map((e) => e.toString()).toList(),
        duongTiem: map['duongTiem'],
        id: map['id'],
        nguonGoc: map['nguonGoc'],
        phongBenh: map['phongBenh'],
        soMui: map['soMui'],
        tacDungPhu: map['tacDungPhu'],
        ten: map['ten']);
  }
}
