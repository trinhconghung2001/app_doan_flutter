import 'package:app_doan_flutter/screens/manage/data/Doctor.dart';
import 'package:app_doan_flutter/screens/manage/data/Room.dart';
import 'package:app_doan_flutter/untils/FutureManageDoctor.dart';
import 'package:app_doan_flutter/untils/FutureRoom.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/InputText.dart';
import '../../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../untils/AppLocalizations.dart';
import 'ChatScreen.dart';
import 'components/ItemDoctorAdvise.dart';

class SelectDoctorAdvise extends StatefulWidget {
  final String idKhoa;
  const SelectDoctorAdvise({super.key, required this.idKhoa});

  @override
  State<SelectDoctorAdvise> createState() => _SelectDoctorAdviseState();
}

class _SelectDoctorAdviseState extends State<SelectDoctorAdvise> {
  late Future<void> _loadDataDoctorAdvise;
  List<Doctor> listBacSi = [];
  @override
  void initState() {
    _loadDataDoctorAdvise = loadData();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDataDoctorAdvise = loadData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: AppLocalizations.of(context)!
                  .translate('category_title_doctor_advise'),
              check: false,
              checkIconBack: false,
              onPress: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: ListView.builder(
              itemCount: 2,
              itemBuilder: (BuildContext context, index) {
                if (index == 0) {
                  return ContainerSearch(
                    dataSearch: searchData,
                  );
                } else {
                  return FutureBuilder(
                    future: _loadDataDoctorAdvise,
                    builder: (context, AsyncSnapshot snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return CircularProgressIndicatorWidget();
                      } else if (snapshot.hasError) {
                        return EmptyData(
                            msg: AppLocalizations.of(context)!
                                .translate('error_data_msg'));
                      } else {
                        if (listBacSi.length > 0) {
                          return Container(
                              margin: const EdgeInsets.only(top: 8),
                              child: ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: listBacSi.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    ChatScreen(
                                                      doctor: listBacSi[index],
                                                    ))).then(
                                            (value) => loadScreen());
                                      },
                                      child: ItemDoctorAdvise(
                                          urlImg: listBacSi[index].imgURL,
                                          hoTen: listBacSi[index].ten,
                                          idSend: StorePreferences.getIdUser(),
                                          idReceive: listBacSi[index].id,
                                          online:
                                              listBacSi[index].online == true));
                                },
                              ));
                        } else {
                          // Không có dữ liệu
                          return EmptyData();
                        }
                      }
                    },
                  );
                }
              }),
        ));
  }

  Future<void> loadData() async {
    List<Room> listPhong = await readAllRoomAdvise_ByIdHospital_ByIdKhoa(
        widget.idKhoa, StorePreferences.getIdHospital());
    listBacSi = [];
    for (Room i in listPhong) {
      if (i.idBacSi.isNotEmpty) {
        Doctor doctor = await readIDDoctor(i.idBacSi);
        listBacSi.add(doctor);
      }
    }
  }

  void searchData(String text) {
    setState(() {
      _loadDataDoctorAdvise = loadDataSearch(text);
    });
  }

  Future<void> loadDataSearch(String value) async {
    List<Room> listPhong = await readAllRoomAdvise_ByIdHospital_ByIdKhoa(
        widget.idKhoa, StorePreferences.getIdHospital());
    List<Doctor> listDoctor = [];
    for (Room i in listPhong) {
      if (i.idBacSi.isNotEmpty) {
        Doctor doctor = await readIDDoctor(i.idBacSi);
        listDoctor.add(doctor);
      }
    }
    listBacSi = listDoctor
        .where((element) =>
            element.ten.toLowerCase().contains(value.trim().toLowerCase()))
        .toList();
  }
}

// ignore: must_be_immutable
class ContainerSearch extends StatefulWidget {
  final void Function(String text) dataSearch;
  ContainerSearch({
    super.key,
    required this.dataSearch,
  });

  @override
  State<ContainerSearch> createState() => _ContainerSearchState();
}

class _ContainerSearchState extends State<ContainerSearch> {
  String textSearch = '';

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecorationContainer(color: filterColor),
        margin: const EdgeInsets.only(
          top: 8,
          left: 16,
          right: 16,
        ),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InputText(
              hintText: AppLocalizations.of(context)!
                  .translate('hint_filter_search_doctor'),
              labelText: AppLocalizations.of(context)!
                  .translate('splash_select_doctor'),
              // textController: textSearch,
              // focusNode: _focusNode,
              onChanged: ((p0) {
                setState(() {
                  textSearch = p0;
                });
                widget.dataSearch(textSearch);
              }),
            )
          ],
        ));
  }
}
