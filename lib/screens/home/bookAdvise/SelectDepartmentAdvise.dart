import 'package:app_doan_flutter/components/filtersearch/RowFilter.dart';
import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/components/loading/EmptyData.dart';
import 'package:app_doan_flutter/screens/category/ListIdCategory.dart';
import 'package:app_doan_flutter/screens/home/bookAdvise/SelectDoctorAdvise.dart';
import 'package:app_doan_flutter/screens/home/bookAdvise/components/ItemDepartmentAdvise.dart';
import 'package:app_doan_flutter/screens/manage/data/DepartmentAdvise.dart';
import 'package:app_doan_flutter/untils/FutureDepartment.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/container/Decoration.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../config/styles/KeyValue.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/StorePreferences.dart';

class SelectDepartmentAdvise extends StatefulWidget {
  const SelectDepartmentAdvise({super.key});

  @override
  State<SelectDepartmentAdvise> createState() => _SelectDepartmentAdviseState();
}

class _SelectDepartmentAdviseState extends State<SelectDepartmentAdvise> {
  late Future<void> _loadDataAdvise;
  List<DepartmentAdvise> listKhoa = [];
  String textBenhVien = '';
  @override
  void initState() {
    _loadDataAdvise = loadData();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDataAdvise = loadDataSearch();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: AppLocalizations.of(context)!
                .translate('filter_search_select_hospital'),
            check: false,
            checkIconBack: false,
            onPress: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: ListView.builder(
            itemCount: 2,
            itemBuilder: (BuildContext context, index) {
              if (index == 0) {
                return FilterTuVan(
                  dataSearch: searchData,
                  textBenhVien: textBenhVien,
                );
              } else {
                return FutureBuilder(
                  future: _loadDataAdvise,
                  builder: (context, AsyncSnapshot snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return CircularProgressIndicatorWidget();
                    } else if (snapshot.hasError) {
                      return EmptyData(
                          msg: AppLocalizations.of(context)!
                              .translate('error_data_msg'));
                    } else {
                      if (listKhoa.length > 0) {
                        return Container(
                            margin: const EdgeInsets.only(top: 8),
                            child: ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: listKhoa.length,
                              itemBuilder: (BuildContext context, int index) {
                                return GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      SelectDoctorAdvise(
                                                          idKhoa:
                                                              listKhoa[index]
                                                                  .id)))
                                          .then((value) => loadScreen());
                                    },
                                    child: ItemDepartmentAdvise(
                                        ten: listKhoa[index].ten,
                                        moTa: listKhoa[index].moTa,
                                        svgPicture: SvgPicture.asset(
                                            'assets/images/logo_app.svg')));
                              },
                            ));
                      } else {
                        // Không có dữ liệu
                        return EmptyData();
                      }
                    }
                  },
                );
              }
            }),
      ),
    );
  }

  void searchData(String text) {
    setState(() {
      _loadDataAdvise = loadDataSearch();
      textBenhVien = text;
    });
  }

  Future<void> loadDataSearch() async {
    listKhoa = await readAllDepartmentAdvise_ByIdHospital(
        StorePreferences.getIdHospital());
  }

  Future<void> loadData() async {
    listKhoa = [];
  }
}

// ignore: must_be_immutable
class FilterTuVan extends StatefulWidget {
  final void Function(String list) dataSearch;
  String textBenhVien;
  FilterTuVan({
    super.key,
    required this.dataSearch,
    required this.textBenhVien,
  });

  @override
  State<FilterTuVan> createState() => _FilterTuVanState();
}

class _FilterTuVanState extends State<FilterTuVan> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecorationContainer(color: filterColor),
        margin: const EdgeInsets.only(top: 8, left: 16, right: 16),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RowFilter(
              title: AppLocalizations.of(context)!
                  .translate('filter_search_select_hospital'),
              value: widget.textBenhVien,
              icon: Icons.arrow_drop_down,
              onPress: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ListIdCategory(
                              loai: defaultBenhVien,
                              titleAppbar: AppLocalizations.of(context)!
                                  .translate('category_title_hospital'),
                            ))).then((value) {
                  if (value != null) {
                    setState(() {
                      widget.textBenhVien = value[0];
                      StorePreferences.setIdHospital(value[1]);
                    });
                    widget.dataSearch(widget.textBenhVien);
                  }
                });
              },
            ),
          ],
        ));
  }
}
