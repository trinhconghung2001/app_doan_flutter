class Message {
  final String senderId;
  final String read;
  final String receiverId;
  final String message;
  final String sent;
  final Type type;

  Message(
      {required this.senderId,
      required this.read,
      required this.receiverId,
      required this.message,
      required this.sent,
      required this.type});

  Map<String, dynamic> toMap() {
    return {
      'senderId': senderId,
      'read': read,
      'receiverId': receiverId,
      'message': message,
      'sent': sent,
      'type': type.name
    };
  }

  factory Message.fromMap(Map<String, dynamic> map) {
    return Message(
        senderId: map['senderId'],
        read: map['read'],
        receiverId: map['receiverId'],
        message: map['message'],
        type: map['type'] == Type.image.name ? Type.image : Type.text,
        sent: map['sent']);
  }
}

enum Type { text, image }
