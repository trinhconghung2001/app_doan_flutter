import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/screens/home/bookAdvise/data/Message.dart';
import 'package:app_doan_flutter/untils/FutureMessage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class MessageCard extends StatefulWidget {
  final Message message;
  final String idSend;
  const MessageCard({super.key, required this.message, required this.idSend});

  @override
  State<MessageCard> createState() => _MessageCardState();
}

class _MessageCardState extends State<MessageCard> {
  @override
  Widget build(BuildContext context) {
    return widget.idSend == widget.message.senderId
        ? _sentMessage()
        : _receiverMessage();
  }

  Widget _receiverMessage() {
    if (widget.message.read.isEmpty) {
      updateMessageReadStatus(widget.message);
    }
    return Row(
      children: [
        Flexible(
          child: Container(
              padding: EdgeInsets.all(widget.message.type == Type.text
                  ? MediaQuery.of(context).size.width * 0.03
                  : MediaQuery.of(context).size.width * 0.02),
              decoration: BoxDecoration(
                  color: buttonColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                      bottomRight: Radius.circular(30))),
              margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.04,
                  vertical: MediaQuery.of(context).size.height * 0.01),
              child: widget.message.type == Type.text
                  ? Text(
                      widget.message.message,
                      style: TextStyle(color: Colors.white),
                    )
                  : ClipRRect(
                      borderRadius: BorderRadius.circular(15),
                      child: CachedNetworkImage(
                        imageUrl: widget.message.message,
                        placeholder: (context, url) =>
                            CircularProgressIndicatorWidget(),
                        errorWidget: (context, url, error) =>
                            const CircleAvatar(
                          child: Icon(
                            Icons.image,
                            size: 70,
                          ),
                        ),
                      ),
                    )),
        ),
        Text(
          getConvertTime(context, widget.message.sent),
          style: TextStyle(fontSize: 12, color: Colors.white),
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.03,
        ),
      ],
    );
  }

  Widget _sentMessage() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Row(
          children: [
            Container(
              width: MediaQuery.of(context).size.width * 0.03,
            ),
            Visibility(
              visible: widget.message.read.isNotEmpty,
              child: Icon(
                Icons.done_all_outlined,
                color: buttonColor,
                size: 16,
              ),
            ),
            const SizedBox(
              width: 2,
            ),
            Visibility(
              visible: widget.message.read.isNotEmpty,
              child: Text(
                getConvertTime(
                  context,
                  widget.message.read,
                ),
                style: TextStyle(fontSize: 12, color: Colors.white),
              ),
            ),
            Visibility(
              visible: widget.message.read.isEmpty,
              child: Text(
                getConvertTime(
                  context,
                  widget.message.sent,
                ),
                style: TextStyle(fontSize: 12, color: Colors.white),
              ),
            ),
          ],
        ),
        Flexible(
          child: Container(
              padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.03),
              decoration: BoxDecoration(
                  color: backgroudSearch,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                      bottomLeft: Radius.circular(30))),
              margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.04,
                  vertical: MediaQuery.of(context).size.height * 0.01),
              child: widget.message.type == Type.text
                  ? Text(
                      widget.message.message,
                      style: TextStyle(color: Colors.white),
                    )
                  : ClipRRect(
                      borderRadius: BorderRadius.circular(15),
                      child: CachedNetworkImage(
                        imageUrl: widget.message.message,
                        placeholder: (context, url) =>
                            CircularProgressIndicatorWidget(),
                        errorWidget: (context, url, error) =>
                            const CircleAvatar(
                          child: Icon(
                            Icons.image,
                            size: 70,
                          ),
                        ),
                      ),
                    )),
        ),
      ],
    );
  }
}
