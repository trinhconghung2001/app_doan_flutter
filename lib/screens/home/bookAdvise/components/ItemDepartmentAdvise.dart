import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../config/styles/CustomColor.dart';

class ItemDepartmentAdvise extends StatelessWidget {
  final String ten;
  final String moTa;
  final SvgPicture svgPicture;
  const ItemDepartmentAdvise(
      {super.key,
      required this.ten,
      required this.moTa,
      required this.svgPicture});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 1,
            blurRadius: 1,
            offset: const Offset(1, 1), // changes position of shadow
          ),
        ],
      ),
      padding: const EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 8),
      margin: const EdgeInsets.only(top: 8, left: 16, right: 16),
      child: Row(
        children: [
          Expanded(
            child: Container(
              // color: Colors.green,
              alignment: Alignment.center,
              child: svgPicture,
            ),
            flex: 2,
          ),
          Expanded(
            flex: 5,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 4,
                ),
                Text(
                  ten,
                  style: const TextStyle(
                      color: textColor,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 4,
                ),
                Text(
                  moTa,
                  style: const TextStyle(
                    color: textColor,
                    fontSize: 14,
                  ),
                ),
                const SizedBox(
                  height: 4,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
