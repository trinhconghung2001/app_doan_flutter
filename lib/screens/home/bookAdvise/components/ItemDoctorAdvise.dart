import 'package:app_doan_flutter/screens/home/bookAdvise/data/Message.dart';
import 'package:app_doan_flutter/untils/FutureMessage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../../../config/styles/CustomColor.dart';

class ItemDoctorAdvise extends StatelessWidget {
  final String hoTen;
  final String urlImg;
  final bool online;
  final String idSend;
  final String idReceive;
  const ItemDoctorAdvise(
      {super.key,
      required this.hoTen,
      required this.urlImg,
      required this.online,
      required this.idSend,
      required this.idReceive});

  @override
  Widget build(BuildContext context) {
    Message? _message;
    return StreamBuilder(
      stream: readLastAllMessage(idSend, idReceive),
      builder: (context, snapshot) {
        final data = snapshot.data?.docs;
        if (data != null && data[0].exists) {
          Message message = Message.fromMap(data[0].data());
          if (message.senderId == idReceive) {
            _message = message;
          } else {
            if (data[1].exists) {
              Message message = Message.fromMap(data[1].data());
              if (message.receiverId == idSend) {
                _message = message;
              }
            }
          }
        }

        return Container(
          // height: 100,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 1,
                offset: const Offset(1, 1), // changes position of shadow
              ),
            ],
          ),
          padding: const EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 8),
          margin: const EdgeInsets.only(top: 8, bottom: 0, left: 16, right: 16),
          child: Row(
            children: [
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  child: ClipOval(
                    child: CachedNetworkImage(
                      width: MediaQuery.of(context).size.height * 0.05,
                      height: MediaQuery.of(context).size.height * 0.05,
                      imageUrl: urlImg,
                      fit: BoxFit.fill,
                      placeholder: (context, url) =>
                          CircularProgressIndicatorWidget(),
                      errorWidget: (context, url, error) => CircleAvatar(
                        child: Image.asset(
                          'assets/images/account.jpg',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
                flex: 2,
              ),
              const SizedBox(
                width: 8,
              ),
              Expanded(
                flex: 5,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      hoTen,
                      style: const TextStyle(
                          color: textColor,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Text(
                      _message != null ? _message!.message : '',
                      maxLines: 1,
                      style: const TextStyle(
                        color: textColor,
                        fontSize: 14,
                      ),
                    ),
                  ],
                ),
              ),
              online == true
                  ? Container(
                      width: 15,
                      height: 15,
                      decoration: BoxDecoration(
                        color: Colors.greenAccent.shade400,
                        borderRadius: BorderRadius.circular(100),
                      ),
                    )
                  : Container(
                      width: 15,
                      height: 15,
                      decoration: BoxDecoration(
                        color: Colors.grey.shade400,
                        borderRadius: BorderRadius.circular(100),
                      ),
                    ),
            ],
          ),
        );
      },
    );
  }
}
