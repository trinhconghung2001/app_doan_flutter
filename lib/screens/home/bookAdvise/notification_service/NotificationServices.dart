import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationServices {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  final FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  Future<void> requestNotificationPermissions() async {
    if (Platform.isIOS) {
      await _firebaseMessaging.requestPermission(
          alert: true,
          announcement: true,
          badge: true,
          carPlay: true,
          criticalAlert: true,
          provisional: true,
          sound: true);
    }
    NotificationSettings notificationSettings =
        await _firebaseMessaging.requestPermission(
            alert: true,
            announcement: true,
            badge: true,
            carPlay: true,
            criticalAlert: true,
            provisional: true,
            sound: true);
    if (notificationSettings.authorizationStatus ==
        AuthorizationStatus.authorized) {
      print('Cấp quyền');
    } else if (notificationSettings.authorizationStatus ==
        AuthorizationStatus.provisional) {
      print('Cấp quyền tạm thời');
    } else {
      print("Từ chối cấp quyền");
    }
  }

  Future forgroundMessages() async {
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
            alert: true, badge: true, sound: true);
  }

  void firebaseInt(BuildContext context) async {
    FirebaseMessaging.onMessage.listen((message) {
      RemoteNotification? notification = message.notification;
      print("Notification title: ${notification!.title}");
      print("Notification title: ${notification.body}");
      print("Data: ${message.data.toString()}");
      if (Platform.isIOS) {
        forgroundMessages();
      }
      if (Platform.isAndroid) {
        // if (message.data['to'] == currentUserToken) {
        print("Gionggggggggggggggg");
        initLocalNotifications(context, message);
        showNotification(message);
        // }
      }
    });
  }

  void initLocalNotifications(
      BuildContext context, RemoteMessage message) async {
    var androidInitSettings =
        const AndroidInitializationSettings('@mipmap/ic_launcher');
    var iosInitSettings = const DarwinInitializationSettings();
    var initSettings = InitializationSettings(
        android: androidInitSettings, iOS: iosInitSettings);
    await _flutterLocalNotificationsPlugin.initialize(
      initSettings,
      onDidReceiveNotificationResponse: (payload) {
        handleMessage(context, message);
      },
    );
  }

  void handleMessage(BuildContext context, RemoteMessage message) {
    // if (message.data['type'] == 'text') {
    // Navigator.push(context, MaterialPageRoute(builder: (context) => ChatScreen(doctor: doctor)))
    // }
  }

  Future<void> showNotification(RemoteMessage message) async {
    AndroidNotificationChannel androidNotificationChannel =
        AndroidNotificationChannel(
            // message.notification!.android!.channelId.toString(),
            // message.notification!.android!.channelId.toString(),
            'channedID1',
            'Channel 1',
            importance: Importance.max,
            showBadge: true,
            playSound: true);
    AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails(
            // androidNotificationChannel.id.toString(),
            // androidNotificationChannel.name.toString(),
            'channedID2',
            'Channel 2',
            channelDescription: 'Flutter Notifications',
            importance: Importance.max,
            priority: Priority.high,
            playSound: true,
            ticker: 'ticker',
            sound: androidNotificationChannel.sound);
    const DarwinNotificationDetails darwinNotificationDetails =
        DarwinNotificationDetails(
            presentAlert: true, presentBadge: true, presentSound: true);

    NotificationDetails notificationDetails = NotificationDetails(
        android: androidNotificationDetails, iOS: darwinNotificationDetails);
    // Tạo một notificationId duy nhất cho mỗi thông báo

    Future.delayed(Duration.zero, () {
      _flutterLocalNotificationsPlugin.show(
          0,
          message.notification!.title.toString(),
          message.notification!.body.toString(),
          notificationDetails);
    });
  }
}
