import 'dart:io';

import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/screens/home/bookAdvise/data/Message.dart';
import 'package:app_doan_flutter/untils/FutureMessage.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../../config/styles/Dimens.dart';
import '../../../untils/AppLocalizations.dart';
import '../../manage/data/Doctor.dart';
import 'components/MessageCard.dart';

class ChatScreen extends StatefulWidget {
  final Doctor doctor;
  const ChatScreen({super.key, required this.doctor});

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  TextEditingController textMessage = TextEditingController();
  List<Message> listMess = [];
  //cờ ẩn hiện emoji
  bool _showEmoij = false;
  //cờ loading ảnh mới
  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: _appBar(),
        ),
        body: Column(
          children: [
            Expanded(
                child: Container(
              color: filterColor,
              child: StreamBuilder(
                stream: readAllMessage(
                    StorePreferences.getIdUser(), widget.doctor.id),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return CircularProgressIndicatorWidget();
                  } else if (snapshot.hasError) {
                    return Center(
                      child: Text(
                        '${AppLocalizations.of(context)!.translate('message_hello')} 👋🏻',
                        style: TextStyle(color: statusColor),
                      ),
                    );
                  } else {
                    final data = snapshot.data?.docs;
                    listMess =
                        data?.map((e) => Message.fromMap(e.data())).toList() ??
                            [];
                    if (listMess.length > 0) {
                      return ListView.builder(
                        reverse: true,
                        itemCount: listMess.length,
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.width * 0.01),
                        physics: const BouncingScrollPhysics(),
                        itemBuilder: ((context, index) {
                          return MessageCard(
                            message: listMess[index],
                            idSend: StorePreferences.getIdUser(),
                          );
                        }),
                      );
                    } else {
                      return Center(
                        child: Text(
                          '${AppLocalizations.of(context)!.translate('message_hello')} 👋🏻',
                          style: TextStyle(color: statusColor),
                        ),
                      );
                    }
                  }
                },
              ),
            )),
            Visibility(
              visible: _loading,
              child: Container(
                color: filterColor,
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                    child: CircularProgressIndicator(),
                  ),
                ),
              ),
            ),
            Container(
              child: chatInput(),
              margin: EdgeInsets.only(bottom: 8),
            ),
            Visibility(
              visible: _showEmoij == true,
              child: SizedBox(
                height: MediaQuery.of(context).size.height * 0.35,
                child: EmojiPicker(
                  textEditingController: textMessage,
                  config: Config(
                      columns: 7,
                      emojiSizeMax: 32 * (Platform.isAndroid ? 1.3 : 1.0)),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _appBar() {
    return AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: SizedBox(
            width: 50,
            height: 50,
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
              // size: 16,
            ),
          ),
        ),
        title: StreamBuilder(
          stream: readDoctorInfo(widget.doctor.id),
          builder: (context, snapshot) {
            final data = snapshot.data?.docs;
            final list =
                data?.map((e) => Doctor.fromMap(e.data())).toList() ?? [];
            return Row(
              children: [
                ClipOval(
                  child: CachedNetworkImage(
                    width: MediaQuery.of(context).size.height * 0.05,
                    height: MediaQuery.of(context).size.height * 0.05,
                    fit: BoxFit.fill,
                    imageUrl:
                        list.isNotEmpty ? list[0].imgURL : widget.doctor.imgURL,
                    placeholder: (context, url) =>
                        CircularProgressIndicatorWidget(),
                    errorWidget: (context, url, error) => CircleAvatar(
                      child: Image.asset(
                        'assets/images/account.jpg',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      list.isNotEmpty ? list[0].ten : widget.doctor.ten,
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                          fontWeight: FontWeight.w500),
                    ),
                    Text(
                      list.isNotEmpty
                          ? list[0].online
                              ? 'Online'
                              : convertTimeOnline(context, list[0].hoatDongCuoi)
                          : convertTimeOnline(
                              context, widget.doctor.hoatDongCuoi),
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.white,
                      ),
                    ),
                  ],
                )
              ],
            );
          },
        ));
  }

  Widget chatInput() {
    return Row(
      children: [
        Expanded(
          child: Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            child: Row(
              children: [
                IconButton(
                    onPressed: () {
                      setState(() {
                        _showEmoij = !_showEmoij;
                      });
                    },
                    icon: Icon(
                      Icons.emoji_emotions,
                      color: primaryColor,
                    )),
                Expanded(
                  flex: 8,
                  child: TextFormField(
                      maxLines: null,
                      controller: textMessage,
                      keyboardType: TextInputType.multiline,
                      style: const TextStyle(
                        color: textColor,
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                      ),
                      onChanged: (value) {
                        if (_showEmoij) {
                          setState(() {
                            _showEmoij = !_showEmoij;
                          });
                        }
                      },
                      // controller: textController,
                      // focusNode: focusNode,
                      // onTap: onTap,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.only(
                            left: defaultTextInsideBoxPadding, right: 0),
                        fillColor: Colors.white,
                        hintText: AppLocalizations.of(context)!.translate('hint_enter_message'),
                        border: InputBorder.none,
                      ),
                      cursorColor: primaryColor,
                      autovalidateMode: AutovalidateMode.onUserInteraction),
                ),
                IconButton(
                    onPressed: () {
                      selectPictureGallery();
                    },
                    icon: Icon(
                      Icons.image,
                      color: primaryColor,
                    )),
                IconButton(
                    onPressed: () {
                      selectPictureCamera();
                    },
                    icon: Icon(
                      Icons.camera_alt_rounded,
                      color: primaryColor,
                    )),
              ],
            ),
          ),
        ),
        //button send message
        MaterialButton(
          onPressed: () {
            if (textMessage.text.trim().isNotEmpty) {
              sendMessage(StorePreferences.getIdUser(), widget.doctor.id,
                      textMessage.text, Type.text)
                  .then((value) {
                textMessage.text = '';
              });
            }
          },
          minWidth: 0,
          padding:
              const EdgeInsets.only(top: 10, bottom: 10, right: 5, left: 10),
          shape: CircleBorder(),
          color: backgroudSearch,
          child: Icon(
            Icons.send,
            color: Colors.white,
          ),
        )
      ],
    );
  }

  Future<void> selectPictureGallery() async {
    final ImagePicker picker = ImagePicker();
    final List<XFile>? images = await picker.pickMultiImage();
    if (images != null) {
      for (var i in images) {
        print('Image path: ${i.path} -- Mimetype: ${i.mimeType}');
        setState(() {
          _loading = true;
        });
        await sendPicture(
                StorePreferences.getIdUser(), widget.doctor.id, File(i.path))
            .then((value) {
          setState(() {
            _loading = false;
          });
        });
      }
    }
  }

  Future<void> selectPictureCamera() async {
    final ImagePicker picker = ImagePicker();
    final XFile? image = await picker.pickImage(source: ImageSource.camera);
    if (image != null) {
      print('Image path: ${image.path} -- Mimetype: ${image.mimeType}');
      await sendPicture(
          StorePreferences.getIdUser(), widget.doctor.id, File(image.path));
    }
  }
}
