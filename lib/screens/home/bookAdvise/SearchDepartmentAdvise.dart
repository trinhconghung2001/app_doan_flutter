import 'package:app_doan_flutter/components/inputform/InputText.dart';
import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/components/loading/EmptyData.dart';
import 'package:app_doan_flutter/screens/home/bookAdvise/SelectDoctorAdvise.dart';
import 'package:app_doan_flutter/screens/home/bookAdvise/components/ItemDepartmentAdvise.dart';
import 'package:app_doan_flutter/screens/manage/data/DepartmentAdvise.dart';
import 'package:app_doan_flutter/screens/manage/data/Hospital.dart';
import 'package:app_doan_flutter/untils/FutureDepartment.dart';
import 'package:app_doan_flutter/untils/FutureHospital.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/container/Decoration.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../untils/AppLocalizations.dart';

class SearchDepartmentAdvise extends StatefulWidget {
  const SearchDepartmentAdvise({super.key});

  @override
  State<SearchDepartmentAdvise> createState() => _SearchDepartmentAdviseState();
}

class _SearchDepartmentAdviseState extends State<SearchDepartmentAdvise> {
  late Future<void> _loadDataAdvise;
  List<List<DepartmentAdvise>> listKhoa = [];
  List<List<String>> benhVien = [];

  @override
  void initState() {
    _loadDataAdvise = loadData();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDataAdvise = loadData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: AppLocalizations.of(context)!
                .translate('app_bar_select_department'),
            check: false,
            checkIconBack: false,
            onPress: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: ListView.builder(
            itemCount: 2,
            itemBuilder: (BuildContext context, index) {
              if (index == 0) {
                return ContainerSearch(
                  dataSearch: searchData,
                );
              } else {
                return FutureBuilder(
                  future: _loadDataAdvise,
                  builder: (context, AsyncSnapshot snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return CircularProgressIndicatorWidget();
                    } else if (snapshot.hasError) {
                      return EmptyData(
                          msg: AppLocalizations.of(context)!
                              .translate('error_data_msg'));
                    } else {
                      if (listKhoa.length > 0) {
                        List<Widget> listWidget = [];
                        for (int i = 0; i < listKhoa.length; i++) {
                          listWidget.add(Column(
                            children: [
                              Container(
                                width: double.infinity,
                                padding: EdgeInsets.only(left: 16, top: 16),
                                child: Text(
                                  benhVien[i][1],
                                  style: const TextStyle(
                                    color: primaryColor,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Visibility(
                                visible: listKhoa[i].length > 0,
                                child: ListView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: listKhoa[i].length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return GestureDetector(
                                        onTap: () {
                                          StorePreferences.setIdHospital(
                                              benhVien[i][0]);
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      SelectDoctorAdvise(
                                                          idKhoa: listKhoa[i]
                                                                  [index]
                                                              .id))).then(
                                              (value) => loadScreen());
                                        },
                                        child: ItemDepartmentAdvise(
                                            ten: listKhoa[i][index].ten,
                                            moTa: listKhoa[i][index].moTa,
                                            svgPicture: SvgPicture.asset(
                                                'assets/images/logo_app.svg')));
                                  },
                                ),
                              ),
                              Visibility(
                                child: EmptyData(),
                                visible: listKhoa[i].length == 0,
                              )
                            ],
                          ));
                        }
                        return Column(
                          children: listWidget,
                        );
                      } else {
                        // Không có dữ liệu
                        return EmptyData();
                      }
                    }
                  },
                );
              }
            }),
      ),
    );
  }

  void searchData(String text) {
    setState(() {
      _loadDataAdvise = loadDataSearch(text);
    });
  }

  Future<void> loadDataSearch(String value) async {
    List<Hospital> listHospital = await readAllHospital();
    for (int i = 0; i < listHospital.length; i++) {
      List<DepartmentAdvise> list =
          await readAllDepartmentAdvise_ByIdHospital(listHospital[i].id);
      listKhoa[i] = list
          .where((element) =>
              element.ten.toLowerCase().contains(value.trim().toLowerCase()))
          .toList();
    }
  }

  Future<void> loadData() async {
    listKhoa = [];
    benhVien = [];
    List<Hospital> listHospital = await readAllHospital();
    for (int i = 0; i < listHospital.length; i++) {
      List<String> e = [];
      e.add(listHospital[i].id);
      e.add(listHospital[i].ten);
      benhVien.add(e);
    }
    for (int i = 0; i < listHospital.length; i++) {
      List<DepartmentAdvise> list =
          await readAllDepartmentAdvise_ByIdHospital(listHospital[i].id);
      listKhoa.add(list);
    }
  }
}

// ignore: must_be_immutable
class ContainerSearch extends StatefulWidget {
  final void Function(String text) dataSearch;
  ContainerSearch({
    super.key,
    required this.dataSearch,
  });

  @override
  State<ContainerSearch> createState() => _ContainerSearchState();
}

class _ContainerSearchState extends State<ContainerSearch> {
  String textSearch = '';

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecorationContainer(color: filterColor),
        margin: const EdgeInsets.only(
          top: 8,
          left: 16,
          right: 16,
        ),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InputText(
              hintText: AppLocalizations.of(context)!.translate('hint_filter_search_department'),
              labelText: AppLocalizations.of(context)!.translate('filter_search_department'),
              // textController: textSearch,
              // focusNode: _focusNode,
              onChanged: ((p0) {
                setState(() {
                  textSearch = p0;
                });
                widget.dataSearch(textSearch);
              }),
            )
          ],
        ));
  }
}
