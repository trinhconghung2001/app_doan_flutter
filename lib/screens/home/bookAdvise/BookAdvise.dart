// import 'package:app_doan_flutter/components/bottomnavigator/BottomNavigator.dart';
// import 'package:app_doan_flutter/config/styles/CustomColor.dart';
// import 'package:app_doan_flutter/config/styles/KeyValue.dart';
// import 'package:app_doan_flutter/screens/home/bookAdvise/data/Advise.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:date_format/date_format.dart';
// import 'package:flutter/material.dart';

// import '../../../components/appbar/CustomAppBar.dart';
// import '../../../components/button/CustomButton.dart';
// import '../../../components/container/Decoration.dart';
// import '../../../components/inputform/InputDropDown.dart';
// import '../../../components/inputform/InputTextField.dart';
// import '../../../components/toast/ToastCommon.dart';
// import '../../../untils/AppLocalizations.dart';
// import '../../../untils/StorePreferences.dart';
// import '../../category/ListAddCategory.dart';
// import '../../category/ListCategory.dart';
// import '../../login/data/FamilyMember.dart';

// class BookAdvise extends StatefulWidget {
//   const BookAdvise({super.key});

//   @override
//   State<BookAdvise> createState() => _BookAdviseState();
// }

// class _BookAdviseState extends State<BookAdvise> {
//   final FocusNode _focusNoiDungTuVan = FocusNode();
//   final FocusNode _focusBenhManTinh = FocusNode();
//   String textHoSo = '';
//   String textBenhVien = '';
//   String textNgayTuVan = '';
//   String textGioTuVan = '';
//   String textTrieuChung = '';
//   FamilyMember nguoi = FamilyMember.defaultContructor();
//   List<String> listTrieuChung = [];

//   bool validate = false;
//   DateTime dateTime = DateTime.now();
//   final TextEditingController textNoiDungTuVan = TextEditingController();
//   final TextEditingController textBenhManTinh = TextEditingController();

//   @override
//   void initState() {
//     readData();
//     for (int i = 0; i < listTrieuChung.length; i++) {
//       textTrieuChung += listTrieuChung[i];
//       if (i < listTrieuChung.length - 1) {
//         textTrieuChung += ', ';
//       }
//     }
//     super.initState();
//     _focusNoiDungTuVan.addListener(_onFocusChange);
//     _focusBenhManTinh.addListener(_onFocusChange);
//   }

//   void _onFocusChange() {
//     setState(() {});
//   }

//   @override
//   void dispose() {
//     _focusNoiDungTuVan.dispose();
//     _focusBenhManTinh.dispose();

//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () {
//         FocusManager.instance.primaryFocus?.unfocus();
//       },
//       child: Scaffold(
//           appBar: PreferredSize(
//             preferredSize: Size.fromHeight(50),
//             child: CustomAppBar(
//               text: AppLocalizations.of(context)!
//                   .translate("app_bar_add_registry_advise"),
//               check: false,
//               onPress: () {
//                 Navigator.pop(context);
//               },
//             ),
//           ),
//           body: Scaffold(
//             body: SingleChildScrollView(
//               child: Container(
//                 decoration: BoxDecorationContainer(),
//                 padding: const EdgeInsets.only(
//                     top: 16, left: 16, right: 16, bottom: 16),
//                 margin: const EdgeInsets.only(
//                     top: 8, left: 16, right: 16, bottom: 0),
//                 child: Column(
//                   children: [
//                     InputDropDown(
//                         onPress: () {
//                           guiNhanDrop(
//                               defaultHoSo,
//                               AppLocalizations.of(context)!
//                                   .translate('category_title_file'));
//                         },
//                         value: textHoSo,
//                         title: AppLocalizations.of(context)!
//                             .translate("select_file"),
//                         checkRangBuoc: true,
//                         icon: Icons.arrow_drop_down,
//                         isError: validate && textHoSo.isEmpty),
//                     const SizedBox(
//                       height: 16,
//                     ),
//                     InputDropDown(
//                         onPress: () {
//                           guiNhanDrop(
//                               defaultBenhVien,
//                               AppLocalizations.of(context)!
//                                   .translate('category_title_hospital'));
//                         },
//                         value: textBenhVien,
//                         title: AppLocalizations.of(context)!
//                             .translate("select_hospital"),
//                         checkRangBuoc: true,
//                         icon: Icons.arrow_drop_down,
//                         isError: validate && textBenhVien.isEmpty),
//                     const SizedBox(
//                       height: 16,
//                     ),
//                     InputDropDown(
//                         onPress: () {
//                           _show();
//                         },
//                         value: textNgayTuVan,
//                         title: AppLocalizations.of(context)!
//                             .translate("select_day_advise"),
//                         checkRangBuoc: true,
//                         icon: Icons.calendar_month,
//                         isError: validate && textNgayTuVan.isEmpty),
//                     const SizedBox(
//                       height: 16,
//                     ),
//                     InputDropDown(
//                         onPress: () {
//                           guiNhanDrop(
//                               defaultGioTuVan,
//                               AppLocalizations.of(context)!
//                                   .translate('category_title_time_advise'));
//                         },
//                         value: textGioTuVan,
//                         title: AppLocalizations.of(context)!
//                             .translate("select_time_advise"),
//                         checkRangBuoc: true,
//                         icon: Icons.arrow_drop_down,
//                         isError: validate && textGioTuVan.isEmpty),
//                     const SizedBox(
//                       height: 16,
//                     ),
//                     InputTextField(
//                       title: AppLocalizations.of(context)!
//                           .translate("content_advise"),
//                       checkRangBuoc: true,
//                       focusNode: _focusNoiDungTuVan,
//                       textController: textNoiDungTuVan,
//                       onPress: () {
//                         setState(() {
//                           textNoiDungTuVan.clear();
//                         });
//                       },
//                       isError: validate && textNoiDungTuVan.text.isEmpty,
//                     ),
//                     const SizedBox(
//                       height: 16,
//                     ),
//                     InputDropDown(
//                         onPress: () {
//                           guiTrieuChung(
//                               listTrieuChung,
//                               AppLocalizations.of(context)!
//                                   .translate('category_title_symptom'));
//                         },
//                         value: textTrieuChung,
//                         title: AppLocalizations.of(context)!
//                             .translate("select_symptom"),
//                         checkRangBuoc: true,
//                         icon: Icons.arrow_drop_down,
//                         isError: validate && textTrieuChung.isEmpty),
//                     const SizedBox(
//                       height: 16,
//                     ),
//                     InputTextField(
//                       title: AppLocalizations.of(context)!
//                           .translate("select_chronic_diseases"),
//                       checkRangBuoc: true,
//                       focusNode: _focusBenhManTinh,
//                       textController: textBenhManTinh,
//                       onPress: () {
//                         setState(() {
//                           textBenhManTinh.clear();
//                         });
//                       },
//                       isError: validate && textBenhManTinh.text.isEmpty,
//                     ),
//                   ],
//                 ),
//               ),
//             ),
//             bottomNavigationBar: BottomNavigator(
//               widget: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                 children: [
//                   Container(
//                     width: 16,
//                   ),
//                   Expanded(
//                       child: CustomButton(
//                     text: AppLocalizations.of(context)!
//                         .translate("button_cancel"),
//                     height: 32,
//                     icon: Icons.cancel_outlined,
//                     iconColor: primaryColor,
//                     textColor: primaryColor,
//                     background: backgroundScreenColor,
//                     press: () {
//                       Navigator.pop(context);
//                     },
//                   )),
//                   Container(
//                     width: 8,
//                   ),
//                   Expanded(
//                       child: CustomButton(
//                     text:
//                         AppLocalizations.of(context)!.translate("button_book"),
//                     icon: Icons.save,
//                     height: 32,
//                     press: () {
//                       checkValidate();
//                       if (validate == false) {
//                         addAdvise();
//                       }
//                     },
//                   )),
//                   Container(
//                     width: 16,
//                   ),
//                 ],
//               ),
//             ),
//           )),
//     );
//   }

//   void guiNhanDrop(String loai, String titleAppbar) {
//     Navigator.push(
//         context,
//         MaterialPageRoute(
//             builder: (context) => ListCategory(
//                   loai: loai,
//                   titleAppbar: titleAppbar,
//                 ))).then((value) {
//       if (value != null) {
//         if (loai == defaultHoSo) {
//           setState(() {
//             textHoSo = value;
//           });
//           if (value == 'Bản thân') {
//             readBanThan(value);
//           } else {
//             readNguoiThan(value);
//           }
//         } else if (loai == defaultBenhVien) {
//           setState(() {
//             textBenhVien = value;
//           });
//         } else if (loai == defaultGioTuVan) {
//           setState(() {
//             textGioTuVan = value;
//           });
//         } else if (loai == defaultTrieuChung) {
//           setState(() {
//             textTrieuChung = value;
//           });
//         }
//       }
//     });
//   }

//   void _show() async {
//     final DateTime? result = await showDatePicker(
//       context: context,
//       initialDate: dateTime,
//       firstDate: DateTime(2019),
//       lastDate: DateTime(2024),
//       locale: const Locale('vi'),
//     );
//     if (result != null) {
//       setState(() {
//         dateTime = result;
//         textNgayTuVan = '${formatDate(dateTime, [dd, '/', mm, '/', yyyy])}';
//       });
//     }
//   }

//   void checkValidate() {
//     if (textHoSo.isNotEmpty &&
//         textBenhVien.isNotEmpty &&
//         textNgayTuVan.isNotEmpty &&
//         textGioTuVan.isNotEmpty &&
//         textBenhManTinh.text.isNotEmpty &&
//         textNoiDungTuVan.text.isNotEmpty &&
//         textTrieuChung.isNotEmpty) {
//       setState(() {
//         validate = false;
//       });
//     } else {
//       setState(() {
//         validate = true;
//       });
//     }
//   }

//   void guiTrieuChung(List<String> list, String titleAppbar) {
//     Navigator.push(
//         context,
//         MaterialPageRoute(
//             builder: (context) => ListAddCategory(
//                   type: defaultTrieuChung,
//                   list: list,
//                   titleAppbar: titleAppbar,
//                 ))).then((value) {
//       String textTrieuChungChon = '';
//       for (int i = 0; i < value.length; i++) {
//         textTrieuChungChon += value[i];
//         if (i < value.length - 1) {
//           textTrieuChungChon += ', ';
//         }
//       }
//       setState(() {
//         listTrieuChung = value;
//         textTrieuChung = textTrieuChungChon;
//       });
//     });
//   }

//   Future<void> readData() async {
//     DocumentReference documentReference = FirebaseFirestore.instance
//         .collection('Categorys')
//         .doc('8nDu9e5HN5HkdyhmO4Lz');
//     DocumentSnapshot documentSnapshot = await documentReference.get();

//     if (documentSnapshot.exists) {
//       Map<String, dynamic> dataCategory =
//           documentSnapshot.data() as Map<String, dynamic>;
//       StorePreferences.setListTrieuChung(
//           List<String>.from(dataCategory['trieuChung'] as List));
//     } else {
//       StorePreferences.setListTrieuChung([]);
//     }
//   }

//   Future<void> readBanThan(String value) async {
//     CollectionReference collectionReference =
//         FirebaseFirestore.instance.collection('Users');
//     DocumentSnapshot documentSnapshot =
//         await collectionReference.doc(StorePreferences.getIdUser()).get();
//     if (documentSnapshot.exists) {
//       //lay du lieu
//       Map<String, dynamic> userData =
//           documentSnapshot.data() as Map<String, dynamic>;
//       FamilyMember banThan = FamilyMember(
//           id: userData['id'],
//           idUser: StorePreferences.getIdUser(),
//           moiQH: userData['moiQH'],
//           ten: userData['ten'],
//           sdt: userData['sdt'],
//           soTheBHYT: userData['soTheBHYT'],
//           ngaySinh: userData['ngaySinh'],
//           ngheNghiep: userData['ngheNghiep'],
//           gioiTinh: userData['gioiTinh'],
//           danToc: userData['danToc'],
//           tinhThanhPho: userData['tinhThanhPho'],
//           quanHuyen: userData['quanHuyen'],
//           phuongXa: userData['phuongXa'],
//           soNha: userData['soNha']);
//       nguoi = banThan;
//     } else {
//       nguoi = FamilyMember.defaultContructor();
//     }
//   }

//   Future<void> readNguoiThan(String value) async {
//     CollectionReference collectionReference =
//         FirebaseFirestore.instance.collection('Users');
//     DocumentSnapshot documentSnapshot =
//         await collectionReference.doc(StorePreferences.getIdUser()).get();
//     if (documentSnapshot.exists) {
//       //lay du lieu
//       Map<String, dynamic> userData =
//           documentSnapshot.data() as Map<String, dynamic>;
//       //convert array data -> List
//       List<Map<String, dynamic>> listHoSo =
//           List<Map<String, dynamic>>.from(userData['listNguoiThan'] as List);
//       //reset lai list nguoi than
//       List<FamilyMember> listNguoiThan = [];
//       for (int i = 0; i < listHoSo.length; i++) {
//         listNguoiThan.add(FamilyMember.fromMap(listHoSo[i]));
//       }
//       for (FamilyMember i in listNguoiThan) {
//         if (i.moiQH == value) {
//           nguoi = i;
//           break;
//         }
//       }
//     } else {
//       nguoi = FamilyMember.defaultContructor();
//     }
//   }

//   Future<void> addAdvise() async {
//     CollectionReference collectionReference =
//         FirebaseFirestore.instance.collection('RegistrationSchedule');
//     List<Map<String, dynamic>> listAdvise = [];
//     Advise advise = Advise(
//         id: DateTime.now().toString(),
//         trangThai: status_1,
//         hoSo: nguoi,
//         phongTuVan: '',
//         bacSi: '',
//         ngayTuVan: textNgayTuVan,
//         gioTuVan: textGioTuVan,
//         noiDungTuVan: textNoiDungTuVan.text,
//         benhManTinh: textBenhManTinh.text,
//         benhVien: textBenhVien,
//         trieuChung: listTrieuChung);
//     listAdvise.add(advise.toMap());
//     await collectionReference.doc(StorePreferences.getIdUser()).set(
//         {'advise': FieldValue.arrayUnion(listAdvise)},
//         SetOptions(merge: true)).then((value) {
//       ToastCommon.showToast(
//           AppLocalizations.of(context)!.translate('toast_book_success'));
//       Navigator.pop(context);
//     }).onError((error, stackTrace) {
//       ToastCommon.showToast(
//           AppLocalizations.of(context)!.translate('toast_book_faild'));
//     });
//   }
// }
