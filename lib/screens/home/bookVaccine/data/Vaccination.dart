class Vaccination {
  final String id;
  final String idUser;
  final String trangThai;
  final String idBenhNhan;
  final String idPhongTiem;
  final String idBenhVien;
  final String gioTiem;
  final String ngayTiem;
  final String idVaccine;
  final String ghiChu;
  final String tienSuBenh;

  Vaccination({
    required this.id,
    required this.idUser,
    required this.trangThai,
    required this.idBenhNhan,
    required this.idBenhVien,
    required this.idPhongTiem,
    required this.gioTiem,
    required this.ngayTiem,
    required this.idVaccine,
    required this.ghiChu,
    required this.tienSuBenh,
  });

  factory Vaccination.defaultContructor() {
    return Vaccination(
        id: '',
        idUser: '',
        trangThai: '',
        idBenhNhan: '',
        idBenhVien: '',
        idPhongTiem: '',
        gioTiem: '',
        ngayTiem: '',
        idVaccine:'',
        ghiChu: '',
        tienSuBenh: '');
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'idUser': idUser,
      'trangThai': trangThai,
      'idBenhNhan': idBenhNhan,
      'idBenhVien': idBenhVien,
      'idPhongTiem': idPhongTiem,
      'gioTiem': gioTiem,
      'ngayTiem': ngayTiem,
      'idVaccine': idVaccine,
      'ghiChu': ghiChu,
      'tienSuBenh': tienSuBenh
    };
  }

  factory Vaccination.fromMap(Map<String, dynamic> map) {
    return Vaccination(
        id: map['id'],
        idUser: map['idUser'],
        trangThai: map['trangThai'],
        idBenhNhan: map['idBenhNhan'],
        idBenhVien: map['idBenhVien'],
        idPhongTiem: map['idPhongTiem'],
        gioTiem: map['gioTiem'],
        ngayTiem: map['ngayTiem'],
        idVaccine: map['idVaccine'],
        ghiChu: map['ghiChu'],
        tienSuBenh: map['tienSuBenh']);
  }
}
