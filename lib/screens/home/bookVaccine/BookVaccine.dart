import 'package:app_doan_flutter/components/bottomnavigator/BottomNavigator.dart';
import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/components/loading/EmptyData.dart';
import 'package:app_doan_flutter/components/toast/ToastCommon.dart';
import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/category/ListIdCategory.dart';
import 'package:app_doan_flutter/screens/home/bookVaccine/data/Vaccination.dart';
import 'package:app_doan_flutter/screens/home/suggestVaccine/Vaccine.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/untils/FutureVaccination.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/InputDropDown.dart';
import '../../../components/inputform/InputTextField.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureUser.dart';
import '../../../untils/StorePreferences.dart';
import '../../category/ListCategory.dart';

class BookVaccine extends StatefulWidget {
  final String idNguoiDat;
  final Vaccine vaccine;
  const BookVaccine({
    super.key,
    required this.idNguoiDat,
    required this.vaccine,
  });

  @override
  State<BookVaccine> createState() => _BookVaccineState();
}

class _BookVaccineState extends State<BookVaccine> {
  final FocusNode _focusGhiChu = FocusNode();
  final FocusNode _focusTienSuBenh = FocusNode();
  String textHoSo = '';
  String textBenhVien = '';
  String textPhongTiem = '';
  String textNgayTiem = '';
  String textGioTiem = '';
  String textGoiVaccine = '';

  String idPhongTiem = '';
  String idBenhVien = '';

  bool validate = false;
  bool checkNguoi = false;
  DateTime dateTime = DateTime.now();
  final TextEditingController textGhiChu = TextEditingController();
  final TextEditingController textTienSuBenh = TextEditingController();
  late Future<void> _futureDataBook;

  @override
  void initState() {
    _futureDataBook = loadDataEdit();
    // textHoSo = widget.nguoiDat.ten;
    super.initState();
    _focusGhiChu.addListener(_onFocusChange);
    _focusTienSuBenh.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    setState(() {});
  }

  @override
  void dispose() {
    _focusGhiChu.dispose();
    _focusTienSuBenh.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: AppLocalizations.of(context)!
                  .translate("app_bar_vaccination"),
              check: false,
              onPress: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: Scaffold(
            body: SingleChildScrollView(
              child: FutureBuilder(
                future: _futureDataBook,
                builder: ((context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return CircularProgressIndicatorWidget();
                  } else if (snapshot.hasError) {
                    return EmptyData();
                  } else {
                    return Container(
                      decoration: BoxDecorationContainer(),
                      padding: const EdgeInsets.only(
                          top: 16, left: 16, right: 16, bottom: 16),
                      margin: const EdgeInsets.only(
                          top: 8, left: 16, right: 16, bottom: 0),
                      child: Column(
                        children: [
                          InputDropDown(
                              value: textHoSo,
                              title: AppLocalizations.of(context)!
                                  .translate("select_file"),
                              checkRangBuoc: true,
                              offEdit: true,
                              showIcon: false,
                              icon: Icons.arrow_drop_down,
                              isError: validate && textHoSo.isEmpty),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                              value: textGoiVaccine,
                              title: AppLocalizations.of(context)!
                                  .translate("select_vaccination_package"),
                              checkRangBuoc: true,
                              offEdit: true,
                              showIcon: false,
                              icon: Icons.arrow_drop_down,
                              isError: validate && textGoiVaccine.isEmpty),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                              onPress: () {
                                guiNhanIDDrop(
                                    defaultBenhVien,
                                    AppLocalizations.of(context)!
                                        .translate('category_title_hospital'));
                              },
                              value: textBenhVien,
                              title: AppLocalizations.of(context)!
                                  .translate("select_hospital"),
                              checkRangBuoc: true,
                              icon: Icons.arrow_drop_down,
                              isError: validate && textBenhVien.isEmpty),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                              onPress: () {
                                _show();
                              },
                              value: textNgayTiem,
                              title: AppLocalizations.of(context)!
                                  .translate("select_day_vaccination"),
                              checkRangBuoc: true,
                              icon: Icons.calendar_month,
                              isError: validate && textNgayTiem.isEmpty),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                              onPress: () {
                                guiNhanIDDrop(defaultPhongTiem, 'phòng tiêm');
                              },
                              value: textPhongTiem,
                              title: AppLocalizations.of(context)!.translate(
                                  "category_title_department_vaccine"),
                              checkRangBuoc: true,
                              icon: Icons.arrow_drop_down,
                              isError: validate && textPhongTiem.isEmpty),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                              onPress: () {
                                guiNhanDrop(
                                    defaultGioTiem,
                                    AppLocalizations.of(context)!.translate(
                                        'category_title_day_vaccination'));
                              },
                              value: textGioTiem,
                              title: AppLocalizations.of(context)!
                                  .translate("select_time_vaccination"),
                              checkRangBuoc: true,
                              icon: Icons.arrow_drop_down,
                              isError: validate && textGioTiem.isEmpty),
                          const SizedBox(
                            height: 16,
                          ),
                          InputTextField(
                            title: AppLocalizations.of(context)!
                                .translate("content_note"),
                            checkRangBuoc: false,
                            focusNode: _focusGhiChu,
                            textController: textGhiChu,
                            onPress: () {
                              setState(() {
                                textGhiChu.clear();
                              });
                            },
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          InputTextField(
                            title: AppLocalizations.of(context)!
                                .translate("select_anamnesis"),
                            checkRangBuoc: true,
                            focusNode: _focusTienSuBenh,
                            textController: textTienSuBenh,
                            isError: validate && textTienSuBenh.text.isEmpty,
                            onPress: () {
                              setState(() {
                                textTienSuBenh.clear();
                              });
                            },
                          ),
                        ],
                      ),
                    );
                  }
                }),
              ),
            ),
            bottomNavigationBar: BottomNavigator(
              widget: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: 16,
                  ),
                  Expanded(
                      child: CustomButton(
                    text: AppLocalizations.of(context)!
                        .translate("button_cancel"),
                    height: 32,
                    icon: Icons.cancel_outlined,
                    iconColor: primaryColor,
                    textColor: primaryColor,
                    background: backgroundScreenColor,
                    press: () {
                      Navigator.pop(context);
                    },
                  )),
                  Container(
                    width: 8,
                  ),
                  Expanded(
                      child: CustomButton(
                    text:
                        AppLocalizations.of(context)!.translate("button_book"),
                    icon: Icons.save,
                    height: 32,
                    press: () {
                      checkValidate();
                      if (validate == false) {
                        addVaccination();
                      }
                    },
                  )),
                  Container(
                    width: 16,
                  ),
                ],
              ),
            ),
          )),
    );
  }

  void guiNhanDrop(String loai, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      if (value != null) {
        if (loai == defaultGioTiem) {
          setState(() {
            textGioTiem = value;
          });
        }
      }
    });
  }

  void guiNhanIDDrop(String loai, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListIdCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      if (value != null) {
        if (loai == defaultBenhVien) {
          setState(() {
            textBenhVien = value[0];
            idBenhVien = value[1];
          });
          StorePreferences.setIdHospital(idBenhVien);
        } else if (loai == defaultPhongTiem) {
          setState(() {
            textPhongTiem = value[0];
            idPhongTiem = value[1];
          });
        }
      }
    });
  }

  void _show() async {
    final DateTime? result = await showDatePicker(
      context: context,
      initialDate: dateTime,
      firstDate: DateTime(2024),
      lastDate: DateTime(2025),
      locale: const Locale('vi'),
    );
    if (result != null) {
      setState(() {
        dateTime = result;
        textNgayTiem = '${formatDate(dateTime, [dd, '-', mm, '-', yyyy])}';
      });
    }
  }

  void checkValidate() {
    if (textHoSo.isNotEmpty &&
        textBenhVien.isNotEmpty &&
        textNgayTiem.isNotEmpty &&
        textGioTiem.isNotEmpty &&
        textGoiVaccine.isNotEmpty &&
        textPhongTiem.isNotEmpty &&
        textTienSuBenh.text.isNotEmpty) {
      setState(() {
        validate = false;
      });
    } else {
      setState(() {
        validate = true;
      });
    }
  }

  Future<void> addVaccination() async {
    final firestore = FirebaseFirestore.instance;
    try {
      List<Map<String, dynamic>> listVaccination = [];
      List<Map<String, dynamic>> listVaccinationHospital = [];
      List<Vaccination> list =
          await readAllVaccination(StorePreferences.getIdUser());
      Vaccination vaccination = Vaccination(
          id: DateTime.now().toString(),
          trangThai: status_1,
          idBenhNhan: widget.idNguoiDat,
          idBenhVien: idBenhVien,
          idPhongTiem: idPhongTiem,
          gioTiem: textGioTiem,
          idVaccine: widget.vaccine.id,
          ghiChu: textGhiChu.text,
          tienSuBenh: textTienSuBenh.text,
          ngayTiem: textNgayTiem,
          idUser: StorePreferences.getIdUser());
      if (list.any((e) =>
              e.idVaccine == vaccination.idVaccine &&
              e.idBenhVien == vaccination.idBenhVien &&
              e.ngayTiem == vaccination.ngayTiem &&
              e.idPhongTiem == vaccination.idPhongTiem &&
              e.gioTiem == vaccination.gioTiem) ==
          false) {
        listVaccination.add(vaccination.toMap());
        listVaccinationHospital
            .add({'idLichTiem': vaccination.id, 'idUser': vaccination.idUser});
        //them data vao doccument users
        CollectionReference collectionReference =
            firestore.collection('RegistrationSchedule');
        final doccummentUsers =
            collectionReference.doc(StorePreferences.getIdUser());
        await doccummentUsers.set(
            {'vaccination': FieldValue.arrayUnion(listVaccination)},
            SetOptions(merge: true));
        //them vao document hospital
        CollectionReference collectionHospital =
            firestore.collection('Hospital');
        QuerySnapshot querySnapshot = await collectionHospital.get();
        for (QueryDocumentSnapshot doc in querySnapshot.docs) {
          DocumentSnapshot document =
              await collectionHospital.doc(doc.id).get();
          if (document.exists) {
            Map<String, dynamic> userData =
                document.data() as Map<String, dynamic>;
            if (userData['id'] == StorePreferences.getIdHospital()) {
              await collectionHospital.doc(doc.id).set(
                  {'lichTiem': FieldValue.arrayUnion(listVaccinationHospital)},
                  SetOptions(merge: true)).then((value) {});
            }
          }
        }
        ToastCommon.showToast(
            AppLocalizations.of(context)!.translate('toast_book_success'));
        Navigator.pop(context);
      } else {
        showDialog(
            context: context,
            builder: (context) {
              return ErrorPopup(
                message: AppLocalizations.of(context)!
                    .translate('error_book_duplicate'),
                buttonText: 'OK',
                onPress: () {
                  Navigator.pop(context);
                },
              );
            });
      }
    } catch (e) {
      ToastCommon.showToast(
          AppLocalizations.of(context)!.translate('toast_book_success'));
      Navigator.pop(context);
    }
  }

  Future<void> loadDataEdit() async {
    textGoiVaccine = widget.vaccine.ten;
    FamilyMember nguoiDat =
        await readIDBenhNhan(StorePreferences.getIdUser(), widget.idNguoiDat);
    textHoSo = nguoiDat.moiQH;
  }
}
