class Examination {
  final String id;
  final String idUser;
  final String trangThai;
  final String idBenhNhan;
  final String idKhoa;
  final String idPhong;
  final String loaiKham;
  final String ngayKham;
  final String gioKham;
  final String noiDungKham;
  final String benhManTinh;
  final String idBenhVien;
  final List<dynamic> trieuChung;
  String idKetQua;

  factory Examination.defaultContructor() {
    return Examination(
        id: '',
        idUser: '',
        trangThai: '',
        idBenhNhan: '',
        idKhoa: '',
        idPhong: '',
        loaiKham: '',
        ngayKham: '',
        gioKham: '',
        noiDungKham: '',
        benhManTinh: '',
        idBenhVien: '',
        idKetQua: '',
        trieuChung: []);
  }
  Examination({
    required this.id,
    required this.idUser,
    required this.trangThai,
    required this.idBenhNhan,
    required this.idKhoa,
    required this.idPhong,
    required this.loaiKham,
    required this.ngayKham,
    required this.gioKham,
    required this.noiDungKham,
    required this.benhManTinh,
    required this.idBenhVien,
    required this.trieuChung,
    required this.idKetQua,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'idUser': idUser,
      'trangThai': trangThai,
      'idBenhNhan': idBenhNhan,
      'idKhoa': idKhoa,
      'idPhong': idPhong,
      'loaiKham': loaiKham,
      'ngayKham': ngayKham,
      'gioKham': gioKham,
      'noiDungKham': noiDungKham,
      'benhManTinh': benhManTinh,
      'idBenhVien': idBenhVien,
      'idKetQua': idKetQua,
      'trieuChung': trieuChung
    };
  }

  factory Examination.fromMap(Map<String, dynamic> map) {
    return Examination(
        id: map['id'],
        idUser: map['idUser'],
        trangThai: map['trangThai'],
        idBenhNhan: map['idBenhNhan'],
        idKhoa: map['idKhoa'],
        idPhong: map['idPhong'],
        loaiKham: map['loaiKham'],
        ngayKham: map['ngayKham'],
        gioKham: map['gioKham'],
        noiDungKham: map['noiDungKham'],
        benhManTinh: map['benhManTinh'],
        idBenhVien: map['idBenhVien'],
        idKetQua: map['idKetQua'],
        trieuChung: map['trieuChung']);
  }
}
