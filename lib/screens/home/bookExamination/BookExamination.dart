import 'package:app_doan_flutter/components/bottomnavigator/BottomNavigator.dart';
import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/category/ListIdCategory.dart';
import 'package:app_doan_flutter/untils/FutureExamination.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/InputDropDown.dart';
import '../../../components/inputform/InputTextField.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/StorePreferences.dart';
import '../../category/ListAddCategory.dart';
import '../../category/ListCategory.dart';
import 'data/Examination.dart';

class BookExamination extends StatefulWidget {
  const BookExamination({super.key});

  @override
  State<BookExamination> createState() => _BookExaminationState();
}

class _BookExaminationState extends State<BookExamination> {
  final FocusNode _focusNoiDungKham = FocusNode();
  final FocusNode _focusBenhManTinh = FocusNode();
  String textHoSo = '';
  String textBenhVien = '';
  String textNgayKham = '';
  String textGioKham = '';
  String textLoaiKham = '';
  String textTrieuChung = '';
  List<String> listTrieuChung = [];
  String idNguoiDat = '';
  String idBenhVien = '';

  bool validate = false;
  DateTime dateTime = DateTime.now();
  final TextEditingController textNoiDungKham = TextEditingController();
  final TextEditingController textBenhManTinh = TextEditingController();

  @override
  void initState() {
    readTrieuChung();
    for (int i = 0; i < listTrieuChung.length; i++) {
      textTrieuChung += listTrieuChung[i];
      if (i < listTrieuChung.length - 1) {
        textTrieuChung += ', ';
      }
    }
    super.initState();
    _focusNoiDungKham.addListener(_onFocusChange);
    _focusBenhManTinh.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    setState(() {});
  }

  @override
  void dispose() {
    _focusNoiDungKham.dispose();
    _focusBenhManTinh.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: AppLocalizations.of(context)!.translate("app_bar_add_book"),
              check: false,
              onPress: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: Scaffold(
            body: SingleChildScrollView(
              child: Container(
                decoration: BoxDecorationContainer(),
                padding: const EdgeInsets.only(
                    top: 16, left: 16, right: 16, bottom: 16),
                margin: const EdgeInsets.only(
                    top: 8, left: 16, right: 16, bottom: 0),
                child: Column(
                  children: [
                    InputDropDown(
                        onPress: () {
                          guiNhanIDDrop(
                              defaultHoSo,
                              AppLocalizations.of(context)!
                                  .translate('category_title_file'));
                        },
                        value: textHoSo,
                        title: AppLocalizations.of(context)!
                            .translate("select_file"),
                        checkRangBuoc: true,
                        icon: Icons.arrow_drop_down,
                        isError: validate && textHoSo.isEmpty),
                    const SizedBox(
                      height: 16,
                    ),
                    InputDropDown(
                        onPress: () {
                          guiNhanIDDrop(
                              defaultBenhVien,
                              AppLocalizations.of(context)!
                                  .translate('category_title_hospital'));
                        },
                        value: textBenhVien,
                        title: AppLocalizations.of(context)!
                            .translate("select_hospital"),
                        checkRangBuoc: true,
                        icon: Icons.arrow_drop_down,
                        isError: validate && textBenhVien.isEmpty),
                    const SizedBox(
                      height: 16,
                    ),
                    InputDropDown(
                        onPress: () {
                          _show();
                        },
                        value: textNgayKham,
                        title: AppLocalizations.of(context)!
                            .translate("select_day_book_examination"),
                        checkRangBuoc: true,
                        icon: Icons.calendar_month,
                        isError: validate && textNgayKham.isEmpty),
                    const SizedBox(
                      height: 16,
                    ),
                    InputDropDown(
                        onPress: () {
                          guiNhanDrop(
                              defaultGioKham,
                              AppLocalizations.of(context)!.translate(
                                  'category_title_time_examination'));
                        },
                        value: textGioKham,
                        title: AppLocalizations.of(context)!
                            .translate("select_time_examination"),
                        checkRangBuoc: true,
                        icon: Icons.arrow_drop_down,
                        isError: validate && textGioKham.isEmpty),
                    const SizedBox(
                      height: 16,
                    ),
                    InputDropDown(
                        onPress: () {
                          guiNhanDrop(
                              defaultLoaiKham,
                              AppLocalizations.of(context)!.translate(
                                  'category_title_type_examination'));
                        },
                        value: textLoaiKham,
                        title: AppLocalizations.of(context)!
                            .translate("select_type_examination"),
                        checkRangBuoc: true,
                        icon: Icons.arrow_drop_down,
                        isError: validate && textGioKham.isEmpty),
                    const SizedBox(
                      height: 16,
                    ),
                    InputTextField(
                      title: AppLocalizations.of(context)!
                          .translate("content_examination"),
                      checkRangBuoc: true,
                      focusNode: _focusNoiDungKham,
                      textController: textNoiDungKham,
                      onPress: () {
                        setState(() {
                          textNoiDungKham.clear();
                        });
                      },
                      isError: validate && textNoiDungKham.text.isEmpty,
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    InputDropDown(
                        onPress: () {
                          guiTrieuChung(
                              listTrieuChung,
                              AppLocalizations.of(context)!
                                  .translate('category_title_symptom'));
                        },
                        value: textTrieuChung,
                        title: AppLocalizations.of(context)!
                            .translate("select_symptom"),
                        checkRangBuoc: true,
                        icon: Icons.arrow_drop_down,
                        isError: validate && textTrieuChung.isEmpty),
                    const SizedBox(
                      height: 16,
                    ),
                    InputTextField(
                      title: AppLocalizations.of(context)!
                          .translate("select_chronic_diseases"),
                      checkRangBuoc: true,
                      focusNode: _focusBenhManTinh,
                      textController: textBenhManTinh,
                      onPress: () {
                        setState(() {
                          textBenhManTinh.clear();
                        });
                      },
                      isError: validate && textBenhManTinh.text.isEmpty,
                    ),
                  ],
                ),
              ),
            ),
            bottomNavigationBar: BottomNavigator(
              widget: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: 16,
                  ),
                  Expanded(
                      child: CustomButton(
                    text: AppLocalizations.of(context)!
                        .translate("button_cancel"),
                    height: 32,
                    icon: Icons.cancel_outlined,
                    iconColor: primaryColor,
                    textColor: primaryColor,
                    background: backgroundScreenColor,
                    press: () {
                      Navigator.pop(context);
                    },
                  )),
                  Container(
                    width: 8,
                  ),
                  Expanded(
                      child: CustomButton(
                    text:
                        AppLocalizations.of(context)!.translate("button_book"),
                    icon: Icons.save,
                    height: 32,
                    press: () {
                      checkValidate();
                      if (validate == false) {
                        addExamination();
                      }
                    },
                  )),
                  Container(
                    width: 16,
                  ),
                ],
              ),
            ),
          )),
    );
  }

  void guiNhanDrop(String loai, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      if (value != null) {
        if (loai == defaultGioKham) {
          setState(() {
            textGioKham = value;
          });
        } else if (loai == defaultLoaiKham) {
          setState(() {
            textLoaiKham = value;
          });
        }
      }
    });
  }

  void guiNhanIDDrop(String loai, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListIdCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      if (value != null) {
        if (loai == defaultHoSo) {
          setState(() {
            textHoSo = value[0];
            idNguoiDat = value[1];
          });
        } else if (loai == defaultBenhVien) {
          setState(() {
            textBenhVien = value[0];
            idBenhVien = value[1];
          });
          StorePreferences.setIdHospital(idBenhVien);
        }
      }
    });
  }

  void guiTrieuChung(List<String> list, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListAddCategory(
                  type: defaultTrieuChung,
                  list: list,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      String textTrieuChungChon = '';
      for (int i = 0; i < value.length; i++) {
        textTrieuChungChon += value[i];
        if (i < value.length - 1) {
          textTrieuChungChon += ', ';
        }
      }
      setState(() {
        listTrieuChung = value;
        textTrieuChung = textTrieuChungChon;
      });
    });
  }

  void _show() async {
    final DateTime? result = await showDatePicker(
      context: context,
      initialDate: dateTime,
      firstDate: DateTime(2024),
      lastDate: DateTime(2025),
      locale: const Locale('vi'),
    );
    if (result != null) {
      setState(() {
        dateTime = result;
        textNgayKham = '${formatDate(dateTime, [dd, '-', mm, '-', yyyy])}';
      });
    }
  }

  void checkValidate() {
    if (textHoSo.isNotEmpty &&
        textBenhVien.isNotEmpty &&
        textNgayKham.isNotEmpty &&
        textGioKham.isNotEmpty &&
        textBenhManTinh.text.isNotEmpty &&
        textNoiDungKham.text.isNotEmpty &&
        textTrieuChung.isNotEmpty) {
      setState(() {
        validate = false;
      });
    } else {
      setState(() {
        validate = true;
      });
    }
  }

  Future<void> readTrieuChung() async {
    DocumentReference documentReference = FirebaseFirestore.instance
        .collection('Categorys')
        .doc('8nDu9e5HN5HkdyhmO4Lz');
    DocumentSnapshot documentSnapshot = await documentReference.get();

    if (documentSnapshot.exists) {
      Map<String, dynamic> dataCategory =
          documentSnapshot.data() as Map<String, dynamic>;
      StorePreferences.setListTrieuChung(
          List<String>.from(dataCategory['trieuChung'] as List));
    } else {
      StorePreferences.setListTrieuChung([]);
    }
  }

  Future<void> addExamination() async {
    try {
      CollectionReference collectionReference =
          FirebaseFirestore.instance.collection('RegistrationSchedule');
      List<Map<String, dynamic>> listExamination = [];
      List<Map<String, dynamic>> listExaminationHospital = [];
      List<Examination> list =
          await readAllExamination(StorePreferences.getIdUser());
      Examination examination = Examination(
          id: DateTime.now().toString(),
          idUser: StorePreferences.getIdUser(),
          trangThai: status_1,
          idBenhNhan: idNguoiDat,
          idPhong: '',
          idKhoa: '',
          idKetQua: '',
          loaiKham: textLoaiKham,
          ngayKham: textNgayKham,
          gioKham: textGioKham,
          noiDungKham: textNoiDungKham.text,
          benhManTinh: textBenhManTinh.text,
          idBenhVien: idBenhVien,
          trieuChung: listTrieuChung);
      if (list.any((e) =>
              e.idPhong == examination.idPhong &&
              e.idKhoa == examination.idKhoa &&
              e.ngayKham == examination.ngayKham &&
              e.gioKham == examination.gioKham &&
              e.idBenhVien == examination.idBenhVien) ==
          false) {
        listExamination.add(examination.toMap());
        listExaminationHospital.add({
          'idLichKham': examination.id,
          'idUser': StorePreferences.getIdUser()
        });
        await collectionReference.doc(StorePreferences.getIdUser()).set(
            {'examination': FieldValue.arrayUnion(listExamination)},
            SetOptions(merge: true));
        CollectionReference collectionHospital =
            FirebaseFirestore.instance.collection('Hospital');
        QuerySnapshot querySnapshot = await collectionHospital.get();
        for (QueryDocumentSnapshot doc in querySnapshot.docs) {
          DocumentSnapshot document =
              await collectionHospital.doc(doc.id).get();
          if (document.exists) {
            Map<String, dynamic> userData =
                document.data() as Map<String, dynamic>;
            if (userData['id'] == StorePreferences.getIdHospital()) {
              await collectionHospital.doc(doc.id).set(
                  {'lichKham': FieldValue.arrayUnion(listExaminationHospital)},
                  SetOptions(merge: true)).then((value) {});
            }
          }
        }
        ToastCommon.showToast(
            AppLocalizations.of(context)!.translate('toast_book_success'));
        Navigator.pop(context);
      } else {
        showDialog(
            context: context,
            builder: (context) {
              return ErrorPopup(
                message: AppLocalizations.of(context)!
                    .translate('error_book_duplicate'),
                buttonText: 'OK',
                onPress: () {
                  Navigator.pop(context);
                },
              );
            });
      }
    } catch (e) {
      ToastCommon.showToast(
          AppLocalizations.of(context)!.translate('toast_book_faild'));
    }
  }
}
