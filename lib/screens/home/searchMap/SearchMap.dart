import 'dart:async';

import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class SearchMap extends StatefulWidget {
  const SearchMap({super.key});

  @override
  State<SearchMap> createState() => _SearchMapState();
}

class _SearchMapState extends State<SearchMap> {
  Completer<GoogleMapController> _gooogleMapController = Completer();
  CameraPosition? _cameraPosition;
  Location? _location;
  LocationData? _currentLocation;

  @override
  void initState() {
    _init();
    super.initState();
  }

  _init() {
    _location = Location();
    _cameraPosition =
        CameraPosition(target: LatLng(21.034906, 105.787498), zoom: 13);
    getPolyPoints(LatLng(21.034906, 105.787498), LatLng(21.018608, 105.860284));
    _initLocation();
  }

  // hàm lắng nghe khi ra dời địa điểm
  _initLocation() {
    _location?.getLocation().then((location) {
      _currentLocation = location;
    });
    _location?.onLocationChanged.listen((newLocation) {
      _currentLocation = newLocation;
      moveToPosition(LatLng(
          _currentLocation?.latitude ?? 0, _currentLocation?.longitude ?? 0));
    });
  }

  moveToPosition(LatLng latLng) async {
    GoogleMapController mapController = await _gooogleMapController.future;
    mapController.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: latLng, zoom: 13)));
  }

  List<LatLng> polylineCoordinates = [];
  void getPolyPoints(LatLng location, LatLng target) async {
    PolylinePoints polylinePoints = PolylinePoints();
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
        "AIzaSyBLRofnbyxXEYHE3BVYaa3D-DOTPJmZzjQ",
        PointLatLng(location.latitude, location.longitude),
        PointLatLng(target.latitude, target.longitude));
    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
      setState(() {});
    }
  }

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(21.034906, 105.787498),
    zoom: 13,
  ); // CameraPosition
  static final Marker hospital_108 = Marker(
    markerId: MarkerId('hospital_108'),
    infoWindow: InfoWindow(title: 'Bệnh viện Trung ương Quân đội 108'),
    icon: BitmapDescriptor.defaultMarker,
    position: LatLng(21.018608, 105.860284),
  );
  static final Marker hospital_Y_HaNoi = Marker(
    markerId: MarkerId('hospital_Y_HaNoi'),
    infoWindow: InfoWindow(title: 'Bệnh viện Đại học Y Hà Nội'),
    icon: BitmapDescriptor.defaultMarker,
    position: LatLng(21.002323, 105.830148),
  );
  static final Marker hospital_103 = Marker(
    markerId: MarkerId('hospital_103'),
    infoWindow: InfoWindow(title: 'Bệnh viện 103'),
    icon: BitmapDescriptor.defaultMarker,
    position: LatLng(20.967762, 105.789008),
  );

  // static final CameraPosition _kLake = CameraPosition(
  //     bearing: 192.8334901395799,
  //     target: LatLng(21.034906, 105.787498),
  //     tilt: 59.440717697143555,
  //     zoom: 19.151926040649414); // CameraPosition
  @override
  Widget build(BuildContext context) {
    print(polylineCoordinates);
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Stack(children: [
        GoogleMap(
          mapType: MapType.normal,
          markers: {hospital_108, hospital_Y_HaNoi, hospital_103},
          polylines: {
            Polyline(
                polylineId: PolylineId("point_108"),
                points: polylineCoordinates,
                color: statusColor),
          },
          initialCameraPosition:
              CameraPosition(target: LatLng(21.034906, 105.787498), zoom: 13),
          // onMapCreated: (GoogleMapController controller) {
          //   if (!_gooogleMapController.isCompleted) {
          //     _gooogleMapController.complete(controller);
          //   }
          // },
        ),
        Positioned.fill(
            child: Align(alignment: Alignment.center, child: _getMarket()))
      ]),
    );
  }

  Widget _getMarket() {
    return Container(
      width: 30,
      height: 30,
      padding: EdgeInsets.all(2),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(100),
          boxShadow: [
            BoxShadow(
                color: Colors.grey,
                offset: Offset(0, 3),
                spreadRadius: 4,
                blurRadius: 6)
          ]),
      child: ClipOval(
          child: SvgPicture.asset('assets/icons/home/IconDatLich.svg')),
    );
  }
}
