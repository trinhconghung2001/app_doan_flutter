class Medication {
  final String id;
  final String trangThai;
  final String idNguoiUong;
  final List<dynamic> loaiThuoc;
  final String idLichKham;
  final List<dynamic> cachDung;
  final String ngayBatDau;
  final String ngayKetThuc;
  final String ghiChu;
  final List<dynamic> gioNhacNho;

  Medication({
    required this.id,
    required this.trangThai,
    required this.idNguoiUong,
    required this.loaiThuoc,
    required this.idLichKham,
    required this.cachDung,
    required this.ngayBatDau,
    required this.ngayKetThuc,
    required this.ghiChu,
    required this.gioNhacNho,
  });

  factory Medication.defaultContructor() {
    return Medication(
        id: '',
        trangThai: '',
        idNguoiUong: '',
        loaiThuoc: [],
        idLichKham: '',
        cachDung: [],
        ngayBatDau: '',
        ngayKetThuc: '',
        ghiChu: '',
        gioNhacNho: []);
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'trangThai': trangThai,
      'idNguoiUong': idNguoiUong,
      'loaiThuoc': loaiThuoc,
      'idLichKham': idLichKham,
      'cachDung': cachDung,
      'ngayBatDau': ngayBatDau,
      'ngayKetThuc': ngayKetThuc,
      'ghiChu': ghiChu,
      'gioNhacNho': gioNhacNho
    };
  }

  factory Medication.fromMap(Map<String, dynamic> map) {
    return Medication(
        id: map['id'],
        trangThai: map['trangThai'],
        idNguoiUong: map['idNguoiUong'],
        loaiThuoc: map['loaiThuoc'],
        idLichKham: map['idLichKham'],
        cachDung: map['cachDung'],
        ngayBatDau: map['ngayBatDau'],
        ngayKetThuc: map['ngayKetThuc'],
        ghiChu: map['ghiChu'],
        gioNhacNho: map['gioNhacNho']);
  }
}
