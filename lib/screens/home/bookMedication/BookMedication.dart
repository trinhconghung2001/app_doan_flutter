import 'package:app_doan_flutter/components/bottomnavigator/BottomNavigator.dart';
import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/category/ListAddCategory.dart';
import 'package:app_doan_flutter/screens/home/bookMedication/data/Medication.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/InputDropDown.dart';
import '../../../components/inputform/InputTextField.dart';
import '../../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureLocalNotification.dart';
import '../../../untils/FutureResult.dart';
import '../../../untils/StorePreferences.dart';
import '../../homeDoctor/resultExamination/Result.dart';
import '../../login/data/FamilyMember.dart';

class BookMedication extends StatefulWidget {
  final String idKetQua;
  final String idNguoiUong;
  final String idLichKham;
  final String moiQH;
  const BookMedication(
      {super.key,
      required this.idKetQua,
      required this.idNguoiUong,
      required this.idLichKham,
      required this.moiQH});

  @override
  State<BookMedication> createState() => _BookMedicationState();
}

class _BookMedicationState extends State<BookMedication> {
  late Future<void> _loadDataBook;
  final FocusNode _focusGhiChu = FocusNode();
  String textNguoiUong = '';
  String textNgayBatDau = '';
  String textNgayKetThuc = '';
  String textTienSuBenh = '';
  List<String> listTime = [];
  List<String> listThuoc = [];
  String textCachDung = '';
  List<String> listCachDung = [];
  String textThuoc = '';
  String textTime = '';
  FamilyMember nguoi = FamilyMember.defaultContructor();

  bool validate = false;
  DateTime dateStart = DateTime.now();
  DateTime dateEnd = DateTime.now();
  final TextEditingController textGhiChu = TextEditingController();

  @override
  void initState() {
    readData();
    _loadDataBook = loadData();
    super.initState();
    _focusGhiChu.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    setState(() {});
  }

  @override
  void dispose() {
    _focusGhiChu.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: AppLocalizations.of(context)!
                  .translate("app_bar_medication_schedule"),
              check: false,
              onPress: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: Scaffold(
            body: SingleChildScrollView(
                child: FutureBuilder(
                    future: _loadDataBook,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return CircularProgressIndicatorWidget();
                      } else if (snapshot.hasError) {
                        return EmptyData();
                      } else {
                        return Container(
                          decoration: BoxDecorationContainer(),
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16, bottom: 16),
                          margin: const EdgeInsets.only(
                              top: 8, left: 16, right: 16, bottom: 0),
                          child: Column(
                            children: [
                              InputDropDown(
                                value: textNguoiUong,
                                title: AppLocalizations.of(context)!
                                    .translate("select_people_use"),
                                checkRangBuoc: false,
                                icon: Icons.arrow_drop_down,
                                showIcon: false,
                                offEdit: true,
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              InputDropDown(
                                  onPress: () {
                                    guiThuoc(
                                        listThuoc,
                                        AppLocalizations.of(context)!.translate(
                                            'category_type_medication'));
                                  },
                                  maxLines: 6,
                                  value: textThuoc,
                                  title: AppLocalizations.of(context)!
                                      .translate("select_type_medication"),
                                  checkRangBuoc: true,
                                  icon: Icons.arrow_drop_down,
                                  isError: validate && textThuoc.isEmpty),
                              const SizedBox(
                                height: 16,
                              ),
                              InputDropDown(
                                value: textCachDung,
                                maxLines: 6,
                                title: AppLocalizations.of(context)!
                                    .translate("select_use"),
                                checkRangBuoc: false,
                                icon: Icons.arrow_drop_down,
                                showIcon: false,
                                offEdit: true,
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              InputDropDown(
                                value: textNgayBatDau,
                                title: AppLocalizations.of(context)!
                                    .translate("select_start_day"),
                                checkRangBuoc: false,
                                showIcon: false,
                                offEdit: true,
                                icon: Icons.calendar_month,
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              InputDropDown(
                                value: textNgayKetThuc,
                                title: AppLocalizations.of(context)!
                                    .translate("select_end_day"),
                                checkRangBuoc: false,
                                offEdit: true,
                                showIcon: false,
                                icon: Icons.calendar_month,
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              InputDropDown(
                                  onPress: () {
                                    guiThoiGian(
                                        listTime,
                                        AppLocalizations.of(context)!.translate(
                                            'category_title_time_use'));
                                  },
                                  value: textTime,
                                  title: AppLocalizations.of(context)!
                                      .translate("select_time_use"),
                                  checkRangBuoc: true,
                                  icon: Icons.arrow_drop_down,
                                  isError: validate && textTime.isEmpty),
                              const SizedBox(
                                height: 16,
                              ),
                              InputTextField(
                                title: AppLocalizations.of(context)!
                                    .translate("content_note"),
                                checkRangBuoc: false,
                                focusNode: _focusGhiChu,
                                textController: textGhiChu,
                                onPress: () {
                                  setState(() {
                                    textGhiChu.clear();
                                  });
                                },
                              ),
                              Container(
                                width: 16,
                              ),
                            ],
                          ),
                        );
                      }
                    })),
            bottomNavigationBar: BottomNavigator(
              widget: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: 16,
                  ),
                  Expanded(
                      child: CustomButton(
                    text: AppLocalizations.of(context)!
                        .translate("button_cancel"),
                    height: 32,
                    icon: Icons.cancel_outlined,
                    iconColor: primaryColor,
                    textColor: primaryColor,
                    background: backgroundScreenColor,
                    press: () {
                      Navigator.pop(context);
                    },
                  )),
                  Container(
                    width: 8,
                  ),
                  Expanded(
                      child: CustomButton(
                    text:
                        AppLocalizations.of(context)!.translate("button_book"),
                    icon: Icons.save,
                    height: 32,
                    press: () {
                      checkValidate();
                      if (validate == false) {
                        addMedication();
                      }
                    },
                  )),
                  Container(
                    width: 16,
                  ),
                ],
              ),
            ),
          )),
    );
  }

  void guiThuoc(List<String> list, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListAddCategory(
                  type: defaultTypeMedication2,
                  list: list,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      String textThuocChon = '';
      for (int i = 0; i < value.length; i++) {
        textThuocChon += value[i];
        if (i < value.length - 1) {
          textThuocChon += ', ';
        }
      }
      List<String> uniqueValuesA = StorePreferences.getListThuoc()
          .where((item) => !value.contains(item))
          .toList();
      if (uniqueValuesA.length > 0) {
        print('Loại thuốc chọn ít hơn thuốc ban đầu');
        listCachDung = StorePreferences.getListCachDung();
        for (String x in uniqueValuesA) {
          listCachDung.removeWhere(
              (e) => e.trim().toLowerCase().contains(x.trim().toLowerCase()));
        }
        String textCachDungMoi = '';
        for (int i = 0; i < listCachDung.length; i++) {
          textCachDungMoi += listCachDung[i];
          if (i < listCachDung.length - 1) {
            textCachDungMoi += ', ';
          }
        }
        String textTimeMoi = '';
        listTime = readListTime(listCachDung);
        for (int i = 0; i < listTime.length; i++) {
          textTimeMoi += listTime[i];
          if (i < listTime.length - 1) {
            textTimeMoi += ', ';
          }
        }
        setState(() {
          listThuoc = value;
          textThuoc = textThuocChon;
          textCachDung = textCachDungMoi;
          textTime = textTimeMoi;
        });
      } else {
        print('Loại thuốc chọn nhiều hơn thuốc ban đầu');
        listCachDung = StorePreferences.getListCachDung();
        String textCachDungMoi = '';
        for (int i = 0; i < listCachDung.length; i++) {
          textCachDungMoi += listCachDung[i];
          if (i < listCachDung.length - 1) {
            textCachDungMoi += ', ';
          }
        }
        String textTimeMoi = '';
        listTime = readListTime(listCachDung);
        for (int i = 0; i < listTime.length; i++) {
          textTimeMoi += listTime[i];
          if (i < listTime.length - 1) {
            textTimeMoi += ', ';
          }
        }
        setState(() {
          listThuoc = value;
          textThuoc = textThuocChon;
          textCachDung = textCachDungMoi;
          textTime = textTimeMoi;
        });
      }
    });
  }

  void guiThoiGian(List<String> list, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListAddCategory(
                  type: defaultGioThongBao,
                  list: list,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      String textTimeSelect = '';
      for (int i = 0; i < value.length; i++) {
        textTimeSelect += value[i];
        if (i < value.length - 1) {
          textTimeSelect += ', ';
        }
      }
      setState(() {
        listTime = value;
        textTime = textTimeSelect;
      });
    });
  }

  void checkValidate() {
    if (textNguoiUong.isNotEmpty &&
        textThuoc.isNotEmpty &&
        textCachDung.isNotEmpty &&
        textTime.isNotEmpty) {
      setState(() {
        validate = false;
      });
    } else {
      setState(() {
        validate = true;
      });
    }
  }

  Future<void> addMedication() async {
    String stringDateNow = DateFormat("dd-MM-yyyy").format(DateTime.now());
    DateTime dateNow = DateFormat("dd-MM-yyyy").parse(stringDateNow);
    DateTime dateStart =
        DateFormat("dd-MM-yyyy").parse(StorePreferences.getStartDate());
    DateTime dateEnd =
        DateFormat("dd-MM-yyyy").parse(StorePreferences.getEndDate());
    if (dateNow.isAfter(dateStart) && dateNow.isBefore(dateEnd)) {
      //print('DateTime nằm trong khoảng thời gian A-B.');
      CollectionReference collectionReference =
          FirebaseFirestore.instance.collection('RegistrationSchedule');
      List<Map<String, dynamic>> listMedication = [];
      Medication medication = new Medication(
          id: DateTime.now().toString(),
          trangThai: status_medication_2,
          idNguoiUong: widget.idNguoiUong,
          loaiThuoc: listThuoc,
          idLichKham: widget.idLichKham,
          cachDung: listCachDung,
          ngayBatDau: textNgayBatDau,
          ngayKetThuc: textNgayKetThuc,
          ghiChu: textGhiChu.text,
          gioNhacNho: listTime);
      listMedication.add(medication.toMap());
      await collectionReference.doc(StorePreferences.getIdUser()).set(
          {'medication': FieldValue.arrayUnion(listMedication)},
          SetOptions(merge: true)).then((value) {
        notificationShow(listTime);
        ToastCommon.showToast(AppLocalizations.of(context)!
            .translate('toast_book_medication_success'));
        Navigator.pop(context);
      }).onError((error, stackTrace) {
        showDialog(
            context: context,
            builder: (context) {
              return ErrorPopup(
                message: AppLocalizations.of(context)!
                    .translate('toast_book_medication_faild'),
                buttonText: 'OK',
                onPress: () {
                  Navigator.pop(context);
                },
              );
            });
      });
    } else {
      //print('DateTime không nằm trong khoảng thời gian A-B.');
      showDialog(
          context: context,
          builder: (context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_add_medication_faild'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          });
    }
  }

  Future<void> readData() async {
    DocumentReference documentReference = FirebaseFirestore.instance
        .collection('Categorys')
        .doc('8nDu9e5HN5HkdyhmO4Lz');
    DocumentSnapshot documentSnapshot = await documentReference.get();

    if (documentSnapshot.exists) {
      Map<String, dynamic> dataCategory =
          documentSnapshot.data() as Map<String, dynamic>;
      StorePreferences.setListTime(
          List<String>.from(dataCategory['gioNhacThuoc'] as List));
    } else {
      StorePreferences.setListTime([]);
    }
  }

  Future<void> loadData() async {
    List<Result> listKetQua =
        await readListResult(StorePreferences.getIdUser());
    Result ketQua = Result.defaultContructor();
    for (Result i in listKetQua) {
      if (i.id == widget.idKetQua) {
        ketQua = i;
        break;
      }
    }
    textNguoiUong = widget.moiQH;
    // textCachDung.text = ketQua.cachDung;
    listCachDung = ketQua.cachDung.map((e) => e.toString()).toList();
    StorePreferences.setListCachDung(listCachDung);
    listThuoc = ketQua.thuoc.map((e) => e.toString()).toList();
    StorePreferences.setListThuoc(listThuoc);
    textNgayBatDau = ketQua.ngayBatDau;
    textNgayKetThuc = ketQua.ngayKetThuc;
    StorePreferences.setStartDate(textNgayBatDau);
    StorePreferences.setEndDate(textNgayKetThuc);
    dateStart = DateFormat("dd-MM-yyyy").parse(ketQua.ngayBatDau);
    dateEnd = DateFormat("dd-MM-yyyy").parse(ketQua.ngayKetThuc);
    for (int i = 0; i < listThuoc.length; i++) {
      textThuoc += listThuoc[i];
      if (i < listThuoc.length - 1) {
        textThuoc += ', ';
      }
    }
    for (int i = 0; i < listCachDung.length; i++) {
      textCachDung += listCachDung[i];
      if (i < listCachDung.length - 1) {
        textCachDung += ', ';
      }
    }
    listTime = readListTime(listCachDung);
    for (int i = 0; i < listTime.length; i++) {
      textTime += listTime[i];
      if (i < listTime.length - 1) {
        textTime += ', ';
      }
    }
  }
}

List<String> readListTime(List<String> listCachDung) {
  List<String> listGio = [];
  for (String e in listCachDung) {
    int colonIndex = e.indexOf("/");
    if (colonIndex != -1) {
      // Lấy phần sau dấu hai chấm và loại bỏ khoảng trắng ở đầu (nếu có)
      String valuesAfterColon = e.substring(colonIndex + 1).trim();

      // Chia chuỗi thành danh sách bằng dấu "/"
      List<String> timeValues = valuesAfterColon.split("/");
      // timeValues sẽ chứa ["Bôi", "Sáng", "Chiều", "Tối"]
      for (String x in timeValues) {
        if (x == 'Sáng') {
          listGio.add('8:00 AM');
        } else if (x == 'Chiều') {
          listGio.add('5:00 PM');
        } else if (x == 'Tối') {
          listGio.add('7:00 PM');
        }
      }
    }
  }
  return listGio.toSet().toList();
}

Future<void> notificationShow(List<String> list) async {
  for (int i = 0; i < list.length; i++) {
    TimeOfDay notificationTime = formatTime(list[i]);
    TimeOfDay currentTime = TimeOfDay.now();
    if (notificationTime.hour > currentTime.hour ||
        (notificationTime.hour == currentTime.hour &&
            notificationTime.minute > currentTime.minute)) {
      Duration delay = Duration(
        hours: notificationTime.hour - currentTime.hour,
        minutes: notificationTime.minute - currentTime.minute,
      );
      await Future.delayed(delay, () {
        sendLocationNotification();
      });
    }
  }
}

TimeOfDay formatTime(String time) {
  DateFormat format = DateFormat.jm();
  DateTime parsedTime = format.parse(time);
  return TimeOfDay.fromDateTime(parsedTime);
}

DateTime fomatDate(String date) {
  DateFormat format = DateFormat.jm();
  DateTime parsedTime = format.parse(date);
  return parsedTime;
}
