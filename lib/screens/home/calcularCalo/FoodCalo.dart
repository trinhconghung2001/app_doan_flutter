import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/screens/home/calcularCalo/AddFood.dart';
import 'package:app_doan_flutter/screens/home/calcularCalo/FoodDetail.dart';
import 'package:app_doan_flutter/untils/FutureFood.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../config/styles/Dimens.dart';
import '../../../config/styles/KeyValue.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/StorePreferences.dart';
import 'components/ItemFood.dart';
import 'data/Food.dart';

class FoodCalo extends StatefulWidget {
  const FoodCalo({super.key});

  @override
  State<FoodCalo> createState() => _FoodCaloState();
}

class _FoodCaloState extends State<FoodCalo> {
  FocusNode focusNode = FocusNode();
  TextEditingController textEditingController = TextEditingController();
  late Future<void> _loadDataFuture;
  List<Food> listFood = [];
  @override
  void initState() {
    _loadDataFuture = loadDataOne();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDataFuture = loadDataOne();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (() {
        FocusManager.instance.primaryFocus?.unfocus();
      }),
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: AppLocalizations.of(context)!.translate("app_bar_food_calo"),
            check: false,
            onPress: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Container(
            child: Column(children: [
          Container(
            margin: const EdgeInsets.only(left: 16, right: 16, top: 16),
            height: 50,
            child: TextField(
                maxLines: 1,
                style: const TextStyle(
                  color: Color(0xFF444444),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
                onChanged: (value) {
                  searchList(value);
                },
                focusNode: focusNode,
                controller: textEditingController,
                decoration: InputDecoration(
                  hintText: AppLocalizations.of(context)!
                      .translate('hint_title_food'),
                  contentPadding: const EdgeInsets.only(
                      left: defaultTextInsideBoxPadding, right: 0),
                  fillColor: statusColor,
                  // enabledBorder: const OutlineInputBorder(
                  //   borderSide: BorderSide(color: Color(0xFFE0E0E0), width: 1.0),
                  // ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  suffixIcon: Visibility(
                    visible: focusNode.hasFocus == true &&
                            textEditingController.text.isNotEmpty
                        ? true
                        : false,
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          textEditingController.clear;
                          searchList('');
                        });
                      },
                      child: const Icon(
                        Icons.cancel_outlined,
                        color: Color(0xFFBBC2C6),
                      ),
                    ),
                  ),
                  suffixIconConstraints: const BoxConstraints(
                    minWidth: 41,
                    minHeight: 25,
                  ),
                )),
          ),
          SizedBox(
            height: 8,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 16),
            child: FutureBuilder(
              future: _loadDataFuture,
              builder: ((context, AsyncSnapshot snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return CircularProgressIndicatorWidget();
                } else if (snapshot.hasError) {
                  return EmptyData(
                      msg: AppLocalizations.of(context)!
                          .translate('error_data_msg'));
                } else {
                  if (listFood.length > 0) {
                    return ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: listFood.length,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => FoodDetail(
                                            typeFood: false,
                                            idFoodDetail: listFood[index].id,                                  
                                          ))).then((value) => loadScreen());
                            },
                            child: listFood[index].edit == true
                                ? ItemFood(
                                    onPress: () {
                                      removeFoodUser(listFood[index])
                                          .then((value) => loadScreen());
                                    },
                                    icon: Icons.delete_forever_rounded,
                                    ten: listFood[index].ten,
                                    khoiLuong: listFood[index].khoiLuong,
                                    donVi: listFood[index].donVi,
                                    calo: listFood[index].calo)
                                : ItemFood(
                                    onPress: () {},
                                    showIcon: true,
                                    ten: listFood[index].ten,
                                    khoiLuong: listFood[index].khoiLuong,
                                    donVi: listFood[index].donVi,
                                    calo: listFood[index].calo),
                          );
                        });
                  } else {
                    return EmptyData();
                  }
                }
              }),
            ),
          )
        ])),
        bottomNavigationBar: BottomNavigator(
          widget: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                width: 16,
              ),
              Expanded(
                  child: CustomButton(
                text:
                    AppLocalizations.of(context)!.translate('button_add_food'),
                height: 32,
                icon: Icons.fastfood_outlined,
                press: () {
                  Navigator.push(context,
                          MaterialPageRoute(builder: (context) => AddFood()))
                      .then((value) => loadScreen());
                },
              )),
              Container(
                width: 16,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> removeFoodUser(Food food) async {
    List<Food> listFoodUser =
        await readCategoryFoodUser(StorePreferences.getIdUser());
    for (int i = 0; i < listFoodUser.length; i++) {
      if (listFoodUser[i].id == food.id) {
        listFoodUser.removeAt(i);
        break;
      }
    }
    try {
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('Food')
          .doc(StorePreferences.getIdUser());
      await documentReference
          .set({'food': listFoodUser.map((e) => e.toMap())}).then((value) {
        ToastCommon.showToast(
            AppLocalizations.of(context)!.translate('toast_remove_food_meal'));
      });
    } catch (e) {
      // TODO
      showDialog(
          context: context,
          builder: ((context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_remove_food_meal'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          }));
    }
  }

  void searchList(String value) {
    setState(() {
      textEditingController.text = value;
      _loadDataFuture = loadDataSearch(value);
    });
  }

  Future<void> loadDataSearch(String value) async {
    List<Food> listFoodCategory = await readCategoryFood();
    List<Food> listFoodUser =
        await readCategoryFoodUser(StorePreferences.getIdUser());
    List<Food> listAll = listFoodCategory + listFoodUser;
    listFood = listAll
        .where((element) =>
            element.ten.toLowerCase().contains(value.trim().toLowerCase()))
        .toList();
  }

  Future<void> loadDataOne() async {
    List<Food> listFoodCategory = await readCategoryFood();
    List<Food> listFoodUser =
        await readCategoryFoodUser(StorePreferences.getIdUser());
    listFood = listFoodCategory + listFoodUser;
  }

  Dialog dialogFood() {
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        child: Container(
          padding: EdgeInsets.only(top: 8),
          width: MediaQuery.of(context).size.width / 2,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.all(defaultPadding),
                child: Text(
                  AppLocalizations.of(context)!
                      .translate('popup_select_picture'),
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.only(
                      left: defaultPadding,
                      right: defaultPadding,
                      bottom: defaultPadding),
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height / 5,
                    child: Column(
                      children: [
                        Expanded(
                            child: CustomButton(
                          text: AppLocalizations.of(context)!
                              .translate('title_menu_breakfase'),
                          icon: Icons.coffee,
                          height: 32,
                          iconColor: primaryColor,
                          textColor: primaryColor,
                          background: backgroundScreenColor,
                          press: () {
                            StorePreferences.setFoodName(food_breakfase);
                          },
                        )),
                        Container(
                          width: 4,
                        ),
                        Expanded(
                            child: CustomButton(
                          text: AppLocalizations.of(context)!
                              .translate('title_menu_lunch'),
                          icon: Icons.local_pizza,
                          height: 32,
                          iconColor: primaryColor,
                          textColor: primaryColor,
                          background: backgroundScreenColor,
                          press: () {
                            StorePreferences.setFoodName(food_lunch);
                          },
                        )),
                        Container(
                          width: 4,
                        ),
                        Expanded(
                            child: CustomButton(
                          text: AppLocalizations.of(context)!
                              .translate('title_menu_dinner'),
                          icon: Icons.dinner_dining_sharp,
                          height: 32,
                          iconColor: primaryColor,
                          textColor: primaryColor,
                          background: backgroundScreenColor,
                          press: () {
                            StorePreferences.setFoodName(food_dinner);
                          },
                        )),
                        Container(
                          width: 4,
                        ),
                        Expanded(
                            child: CustomButton(
                          text: AppLocalizations.of(context)!
                              .translate('title_menu_snacks'),
                          icon: Icons.fastfood,
                          height: 32,
                          iconColor: primaryColor,
                          textColor: primaryColor,
                          background: backgroundScreenColor,
                          press: () {
                            StorePreferences.setFoodName(food_snacks);
                          },
                        )),
                        Container(
                          width: 8,
                        ),
                      ],
                    ),
                  ))
            ],
          ),
        ));
  }
}
