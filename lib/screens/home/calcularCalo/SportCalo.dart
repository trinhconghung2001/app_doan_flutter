import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/screens/home/calcularCalo/components/ItemFood.dart';
import 'package:app_doan_flutter/screens/home/calcularCalo/data/Sport.dart';
import 'package:app_doan_flutter/untils/FutureSport.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/inputform/InputTextField.dart';
import '../../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../config/styles/Dimens.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureCalo.dart';
import '../../../untils/StorePreferences.dart';
import 'data/CaloDay.dart';

class SportCalo extends StatefulWidget {
  const SportCalo({super.key});

  @override
  State<SportCalo> createState() => _SportCaloState();
}

class _SportCaloState extends State<SportCalo> {
  FocusNode focusNode = FocusNode();
  TextEditingController textEditingController = TextEditingController();
  late Future<void> _loadDataFuture;
  List<Sport> listSport = [];
  @override
  void initState() {
    _loadDataFuture = loadDataOne();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDataFuture = loadDataOne();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (() {
        FocusManager.instance.primaryFocus?.unfocus();
      }),
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: AppLocalizations.of(context)!.translate("app_bar_sport_calo"),
            check: false,
            onPress: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Container(
            child: Column(children: [
          Container(
            margin: const EdgeInsets.only(left: 16, right: 16, top: 16),
            height: 50,
            child: TextField(
                maxLines: 1,
                style: const TextStyle(
                  color: Color(0xFF444444),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
                onChanged: (value) {
                  searchList(value);
                },
                focusNode: focusNode,
                controller: textEditingController,
                decoration: InputDecoration(
                  hintText: AppLocalizations.of(context)!
                      .translate('hint_title_sport'),
                  contentPadding: const EdgeInsets.only(
                      left: defaultTextInsideBoxPadding, right: 0),
                  fillColor: statusColor,
                  // enabledBorder: const OutlineInputBorder(
                  //   borderSide: BorderSide(color: Color(0xFFE0E0E0), width: 1.0),
                  // ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  suffixIcon: Visibility(
                    visible: focusNode.hasFocus == true &&
                            textEditingController.text.isNotEmpty
                        ? true
                        : false,
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          textEditingController.clear;
                          searchList('');
                        });
                      },
                      child: const Icon(
                        Icons.cancel_outlined,
                        color: Color(0xFFBBC2C6),
                      ),
                    ),
                  ),
                  suffixIconConstraints: const BoxConstraints(
                    minWidth: 41,
                    minHeight: 25,
                  ),
                )),
          ),
          SizedBox(
            height: 8,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 16),
            child: FutureBuilder(
              future: _loadDataFuture,
              builder: ((context, AsyncSnapshot snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return CircularProgressIndicatorWidget();
                } else if (snapshot.hasError) {
                  return EmptyData(
                      msg: AppLocalizations.of(context)!
                          .translate('error_data_msg'));
                } else {
                  if (listSport.length > 0) {
                    return ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: listSport.length,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: ((context) {
                                    return PopupAddSport(
                                      baiTap: listSport[index],
                                      typeAddSport: false,
                                    );
                                  })).then((value) => loadScreen());
                            },
                            child: listSport[index].edit == true
                                ? ItemFood(
                                    onPress: () {
                                      showDialog(
                                          context: context,
                                          builder: ((context) {
                                            return PopupEdit(
                                                baiTap: listSport[index]);
                                          })).then((value) => loadScreen());
                                    },
                                    icon: Icons.edit_document,
                                    ten: listSport[index].ten,
                                    khoiLuong: listSport[index].soPhut,
                                    donVi: listSport[index].donVi,
                                    calo: listSport[index].calo)
                                : ItemFood(
                                    onPress: () {},
                                    showIcon: true,
                                    ten: listSport[index].ten,
                                    khoiLuong: listSport[index].soPhut,
                                    donVi: listSport[index].donVi,
                                    calo: listSport[index].calo),
                          );
                        });
                  } else {
                    return EmptyData();
                  }
                }
              }),
            ),
          )
        ])),
        bottomNavigationBar: BottomNavigator(
          widget: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                width: 16,
              ),
              Expanded(
                  child: CustomButton(
                text: AppLocalizations.of(context)!
                    .translate('button_add_sport_user_calo'),
                height: 32,
                icon: Icons.sports_cricket,
                press: () {
                  showDialog(
                      context: context,
                      builder: ((context) {
                        return PopupAdd();
                      })).then((value) => loadScreen());
                },
              )),
              Container(
                width: 16,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void searchList(String value) {
    setState(() {
      textEditingController.text = value;
      _loadDataFuture = loadDataSearch(value);
    });
  }

  Future<void> loadDataSearch(String value) async {
    List<Sport> listSportCategory = await readCategorySport();
    List<Sport> listSportUser =
        await readCategorySportUser(StorePreferences.getIdUser());
    List<Sport> listAll = listSportCategory + listSportUser;
    listSport = listAll
        .where((element) =>
            element.ten.toLowerCase().contains(value.trim().toLowerCase()))
        .toList();
  }

  Future<void> loadDataOne() async {
    List<Sport> listSportCategory = await readCategorySport();
    List<Sport> listSportUser =
        await readCategorySportUser(StorePreferences.getIdUser());
    listSport = listSportCategory + listSportUser;
  }
}

class PopupAddSport extends StatefulWidget {
  final Sport baiTap;
  final bool typeAddSport;
  const PopupAddSport(
      {super.key, required this.baiTap, required this.typeAddSport});

  @override
  State<PopupAddSport> createState() => _PopupAddSportState();
}

class _PopupAddSportState extends State<PopupAddSport> {
  FocusNode focusNode = FocusNode();
  TextEditingController textEditingController = TextEditingController();
  String ten = '';
  double thoiGian = 0.0;
  String donVi = '';
  double calo = 0.00;
  double tiLe = 0.00;
  double textThoiGian = 0.0;
  double textCalo = 0.00;
  @override
  void initState() {
    ten = widget.baiTap.ten;
    textThoiGian = (double.parse(widget.baiTap.soPhut));
    thoiGian = double.parse(widget.baiTap.soPhut);
    donVi = widget.baiTap.donVi;
    textCalo = double.parse(widget.baiTap.calo);
    calo = double.parse(widget.baiTap.calo);
    tiLe = calo / thoiGian;
    textEditingController.text = formatValue(textThoiGian);
    super.initState();
  }

  String formatValue(double value) {
    if (value == value.toInt()) {
      return value
          .toInt()
          .toString(); // Nếu là số nguyên, chuyển thành chuỗi và trả về
    } else {
      return value
          .toString(); // Nếu không phải là số nguyên, giữ nguyên giá trị và chuyển thành chuỗi
    }
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        child: Container(
          padding: EdgeInsets.all(defaultPadding),
          width: MediaQuery.of(context).size.width / 1.2,
          height: widget.baiTap.edit == true && widget.typeAddSport == false
              ? MediaQuery.of(context).size.height * 0.29
              : MediaQuery.of(context).size.height * 0.23,
          child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
            Text(
              ten,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
                '${formatValue(textThoiGian)} ${donVi} - ${formatValue(double.parse(textCalo.toStringAsFixed(2)))} calo'),
            const SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                    child: GestureDetector(
                  onTap: () {
                    setState(() {
                      textThoiGian = textThoiGian - 1;
                      textCalo = (textThoiGian * tiLe);
                      textEditingController.text = formatValue(textThoiGian);
                    });
                  },
                  child: Icon(
                    Icons.remove_circle,
                    color: backgroudSearch,
                  ),
                )),
                Expanded(
                    child: Container(
                  alignment: Alignment.center,
                  height: 36,
                  decoration: BoxDecoration(
                      color: backgroudSearch,
                      borderRadius: BorderRadius.all(Radius.circular(7)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 2,
                          offset:
                              const Offset(0, 2), // changes position of shadow
                        ),
                      ]),
                  child: TextField(
                      maxLines: 1,
                      style: const TextStyle(
                        color: backgroundScreenColor,
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                      ),
                      onChanged: (value) {
                        setState(() {
                          textThoiGian = double.parse(value);
                          textCalo = (textThoiGian * tiLe);
                        });
                        print(textEditingController.text);
                      },
                      focusNode: focusNode,
                      controller: textEditingController,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        fillColor: primaryColor,
                        contentPadding: EdgeInsets.symmetric(vertical: 5),
                        enabledBorder: const OutlineInputBorder(
                          borderSide:
                              BorderSide(color: backgroudSearch, width: 1.0),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                        ),
                      )),
                )),
                Expanded(
                    child: GestureDetector(
                  onTap: () {
                    setState(() {
                      textThoiGian = textThoiGian + 1;
                      textCalo = (textThoiGian * tiLe);
                      textEditingController.text = formatValue(textThoiGian);
                    });
                  },
                  child: Icon(
                    Icons.add_circle,
                    color: backgroudSearch,
                  ),
                ))
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            CustomButton(
              text: AppLocalizations.of(context)!
                  .translate('button_add_sport_calo'),
              height: 32,
              icon: Icons.add,
              press: () {
                if (widget.typeAddSport == false) {
                  addSport();
                } else {
                  editSport();
                }
              },
            ),
            Visibility(
              visible: widget.baiTap.edit && widget.typeAddSport == false,
              child: const SizedBox(
                height: 16,
              ),
            ),
            Visibility(
              visible: widget.baiTap.edit && widget.typeAddSport == false,
              child: CustomButton(
                text: AppLocalizations.of(context)!
                    .translate('button_remove_sport'),
                height: 32,
                icon: Icons.delete_forever_rounded,
                press: () {
                  removeSportUser(widget.baiTap);
                },
              ),
            )
          ]),
        ));
  }

  Future<void> removeSportUser(Sport baiTap) async {
    List<Sport> listSportUser =
        await readCategorySportUser(StorePreferences.getIdUser());
    for (int i = 0; i < listSportUser.length; i++) {
      if (listSportUser[i].id == baiTap.id) {
        listSportUser.removeAt(i);
        break;
      }
    }
    try {
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('Sport')
          .doc(StorePreferences.getIdUser());
      await documentReference
          .set({'sport': listSportUser.map((e) => e.toMap())}).then((value) {
        ToastCommon.showToast(AppLocalizations.of(context)!
            .translate('toast_remove_sport_success'));
        Navigator.pop(context);
      });
    } catch (e) {
      // TODO
      showDialog(
          context: context,
          builder: ((context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_remove_sport_faild'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          }));
    }
  }

  Future<void> addSport() async {
    try {
      Map<String, dynamic> result =
          await readCalo(StorePreferences.getIdFamily());
      CaloDay calo = CaloDay.fromMap(result[StorePreferences.getDate()]);
      List<Sport> list = [];
      //them bai tap
      list = calo.theThao;
      Sport sport = Sport(
          id: DateTime.now().toString(),
          edit: widget.baiTap.edit,
          ten: widget.baiTap.ten,
          soPhut: formatValue(textThoiGian),
          calo: formatValue(textCalo),
          donVi: widget.baiTap.donVi);
      list.add(sport);
      calo.theThao = list;
      //them calo tieu hao
      double caloTieuHao =
          double.parse(calo.caloTieuHao) + double.parse(sport.calo);
      calo.caloTieuHao = caloTieuHao.toStringAsFixed(2);
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('Calo')
          .doc(StorePreferences.getIdFamily());
      documentReference
          .update({StorePreferences.getDate(): calo.toMap()}).then((value) {
        ToastCommon.showToast(
            AppLocalizations.of(context)!.translate('toast_add_sport_success'));
        Navigator.pop(context);
      });
    } catch (e) {
      showDialog(
          context: context,
          builder: ((context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_add_sport_faild'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          }));
    }
  }

  Future<void> editSport() async {
    try {
      Map<String, dynamic> result =
          await readCalo(StorePreferences.getIdFamily());
      print('date: ${StorePreferences.getDate()}');
      CaloDay calo = CaloDay.fromMap(result[StorePreferences.getDate()]);
      List<Sport> list = [];
      //them bai tap
      list = calo.theThao;
      Sport sport = Sport(
          id: widget.baiTap.id,
          ten: widget.baiTap.ten,
          edit: widget.baiTap.edit,
          soPhut: formatValue(textThoiGian),
          calo: formatValue(double.parse(textCalo.toStringAsFixed(2))),
          donVi: widget.baiTap.donVi);
      for (Sport e in list) {
        if (e.id == widget.baiTap.id) {
          list.remove(e);
          break;
        }
      }
      list.add(sport);
      calo.theThao = list;
      //them calo tieu hao
      double caloTieuHao = double.parse(calo.caloTieuHao) +
          double.parse(sport.calo) -
          double.parse(widget.baiTap.calo);
      calo.caloTieuHao = caloTieuHao.toStringAsFixed(2);
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('Calo')
          .doc(StorePreferences.getIdFamily());
      documentReference
          .update({StorePreferences.getDate(): calo.toMap()}).then((value) {
        ToastCommon.showToast(AppLocalizations.of(context)!
            .translate('toast_edit_sport_success'));
        Navigator.pop(context);
      });
    } catch (e) {
      showDialog(
          context: context,
          builder: ((context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_edit_sport_faild'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          }));
    }
  }
}

class PopupAdd extends StatefulWidget {
  const PopupAdd({super.key});

  @override
  State<PopupAdd> createState() => _PopupAddState();
}

class _PopupAddState extends State<PopupAdd> {
  TextEditingController textTen = TextEditingController();
  TextEditingController textPhut = TextEditingController();
  TextEditingController textCalo = TextEditingController();
  FocusNode focusTen = FocusNode();
  FocusNode focusPhut = FocusNode();
  FocusNode focusCalo = FocusNode();
  bool validate = false;
  @override
  void initState() {
    focusTen.addListener(_onFocusChange);
    focusPhut.addListener(_onFocusChange);
    focusCalo.addListener(_onFocusChange);
    super.initState();
  }

  void _onFocusChange() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        child: Container(
          padding: EdgeInsets.all(defaultPadding),
          width: MediaQuery.of(context).size.width / 1.2,
          height: MediaQuery.of(context).size.height * 0.28,
          child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
            InputTextField(
              title:
                  AppLocalizations.of(context)!.translate('title_name_sport'),
              checkRangBuoc: true,
              focusNode: focusTen,
              textController: textTen,
              onPress: () {
                setState(() {
                  textTen.clear();
                });
              },
              isError: validate && textTen.text.isEmpty,
            ),
            const SizedBox(
              height: 16,
            ),
            InputTextField(
              title:
                  AppLocalizations.of(context)!.translate('title_time_minute'),
              checkRangBuoc: true,
              focusNode: focusPhut,
              textController: textPhut,
              onPress: () {
                setState(() {
                  textPhut.clear();
                });
              },
              isError: validate && textPhut.text.isEmpty,
            ),
            const SizedBox(
              height: 16,
            ),
            InputTextField(
              title: AppLocalizations.of(context)!
                  .translate('title_calo_consumed'),
              checkRangBuoc: true,
              focusNode: focusCalo,
              textController: textCalo,
              onPress: () {
                setState(() {
                  textCalo.clear();
                });
              },
              isError: validate && textCalo.text.isEmpty,
            ),
            const SizedBox(
              height: 16,
            ),
            CustomButton(
              text: AppLocalizations.of(context)!
                  .translate('button_add_sport_calo'),
              height: 32,
              icon: Icons.add,
              press: () {
                checkValidate();
                if (validate == false) {
                  addSportUser().then((value) => Navigator.pop(context));
                }
              },
            )
          ]),
        ));
  }

  void checkValidate() {
    if (textTen.text.isNotEmpty &&
        textPhut.text.isNotEmpty &&
        textCalo.text.isNotEmpty) {
      setState(() {
        validate = false;
      });
    } else {
      setState(() {
        validate = true;
      });
    }
  }

  Future<void> addSportUser() async {
    List<Sport> listSport =
        await readCategorySportUser(StorePreferences.getIdUser());
    String id = DateTime.now().toString();
    Sport sportNew = Sport(
        id: id,
        edit: true,
        ten: textTen.text,
        soPhut: textPhut.text,
        calo: textCalo.text,
        donVi: 'phút');
    if (listSport.any((element) =>
            element.ten == sportNew.ten &&
            element.soPhut == sportNew.soPhut &&
            element.calo == sportNew.calo &&
            element.donVi == sportNew.donVi) ==
        false) {
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('Sport')
          .doc(StorePreferences.getIdUser());
      DocumentSnapshot documentSnapshot = await documentReference.get();
      if (documentSnapshot.exists) {
        await documentReference.update({
          'sport': FieldValue.arrayUnion([sportNew.toMap()])
        }).then((value) {
          ToastCommon.showToast(AppLocalizations.of(context)!
              .translate('toast_add_sport_success'));
        });
      } else {
        listSport.add(sportNew);
        await documentReference
            .set({'sport': listSport.map((e) => e.toMap())}).then((value) {
          ToastCommon.showToast(AppLocalizations.of(context)!
              .translate('toast_add_sport_success'));
        });
      }
    } else {
      showDialog(
          context: context,
          builder: (context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_add_sport_duplicate'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          });
    }
  }
}

class PopupEdit extends StatefulWidget {
  final Sport baiTap;
  const PopupEdit({super.key, required this.baiTap});

  @override
  State<PopupEdit> createState() => _PopupEditState();
}

class _PopupEditState extends State<PopupEdit> {
  TextEditingController textTen = TextEditingController();
  TextEditingController textPhut = TextEditingController();
  TextEditingController textCalo = TextEditingController();
  FocusNode focusTen = FocusNode();
  FocusNode focusPhut = FocusNode();
  FocusNode focusCalo = FocusNode();
  bool validate = false;
  @override
  void initState() {
    textTen.text = widget.baiTap.ten;
    textPhut.text = widget.baiTap.soPhut;
    textCalo.text = widget.baiTap.calo;
    focusTen.addListener(_onFocusChange);
    focusPhut.addListener(_onFocusChange);
    focusCalo.addListener(_onFocusChange);
    super.initState();
  }

  void _onFocusChange() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        child: Container(
          padding: EdgeInsets.all(defaultPadding),
          width: MediaQuery.of(context).size.width / 1.2,
          height: MediaQuery.of(context).size.height * 0.28,
          child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
            InputTextField(
              title:
                  AppLocalizations.of(context)!.translate('title_name_sport'),
              checkRangBuoc: true,
              focusNode: focusTen,
              textController: textTen,
              onPress: () {
                setState(() {
                  textTen.clear();
                });
              },
              isError: validate && textTen.text.isEmpty,
            ),
            const SizedBox(
              height: 16,
            ),
            InputTextField(
              title:
                  AppLocalizations.of(context)!.translate('title_time_minute'),
              checkRangBuoc: true,
              focusNode: focusPhut,
              textController: textPhut,
              onPress: () {
                setState(() {
                  textPhut.clear();
                });
              },
              isError: validate && textPhut.text.isEmpty,
            ),
            const SizedBox(
              height: 16,
            ),
            InputTextField(
              title: AppLocalizations.of(context)!
                  .translate('title_calo_consumed'),
              checkRangBuoc: true,
              focusNode: focusCalo,
              textController: textCalo,
              onPress: () {
                setState(() {
                  textCalo.clear();
                });
              },
              isError: validate && textCalo.text.isEmpty,
            ),
            const SizedBox(
              height: 16,
            ),
            CustomButton(
              text:
                  AppLocalizations.of(context)!.translate('button_edit_sport'),
              height: 32,
              icon: Icons.edit_document,
              press: () {
                checkValidate();
                if (validate == false) {
                  editSportUser().then((value) => Navigator.pop(context));
                }
              },
            )
          ]),
        ));
  }

  void checkValidate() {
    if (textTen.text.isNotEmpty &&
        textPhut.text.isNotEmpty &&
        textCalo.text.isNotEmpty) {
      setState(() {
        validate = false;
      });
    } else {
      setState(() {
        validate = true;
      });
    }
  }

  Future<void> editSportUser() async {
    List<Sport> listSport =
        await readCategorySportUser(StorePreferences.getIdUser());
    Sport sportNew = Sport(
        id: widget.baiTap.id,
        edit: widget.baiTap.edit,
        ten: textTen.text,
        soPhut: textPhut.text,
        calo: textCalo.text,
        donVi: widget.baiTap.donVi);
    for (int i = 0; i < listSport.length; i++) {
      if (listSport[i].id == widget.baiTap.id) {
        listSport.removeAt(i);
      }
    }
    if (listSport.any((element) =>
            element.ten == sportNew.ten &&
            element.soPhut == sportNew.soPhut &&
            element.calo == sportNew.calo &&
            element.donVi == sportNew.donVi) ==
        false) {
      listSport.add(sportNew);
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('Sport')
          .doc(StorePreferences.getIdUser());
      await documentReference
          .set({'sport': listSport.map((e) => e.toMap())}).then((value) {
        ToastCommon.showToast(
            AppLocalizations.of(context)!.translate('toast_add_sport_success'));
      });
    } else {
      showDialog(
          context: context,
          builder: (context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_add_sport_duplicate'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          });
    }
  }
}
