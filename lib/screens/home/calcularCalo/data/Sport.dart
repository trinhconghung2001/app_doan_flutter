class Sport {
  String id;
  String ten;
  String soPhut;
  String calo;
  String donVi;
  bool edit;

  factory Sport.defaultContructor() {
    return Sport(
      id: '',
      ten: '',
      soPhut: '',
      calo: '',
      donVi: '',
      edit: true,
    );
  }
  Sport({
    required this.id,
    required this.ten,
    required this.soPhut,
    required this.calo,
    required this.donVi,
    required this.edit,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'ten': ten,
      'soPhut': soPhut,
      'calo': calo,
      'donVi': donVi,
      'edit': edit
    };
  }

  factory Sport.fromMap(Map<String, dynamic> map) {
    return Sport(
        id: map['id'],
        ten: map['ten'],
        soPhut: map['soPhut'],
        donVi: map['donVi'],
        edit: map['edit'],
        calo: map['calo']);
  }
}
