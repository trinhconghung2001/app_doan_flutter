class Food {
  String id;
  String ten;
  String chatBeo;
  String tinhBot;
  String chatDam;
  String khoiLuong;
  String calo;
  bool edit;
  String donVi;
  String doiDonVi;

  factory Food.defaultContructor() {
    return Food(
        id: '',
        ten: '',
        chatBeo: '',
        tinhBot: '',
        chatDam: '',
        khoiLuong: '',
        calo: '',
        edit: true,
        doiDonVi: '',
        donVi: '');
  }
  Food({
    required this.id,
    required this.ten,
    required this.chatBeo,
    required this.tinhBot,
    required this.chatDam,
    required this.khoiLuong,
    required this.calo,
    required this.edit,
    required this.donVi,
    required this.doiDonVi,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'ten': ten,
      'chatBeo': chatBeo,
      'tinhBot': tinhBot,
      'chatDam': chatDam,
      'khoiLuong': khoiLuong,
      'calo': calo,
      'edit': edit,
      'donVi': donVi,
      'doiDonVi': doiDonVi,
    };
  }

  factory Food.fromMap(Map<String, dynamic> map) {
    return Food(
        id: map['id'],
        ten: map['ten'],
        chatBeo: map['chatBeo'],
        tinhBot: map['tinhBot'],
        chatDam: map['chatDam'],
        khoiLuong: map['khoiLuong'],
        calo: map['calo'],
        edit: map['edit'],
        doiDonVi: map['doiDonVi'],
        donVi: map['donVi']);
  }
  
}
