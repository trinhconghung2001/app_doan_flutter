import 'package:app_doan_flutter/screens/home/calcularCalo/data/Sport.dart';

import 'Food.dart';

class CaloDay {
  String caloCan;
  String caloNap;
  String caloTieuHao;
  String chatBeos;
  String chatDams;
  String tinhBots;
  String carbsDay;
  String proteinDay;
  String fatDay;
  List<Food> buaSang;
  List<Food> buaTrua;
  List<Food> buaToi;
  List<Food> buaPhu;
  List<Sport> theThao;

  factory CaloDay.defaultContructor() {
    return CaloDay(
        caloCan: '',
        caloNap: '',
        caloTieuHao: '',
        chatBeos: '',
        chatDams: '',
        tinhBots: '',
        carbsDay: '',
        proteinDay: '',
        fatDay: '',
        buaSang: [],
        buaTrua: [],
        buaToi: [],
        buaPhu: [],
        theThao: []);
  }

  CaloDay({
    required this.caloCan,
    required this.caloNap,
    required this.caloTieuHao,
    required this.chatBeos,
    required this.chatDams,
    required this.carbsDay,
    required this.proteinDay,
    required this.fatDay,
    required this.tinhBots,
    required this.buaSang,
    required this.buaTrua,
    required this.buaToi,
    required this.buaPhu,
    required this.theThao,
  });

  Map<String, dynamic> toMap() {
    return {
      'caloCan': caloCan,
      'caloNap': caloNap,
      'caloTieuHao': caloTieuHao,
      'chatBeos': chatBeos,
      'chatDams': chatDams,
      'tinhBots': tinhBots,
      'carbsDay': carbsDay,
      'proteinDay': proteinDay,
      'fatDay': fatDay,
      'buaSang': buaSang.map((e) => e.toMap()).toList(),
      'buaTrua': buaTrua.map((e) => e.toMap()).toList(),
      'buaToi': buaToi.map((e) => e.toMap()).toList(),
      'buaPhu': buaPhu.map((e) => e.toMap()).toList(),
      'theThao':theThao.map((e) => e.toMap()).toList(),
    };
  }

  factory CaloDay.fromMap(Map<String, dynamic> map) {
    final List<dynamic> listSang = map['buaSang'];
    final List<dynamic> listTrua = map['buaTrua'];
    final List<dynamic> listToi = map['buaToi'];
    final List<dynamic> listPhu = map['buaPhu'];
    final List<dynamic> listTheThao = map['theThao'];
    List<Food> listBuaSang = listSang.map((e) => Food.fromMap(e)).toList();
    List<Food> listBuaTrua = listTrua.map((e) => Food.fromMap(e)).toList();
    List<Food> listBuaToi = listToi.map((e) => Food.fromMap(e)).toList();
    List<Food> listBuaPhu = listPhu.map((e) => Food.fromMap(e)).toList();
    List<Sport> listSport = listTheThao.map((e) => Sport.fromMap(e)).toList();
    return CaloDay(
        caloCan: map['caloCan'],
        caloNap: map['caloNap'],
        caloTieuHao: map['caloTieuHao'],
        chatBeos: map['chatBeos'],
        chatDams: map['chatDams'],
        tinhBots: map['tinhBots'],
        carbsDay: map['carbsDay'],
        proteinDay: map['proteinDay'],
        fatDay: map['fatDay'],
        buaSang: listBuaSang,
        buaTrua: listBuaTrua,
        buaToi: listBuaToi,
        buaPhu: listBuaPhu,
        theThao: listSport);
  }
}
