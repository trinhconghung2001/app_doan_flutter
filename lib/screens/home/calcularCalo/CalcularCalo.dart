// ignore_for_file: must_be_immutable

import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/home/calcularCalo/AccountCalo.dart';
import 'package:app_doan_flutter/screens/home/calcularCalo/FoodCalo.dart';
import 'package:app_doan_flutter/screens/home/calcularCalo/SportCalo.dart';
import 'package:app_doan_flutter/screens/home/calcularCalo/components/ItemFood.dart';
import 'package:app_doan_flutter/screens/home/calcularCalo/data/CaloDay.dart';
import 'package:app_doan_flutter/screens/home/calcularCalo/data/Sport.dart';
import 'package:app_doan_flutter/untils/FutureCalo.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/percent_indicator.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/filtersearch/RowFilter.dart';
import '../../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../untils/AppLocalizations.dart';
import '../../category/ListIdCategory.dart';
import 'FoodDetail.dart';
import 'UpdateBMI.dart';
import 'data/Food.dart';

class CalcularCalo extends StatefulWidget {
  const CalcularCalo({
    super.key,
  });

  @override
  State<CalcularCalo> createState() => _CalcularCaloState();
}

class _CalcularCaloState extends State<CalcularCalo> {
  String date = '';
  @override
  void initState() {
    date = StorePreferences.getDate();
    StorePreferences.setIdFamily(StorePreferences.getIdUser());
    super.initState();
  }

  String moiQH = 'Bản thân';
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(50),
              child: CustomAppBar(
                text: AppLocalizations.of(context)!
                    .translate('app_bar_calcular_calo'),
                check: true,
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AccountCalo(
                            idAccount: StorePreferences.getIdFamily()),
                      )).then((value) {
                    loadDataDate(StorePreferences.getDate());
                  });
                },
                icon: Icon(Icons.account_circle),
                checkIconBack: false,
                onPress: () {
                  Navigator.pop(context);
                },
              ),
            ),
            body: Scaffold(
              body: ListView.builder(
                  itemCount: 2,
                  itemBuilder: (BuildContext context, index) {
                    if (index == 0) {
                      return FilterUser(
                        dataSearch: searchData,
                        moiQH: moiQH,
                      );
                    } else {
                      print('vao lai');
                      print(
                          "CacularCalo -> idFamily: ${StorePreferences.getIdFamily()}");
                      return Column(
                        children: [
                          BodyCalo(
                            dataDate: loadDataDate,
                          ),
                          BodyFood(
                            dateData: date,
                            date: loadDataDate,
                          ),
                        ],
                      );
                    }
                  }),
              bottomNavigationBar: BottomNavigator(
                widget: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: 16,
                    ),
                    Expanded(
                        child: CustomButton(
                      text: AppLocalizations.of(context)!
                          .translate('button_food'),
                      height: 32,
                      icon: Icons.fastfood_outlined,
                      press: () {
                        Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FoodCalo()))
                            .then((value) {
                          loadDataDate(StorePreferences.getDate());
                        });
                      },
                    )),
                    Container(
                      width: 8,
                    ),
                    Expanded(
                        child: CustomButton(
                      text: AppLocalizations.of(context)!
                          .translate('button_sport_calo'),
                      icon: Icons.sports_handball_outlined,
                      height: 32,
                      press: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => SportCalo(),
                            )).then((value) {
                          loadDataDate(StorePreferences.getDate());
                        });
                      },
                    )),
                    Container(
                      width: 16,
                    ),
                  ],
                ),
              ),
            )));
  }

  void searchData(String text) {
    setState(() {
      moiQH = text;
    });
  }

  void loadDataDate(String dateData) {
    setState(() {
      date = dateData;
    });
  }
}

class FilterUser extends StatefulWidget {
  final void Function(String list) dataSearch;
  String moiQH;
  FilterUser({super.key, required this.dataSearch, required this.moiQH});

  @override
  State<FilterUser> createState() => _FilterUserState();
}

class _FilterUserState extends State<FilterUser> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecorationContainer(color: filterColor),
        margin: const EdgeInsets.only(top: 8, left: 16, right: 16),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RowFilter(
              title: AppLocalizations.of(context)!
                  .translate('account_title_relationship'),
              value: widget.moiQH,
              icon: Icons.arrow_drop_down,
              onPress: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ListIdCategory(
                              loai: defaultHoSo,
                              titleAppbar: AppLocalizations.of(context)!
                                  .translate('category_title_relationship'),
                            ))).then((value) {
                  if (value != null) {
                    setState(() {
                      widget.moiQH = value[0];
                      StorePreferences.setIdFamily(value[1]);
                    });
                    widget.dataSearch(widget.moiQH);
                  }
                });
              },
            ),
          ],
        ));
  }
}

class BodyCalo extends StatefulWidget {
  final void Function(String date) dataDate;
  const BodyCalo({super.key, required this.dataDate});

  @override
  State<BodyCalo> createState() => _BodyCaloState();
}

class _BodyCaloState extends State<BodyCalo> {
  late Future<void> _loadBodyCalo;
  DateTime date = DateTime.now();
  String textDate = '';
  Map<String, dynamic> resultCalo = {};
  CaloDay calo = CaloDay.defaultContructor();

  @override
  void initState() {
    StorePreferences.setDate(date);
    _loadBodyCalo = loadDataBodyCalo(StorePreferences.getIdFamily());
    super.initState();
  }

  void loadData() {
    setState(() {
      _loadBodyCalo = loadDataBodyCalo(StorePreferences.getIdFamily());
    });
  }

  @override
  Widget build(BuildContext context) {
    loadData();
    print("BodyCalo => idFamily: ${StorePreferences.getIdFamily()}");
    return Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecorationContainer(color: filterColor),
        margin: const EdgeInsets.only(top: 8, left: 16, right: 16),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: FutureBuilder(
            future: _loadBodyCalo,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return CircularProgressIndicatorWidget();
              } else if (snapshot.hasError) {
                return EmptyData(
                    scale: 0.1,
                    msg: AppLocalizations.of(context)!
                        .translate('error_data_msg'));
              } else {
                return Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text(
                              textDateDifference(date),
                              style: const TextStyle(
                                  fontSize: 14, color: statusColor),
                            ),
                          ),
                          Row(
                            children: [
                              GestureDetector(
                                onTap: (() {
                                  setState(() {
                                    date = date.subtract(Duration(days: 1));
                                    StorePreferences.setDate(date);
                                    loadData();
                                    // _loadBodyCalo = loadDataBodyCalo(
                                    //     StorePreferences.getIdFamily());
                                  });
                                  widget.dataDate(StorePreferences.getDate());
                                }),
                                child: Icon(
                                  Icons.navigate_before,
                                  color: statusColor,
                                  size: 32,
                                ),
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                              GestureDetector(
                                onTap: () {
                                  _show();
                                },
                                child: Icon(
                                  Icons.calendar_today,
                                  color: statusColor,
                                ),
                              ),
                              const SizedBox(
                                width: 16,
                              ),
                              Text(
                                DateFormat('dd-MM-yyyy').format(date),
                                style: const TextStyle(
                                  fontSize: 14,
                                  color: statusColor,
                                ),
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                              GestureDetector(
                                onTap: (() {
                                  setState(() {
                                    date = date.add(Duration(days: 1));
                                    StorePreferences.setDate(date);
                                    // _loadBodyCalo = loadDataBodyCalo(
                                    //     StorePreferences.getIdFamily());
                                  });
                                  loadData();
                                  widget.dataDate(StorePreferences.getDate());
                                }),
                                child: Icon(
                                  Icons.navigate_next,
                                  color: statusColor,
                                  size: 32,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  '${calo.caloNap}',
                                  style: TextStyle(
                                      fontSize: 14, color: statusColor),
                                ),
                                Text(
                                    AppLocalizations.of(context)!
                                        .translate('calorie_intake'),
                                    style: TextStyle(
                                        fontSize: 14, color: statusColor))
                              ],
                            ),
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          Expanded(
                              flex: 2,
                              child: Container(
                                width: 150,
                                height: 150,
                                child: Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Container(
                                        height: 40,
                                        child: Column(
                                          children: [
                                            Text('${calo.caloCan}',
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: statusColor)),
                                            Text(
                                                AppLocalizations.of(context)!
                                                    .translate(
                                                        'calories_needed'),
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: statusColor)),
                                          ],
                                        ),
                                      ),
                                      PieChart(
                                          swapAnimationDuration:
                                              Duration(milliseconds: 500),
                                          swapAnimationCurve:
                                              Curves.easeInOutQuint,
                                          PieChartData(
                                              startDegreeOffset: 270,
                                              sections: [
                                                PieChartSectionData(
                                                    radius: 20,
                                                    value: double.parse(
                                                            calo.caloNap) -
                                                        double.parse(
                                                            calo.caloTieuHao),
                                                    title: (double.parse(
                                                                calo.caloNap) -
                                                            double.parse(calo
                                                                .caloTieuHao))
                                                        .toString(),
                                                    titleStyle: TextStyle(
                                                        fontSize: 12,
                                                        color:
                                                            backgroundScreenColor,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                    color: statusColor),
                                                PieChartSectionData(
                                                    radius: 20,
                                                    value: double.parse((double.parse(calo.caloCan) -
                                                            double.parse(
                                                                calo.caloNap) +
                                                            double.parse(calo
                                                                .caloTieuHao))
                                                        .toStringAsFixed(2)),
                                                    title: (double.parse(calo.caloCan) -
                                                            double.parse(
                                                                calo.caloNap) +
                                                            double.parse(calo
                                                                .caloTieuHao))
                                                        .toStringAsFixed(2),
                                                    titleStyle: TextStyle(
                                                        fontSize: 12,
                                                        color: backgroundScreenColor,
                                                        fontWeight: FontWeight.bold),
                                                    color: primaryColor)
                                              ],
                                              borderData: FlBorderData(
                                                  show: true,
                                                  border: Border.all(
                                                      width: 2,
                                                      color: warningColor)),
                                              centerSpaceRadius: 60)),
                                    ]),
                              )),
                          const SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                Text('${calo.caloTieuHao}',
                                    style: TextStyle(
                                        fontSize: 14, color: statusColor)),
                                Text(
                                    AppLocalizations.of(context)!
                                        .translate('consume_calories'),
                                    style: TextStyle(
                                        fontSize: 14, color: statusColor))
                              ],
                            ),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  AppLocalizations.of(context)!
                                      .translate('carbs'),
                                  style: TextStyle(
                                      fontSize: 14, color: statusColor),
                                ),
                                widgetBelow(double.parse(calo.tinhBots),
                                    double.parse(calo.carbsDay)),
                                Text('${calo.tinhBots}/${calo.carbsDay}',
                                    style: TextStyle(
                                        fontSize: 14, color: statusColor))
                              ],
                            ),
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            flex: 2,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  AppLocalizations.of(context)!
                                      .translate('protein'),
                                  style: TextStyle(
                                      fontSize: 14, color: statusColor),
                                ),
                                widgetBelow(double.parse(calo.chatDams),
                                    double.parse(calo.proteinDay)),
                                Text('${calo.chatDams}/${calo.proteinDay}',
                                    style: TextStyle(
                                        fontSize: 14, color: statusColor))
                              ],
                            ),
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                Text(
                                    AppLocalizations.of(context)!
                                        .translate('fat'),
                                    style: TextStyle(
                                        fontSize: 14, color: statusColor)),
                                widgetBelow(double.parse(calo.chatBeos),
                                    double.parse(calo.fatDay)),
                                Text('${calo.chatBeos}/${calo.fatDay}',
                                    style: TextStyle(
                                        fontSize: 14, color: statusColor))
                              ],
                            ),
                          )
                        ],
                      )
                    ]);
              }
            }));
  }

  Widget widgetBelow(double a, double b) {
    return LinearPercentIndicator(
      lineHeight: 8.0,
      barRadius: Radius.circular(10),
      percent: a / b < 1
          ? a / b
          : 1, // Thay đổi giá trị này để thể hiện phần trăm khác nhau
      backgroundColor: primaryColor,
      progressColor: statusColor,
    );
  }

  void _show() async {
    final DateTime? result = await showDatePicker(
      context: context,
      initialDate: date,
      firstDate: DateTime(2024, 1, 1),
      lastDate: DateTime(2024, 12, 31),
      locale: const Locale('vi'),
    );
    if (result != null) {
      setState(() {
        date = result;
        StorePreferences.setDate(date);
      });
      loadData();

      widget.dataDate(StorePreferences.getDate());
    }
  }

  String textDateDifference(DateTime date) {
    String textDate = DateFormat('dd-MM-yyyy').format(date);
    DateTime dateTime = DateFormat("dd-MM-yyyy").parse(textDate);
    String textDateNow = DateFormat("dd-MM-yyyy").format(DateTime.now());
    DateTime dateNow = DateFormat("dd-MM-yyyy").parse(textDateNow);
    if (dateTime == dateNow) {
      return AppLocalizations.of(context)!.translate('date_now');
    }
    // Tính số ngày
    Duration differenceDay = dateTime.difference(dateNow);
    int numberOfDays = differenceDay.inDays;
    if (numberOfDays < 0) {
      numberOfDays = numberOfDays.abs();
      if (numberOfDays < 7) {
        return '${numberOfDays} ${AppLocalizations.of(context)!.translate('date_day_before')}';
      }
      int numberOfWeeks = (numberOfDays / 7).round();
      return '${numberOfWeeks} ${AppLocalizations.of(context)!.translate('date_week_befor')}';
    } else {
      if (numberOfDays < 7) {
        return '${numberOfDays} ${AppLocalizations.of(context)!.translate('date_day_after')}';
      }
      int numberOfWeeks = (numberOfDays / 7).round();
      return '${numberOfWeeks} ${AppLocalizations.of(context)!.translate('date_week_after')}';
    }
  }

  Future<void> loadDataBodyCalo(String idUser) async {
    print("Co vao loadDataBodyCalo -> id: ${idUser}");
    resultCalo = await readCalo(idUser);
    if (resultCalo.isNotEmpty) {
      String doc = StorePreferences.getDate();
      if (resultCalo[doc] != null && resultCalo[doc] is Map<String, dynamic>) {
        Map<String, dynamic> mapCalo = resultCalo[doc] as Map<String, dynamic>;
        calo = CaloDay.fromMap(mapCalo);
      } else {
        CaloDay caloNew = CaloDay(
            caloCan: resultCalo['caloDay'],
            caloNap: '0.0',
            caloTieuHao: '0.0',
            chatBeos: '0.0',
            chatDams: '0.0',
            tinhBots: '0.0',
            buaSang: [],
            buaTrua: [],
            buaToi: [],
            buaPhu: [],
            theThao: [],
            carbsDay: resultCalo['carbsDay'],
            proteinDay: resultCalo['proteinDay'],
            fatDay: resultCalo['fatDay']);
        Map<String, dynamic> caloMap = {doc: caloNew.toMap()};
        DocumentReference documentReference =
            FirebaseFirestore.instance.collection('Calo').doc(idUser);
        documentReference.update(caloMap);
      }
    } else {
      Navigator.push(
              context, MaterialPageRoute(builder: (context) => UpdateBMI()))
          .then((value) => loadData());
    }
  }
}

class BodyFood extends StatefulWidget {
  final String dateData;
  final void Function(String date) date;
  const BodyFood({super.key, required this.dateData, required this.date});

  @override
  State<BodyFood> createState() => _BodyFoodState();
}

class _BodyFoodState extends State<BodyFood> {
  late Future<void> _loadBodyFood;
  List<Food> listFoodBreakfast = [];
  List<Food> listFoodLunch = [];
  List<Food> listFoodDinner = [];
  List<Food> listFoodSnacks = [];
  List<Sport> listSport = [];
  bool checkBreakfast = false;
  bool checkLunch = false;
  bool checkDinner = false;
  bool checkSnacks = false;
  bool checkSport = false;
  @override
  Widget build(BuildContext context) {
    _loadBodyFood = loadData();
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecorationContainer(color: filterColor),
      margin: const EdgeInsets.only(top: 8, left: 16, right: 16),
      padding: const EdgeInsets.only(bottom: 16, right: 16, left: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FutureBuilder(
            future: _loadBodyFood,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return CircularProgressIndicatorWidget();
              } else if (snapshot.hasError) {
                return EmptyData(
                    scale: 0.1,
                    msg: AppLocalizations.of(context)!
                        .translate('error_data_msg'));
              } else {
                return listViewWidget(
                    listFoodBreakfast,
                    food_breakfase,
                    AppLocalizations.of(context)!
                        .translate('title_menu_breakfase'));
              }
            },
          ),
          FutureBuilder(
            future: _loadBodyFood,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return CircularProgressIndicatorWidget();
              } else if (snapshot.hasError) {
                return EmptyData(
                    scale: 0.1,
                    msg: AppLocalizations.of(context)!
                        .translate('error_data_msg'));
              } else {
                return listViewWidget(
                    listFoodLunch,
                    food_lunch,
                    AppLocalizations.of(context)!
                        .translate('title_menu_lunch'));
              }
            },
          ),
          FutureBuilder(
            future: _loadBodyFood,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return CircularProgressIndicatorWidget();
              } else if (snapshot.hasError) {
                return EmptyData(
                    scale: 0.1,
                    msg: AppLocalizations.of(context)!
                        .translate('error_data_msg'));
              } else {
                return listViewWidget(
                    listFoodDinner,
                    food_dinner,
                    AppLocalizations.of(context)!
                        .translate('title_menu_dinner'));
              }
            },
          ),
          FutureBuilder(
            future: _loadBodyFood,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return CircularProgressIndicatorWidget();
              } else if (snapshot.hasError) {
                return EmptyData(
                    scale: 0.1,
                    msg: AppLocalizations.of(context)!
                        .translate('error_data_msg'));
              } else {
                return listViewWidget(
                    listFoodSnacks,
                    food_snacks,
                    AppLocalizations.of(context)!
                        .translate('title_menu_snacks'));
              }
            },
          ),
          FutureBuilder(
            future: _loadBodyFood,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return CircularProgressIndicatorWidget();
              } else if (snapshot.hasError) {
                return EmptyData(
                    scale: 0.1,
                    msg: AppLocalizations.of(context)!
                        .translate('error_data_msg'));
              } else {
                return listViewWidgetSport(listSport);
              }
            },
          )
        ],
      ),
    );
  }

  Widget listViewWidget(List<Food> list, String typeFood, String textTitle) {
    if (list.length > 0) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 8,
          ),
          Text(
            textTitle,
            style: const TextStyle(
              color: statusColor,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.left,
          ),
          ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: list.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    StorePreferences.setFoodName(typeFood);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => FoodDetail(
                                  typeFood: true,
                                  idFoodDetail: list[index].id,
                                ))).then((value) {
                      loadData();
                      widget.date(StorePreferences.getDate());
                    });
                  },
                  child: ItemFood(
                      onPress: () {
                        StorePreferences.setFoodName(typeFood);
                        removeFoodMeal(list[index]);
                      },
                      ten: list[index].ten,
                      khoiLuong: list[index].khoiLuong,
                      donVi: list[index].donVi,
                      calo: list[index].calo),
                );
              }),
        ],
      );
    } else {
      return Container();
    }
  }

  Widget listViewWidgetSport(List<Sport> list) {
    if (list.length > 0) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 8,
          ),
          Text(
            AppLocalizations.of(context)!.translate('button_sport_calo'),
            style: const TextStyle(
              color: statusColor,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.left,
          ),
          ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: list.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    showDialog(
                            context: context,
                            builder: ((context) {
                              return PopupAddSport(
                                baiTap: list[index],
                                typeAddSport: true,
                              );
                            }))
                        .then(
                            (value) => widget.date(StorePreferences.getDate()));
                  },
                  child: ItemFood(
                      onPress: () {
                        removeSport(list[index]);
                      },
                      ten: list[index].ten,
                      khoiLuong: list[index].soPhut,
                      donVi: list[index].donVi,
                      calo: list[index].calo),
                );
              }),
        ],
      );
    } else {
      return Container();
    }
  }

  Future<void> loadData() async {
    Map<String, dynamic> resultCalo =
        await readCalo(StorePreferences.getIdFamily());
    ;
    listFoodBreakfast = [];
    listFoodLunch = [];
    listFoodDinner = [];
    listFoodSnacks = [];
    listSport = [];
    if (resultCalo[widget.dateData] != null &&
        resultCalo[widget.dateData] is Map<String, dynamic>) {
      Map<String, dynamic> mapCalo =
          resultCalo[widget.dateData] as Map<String, dynamic>;
      CaloDay calo = CaloDay.fromMap(mapCalo);

      listFoodBreakfast = calo.buaSang;
      if (listFoodBreakfast.length > 0) {
        checkBreakfast = true;
      }
      listFoodLunch = calo.buaTrua;
      if (listFoodLunch.length > 0) {
        checkLunch = true;
      }
      listFoodDinner = calo.buaToi;
      if (listFoodDinner.length > 0) {
        checkDinner = true;
      }
      listFoodSnacks = calo.buaPhu;
      if (listFoodSnacks.length > 0) {
        checkSnacks = true;
      }
      listSport = calo.theThao;
      if (listSport.length > 0) {
        checkSport = true;
      }
    }
  }

  void loadFood(
      List<String> list, List<Food> listAllFood, List<Food> listFood) {
    for (String i in list) {
      for (Food e in listAllFood) {
        if (e.id == i) {
          listFood.add(e);
        }
      }
    }
  }

  Future<void> removeFoodMeal(Food foodMeal) async {
    try {
      Map<String, dynamic> result =
          await readCalo(StorePreferences.getIdFamily());
      CaloDay calo = CaloDay.fromMap(result[StorePreferences.getDate()]);
      List<Food> list = [];
      //tính calo ngày
      double caloNapNgay =
          double.parse(calo.caloNap) - double.parse(foodMeal.calo);
      double chatBeoNgay =
          double.parse(calo.chatBeos) - double.parse(foodMeal.chatBeo);
      double chatDamNgay =
          double.parse(calo.chatDams) - double.parse(foodMeal.chatDam);
      double tinhBotNgay =
          double.parse(calo.tinhBots) - double.parse(foodMeal.tinhBot);
      //set calo ngày vào data
      calo.caloNap = caloNapNgay.toStringAsFixed(2);
      calo.chatBeos = chatBeoNgay.toStringAsFixed(2);
      calo.chatDams = chatDamNgay.toStringAsFixed(2);
      calo.tinhBots = tinhBotNgay.toStringAsFixed(2);
      if (StorePreferences.getFoodName() == food_breakfase) {
        list = calo.buaSang;
        for (int e = 0; e < list.length; e++) {
          if (list[e].id == foodMeal.id) {
            list.removeAt(e);
            break;
          }
        }
        calo.buaSang = list;
      } else if (StorePreferences.getFoodName() == food_lunch) {
        list = calo.buaTrua;
        for (int e = 0; e < list.length; e++) {
          if (list[e].id == foodMeal.id) {
            list.removeAt(e);
            break;
          }
        }
        calo.buaTrua = list;
      } else if (StorePreferences.getFoodName() == food_dinner) {
        list = calo.buaToi;
        for (int e = 0; e < list.length; e++) {
          if (list[e].id == foodMeal.id) {
            list.removeAt(e);
            break;
          }
        }
        calo.buaToi = list;
      } else if (StorePreferences.getFoodName() == food_snacks) {
        list = calo.buaPhu;
        for (int e = 0; e < list.length; e++) {
          if (list[e].id == foodMeal.id) {
            list.removeAt(e);
            break;
          }
        }
        calo.buaPhu = list;
      }
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('Calo')
          .doc(StorePreferences.getIdFamily());
      documentReference
          .update({StorePreferences.getDate(): calo.toMap()}).then((value) {
        ToastCommon.showToast(
            AppLocalizations.of(context)!.translate('toast_remove_food_meal'));
        widget.date(StorePreferences.getDate());
      });
    } catch (e) {
      showDialog(
          context: context,
          builder: ((context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_remove_food_meal'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          }));
    }
  }

  Future<void> removeSport(Sport sport) async {
    try {
      Map<String, dynamic> result =
          await readCalo(StorePreferences.getIdFamily());
      CaloDay calo = CaloDay.fromMap(result[StorePreferences.getDate()]);
      List<Sport> list = [];
      //tru calo tieu hao
      double caloTieuHao =
          double.parse(calo.caloTieuHao) - double.parse(sport.calo);
      calo.caloTieuHao = caloTieuHao.toStringAsFixed(2);
      //xoa ptu khoi list
      list = calo.theThao;
      for (int e = 0; e < list.length; e++) {
        if (list[e].id == sport.id) {
          list.removeAt(e);
          break;
        }
      }
      calo.theThao = list;
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('Calo')
          .doc(StorePreferences.getIdFamily());
      documentReference
          .update({StorePreferences.getDate(): calo.toMap()}).then((value) {
        ToastCommon.showToast(AppLocalizations.of(context)!
            .translate('toast_remove_sport_success'));
        widget.date(StorePreferences.getDate());
      });
    } catch (e) {
      showDialog(
          context: context,
          builder: ((context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_remove_sport_faild'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          }));
    }
  }
}
