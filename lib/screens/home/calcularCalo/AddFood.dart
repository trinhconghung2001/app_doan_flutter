import 'package:app_doan_flutter/screens/home/calcularCalo/data/Food.dart';
import 'package:app_doan_flutter/untils/AppLocalizations.dart';
import 'package:app_doan_flutter/untils/FutureFood.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/InputDropDown.dart';
import '../../../components/inputform/InputTextField.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../config/styles/KeyValue.dart';
import '../../category/ListCategory.dart';

class AddFood extends StatefulWidget {
  const AddFood({super.key});

  @override
  State<AddFood> createState() => _AddFoodState();
}

class _AddFoodState extends State<AddFood> {
  FocusNode focusChatBeo = FocusNode();
  FocusNode focusChatDam = FocusNode();
  FocusNode focusKLuongDvi = FocusNode();
  FocusNode focusTinhBot = FocusNode();
  FocusNode focusTen = FocusNode();
  FocusNode focusCalo = FocusNode();
  FocusNode focusDoiDonVi = FocusNode();
  TextEditingController textChatBeo = TextEditingController();
  TextEditingController textChatDam = TextEditingController();
  TextEditingController textKLuongDvi = TextEditingController();
  TextEditingController textTinhBot = TextEditingController();
  TextEditingController textTen = TextEditingController();
  TextEditingController textCalo = TextEditingController();
  TextEditingController textDoiDonVi = TextEditingController();

  String textDonVi = '';
  String hintDonVi = '';
  bool validate = false;

  @override
  void initState() {
    textChatBeo.text = '0';
    textChatDam.text = '0';
    textKLuongDvi.text = '0';
    textTinhBot.text = '0';
    textCalo.text = '0';
    textDoiDonVi.text = '0';
    super.initState();
    focusChatBeo.addListener(_onFocusChange);
    focusChatDam.addListener(_onFocusChange);
    focusKLuongDvi.addListener(_onFocusChange);
    focusTinhBot.addListener(_onFocusChange);
    focusTen.addListener(_onFocusChange);
    focusCalo.addListener(_onFocusChange);
    focusDoiDonVi.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    setState(() {
      if (textDoiDonVi.text.isNotEmpty &&
          textChatDam.text.isNotEmpty &&
          textChatBeo.text.isNotEmpty &&
          textTinhBot.text.isNotEmpty) {
        double doiDonVi = double.parse(textDoiDonVi.text);
        double chatDam = double.parse(textChatDam.text);
        double chatBeo = double.parse(textChatBeo.text);
        double tinhBot = double.parse(textTinhBot.text);
        double calo = (chatDam * 4 + chatBeo * 9 + tinhBot * 4) * doiDonVi;
        textCalo.text = calo.toStringAsFixed(2);
      }
    });
  }

  @override
  void dispose() {
    focusChatBeo.dispose();
    focusChatDam.dispose();
    focusKLuongDvi.dispose();
    focusTinhBot.dispose();
    focusTen.dispose();
    focusCalo.dispose();
    focusDoiDonVi.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: (() {
          FocusManager.instance.primaryFocus?.unfocus();
        }),
        child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
                text:
                    AppLocalizations.of(context)!.translate('button_add_food'),
                check: false,
                onPress: () {
                  Navigator.pop(context);
                }),
          ),
          body: Scaffold(
            body: SingleChildScrollView(
              child: Column(children: [
                Container(
                    width: MediaQuery.of(context).size.width,
                    decoration:
                        BoxDecorationContainer(color: backgroundScreenColor),
                    margin: const EdgeInsets.only(top: 8, left: 16, right: 16),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 16),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .translate('title_nutritional_ingredients'),
                            style: const TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: statusColor),
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          InputTextField(
                            title: AppLocalizations.of(context)!
                                .translate('title_name_food'),
                            checkRangBuoc: true,
                            focusNode: focusTen,
                            textController: textTen,
                            onPress: () {
                              setState(() {
                                textTen.clear();
                              });
                            },
                            isError: validate && textTen.text.isEmpty,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                              onPress: () {
                                guiNhanDrop(
                                    defaultUnit,
                                    AppLocalizations.of(context)!
                                        .translate('category_unit_food'));
                              },
                              value: textDonVi,
                              title: AppLocalizations.of(context)!
                                  .translate('title_unit_food'),
                              checkRangBuoc: true,
                              icon: Icons.arrow_drop_down,
                              isError: validate && textDonVi.isEmpty),
                          const SizedBox(
                            height: 16,
                          ),
                          InputTextField(
                            title: '1 ${textDonVi} ${AppLocalizations.of(context)!.translate('equals')}',
                            checkRangBuoc: true,
                            hintText:
                                '${AppLocalizations.of(context)!.translate('title_unit_food')} gam',
                            focusNode: focusDoiDonVi,
                            textController: textDoiDonVi,
                            onPress: () {
                              setState(() {
                                textDoiDonVi.clear();
                              });
                            },
                            isError: validate && textDoiDonVi.text.isEmpty,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          InputTextField(
                            title: AppLocalizations.of(context)!
                                .translate('protein'),
                            checkRangBuoc: true,
                            focusNode: focusChatDam,
                            textController: textChatDam,
                            hintText: hintDonVi,
                            onPress: () {
                              setState(() {
                                textChatDam.clear();
                              });
                            },
                            isError: validate && textChatDam.text.isEmpty,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          InputTextField(
                            title:
                                AppLocalizations.of(context)!.translate('fat'),
                            checkRangBuoc: true,
                            focusNode: focusChatBeo,
                            textController: textChatBeo,
                            hintText: hintDonVi,
                            onPress: () {
                              setState(() {
                                textChatBeo.clear();
                              });
                            },
                            isError: validate && textChatBeo.text.isEmpty,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          InputTextField(
                            title: AppLocalizations.of(context)!
                                .translate('carbs'),
                            checkRangBuoc: true,
                            focusNode: focusTinhBot,
                            textController: textTinhBot,
                            hintText: hintDonVi,
                            onPress: () {
                              setState(() {
                                textTinhBot.clear();
                              });
                            },
                            isError: validate && textTinhBot.text.isEmpty,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          InputTextField(
                            title: AppLocalizations.of(context)!
                                .translate('title_mass_unit_food'),
                            checkRangBuoc: true,
                            focusNode: focusKLuongDvi,
                            textController: textKLuongDvi,
                            hintText: hintDonVi,
                            onPress: () {
                              setState(() {
                                textKLuongDvi.clear();
                              });
                            },
                            isError: validate && textKLuongDvi.text.isEmpty,
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          InputTextField(
                            title: 'Calo',
                            checkRangBuoc: true,
                            focusNode: focusCalo,
                            textController: textCalo,
                            onPress: () {
                              setState(() {
                                textCalo.clear();
                              });
                            },
                            isError: validate && textCalo.text.isEmpty,
                          ),
                        ]))
              ]),
            ),
            bottomNavigationBar: BottomNavigator(
              widget: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: 16,
                  ),
                  Expanded(
                      child: CustomButton(
                    text: AppLocalizations.of(context)!
                        .translate("button_update"),
                    icon: Icons.save,
                    height: 32,
                    press: () {
                      checkValidate();
                      if (validate == false) {
                        addFoodUser();
                      }
                    },
                  )),
                  Container(
                    width: 16,
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  void checkValidate() {
    if (textTen.text.isNotEmpty &&
        textChatBeo.text.isNotEmpty &&
        textDonVi.isNotEmpty &&
        textKLuongDvi.text.isNotEmpty &&
        textTinhBot.text.isNotEmpty &&
        textCalo.text.isNotEmpty &&
        textDoiDonVi.text.isNotEmpty &&
        textChatDam.text.isNotEmpty) {
      setState(() {
        validate = false;
      });
    } else {
      setState(() {
        validate = true;
      });
    }
  }

  void guiNhanDrop(String loai, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      if (value != null) {
        if (loai == defaultUnit) {
          setState(() {
            if (value == unit_gram) {
              textDonVi = 'gam';
              textDoiDonVi.text = '1';
            } else if (value == unit_teaspoon) {
              textDonVi = 'tsp';
              textDoiDonVi.text = '5';
            } else if (value == unit_tablespoon) {
              textDonVi = 'tbsp';
              textDoiDonVi.text = '15';
            } else if (value == unit_cup) {
              textDonVi = 'cup';
              textDoiDonVi.text = '250';
            }
            hintDonVi =
                '${AppLocalizations.of(context)!.translate('title_unit_food')} ${textDonVi}';
          });
        }
        _onFocusChange();
      }
    });
  }

  Future<void> addFoodUser() async {
    List<Food> listFood =
        await readCategoryFoodUser(StorePreferences.getIdUser());
    String id = DateTime.now().toString();
    Food foodNew = Food(
        id: id,
        ten: textTen.text,
        chatBeo: textChatBeo.text,
        tinhBot: textTinhBot.text,
        chatDam: textChatDam.text,
        khoiLuong: textKLuongDvi.text,
        calo: textCalo.text,
        edit: true,
        doiDonVi: textDoiDonVi.text,
        donVi: textDonVi);
    if (listFood.any((element) =>
            element.ten == foodNew.ten &&
            element.chatBeo == foodNew.chatBeo &&
            element.tinhBot == foodNew.tinhBot &&
            element.chatDam == foodNew.chatDam &&
            element.khoiLuong == foodNew.khoiLuong &&
            element.calo == foodNew.calo &&
            element.donVi == foodNew.donVi) ==
        false) {
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('Food')
          .doc(StorePreferences.getIdUser());
      await documentReference.update({
        'food': FieldValue.arrayUnion([foodNew.toMap()])
      }).then((value) {
        ToastCommon.showToast(
            AppLocalizations.of(context)!.translate('toast_add_food_success'));
        Navigator.pop(context);
      });
    } else {
      showDialog(
          context: context,
          builder: (context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_add_food_duplicate'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          });
    }
  }
}
