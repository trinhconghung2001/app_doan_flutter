import 'package:app_doan_flutter/components/bottomnavigator/BottomNavigator.dart';
import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/components/loading/EmptyData.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/untils/FutureCalo.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/InputDropDown.dart';
import '../../../components/inputform/InputTextField.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureUser.dart';
import '../../../untils/StorePreferences.dart';
import '../../category/ListCategory.dart';

class EditAccountCalo extends StatefulWidget {
  const EditAccountCalo({
    super.key,
  });

  @override
  State<EditAccountCalo> createState() => _EditAccountCaloState();
}

class _EditAccountCaloState extends State<EditAccountCalo> {
  FamilyMember familyMember = FamilyMember.defaultContructor();
  late Future<void> _loadDataUpdate;
  final FocusNode _focusChieuCao = FocusNode();
  final FocusNode _focusCanNang = FocusNode();
  final FocusNode _focusMucTieu = FocusNode();
  String gioiTinh = '';
  String tuoi = '';
  String cuongDo = '';
  String textKetLuan = '';
  String textGoiY = '';
  bool validate = false;
  final TextEditingController textCanNang = TextEditingController();
  final TextEditingController textChieuCao = TextEditingController();
  final TextEditingController textMucTieu = TextEditingController();
  double BMI = 0.0;

  @override
  void initState() {
    _loadDataUpdate = loadData();
    super.initState();
    _focusChieuCao.addListener(_onFocusChange);
    _focusCanNang.addListener(_onFocusChange);
    _focusMucTieu.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    setState(() {
      if (textChieuCao.text.isNotEmpty && textCanNang.text.isNotEmpty) {
        double doubleCanNang = double.parse(textCanNang.text);
        double doubleChieuCao = double.parse(textChieuCao.text);
        BMI = doubleCanNang / (doubleChieuCao * doubleChieuCao);
        print("BMI : $BMI");
        textGoiY = textSuggest(doubleChieuCao);
        if (BMI < 18.5) {
          textKetLuan = conclusion_underweight;
        } else if (18.5 <= BMI && BMI < 23) {
          textKetLuan = conclusion_normal;
        } else if (23 <= BMI && BMI < 25) {
          textKetLuan = conclusion_overweight;
        } else if (25 <= BMI && BMI < 30) {
          textKetLuan = conclusion_obesity_1;
        } else if (30 <= BMI && BMI < 40) {
          textKetLuan = conclusion_obesity_2;
        } else if (BMI >= 40) {
          textKetLuan = conclusion_obesity_3;
        }
      }
    });
  }

  String textSuggest(double height) {
    return '${(18.5 * height * height).toStringAsFixed(2)} <= ${AppLocalizations.of(context)!.translate('title_weight')} <= ${(22.9 * height * height).toStringAsFixed(2)}';
  }

  @override
  void dispose() {
    _focusChieuCao.dispose();
    _focusCanNang.dispose();
    _focusMucTieu.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: AppLocalizations.of(context)!
                  .translate("app_bar_update_information_bmi"),
              checkIconBack: false,
              onPress: () {
                Navigator.pop(context);
              },
              check: false,
            ),
          ),
          body: Scaffold(
            body: SingleChildScrollView(
                child: FutureBuilder(
              future: _loadDataUpdate,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return CircularProgressIndicatorWidget();
                } else if (snapshot.hasError) {
                  return EmptyData();
                } else {
                  return Container(
                    decoration: BoxDecorationContainer(),
                    padding: const EdgeInsets.only(
                        top: 16, left: 16, right: 16, bottom: 16),
                    margin: const EdgeInsets.only(
                        top: 8, left: 16, right: 16, bottom: 0),
                    child: Column(
                      children: [
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate('title_height'),
                          checkRangBuoc: true,
                          focusNode: _focusChieuCao,
                          textController: textChieuCao,
                          hintText: AppLocalizations.of(context)!
                              .translate('hint_title_height'),
                          onPress: () {
                            setState(() {
                              textChieuCao.clear();
                            });
                          },
                          isError: validate && textChieuCao.text.isEmpty,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("title_weight"),
                          checkRangBuoc: true,
                          focusNode: _focusCanNang,
                          textController: textCanNang,
                          hintText: AppLocalizations.of(context)!
                              .translate('hint_title_weight'),
                          onPress: () {
                            setState(() {
                              textCanNang.clear();
                            });
                          },
                          isError: validate && textCanNang.text.isEmpty,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                          value: gioiTinh,
                          title: AppLocalizations.of(context)!
                              .translate('account_title_sex'),
                          checkRangBuoc: false,
                          showIcon: false,
                          offEdit: true,
                          icon: Icons.arrow_drop_down,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                          value: tuoi,
                          title: AppLocalizations.of(context)!
                              .translate('title_age'),
                          checkRangBuoc: false,
                          showIcon: false,
                          offEdit: true,
                          icon: Icons.arrow_drop_down,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultIntensity,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_intensity'));
                            },
                            value: cuongDo,
                            title: AppLocalizations.of(context)!
                                .translate("title_intensity"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && cuongDo.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                          value: textKetLuan,
                          title: AppLocalizations.of(context)!
                              .translate('title_conclude'),
                          checkRangBuoc: false,
                          showIcon: false,
                          offEdit: true,
                          icon: Icons.arrow_drop_down,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                          value: textGoiY,
                          title: AppLocalizations.of(context)!
                              .translate('title_suggest'),
                          checkRangBuoc: false,
                          showIcon: false,
                          offEdit: true,
                          maxLines: 3,
                          icon: Icons.arrow_drop_down,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("title_target"),
                          checkRangBuoc: true,
                          focusNode: _focusMucTieu,
                          textController: textMucTieu,
                          hintText: AppLocalizations.of(context)!
                              .translate('hint_title_weight'),
                          onPress: () {
                            setState(() {
                              textMucTieu.clear();
                            });
                          },
                          isError: validate && textMucTieu.text.isEmpty,
                        ),
                      ],
                    ),
                  );
                }
              },
            )),
            bottomNavigationBar: BottomNavigator(
              widget: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: 16,
                  ),
                  Expanded(
                      child: CustomButton(
                    text: AppLocalizations.of(context)!
                        .translate("button_update"),
                    icon: Icons.save,
                    height: 32,
                    press: () {
                      checkValidate();
                      if (validate == false) {
                        updateCalo();
                      }
                    },
                  )),
                  Container(
                    width: 16,
                  ),
                ],
              ),
            ),
          )),
    );
  }

  void guiNhanDrop(String loai, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      if (value != null) {
        if (loai == defaultIntensity) {
          setState(() {
            if (value == key_intensity_few) {
              cuongDo = key_title_few;
            } else if (value == key_intensity_little) {
              cuongDo = key_title_little;
            } else if (value == key_intensity_medium) {
              cuongDo = key_title_medium;
            } else if (value == key_intensity_high) {
              cuongDo = key_title_high;
            } else if (value == key_intensity_very_high) {
              cuongDo = key_title_very_high;
            }
          });
        }
      }
    });
  }

  void checkValidate() {
    if (textChieuCao.text.isNotEmpty &&
        textCanNang.text.isNotEmpty &&
        textMucTieu.text.isNotEmpty) {
      setState(() {
        validate = false;
      });
    } else {
      setState(() {
        validate = true;
      });
    }
  }

  Future<void> loadData() async {
    familyMember = await readIDBenhNhan(
        StorePreferences.getIdUser(), StorePreferences.getIdFamily());
    gioiTinh = familyMember.gioiTinh;
    DateTime ngaySinh = DateFormat('dd-MM-yyyy').parse(familyMember.ngaySinh);
    DateTime ngayHienTai = DateTime.now();
    //tinh su khac biet giữa ngày hiện tại và ngày sinh
    Duration difference = ngayHienTai.difference(ngaySinh);
    int age = (difference.inDays / 365).floor();
    tuoi = age.toString();
    Map<String, dynamic> resultCalo =
        await readCalo(StorePreferences.getIdFamily());
    cuongDo = resultCalo['cuongDo'];
    textKetLuan = '';
    textGoiY = '';
    textCanNang.text = resultCalo['canNang'];
    textChieuCao.text = resultCalo['chieuCao'];
    textMucTieu.text = resultCalo['mucTieu'];
    double doubleCanNang = double.parse(textCanNang.text);
    double doubleChieuCao = double.parse(textChieuCao.text);
    BMI = doubleCanNang / (doubleChieuCao * doubleChieuCao);
    print("BMI : $BMI");
    textGoiY = textSuggest(doubleChieuCao);
    if (BMI < 18.5) {
      textKetLuan = conclusion_underweight;
    } else if (18.5 <= BMI && BMI < 23) {
      textKetLuan = conclusion_normal;
    } else if (23 <= BMI && BMI < 25) {
      textKetLuan = conclusion_overweight;
    } else if (25 <= BMI && BMI < 30) {
      textKetLuan = conclusion_obesity_1;
    } else if (30 <= BMI && BMI < 40) {
      textKetLuan = conclusion_obesity_2;
    } else if (BMI >= 40) {
      textKetLuan = conclusion_obesity_3;
    }
  }

  Future<void> updateCalo() async {
    try {
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('Calo')
          .doc(StorePreferences.getIdFamily());
      double weight = double.parse(textCanNang.text);
      double height = double.parse(textChieuCao.text);
      bool isMale = gioiTinh == 'Nam' ? true : false;
      int age = int.parse(tuoi);
      double bmr = calculateBMR(weight, height, age, isMale);
      double activityFactor = calculateActivityFactor(cuongDo);
      double TDEE = caclulateTDEE(bmr, activityFactor);
      double caloDay = 0;
      if (textKetLuan == conclusion_underweight) {
        caloDay = caculateWeightGain(TDEE, 0.5);
      } else if (textKetLuan == conclusion_normal) {
        caloDay = TDEE;
      } else {
        caloDay = caculateWeightLoss(TDEE, 0.5);
      }
      //tính tinh bột, chất đạm, chất béo
      double textCarbs = calculateCarbs(caloDay);
      double textProtein = calculateProtein(textKetLuan, caloDay);
      double textFat = calculateFat(textKetLuan, caloDay);

      documentReference.update({
        'canNang': textCanNang.text,
        'chieuCao': textChieuCao.text,
        'cuongDo': cuongDo,
        'gioiTinh': gioiTinh,
        'tuoi': tuoi,
        'mucTieu': textMucTieu.text,
        'TDEE': TDEE.toString(), // Make sure TDEE is a String
        'caloDay': caloDay.toString(),
        'carbsDay': textCarbs.toString(),
        'proteinDay': textProtein.toString(),
        'fatDay': textFat.toString(),
      }).then((value) {
        ToastCommon.showToast(AppLocalizations.of(context)!
            .translate('toast_update_information_bmi'));
        Navigator.pop(context);
      });
    } catch (e) {
      showDialog(
          context: context,
          builder: ((context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_update_information_bmi'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          }));
    }
  }
}
