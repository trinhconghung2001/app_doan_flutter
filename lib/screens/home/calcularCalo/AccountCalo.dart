import 'package:app_doan_flutter/screens/home/calcularCalo/EditAccountCalo..dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/InputDropDown.dart';
import '../../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../config/styles/KeyValue.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureCalo.dart';
import '../../../untils/FutureUser.dart';
import '../../../untils/StorePreferences.dart';
import '../../login/data/FamilyMember.dart';

class AccountCalo extends StatefulWidget {
  final String idAccount;
  const AccountCalo({super.key, required this.idAccount});

  @override
  State<AccountCalo> createState() => _AccountCaloState();
}

class _AccountCaloState extends State<AccountCalo> {
  FamilyMember familyMember = FamilyMember.defaultContructor();
  late Future<void> _loadDataDetail;
  String gioiTinh = '';
  String tuoi = '';
  String cuongDo = '';
  String textKetLuan = '';
  String textGoiY = '';
  String textCanNang = '';
  String textChieuCao = '';
  String textMucTieu = '';
  double BMI = 0.0;
  @override
  void initState() {
    _loadDataDetail = loadData();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDataDetail = loadData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(50),
              child: CustomAppBar(
                text: AppLocalizations.of(context)!
                    .translate('app_bar_detail_calo_account'),
                check: false,
                checkIconBack: false,
                onPress: () {
                  Navigator.pop(context);
                },
              ),
            ),
            body: Scaffold(
              body: SingleChildScrollView(
                  child: FutureBuilder(
                future: _loadDataDetail,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return CircularProgressIndicatorWidget();
                  } else if (snapshot.hasError) {
                    return EmptyData();
                  } else {
                    return Container(
                      decoration: BoxDecorationContainer(),
                      padding: const EdgeInsets.only(
                          top: 16, left: 16, right: 16, bottom: 16),
                      margin: const EdgeInsets.only(
                          top: 8, left: 16, right: 16, bottom: 0),
                      child: Column(
                        children: [
                          InputDropDown(
                            value: textChieuCao,
                            title: AppLocalizations.of(context)!
                                .translate('title_height'),
                            checkRangBuoc: false,
                            showIcon: false,
                            offEdit: true,
                            icon: Icons.arrow_drop_down,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                            value: textCanNang,
                            title: AppLocalizations.of(context)!
                                .translate('title_weight'),
                            checkRangBuoc: false,
                            showIcon: false,
                            offEdit: true,
                            icon: Icons.arrow_drop_down,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                            value: gioiTinh,
                            title: AppLocalizations.of(context)!
                                .translate('account_title_sex'),
                            checkRangBuoc: false,
                            showIcon: false,
                            offEdit: true,
                            icon: Icons.arrow_drop_down,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                            value: tuoi,
                            title: AppLocalizations.of(context)!
                                .translate('title_age'),
                            checkRangBuoc: false,
                            showIcon: false,
                            offEdit: true,
                            icon: Icons.arrow_drop_down,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                            value: cuongDo,
                            title: AppLocalizations.of(context)!
                                .translate("title_intensity"),
                            checkRangBuoc: false,
                            showIcon: false,
                            offEdit: true,
                            icon: Icons.arrow_drop_down,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                            value: textKetLuan,
                            title: AppLocalizations.of(context)!
                                .translate('title_conclude'),
                            checkRangBuoc: false,
                            showIcon: false,
                            offEdit: true,
                            icon: Icons.arrow_drop_down,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                            value: textGoiY,
                            title: AppLocalizations.of(context)!
                                .translate('title_suggest'),
                            checkRangBuoc: false,
                            showIcon: false,
                            offEdit: true,
                            maxLines: 3,
                            icon: Icons.arrow_drop_down,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                            value: textMucTieu,
                            title: AppLocalizations.of(context)!
                                .translate('title_target'),
                            checkRangBuoc: false,
                            showIcon: false,
                            offEdit: true,
                            icon: Icons.arrow_drop_down,
                          ),
                        ],
                      ),
                    );
                  }
                },
              )),
              bottomNavigationBar: BottomNavigator(
                widget: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: 16,
                    ),
                    Expanded(
                        child: CustomButton(
                      text: AppLocalizations.of(context)!
                          .translate('button_edit_calo_account'),
                      height: 32,
                      icon: Icons.edit_square,
                      press: () {
                        Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => EditAccountCalo()))
                            .then((value) => loadScreen());
                      },
                    )),
                    Container(
                      width: 16,
                    ),
                  ],
                ),
              ),
            )));
  }

  Future<void> loadData() async {
    familyMember = await readIDBenhNhan(
        StorePreferences.getIdUser(), StorePreferences.getIdFamily());
    gioiTinh = familyMember.gioiTinh;
    DateTime ngaySinh = DateFormat('dd-MM-yyyy').parse(familyMember.ngaySinh);
    DateTime ngayHienTai = DateTime.now();
    //tinh su khac biet giữa ngày hiện tại và ngày sinh
    Duration difference = ngayHienTai.difference(ngaySinh);
    int age = (difference.inDays / 365).floor();
    tuoi = age.toString();
    Map<String, dynamic> resultCalo = await readCalo(widget.idAccount);
    cuongDo = resultCalo['cuongDo'];
    textKetLuan = '';
    textGoiY = '';
    textCanNang = '${resultCalo['canNang']} kg';
    textChieuCao = '${resultCalo['chieuCao']} m';
    textMucTieu = '${resultCalo['mucTieu']} kg';
    double doubleCanNang = double.parse(resultCalo['canNang']);
    double doubleChieuCao = double.parse(resultCalo['chieuCao']);
    BMI = doubleCanNang / (doubleChieuCao * doubleChieuCao);
    print("BMI : $BMI");
    textGoiY = textSuggest(doubleChieuCao);
    if (BMI < 18.5) {
      textKetLuan = conclusion_underweight;
    } else if (18.5 <= BMI && BMI < 23) {
      textKetLuan = conclusion_normal;
    } else if (23 <= BMI && BMI < 25) {
      textKetLuan = conclusion_overweight;
    } else if (25 <= BMI && BMI < 30) {
      textKetLuan = conclusion_obesity_1;
    } else if (30 <= BMI && BMI < 40) {
      textKetLuan = conclusion_obesity_2;
    } else if (BMI >= 40) {
      textKetLuan = conclusion_obesity_3;
    }
  }

  String textSuggest(double height) {
    return '${(18.5 * height * height).toStringAsFixed(2)} <= Cân nặng <= ${(22.9 * height * height).toStringAsFixed(2)}';
  }
}
