import 'package:flutter/material.dart';

import '../../../../components/container/Decoration.dart';
import '../../../../config/styles/CustomColor.dart';

class ItemFood extends StatelessWidget {
  final String ten;
  final String khoiLuong;
  final String calo;
  final String donVi;
  final IconData? icon;
  final bool showIcon;
  final VoidCallback onPress;
  const ItemFood(
      {super.key,
      required this.ten,
      required this.khoiLuong,
      required this.calo,
      required this.donVi,
      required this.onPress,
      this.icon,
      this.showIcon = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecorationContainer(color: backgroundScreenColor),
      margin: EdgeInsets.only(top: 16),
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              Text(ten),
              Text('${khoiLuong} ${donVi} - ${calo} calo'),
            ],
          ),
          GestureDetector(
            onTap: onPress,
            child: Visibility(
              visible: !showIcon,
              child: Icon(
                icon ?? Icons.remove_circle,
                color: Color(0xFFBBC2C6),
              ),
            ),
          )
        ],
      ),
    );
  }
}
