import 'package:flutter/material.dart';

import '../../../../components/container/Decoration.dart';
import '../../../../config/styles/CustomColor.dart';

class ItemMenu extends StatelessWidget {
  final String ten;
  final String khoiLuong;
  final String calo;
  final String donVi;
  const ItemMenu(
      {super.key,
      required this.ten,
      required this.khoiLuong,
      required this.calo,
      required this.donVi});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecorationContainer(color: backgroundScreenColor),
      margin: EdgeInsets.only(top: 8, left: 16, right: 16),
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(ten),
          Text('${khoiLuong} ${donVi} - ${calo} calo'),
        ],
      ),
    );
  }
}
