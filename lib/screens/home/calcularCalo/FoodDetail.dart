import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/screens/home/calcularCalo/EditFood.dart';
import 'package:app_doan_flutter/screens/home/calcularCalo/data/CaloDay.dart';
import 'package:app_doan_flutter/screens/home/calcularCalo/data/Food.dart';
import 'package:app_doan_flutter/untils/FutureFood.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../config/styles/Dimens.dart';
import '../../../config/styles/KeyValue.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureCalo.dart';
import '../../../untils/StorePreferences.dart';

class FoodDetail extends StatefulWidget {
  final bool typeFood;
  final String idFoodDetail;
  FoodDetail({
    super.key,
    required this.idFoodDetail,
    required this.typeFood,
  });

  @override
  State<FoodDetail> createState() => _FoodDetailState();
}

class _FoodDetailState extends State<FoodDetail> {
  late Future<void> _futureData;
  Food foodDetail = Food.defaultContructor();
  FocusNode focusNode = FocusNode();
  TextEditingController textEditingController = TextEditingController();
  String fatDay = '';
  String caloDay = '';
  String proteinDay = '';
  String carbsDay = '';
  @override
  void initState() {
    _futureData = loadData();
    super.initState();
    focusNode.addListener(_onFocusChange);
  }

  void loadScreen() {
    setState(() {
      _futureData = loadData();
    });
  }

  void _onFocusChange() {
    setState(() {});
  }

  @override
  void dispose() {
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: (() {
          FocusManager.instance.primaryFocus?.unfocus();
        }),
        child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: FutureBuilder(
              future: _futureData,
              builder: (context, snapshot) {
                String textAppbar = '';
                if (snapshot.connectionState == ConnectionState.waiting) {
                  textAppbar = '';
                } else if (snapshot.hasError) {
                  textAppbar = '';
                } else {
                  textAppbar = foodDetail.ten;
                }
                return CustomAppBar(
                    text: textAppbar,
                    check: false,
                    onPress: () {
                      Navigator.pop(context);
                    });
              },
            ),
          ),
          body: SingleChildScrollView(
            child: FutureBuilder(
              future: _futureData,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return CircularProgressIndicatorWidget();
                } else if (snapshot.hasError) {
                  return EmptyData(
                      msg: AppLocalizations.of(context)!
                          .translate('error_data_msg'));
                } else {
                  return Column(
                    children: [
                      Container(
                        decoration: BoxDecorationContainer(
                            color: backgroundScreenColor),
                        margin:
                            const EdgeInsets.only(top: 8, left: 16, right: 16),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 16),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                AppLocalizations.of(context)!
                                    .translate('title_nutritional_ingredients'),
                                style: const TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: statusColor),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 10,
                                    child: TextField(
                                        maxLines: 1,
                                        style: const TextStyle(
                                          color: Color(0xFF444444),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                        ),
                                        onChanged: (value) {
                                          // searchList(value);
                                        },
                                        focusNode: focusNode,
                                        controller: textEditingController,
                                        decoration: InputDecoration(
                                          hintText: foodDetail.khoiLuong,
                                          contentPadding: const EdgeInsets.only(
                                              left: defaultTextInsideBoxPadding,
                                              right: 0),
                                          fillColor: statusColor,
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                          ),
                                        )),
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    foodDetail.donVi,
                                    style: const TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: statusColor),
                                  ), //
                                ],
                              ),
                            ]),
                      ),
                      ChartFoodDetail(
                        khoiLuong: double.parse(textEditingController.text),
                        khoiLuongBanDau: double.parse(foodDetail.khoiLuong),
                        tinhBot: double.parse(foodDetail.tinhBot),
                        chatBeo: double.parse(foodDetail.chatBeo),
                        chatDam: double.parse(foodDetail.chatDam),
                        calo: double.parse(foodDetail.calo),
                        donVi: foodDetail.donVi,
                      ),
                      PercentFoodDetail(
                        khoiLuong: double.parse(textEditingController.text),
                        khoiLuongBanDau: double.parse(foodDetail.khoiLuong),
                        tinhBot: double.parse(foodDetail.tinhBot) *
                            double.parse(foodDetail.doiDonVi),
                        chatBeo: double.parse(foodDetail.chatBeo) *
                            double.parse(foodDetail.doiDonVi),
                        chatDam: double.parse(foodDetail.chatDam) *
                            double.parse(foodDetail.doiDonVi),
                        calo: double.parse(foodDetail.calo),
                        caloDay: double.parse(caloDay),
                        fatDay: double.parse(fatDay),
                        proteinDay: double.parse(proteinDay),
                        carbsDay: double.parse(carbsDay),
                      ),
                    ],
                  );
                }
              },
            ),
          ),
          bottomNavigationBar: FutureBuilder(
              future: _futureData,
              builder: (context, snapshot) {
                // String textAppbar = '';
                if (snapshot.connectionState == ConnectionState.waiting) {
                  // textAppbar = '';
                  return Container();
                } else if (snapshot.hasError) {
                  // textAppbar = '';
                  return Container();
                } else {
                  return BottomNavigator(
                    widget: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: 16,
                        ),
                        Visibility(
                          visible: foodDetail.edit == true ? true : false,
                          child: Expanded(
                              child: Container(
                            margin: EdgeInsets.only(right: 8),
                            child: CustomButton(
                              text: AppLocalizations.of(context)!
                                  .translate('button_edit_food'),
                              height: 32,
                              icon: Icons.edit_square,
                              press: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => EditFood(
                                              idFood: widget.idFoodDetail,
                                            ))).then((value) => loadScreen());
                              },
                            ),
                          )),
                        ),
                        Expanded(
                            child: CustomButton(
                          text: widget.typeFood == false
                              ? AppLocalizations.of(context)!
                                  .translate('button_add_food_meal')
                              : AppLocalizations.of(context)!
                                  .translate('button_update'),
                          height: 32,
                          icon: widget.typeFood == false
                              ? Icons.fastfood_outlined
                              : Icons.save_alt_rounded,
                          press: () {
                            if (widget.typeFood == false) {
                              showDialog(
                                context: context,
                                builder: (context) {
                                  return dialogFood();
                                },
                              );
                            } else {
                              updateFoodMeal();
                            }
                          },
                        )),
                        Container(
                          width: 16,
                        ),
                      ],
                    ),
                  );
                }
              }),
        ));
  }

  Dialog dialogFood() {
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        child: Container(
          padding: EdgeInsets.only(top: 8),
          width: MediaQuery.of(context).size.width / 2,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.all(defaultPadding),
                child: Text(
                  AppLocalizations.of(context)!
                      .translate('popup_select_picture'),
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.only(
                      left: defaultPadding,
                      right: defaultPadding,
                      bottom: defaultPadding),
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height / 5,
                    child: Column(
                      children: [
                        Expanded(
                            child: CustomButton(
                          text: AppLocalizations.of(context)!
                              .translate('title_menu_breakfase'),
                          icon: Icons.coffee,
                          height: 32,
                          iconColor: primaryColor,
                          textColor: primaryColor,
                          background: backgroundScreenColor,
                          press: () {
                            StorePreferences.setFoodName(food_breakfase);
                            addFood(food_breakfase);
                          },
                        )),
                        Container(
                          width: 4,
                        ),
                        Expanded(
                            child: CustomButton(
                          text: AppLocalizations.of(context)!
                              .translate('title_menu_lunch'),
                          icon: Icons.local_pizza,
                          height: 32,
                          iconColor: primaryColor,
                          textColor: primaryColor,
                          background: backgroundScreenColor,
                          press: () {
                            StorePreferences.setFoodName(food_lunch);
                            addFood(food_lunch);
                          },
                        )),
                        Container(
                          width: 4,
                        ),
                        Expanded(
                            child: CustomButton(
                          text: AppLocalizations.of(context)!
                              .translate('title_menu_dinner'),
                          icon: Icons.dinner_dining_sharp,
                          height: 32,
                          iconColor: primaryColor,
                          textColor: primaryColor,
                          background: backgroundScreenColor,
                          press: () {
                            StorePreferences.setFoodName(food_dinner);
                            addFood(food_dinner);
                          },
                        )),
                        Container(
                          width: 4,
                        ),
                        Expanded(
                            child: CustomButton(
                          text: AppLocalizations.of(context)!
                              .translate('title_menu_snacks'),
                          icon: Icons.fastfood,
                          height: 32,
                          iconColor: primaryColor,
                          textColor: primaryColor,
                          background: backgroundScreenColor,
                          press: () {
                            StorePreferences.setFoodName(food_snacks);
                            addFood(food_snacks);
                          },
                        )),
                        Container(
                          width: 8,
                        ),
                      ],
                    ),
                  ))
            ],
          ),
        ));
  }

  Future<void> loadData() async {
    if (widget.typeFood == false) {
      List<Food> listFood = await readListAllFood(StorePreferences.getIdUser());
      for (Food e in listFood) {
        if (widget.idFoodDetail == e.id) {
          foodDetail = e;
          break;
        }
      }
    } else {
      List<Food> listFood = await readListFoodMeal(
          StorePreferences.getIdFamily(),
          StorePreferences.getDate(),
          StorePreferences.getFoodName());
      for (Food e in listFood) {
        if (widget.idFoodDetail == e.id) {
          foodDetail = e;
          break;
        }
      }
    }

    textEditingController.text = foodDetail.khoiLuong;
    Map<String, dynamic> resultCalo =
        await readCalo(StorePreferences.getIdFamily());
    caloDay = resultCalo['caloDay'];
    fatDay = resultCalo['fatDay'];
    carbsDay = resultCalo['carbsDay'];
    proteinDay = resultCalo['proteinDay'];
  }

  Future<void> addFood(String meal) async {
    try {
      Map<String, dynamic> result =
          await readCalo(StorePreferences.getIdFamily());
      CaloDay calo = CaloDay.fromMap(result[StorePreferences.getDate()]);
      List<Food> list = [];
      double tiLe = double.parse(textEditingController.text) /
          double.parse(foodDetail.khoiLuong);
      //tính calo thực đơn mới
      double chatBeoNew = double.parse(foodDetail.chatBeo) * tiLe;
      double tinhBotMoi = (double.parse(foodDetail.tinhBot) * tiLe);
      double chatDamMoi = (double.parse(foodDetail.chatDam) * tiLe);
      double caloMoi = (double.parse(foodDetail.calo) * tiLe);
      //tính calo ngày
      double caloNapNgay = double.parse(calo.caloNap) + caloMoi;
      double chatBeoNgay = double.parse(calo.chatBeos) + chatBeoNew;
      double chatDamNgay = double.parse(calo.chatDams) + chatDamMoi;
      double tinhBotNgay = double.parse(calo.tinhBots) + tinhBotMoi;
      //set calo ngày vào data
      calo.caloNap = caloNapNgay.toStringAsFixed(2);
      calo.chatBeos = chatBeoNgay.toStringAsFixed(2);
      calo.chatDams = chatDamNgay.toStringAsFixed(2);
      calo.tinhBots = tinhBotNgay.toStringAsFixed(2);
      Food foodNew = Food(
          id: DateTime.now().toString(),
          ten: foodDetail.ten,
          chatBeo: chatBeoNew.toStringAsFixed(2),
          tinhBot: tinhBotMoi.toStringAsFixed(2),
          chatDam: chatDamMoi.toStringAsFixed(2),
          khoiLuong: textEditingController.text,
          calo: caloMoi.toStringAsFixed(2),
          edit: false,
          doiDonVi: foodDetail.doiDonVi,
          donVi: foodDetail.donVi);
      if (meal == food_breakfase) {
        list = calo.buaSang;
        list.add(foodNew);
        calo.buaSang = list;
      } else if (meal == food_lunch) {
        list = calo.buaTrua;
        list.add(foodNew);
        calo.buaTrua = list;
      } else if (meal == food_dinner) {
        list = calo.buaToi;
        list.add(foodNew);
        calo.buaToi = list;
      } else if (meal == food_snacks) {
        list = calo.buaPhu;
        list.add(foodNew);
        calo.buaPhu = list;
      }
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('Calo')
          .doc(StorePreferences.getIdFamily());
      documentReference
          .update({StorePreferences.getDate(): calo.toMap()}).then((value) {
        ToastCommon.showToast(
            AppLocalizations.of(context)!.translate('toast_add_food_meal'));
        Navigator.pop(context);
      });
    } catch (e) {
      showDialog(
          context: context,
          builder: ((context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_add_food_meal'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          }));
    }
  }

  Future<void> updateFoodMeal() async {
    try {
      Map<String, dynamic> result =
          await readCalo(StorePreferences.getIdFamily());
      CaloDay calo = CaloDay.fromMap(result[StorePreferences.getDate()]);
      List<Food> list = [];
      double tiLe = double.parse(textEditingController.text) /
          double.parse(foodDetail.khoiLuong);
      //tính calo thực đơn mới
      double chatBeoNew = double.parse(foodDetail.chatBeo) * tiLe;
      double tinhBotMoi = (double.parse(foodDetail.tinhBot) * tiLe);
      double chatDamMoi = (double.parse(foodDetail.chatDam) * tiLe);
      double caloMoi = (double.parse(foodDetail.calo) * tiLe);
      //tính calo ngày
      double caloNapNgay = double.parse(calo.caloNap) + caloMoi;
      double chatBeoNgay = double.parse(calo.chatBeos) + chatBeoNew;
      double chatDamNgay = double.parse(calo.chatDams) + chatDamMoi;
      double tinhBotNgay = double.parse(calo.tinhBots) + tinhBotMoi;
      //set calo ngày vào data
      calo.caloNap = caloNapNgay.toStringAsFixed(2);
      calo.chatBeos = chatBeoNgay.toStringAsFixed(2);
      calo.chatDams = chatDamNgay.toStringAsFixed(2);
      calo.tinhBots = tinhBotNgay.toStringAsFixed(2);
      Food foodNew = Food(
          id: foodDetail.id,
          ten: foodDetail.ten,
          chatBeo: chatBeoNew.toStringAsFixed(2),
          tinhBot: tinhBotMoi.toStringAsFixed(2),
          chatDam: chatDamMoi.toStringAsFixed(2),
          khoiLuong: textEditingController.text,
          calo: caloMoi.toStringAsFixed(2),
          edit: false,
          doiDonVi: foodDetail.doiDonVi,
          donVi: foodDetail.donVi);
      if (StorePreferences.getFoodName() == food_breakfase) {
        list = calo.buaSang;
        for (int e = 0; e < list.length; e++) {
          if (list[e].id == foodDetail.id) {
            list[e] = foodNew;
            break;
          }
        }
        calo.buaSang = list;
      } else if (StorePreferences.getFoodName() == food_lunch) {
        list = calo.buaTrua;
        for (int e = 0; e < list.length; e++) {
          if (list[e].id == foodDetail.id) {
            list[e] = foodNew;
            break;
          }
        }
        calo.buaTrua = list;
      } else if (StorePreferences.getFoodName() == food_dinner) {
        list = calo.buaToi;
        for (int e = 0; e < list.length; e++) {
          if (list[e].id == foodDetail.id) {
            list[e] = foodNew;
            break;
          }
        }
        calo.buaToi = list;
      } else if (StorePreferences.getFoodName() == food_snacks) {
        list = calo.buaPhu;
        for (int e = 0; e < list.length; e++) {
          if (list[e].id == foodDetail.id) {
            list[e] = foodNew;
            break;
          }
        }
        calo.buaPhu = list;
      }
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('Calo')
          .doc(StorePreferences.getIdFamily());
      documentReference
          .update({StorePreferences.getDate(): calo.toMap()}).then((value) {
        ToastCommon.showToast(
            AppLocalizations.of(context)!.translate('toast_add_food_meal'));
        Navigator.pop(context);
      });
    } catch (e) {
      showDialog(
          context: context,
          builder: ((context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_add_food_meal'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          }));
    }
  }
}

class ChartFoodDetail extends StatelessWidget {
  final double khoiLuong;
  final double khoiLuongBanDau;
  final double chatBeo;
  final double tinhBot;
  final double chatDam;
  final double calo;
  final String donVi;
  const ChartFoodDetail(
      {super.key,
      required this.khoiLuong,
      required this.khoiLuongBanDau,
      required this.chatBeo,
      required this.tinhBot,
      required this.chatDam,
      required this.calo,
      required this.donVi});

  @override
  Widget build(BuildContext context) {
    double tiLe = khoiLuong / khoiLuongBanDau;
    return Container(
      decoration: BoxDecorationContainer(color: backgroundScreenColor),
      margin: const EdgeInsets.only(top: 8, left: 16, right: 16),
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 2,
            child: Container(
              height: 150,
              child: Stack(alignment: Alignment.center, children: [
                Container(
                  height: 40,
                  child: Column(
                    children: [
                      Text((calo * tiLe).toString(),
                          style: TextStyle(fontSize: 14, color: statusColor)),
                      Text('Calo',
                          style: TextStyle(fontSize: 14, color: statusColor)),
                    ],
                  ),
                ),
                PieChart(
                    swapAnimationDuration: Duration(milliseconds: 500),
                    swapAnimationCurve: Curves.easeInOutQuint,
                    PieChartData(
                        startDegreeOffset: 270,
                        sections: [
                          PieChartSectionData(
                              radius: 20,
                              value: (tinhBot * tiLe),
                              showTitle: false,
                              color: statusColor),
                          PieChartSectionData(
                              radius: 20,
                              showTitle: false,
                              value: (chatDam * tiLe),
                              color: backgroudSearch),
                          PieChartSectionData(
                              radius: 20,
                              value: (chatBeo * tiLe),
                              showTitle: false,
                              color: primaryColor)
                        ],
                        borderData: FlBorderData(
                            show: true,
                            border: Border.all(width: 2, color: warningColor)),
                        centerSpaceRadius: 50)),
              ]),
            ),
          ),
          Expanded(
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 8),
                        width: 18,
                        height: 18,
                        color: statusColor,
                      ),
                      Text(AppLocalizations.of(context)!.translate('carbs')),
                    ],
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text('${(tinhBot * tiLe).toString()} ${donVi}'),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 8),
                        width: 18,
                        height: 18,
                        color: backgroudSearch,
                      ),
                      Text(AppLocalizations.of(context)!.translate('protein')),
                    ],
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text('${(chatDam * tiLe).toString()} ${donVi}'),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 8),
                        width: 18,
                        height: 18,
                        color: primaryColor,
                      ),
                      Text(AppLocalizations.of(context)!.translate('fat')),
                    ],
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text('${(chatBeo * tiLe).toString()} ${donVi}'),
                ],
              ))
        ],
      ),
    );
  }
}

class PercentFoodDetail extends StatelessWidget {
  final double khoiLuong;
  final double khoiLuongBanDau;
  final double chatBeo;
  final double tinhBot;
  final double chatDam;
  final double calo;
  final double caloDay;
  final double fatDay;
  final double proteinDay;
  final double carbsDay;
  const PercentFoodDetail(
      {super.key,
      required this.khoiLuong,
      required this.khoiLuongBanDau,
      required this.chatBeo,
      required this.tinhBot,
      required this.chatDam,
      required this.calo,
      required this.caloDay,
      required this.fatDay,
      required this.proteinDay,
      required this.carbsDay});

  @override
  Widget build(BuildContext context) {
    double tiLe = khoiLuong / khoiLuongBanDau;
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecorationContainer(color: backgroundScreenColor),
      margin: const EdgeInsets.only(top: 8, left: 16, right: 16),
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              AppLocalizations.of(context)!.translate('title_target_date'),
              style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: statusColor),
            ),
            SizedBox(
              height: 8,
            ),
            Row(
              children: [
                Expanded(
                    child: Column(
                  children: [
                    widgetBelow((calo * tiLe), caloDay, Color(0xFF68E1FD),
                        Color(0xFFB7DBE3)),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                        '${double.parse(((calo * tiLe * 100) / caloDay).toStringAsFixed(2)).toString()}% Calo')
                  ],
                )),
                Expanded(
                    child: Column(
                  children: [
                    widgetBelow((chatDam * tiLe), proteinDay, backgroudSearch,
                        Color(0xFF9FCFD6)),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                        '${double.parse(((chatDam * tiLe * 100) / proteinDay).toStringAsFixed(2)).toString()}% ${AppLocalizations.of(context)!.translate('protein')}')
                  ],
                )),
              ],
            ),
            SizedBox(
              height: 8,
            ),
            Row(
              children: [
                Expanded(
                    child: Column(
                  children: [
                    widgetBelow((tinhBot * tiLe), carbsDay, statusColor,
                        Color(0xFF7B8DA5)),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                        '${double.parse(((tinhBot * tiLe * 100) / carbsDay).toStringAsFixed(2)).toString()}% ${AppLocalizations.of(context)!.translate('carbs')}')
                  ],
                )),
                Expanded(
                    child: Column(
                  children: [
                    widgetBelow((chatBeo * tiLe), fatDay, primaryColor,
                        Color(0xFFA0B6D2)),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                        '${double.parse(((chatBeo * tiLe * 100) / fatDay).toStringAsFixed(2)).toString()}% ${AppLocalizations.of(context)!.translate('fat')}'),
                  ],
                )),
              ],
            ),
          ]),
    );
  }

  Widget widgetBelow(double a, double b, Color color1, Color color2) {
    return LinearPercentIndicator(
      lineHeight: 8.0,
      barRadius: Radius.circular(10),
      percent: a / b < 1
          ? a / b
          : 1, // Thay đổi giá trị này để thể hiện phần trăm khác nhau
      backgroundColor: color2,
      progressColor: color1,
    );
  }
}
