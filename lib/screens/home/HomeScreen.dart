import 'dart:io';

import 'package:app_doan_flutter/components/button/CustomButton.dart';
import 'package:app_doan_flutter/config/styles/Dimens.dart';
import 'package:app_doan_flutter/screens/home/bookAdvise/SearchDepartmentAdvise.dart';
import 'package:app_doan_flutter/screens/home/bookAdvise/notification_service/NotificationServices.dart';
import 'package:app_doan_flutter/screens/home/calcularCalo/CalcularCalo.dart';
import 'package:app_doan_flutter/screens/home/components/BackgroundHomeScreen.dart';
import 'package:app_doan_flutter/screens/home/components/Body.dart';
import 'package:app_doan_flutter/screens/home/searchMap/SearchMap.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../config/styles/CustomColor.dart';
import '../../untils/AppLocalizations.dart';
import '../../untils/FutureCalo.dart';
import 'bookAdvise/SelectDepartmentAdvise.dart';
import 'bookExamination/BookExamination.dart';
import 'calcularCalo/data/CaloDay.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  NotificationServices notificationServices = NotificationServices();
  @override
  void initState() {
    // TODO: implement initState
    notificationServices.requestNotificationPermissions();
    notificationServices.forgroundMessages();
    notificationServices.firebaseInt(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double deviceHeight = MediaQuery.of(context).size.height;
    final isSmallMobile =
        Platform.isAndroid ? deviceHeight < 600 : deviceHeight < 700;
    final isMediumMobile =
        Platform.isAndroid ? deviceHeight < 1200 : deviceHeight < 1000;
    // This size provide us total height and width of our screen
    return BackgroundHomeScreen(
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.bottomCenter,
            child: SingleChildScrollView(
              child: Container(
                  height: isSmallMobile ? size.height * 0.4 : size.height * 0.4,
                  width: size.width,
                  decoration: const BoxDecoration(
                      color: Color(0xFFFDFDFD),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40.0),
                        topRight: Radius.circular(40.0),
                      )),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: defaultPaddingMin,
                        right: defaultPaddingMin,
                        top: defaultPadding),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          flex: 1,
                          child: Text(
                            AppLocalizations.of(context)!
                                .translate('home_card_title'),
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: isMediumMobile ? 20 : 36,
                              shadows: const <Shadow>[
                                Shadow(
                                  offset: Offset(0.0, 4.0),
                                  blurRadius: 4.0,
                                  color: Color.fromARGB(0, 0, 0, 1),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Expanded(
                                child: Body(
                                  svgPicture: SvgPicture.asset(
                                      'assets/icons/home/IconTuVan.svg'),
                                  title: AppLocalizations.of(context)!
                                      .translate('home_icon_tu_van'),
                                  onClick: () {
                                    showDialog(
                                        context: context,
                                        builder: (context) {
                                          return Dialog(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8.0)),
                                              child: Container(
                                                padding: EdgeInsets.only(
                                                    top: defaultPadding),
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width /
                                                    1.2,
                                                child: Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              defaultPadding),
                                                      child: Text(
                                                        AppLocalizations.of(
                                                                context)!
                                                            .translate(
                                                                'popup_select_picture'),
                                                        style: TextStyle(
                                                            fontSize: 20,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                    ),
                                                    Padding(
                                                        padding: const EdgeInsets
                                                            .only(
                                                            left:
                                                                defaultPadding,
                                                            right:
                                                                defaultPadding,
                                                            bottom:
                                                                defaultPadding),
                                                        child: SizedBox(
                                                          height: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .height /
                                                              8,
                                                          child: Column(
                                                            children: [
                                                              Expanded(
                                                                  child:
                                                                      CustomButton(
                                                                text: AppLocalizations.of(
                                                                        context)!
                                                                    .translate(
                                                                        'app_bar_select_department'),
                                                                height: 32,
                                                                icon: Icons
                                                                    .search_outlined,
                                                                iconColor:
                                                                    primaryColor,
                                                                textColor:
                                                                    primaryColor,
                                                                background:
                                                                    backgroundScreenColor,
                                                                press: () {
                                                                  Navigator.push(
                                                                      context,
                                                                      MaterialPageRoute(
                                                                          builder: (context) =>
                                                                              SearchDepartmentAdvise()));
                                                                },
                                                              )),
                                                              Container(
                                                                width: 4,
                                                              ),
                                                              Expanded(
                                                                  child:
                                                                      CustomButton(
                                                                text: AppLocalizations.of(
                                                                        context)!
                                                                    .translate(
                                                                        'filter_search_select_hospital'),
                                                                icon: Icons
                                                                    .local_hospital_outlined,
                                                                height: 32,
                                                                press: () {
                                                                  Navigator.push(
                                                                      context,
                                                                      MaterialPageRoute(
                                                                          builder: (context) =>
                                                                              SelectDepartmentAdvise()));
                                                                },
                                                              )),
                                                              Container(
                                                                width: 8,
                                                              ),
                                                            ],
                                                          ),
                                                        ))
                                                  ],
                                                ),
                                              ));
                                        });
                                  },
                                ),
                              ),
                              Expanded(
                                child: Body(
                                  svgPicture: SvgPicture.asset(
                                      'assets/icons/home/IconDatLich.svg'),
                                  title: AppLocalizations.of(context)!
                                      .translate('home_icon_dat_lich'),
                                  onClick: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                BookExamination()));
                                  },
                                ),
                              ),
                              // Expanded(
                              //   child: Body(
                              //     svgPicture: SvgPicture.asset(
                              //         'assets/icons/home/IconVaccin.svg'),
                              //     title: AppLocalizations.of(context)!
                              //         .translate('home_icon_vaccin'),
                              //     onClick: () {
                              //       Navigator.push(
                              //           context,
                              //           MaterialPageRoute(
                              //               builder: (context) =>
                              //                   SuggestVaccine()));
                              //     },
                              //   ),
                              // ),
                              Expanded(
                                child: Body(
                                  svgPicture: SvgPicture.asset(
                                      'assets/icons/home/IconCalo.svg'),
                                  title: AppLocalizations.of(context)!
                                      .translate('app_bar_calcular_calo'),
                                  onClick: () {
                                    StorePreferences.setDate(DateTime.now());
                                    checkCaloDay(StorePreferences.getIdUser())
                                        .then((value) => Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    CalcularCalo())));
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Container(
                            decoration: const BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                                color: backgroudSearch),
                            alignment: Alignment.center,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                  flex: 3,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Text(
                                          AppLocalizations.of(context)!
                                              .translate(
                                                  'home_title_search_map'),
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: buttonTextColor),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: defaultPadding,
                                              horizontal: defaultPadding),
                                          child: CustomButton(
                                              press: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            SearchMap()));
                                              },
                                              text: AppLocalizations.of(
                                                      context)!
                                                  .translate('button_search'),
                                              icon: Icons.save,
                                              background: backgroundScreenColor,
                                              textColor: primaryColor,
                                              iconDisplay: false,
                                              height: 32),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                    flex: 2,
                                    child: SvgPicture.asset(
                                        'assets/icons/home/Map.svg'))
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  )),
            ),
          ),
        ],
      ),
      userName: StorePreferences.getName(),
    );
  }

  Future<void> checkCaloDay(String idUser) async {
    Map<String, dynamic> resultCalo = await readCalo(idUser);
    if (resultCalo.isNotEmpty) {
      String doc = StorePreferences.getDate();
      if (resultCalo[doc] != null && resultCalo[doc] is Map<String, dynamic>) {
        print('OK');
      } else {
        CaloDay caloNew = CaloDay(
            caloCan: resultCalo['caloDay'],
            caloNap: '0.0',
            caloTieuHao: '0.0',
            chatBeos: '0.0',
            chatDams: '0.0',
            tinhBots: '0.0',
            buaSang: [],
            buaTrua: [],
            buaToi: [],
            buaPhu: [],
            theThao: [],
            carbsDay: resultCalo['carbsDay'],
            proteinDay: resultCalo['proteinDay'],
            fatDay: resultCalo['fatDay']);
        Map<String, dynamic> caloMap = {doc: caloNew.toMap()};
        DocumentReference documentReference =
            FirebaseFirestore.instance.collection('Calo').doc(idUser);
        documentReference.update(caloMap);
      }
    }
  }
}
