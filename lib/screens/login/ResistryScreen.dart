import 'package:app_doan_flutter/components/button/PrimaryButton.dart';
import 'package:app_doan_flutter/components/inputform/InputText.dart';
import 'package:app_doan_flutter/components/inputform/SecurityInputText.dart';
import 'package:app_doan_flutter/components/toast/ToastCommon.dart';
import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/screens/login/data/User.dart';
import 'package:app_doan_flutter/untils/AppLocalizations.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../components/popup/ErrorPopup.dart';
import '../../config/styles/Dimens.dart';

class RegistryScreen extends StatefulWidget {
  const RegistryScreen({super.key});

  @override
  State<RegistryScreen> createState() => _RegistryScreenState();
}

class _RegistryScreenState extends State<RegistryScreen> {
  TextEditingController textEmailController = TextEditingController();
  TextEditingController textNameController = TextEditingController();
  FocusNode _focusNodeEmail = FocusNode();
  FocusNode _focusNodeName = FocusNode();
  String pass = '';
  String confirmPass = '';
  bool hidePassword = true;
  bool hideConfirmPass = true;
  bool isInvalid = false;
  String errorMsg = '';
  String idUserRegistry = '';

  @override
  void initState() {
    _focusNodeEmail.addListener(_onFocusChange);
    _focusNodeName.addListener(_onFocusChange);
    super.initState();
  }

  void _onFocusChange() {
    setState(() {});
  }

  @override
  void dispose() {
    _focusNodeEmail.dispose();
    _focusNodeName.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        body: LayoutBuilder(
          builder: (context, constraints) {
            double deviceHeight = MediaQuery.of(context).size.height;
            double sizedBoxHeight = 0;
            if (deviceHeight - 350 < 340) {
              sizedBoxHeight = deviceHeight -
                  350; // 350 là chiều cao tối thiểu của khung đăng nhập
            } else {
              sizedBoxHeight = deviceHeight *
                  0.45; // 340 là chiều cao tối thiểu cho tiêu đề và ảnh
            }
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                    minWidth: constraints.maxWidth,
                    minHeight: constraints.maxHeight),
                child: IntrinsicHeight(
                  child: Stack(
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(16, 53, 16, 16),
                            child: Row(
                              children: [
                                SvgPicture.asset(
                                  'assets/images/logo_app.svg',
                                  width: 57,
                                  height: 57,
                                ),
                                Container(
                                  padding:
                                      const EdgeInsets.fromLTRB(12, 0, 0, 0),
                                  child: Column(
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.fromLTRB(
                                            0, 0, 0, 8),
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .translate('name_app'),
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 20,
                                            color: Color(0xFF1C4983),
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                              mainAxisAlignment: MainAxisAlignment.center,
                            ),
                          ),
                          Padding(
                            child: SvgPicture.asset(
                              'assets/images/hospital_login.svg',
                              width: 270.03, // 270.03
                              height: 183, // 183
                            ),
                            padding: EdgeInsets.fromLTRB(0, 0, 0, 17),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          SizedBox(
                            height: sizedBoxHeight,
                          ),
                          Expanded(
                            child: Container(
                              constraints: const BoxConstraints(
                                minHeight: 350,
                              ),
                              decoration: const BoxDecoration(
                                color: primaryColor,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(30),
                                    topRight: Radius.circular(30)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Color(0x26006A67),
                                    offset: Offset(0, -4),
                                    blurRadius: 6,
                                  ),
                                ],
                              ),
                              child: Stack(
                                children: [
                                  Positioned(
                                    top: -3,
                                    right: 38,
                                    child: Container(
                                        width: 130,
                                        height: 130,
                                        decoration: const BoxDecoration(
                                          color: Color(0x0DFFFFFF),
                                          shape: BoxShape.circle,
                                        )),
                                  ),
                                  Positioned(
                                    top: 31,
                                    right: -106,
                                    child: Container(
                                        width: 212,
                                        height: 212,
                                        decoration: const BoxDecoration(
                                          color: Color(0x0DFFFFFF),
                                          shape: BoxShape.circle,
                                        )),
                                  ),
                                  Positioned(
                                    left: -105,
                                    bottom: -53,
                                    child: Container(
                                        width: 272,
                                        height: 272,
                                        decoration: const BoxDecoration(
                                          color: Color(0x0DFFFFFF),
                                          shape: BoxShape.circle,
                                        )),
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.only(
                                        top: defaultButtonHeight),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Text(
                                          AppLocalizations.of(context)!
                                              .translate('registry_title'),
                                          textAlign: TextAlign.center,
                                          style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20,
                                              color: Color(0xFFFDFDFD)),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(top: 8),
                                          child: Visibility(
                                            child: Text(
                                              errorMsg,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Color(0xFFB43939),
                                                fontSize: 14,
                                              ),
                                            ),
                                            visible: isInvalid,
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              19.52, 16, 31.34, 0),
                                          child: InputText(
                                              focusedBorderColor: Colors.white,
                                              labelText: AppLocalizations.of(
                                                      context)!
                                                  .translate('email_registry'),
                                              hintText: AppLocalizations.of(
                                                      context)!
                                                  .translate(
                                                      'email_registry_hint'),
                                              textController: textEmailController,
                                              focusNode: _focusNodeEmail,
                                              ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              19.52, 16, 31.34, 0),
                                          child: InputText(
                                              focusedBorderColor: Colors.white,
                                              labelText:
                                                  AppLocalizations.of(context)!
                                                      .translate('username'),
                                              hintText: AppLocalizations.of(
                                                      context)!
                                                  .translate('username_hint'),
                                              textController: textNameController,
                                              focusNode: _focusNodeName,),
                                        ),
                                        SecurityInputText(
                                          focusedBorderColor: Colors.white,
                                          hintText:
                                              AppLocalizations.of(context)!
                                                  .translate('password_hint'),
                                          labelText: AppLocalizations.of(
                                                  context)!
                                              .translate('password_registry'),
                                          value: pass,
                                          isHidePassword: hidePassword,
                                          onChanged: (value) =>
                                              setState(() => pass = value),
                                          onClickIcon: () => setState(() =>
                                              hidePassword = !hidePassword),
                                        ),
                                        SecurityInputText(
                                          focusedBorderColor: Colors.white,
                                          hintText:
                                              AppLocalizations.of(context)!
                                                  .translate(
                                                      'confirm_password_hint'),
                                          labelText: AppLocalizations.of(
                                                  context)!
                                              .translate('confirm_password'),
                                          value: confirmPass,
                                          isHidePassword: hideConfirmPass,
                                          onChanged: (value) => setState(
                                              () => confirmPass = value),
                                          onClickIcon: () => setState(() =>
                                              hideConfirmPass =
                                                  !hideConfirmPass),
                                        ),
                                        const SizedBox(height: 32),
                                        PrimaryButton(
                                          press: () async {
                                            if (textEmailController.text.trim().isEmpty ||
                                                textNameController.text.trim().isEmpty ||
                                                pass.trim().isEmpty ||
                                                confirmPass.trim().isEmpty) {
                                              setState(() {
                                                isInvalid = true;
                                                errorMsg = AppLocalizations.of(
                                                        context)!
                                                    .translate(
                                                        'error_text_empty');
                                              });
                                            } else if (isEmailValid(textEmailController.text) ==
                                                false) {
                                              setState(() {
                                                isInvalid = true;
                                                errorMsg = AppLocalizations.of(
                                                        context)!
                                                    .translate(
                                                        'email_error_format');
                                              });
                                            } else if (isAtLeastSixCharacters(
                                                    pass) ==
                                                false) {
                                              setState(() {
                                                isInvalid = true;
                                                errorMsg = AppLocalizations.of(
                                                        context)!
                                                    .translate(
                                                        'error_text_min_password');
                                              });
                                            } else {
                                              if (confirmPass == pass) {
                                                // UserCredential userCredential =
                                                try {
                                                  await FirebaseAuth.instance
                                                      .createUserWithEmailAndPassword(
                                                          email: textEmailController.text,
                                                          password: pass)
                                                      .then((value) {
                                                    // Đăng ký thành công
                                                    idUserRegistry =
                                                        value.user!.uid;
                                                    StorePreferences.setEmail(
                                                        value.user!.email!);
                                                    // Gọi hàm để tạo dữ liệu người dùng (createUserStore)
                                                    createUserStore();
                                                    // Hiển thị thông báo thành công và đóng màn hình đăng ký
                                                    ToastCommon.showToast(
                                                        AppLocalizations.of(
                                                                context)!
                                                            .translate(
                                                                'toast_registry_success'));
                                                    Navigator.pop(context);
                                                  });
                                                } catch (e) {
                                                  // Xử lý lỗi trong `.then()`
                                                  if (e
                                                      is FirebaseAuthException) {
                                                    if (e.code ==
                                                        'email-already-in-use') {
                                                      // Email đã tồn tại, hiển thị thông báo tương ứng
                                                      // ignore: use_build_context_synchronously
                                                      showDialog(
                                                        context: context,
                                                        builder: (context) {
                                                          return ErrorPopup(
                                                            message: AppLocalizations
                                                                    .of(
                                                                        context)!
                                                                .translate(
                                                                    'toast_registry_email_exits'),
                                                            buttonText: 'OK',
                                                            onPress: () {
                                                              Navigator.pop(
                                                                  context);
                                                            },
                                                          );
                                                        },
                                                      );
                                                    }
                                                  }
                                                }
                                                ;
                                              } else {
                                                setState(() {
                                                  isInvalid = true;
                                                  errorMsg = AppLocalizations
                                                          .of(context)!
                                                      .translate(
                                                          'error_text_password_confirm');
                                                });
                                              }
                                            }
                                          },
                                          text: AppLocalizations.of(context)!
                                              .translate('registry'),
                                          width: double.infinity,
                                          height: defaultButtonHeight,
                                        ),
                                        const SizedBox(
                                          height: 16,
                                        ),
                                        PrimaryButton(
                                          press: () {
                                            Navigator.pop(context);
                                          },
                                          text: AppLocalizations.of(context)!
                                              .translate('cancel'),
                                          width: double.infinity,
                                          height: defaultButtonHeight,
                                        ),
                                        const SizedBox(height: 20),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  bool isEmailValid(String email) {
    // Biểu thức chính quy kiểm tra định dạng email
    final emailRegExp = RegExp(
      r'^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$',
    );
    return emailRegExp.hasMatch(email);
  }

  bool isAtLeastSixCharacters(String input) {
    // Biểu thức chính quy kiểm tra xem chuỗi có ít nhất 6 ký tự không
    final RegExp regex = RegExp(r'^.{6,}$');
    return regex.hasMatch(input);
  }

  void createUserStore() {
    CollectionReference collectionReference =
        FirebaseFirestore.instance.collection("Users");
    String moiQhe = 'Bản thân';
    Users user = Users(
        id: idUserRegistry,
        idUser: idUserRegistry,
        ten: textNameController.text,
        email: textEmailController.text,
        sdt: '',
        soTheBHYT: '',
        ngaySinh: '',
        imgURL: '',
        ngheNghiep: '',
        gioiTinh: '',
        danToc: '',
        tinhThanhPho: '',
        quanHuyen: '',
        phuongXa: '',
        soNha: '',
        moiQH: moiQhe,
        online: false,
        hoatDongCuoi: '',
        pushToken: '',
        listMember: []);
    Map<String, dynamic> users = user.toMap();
    collectionReference.doc(idUserRegistry).set(users);
  }
}
