import 'package:app_doan_flutter/components/button/PrimaryButton.dart';
import 'package:app_doan_flutter/components/inputform/InputText.dart';
import 'package:app_doan_flutter/components/inputform/SecurityInputText.dart';
import 'package:app_doan_flutter/components/popup/InputPopup.dart';
import 'package:app_doan_flutter/components/toast/ToastCommon.dart';
import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/screens/login/ResistryScreen.dart';
import 'package:app_doan_flutter/screens/main/MainScreen.dart';
import 'package:app_doan_flutter/untils/AppLocalizations.dart';
import 'package:app_doan_flutter/untils/FutureMessage.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../config/styles/Dimens.dart';
import '../splash/FirstScreen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});
  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController textEmailController = TextEditingController();
  FocusNode _focusEmail = FocusNode();
  String pass = '';
  bool hidePassword = true;
  bool isInvalid = false;
  String errorMsg = '';
  String resetPass = '';

  @override
  void initState() {
    textEmailController.text = StorePreferences.getEmail();
    resetPass = textEmailController.text;
    super.initState();
    _focusEmail.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    setState(() {});
  }

  @override
  void dispose() {
    _focusEmail.dispose();
    super.dispose();
  }

  void reloadScreen() {
    textEmailController.text = StorePreferences.getEmail();
    resetPass = textEmailController.text;
  }

  // Sử dụng SharedPreferencesManager
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: LayoutBuilder(
          builder: (context, constraints) {
            double deviceHeight = MediaQuery.of(context).size.height;
            double sizedBoxHeight = 0;
            if (deviceHeight - 350 < 340)
              sizedBoxHeight = deviceHeight -
                  350; // 350 là chiều cao tối thiểu của khung đăng nhập
            else
              sizedBoxHeight = deviceHeight *
                  0.45; // 340 là chiều cao tối thiểu cho tiêu đề và ảnh
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                    minWidth: constraints.maxWidth,
                    minHeight: constraints.maxHeight),
                child: IntrinsicHeight(
                  child: Stack(
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(16, 53, 16, 16),
                            child: Row(
                              children: [
                                SvgPicture.asset(
                                  'assets/images/logo_app.svg',
                                  width: 57,
                                  height: 57,
                                ),
                                Container(
                                  padding:
                                      const EdgeInsets.fromLTRB(12, 0, 0, 0),
                                  child: Column(
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.fromLTRB(
                                            0, 0, 0, 8),
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .translate('name_app'),
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 20,
                                            color: Color(0xFF1C4983),
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                              mainAxisAlignment: MainAxisAlignment.center,
                            ),
                          ),
                          Padding(
                            child: SvgPicture.asset(
                              'assets/images/hospital_login.svg',
                              width: 270.03, // 270.03
                              height: 183, // 183
                            ),
                            padding: EdgeInsets.fromLTRB(0, 0, 0, 17),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          SizedBox(
                            height: sizedBoxHeight,
                          ),
                          Expanded(
                            child: Container(
                              constraints: const BoxConstraints(
                                minHeight: 350,
                              ),
                              decoration: const BoxDecoration(
                                color: primaryColor,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(30),
                                    topRight: Radius.circular(30)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Color(0x26006A67),
                                    offset: Offset(0, -4),
                                    blurRadius: 6,
                                  ),
                                ],
                              ),
                              child: Stack(
                                children: [
                                  Positioned(
                                    top: -3,
                                    right: 38,
                                    child: Container(
                                        width: 130,
                                        height: 130,
                                        decoration: const BoxDecoration(
                                          color: Color(0x0DFFFFFF),
                                          shape: BoxShape.circle,
                                        )),
                                  ),
                                  Positioned(
                                    top: 31,
                                    right: -106,
                                    child: Container(
                                        width: 212,
                                        height: 212,
                                        decoration: const BoxDecoration(
                                          color: Color(0x0DFFFFFF),
                                          shape: BoxShape.circle,
                                        )),
                                  ),
                                  Positioned(
                                    left: -105,
                                    bottom: -53,
                                    child: Container(
                                        width: 272,
                                        height: 272,
                                        decoration: const BoxDecoration(
                                          color: Color(0x0DFFFFFF),
                                          shape: BoxShape.circle,
                                        )),
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.only(
                                        top: defaultButtonHeight),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Text(
                                          AppLocalizations.of(context)!
                                              .translate('login_title'),
                                          textAlign: TextAlign.center,
                                          style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20,
                                              color: Color(0xFFFDFDFD)),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(top: 8),
                                          child: Visibility(
                                            child: Text(
                                              errorMsg,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Color(0xFFB43939),
                                                fontSize: 14,
                                              ),
                                            ),
                                            visible: isInvalid,
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              19.52, 16, 31.34, 0),
                                          child: InputText(
                                            focusedBorderColor: Colors.white,
                                            labelText:
                                                AppLocalizations.of(context)!
                                                    .translate('email'),
                                            hintText:
                                                AppLocalizations.of(context)!
                                                    .translate(
                                                        'email_registry_hint'),
                                            textController: textEmailController,
                                            focusNode: _focusEmail,
                                          ),
                                        ),
                                        SecurityInputText(
                                          focusedBorderColor: Colors.white,
                                          hintText:
                                              AppLocalizations.of(context)!
                                                  .translate('password_hint'),
                                          labelText:
                                              AppLocalizations.of(context)!
                                                  .translate('password'),
                                          value: pass,
                                          isHidePassword: hidePassword,
                                          onChanged: (value) =>
                                              setState(() => pass = value),
                                          onClickIcon: () => setState(() =>
                                              hidePassword = !hidePassword),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              19.52, 16, 31.34, 0),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              GestureDetector(
                                                child: Text(
                                                  AppLocalizations.of(context)!
                                                      .translate(
                                                          'forgot_password'),
                                                  style: const TextStyle(
                                                      color: buttonTextColor),
                                                ),
                                                onTap: (() {
                                                  showDialog(
                                                      context: context,
                                                      builder: ((context) {
                                                        return InputPopup(
                                                            value: resetPass,
                                                            buttonText: "OK",
                                                            title: AppLocalizations
                                                                    .of(
                                                                        context)!
                                                                .translate(
                                                                    'input_popup_email'),
                                                            hintText: AppLocalizations
                                                                    .of(
                                                                        context)!
                                                                .translate(
                                                                    'email_registry_hint'),
                                                            onChanged: (value) {
                                                              setState(() {
                                                                resetPass =
                                                                    value;
                                                              });
                                                            },
                                                            onPress: () {
                                                              resetPassword(
                                                                  resetPass);
                                                            });
                                                      }));
                                                }),
                                              ),
                                            ],
                                          ),
                                        ),
                                        const SizedBox(height: 16),
                                        PrimaryButton(
                                          press: () {
                                            if (textEmailController.text
                                                    .trim()
                                                    .isEmpty ||
                                                pass.trim().isEmpty) {
                                              setState(() {
                                                isInvalid = true;
                                                errorMsg = AppLocalizations.of(
                                                        context)!
                                                    .translate(
                                                        'error_login_empty');
                                              });
                                            } else {
                                              FirebaseAuth.instance
                                                  .signInWithEmailAndPassword(
                                                      email: textEmailController
                                                          .text,
                                                      password: pass)
                                                  .then((value) async {
                                                String idUserCurrent =
                                                    FirebaseAuth.instance
                                                        .currentUser!.uid;
                                                StorePreferences.setIdUser(
                                                    idUserCurrent);
                                                StorePreferences.setEmail(
                                                    textEmailController.text);
                                                StorePreferences.setPassword(
                                                    pass);
                                                updateStatusOnlineUser(
                                                    idUserCurrent, true);
                                                ToastCommon.showToast(
                                                    AppLocalizations.of(
                                                            context)!
                                                        .translate(
                                                            'toast_login_success'));
                                                readUsers().then((value) =>
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                MainScreen())));
                                              }).onError((error, stackTrace) {
                                                setState(() {
                                                  isInvalid = true;
                                                  errorMsg = AppLocalizations
                                                          .of(context)!
                                                      .translate(
                                                          'error_login_incorrect');
                                                });
                                              });
                                            }
                                          },
                                          text: AppLocalizations.of(context)!
                                              .translate('login'),
                                          width: double.infinity,
                                          height: defaultButtonHeight,
                                        ),
                                        const SizedBox(
                                          height: 16,
                                        ),
                                        PrimaryButton(
                                          press: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        FirstScreen()));
                                          },
                                          text: AppLocalizations.of(context)!
                                              .translate('cancel'),
                                          width: double.infinity,
                                          height: defaultButtonHeight,
                                        ),
                                        const SizedBox(height: 16),
                                        RichText(
                                            text: TextSpan(children: [
                                          TextSpan(
                                            text: AppLocalizations.of(context)!.translate('request_login'),
                                            style: TextStyle(
                                              color: buttonTextColor,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400,
                                            ),
                                            // textAlign: TextAlign.left,
                                          ),
                                          TextSpan(
                                            text: AppLocalizations.of(context)!.translate('registry_title'),
                                            style: const TextStyle(
                                                color: warningColor),
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () {
                                                Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                RegistryScreen()))
                                                    .then((value) =>
                                                        reloadScreen());
                                              },
                                          )
                                        ])),
                                        const SizedBox(height: 20),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Future<void> readUsers() async {
    DocumentSnapshot documentSnapshot = await FirebaseFirestore.instance
        .collection("Users")
        .doc(StorePreferences.getIdUser())
        .get();
    if (documentSnapshot.exists) {
      //lay du lieu
      Map<String, dynamic> userData =
          documentSnapshot.data() as Map<String, dynamic>;
      StorePreferences.setName(userData['ten']);
    } else {
      StorePreferences.setName('');
      print(StorePreferences.getName());
    }
  }

  Future<void> resetPassword(String textpass) async {
    try {
      if (isEmailValid(textpass) == false) {
        ToastCommon.showToast(
            AppLocalizations.of(context)!.translate('email_error_format'));
      } else {
        await FirebaseAuth.instance
            .sendPasswordResetEmail(email: textpass.trim())
            .then((value) {
          ToastCommon.showToast(AppLocalizations.of(context)!
              .translate('toast_reset_email_success'));
          Navigator.pop(context);
        }).catchError((e) {
          ToastCommon.showToast("Rất tiếc lỗi: ${e.toString()}");
        });
      }
    } on FirebaseAuthException catch (e) {
      ToastCommon.showToast("Rất tiếc lỗi: ${e.toString()}");
    }
  }

  bool isEmailValid(String email) {
    // Biểu thức chính quy kiểm tra định dạng email
    final emailRegExp = RegExp(
      r'^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$',
    );
    return emailRegExp.hasMatch(email);
  }
}
