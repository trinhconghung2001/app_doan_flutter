class FamilyMember {
  String id;
  String idUser;
  String moiQH;
  String ten;
  String sdt;
  String soTheBHYT;
  String ngaySinh;
  String ngheNghiep;
  String gioiTinh;
  String danToc;
  String tinhThanhPho;
  String quanHuyen;
  String phuongXa;
  String soNha;
  String imgURL;

  factory FamilyMember.defaultContructor() {
    return FamilyMember(
        id: '',
        idUser: '',
        moiQH: '',
        ten: '',
        sdt: '',
        soTheBHYT: '',
        ngaySinh: '',
        ngheNghiep: '',
        gioiTinh: '',
        danToc: '',
        tinhThanhPho: '',
        quanHuyen: '',
        phuongXa: '',
        imgURL: '',
        soNha: '');
  }

  FamilyMember({
    required this.id,
    required this.idUser,
    required this.moiQH,
    required this.ten,
    required this.sdt,
    required this.soTheBHYT,
    required this.ngaySinh,
    required this.ngheNghiep,
    required this.gioiTinh,
    required this.danToc,
    required this.tinhThanhPho,
    required this.quanHuyen,
    required this.phuongXa,
    required this.soNha,
    required this.imgURL,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'idUser': idUser,
      'moiQH': moiQH,
      'ten': ten,
      'sdt': sdt,
      'soTheBHYT': soTheBHYT,
      'ngaySinh': ngaySinh,
      'ngheNghiep': ngheNghiep,
      'gioiTinh': gioiTinh,
      'danToc': danToc,
      'tinhThanhPho': tinhThanhPho,
      'quanHuyen': quanHuyen,
      'phuongXa': phuongXa,
      'soNha': soNha,
      'imgURL': imgURL,
    };
  }

  factory FamilyMember.fromMap(Map<String, dynamic> map) {
    return FamilyMember(
      id: map['id'],
      idUser: map['idUser'],
      moiQH: map['moiQH'],
      ten: map['ten'],
      sdt: map['sdt'],
      soTheBHYT: map['soTheBHYT'],
      ngaySinh: map['ngaySinh'],
      ngheNghiep: map['ngheNghiep'],
      gioiTinh: map['gioiTinh'],
      danToc: map['danToc'],
      tinhThanhPho: map['tinhThanhPho'],
      quanHuyen: map['quanHuyen'],
      phuongXa: map['phuongXa'],
      soNha: map['soNha'],
      imgURL: map['imgURL']
    );
  }
}
