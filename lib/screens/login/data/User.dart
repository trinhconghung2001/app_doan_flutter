import 'FamilyMember.dart';

class Users extends FamilyMember {
  final String email;
  final bool online;
  final String hoatDongCuoi;
  String pushToken;
  final List<FamilyMember> listMember;

  factory Users.defaultContructor() {
    return Users(
        id: '',
        idUser: '',
        moiQH: '',
        ten: '',
        email: '',
        sdt: '',
        soTheBHYT: '',
        ngaySinh: '',
        ngheNghiep: '',
        gioiTinh: '',
        danToc: '',
        tinhThanhPho: '',
        quanHuyen: '',
        phuongXa: '',
        soNha: '',
        imgURL: '',
        online: false,
        hoatDongCuoi: '',
        pushToken: '',
        listMember: []);
  }
  Users({
    required super.id,
    required super.idUser,
    required super.moiQH,
    required super.ten,
    required this.email,
    required super.sdt,
    required super.soTheBHYT,
    required super.ngaySinh,
    required super.ngheNghiep,
    required super.gioiTinh,
    required super.danToc,
    required super.tinhThanhPho,
    required super.quanHuyen,
    required super.phuongXa,
    required super.soNha,
    required super.imgURL,
    required this.listMember,
    required this.online,
    required this.hoatDongCuoi,
    required this.pushToken,
  });

  @override
  Map<String, dynamic> toMap() {
    final map = super.toMap();
    map['email'] = email;
    map['online'] = online;
    map['hoatDongCuoi'] = hoatDongCuoi;
    map['pushToken'] = pushToken;
    map['listNguoiThan'] = listMember.map((member) => member.toMap()).toList();
    return map;
  }

  factory Users.fromMap(Map<String, dynamic> map) {
    final List<dynamic> memberList = map['listNguoiThan'];
    final List<FamilyMember> members =
        memberList.map(((e) => FamilyMember.fromMap(e))).toList();
    return Users(
      id: map['id'],
      idUser: map['idUser'],
      moiQH: map['moiQH'],
      ten: map['ten'],
      email: map['email'],
      sdt: map['sdt'],
      soTheBHYT: map['soTheBHYT'],
      ngaySinh: map['ngaySinh'],
      ngheNghiep: map['ngheNghiep'],
      gioiTinh: map['gioiTinh'],
      danToc: map['danToc'],
      tinhThanhPho: map['tinhThanhPho'],
      quanHuyen: map['quanHuyen'],
      phuongXa: map['phuongXa'],
      soNha: map['soNha'],
      imgURL: map['imgURL'],
      online: map['online'],
      hoatDongCuoi: map['hoatDongCuoi'],
      pushToken: map['pushToken'],
      listMember: members,
    );
  }
}
