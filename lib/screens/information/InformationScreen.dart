import 'package:app_doan_flutter/screens/information/components/Body.dart';
import 'package:app_doan_flutter/screens/information/infoAdvise/InforAdvise.dart';
import 'package:app_doan_flutter/screens/information/infoResultExamination/InforResultExamination.dart';
import 'package:flutter/material.dart';

import '../../components/appbar/CustomAppBar.dart';
import '../../untils/AppLocalizations.dart';
import 'inforExamination/InforExamination.dart';
import 'inforMedication/InforMedication.dart';

class InformationScreen extends StatefulWidget {
  const InformationScreen({super.key});

  @override
  State<InformationScreen> createState() => _InformationScreenState();
}

class _InformationScreenState extends State<InformationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: AppLocalizations.of(context)!
                .translate("bottom_navigation_bar_information_title"),
            check: false,
            checkIconBack: true,
          ),
        ),
        body: Column(children: [
          Body(
            title:
                AppLocalizations.of(context)!.translate('information_advise'),
            icon: Icons.arrow_forward_ios,
            onpress: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => InforAdvise()));
            },
          ),
          const SizedBox(
            height: 8,
          ),
          Body(
            title: AppLocalizations.of(context)!
                .translate('information_book_examination'),
            icon: Icons.arrow_forward_ios,
            onpress: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => InfoExamination()));
            },
          ),
          const SizedBox(
            height: 8,
          ),
          Body(
            title: AppLocalizations.of(context)!
                .translate('information_book_medication_schedule'),
            icon: Icons.arrow_forward_ios,
            onpress: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => InforMedication()));
            },
          ),
          const SizedBox(
            height: 8,
          ),
          // Body(
          //   title: AppLocalizations.of(context)!
          //       .translate('information_book_vaccination'),
          //   icon: Icons.arrow_forward_ios,
          //   onpress: () {
          //     Navigator.push(context,
          //         MaterialPageRoute(builder: (context) => InforVaccination()));
          //   },
          // ),
          // const SizedBox(
          //   height: 8,
          // ),
          Body(
            title: AppLocalizations.of(context)!
                .translate('information_result_examination'),
            icon: Icons.arrow_forward_ios,
            onpress: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => InforResultExamination()));
            },
          )
        ]));
  }
}
