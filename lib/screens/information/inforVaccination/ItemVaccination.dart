import 'package:app_doan_flutter/config/styles/Dimens.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:flutter/material.dart';

import '../../../components/inputform/RowTextView.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../item/ItemInformation.dart';
import '../../../untils/AppLocalizations.dart';

class ItemVaccination extends StatelessWidget {
  final bool? visibleBVien;
  final String trangThai;
  final String benhNhan;
  final String benhVien;
  final String goiVaccine;
  final String ngayTiem;
  final String gioTiem;
  const ItemVaccination({
    super.key,
    this.visibleBVien = true,
    required this.trangThai,
    required this.benhNhan,
    required this.benhVien,
    required this.goiVaccine,
    required this.ngayTiem,
    required this.gioTiem,
  });

  @override
  Widget build(BuildContext context) {
    Color? titleColor;
    if (trangThai == status_1) {
      titleColor = primaryColor;
    } else if (trangThai == status_2) {
      titleColor = backgroudSearch;
    } else if (trangThai == status_3) {
      titleColor = warningColor;
    }
    return ItemInformation(
      widgetBottom: Column(
        children: [
          RowTextView(
            title: AppLocalizations.of(context)!.translate('title_patient'),
            content: benhNhan,
            flexItemLeft: 2,
            flexItemRight: 4,
          ),
          Visibility(
            visible: visibleBVien!,
            child: RowTextView(
              title: AppLocalizations.of(context)!.translate('select_hospital'),
              content: benhVien,
              flexItemLeft: 2,
              flexItemRight: 4,
            ),
          ),
          RowTextView(
            title: AppLocalizations.of(context)!
                .translate('select_vaccination_package'),
            content: goiVaccine,
            flexItemLeft: 2,
            flexItemRight: 4,
          ),
          RowTextView(
            title: AppLocalizations.of(context)!
                .translate('select_day_vaccination'),
            content: ngayTiem,
            flexItemLeft: 2,
            flexItemRight: 4,
          ),
          RowTextView(
            title: AppLocalizations.of(context)!
                .translate('select_time_vaccination'),
            content: gioTiem,
            flexItemLeft: 2,
            flexItemRight: 4,
            paddingBottom: defaultPaddingMin,
          ),
        ],
      ),
      widgetTop: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Text(
              trangThai,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
      titleColor: titleColor,
    );
  }
}
