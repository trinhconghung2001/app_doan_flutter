import 'package:app_doan_flutter/components/bottomnavigator/BottomNavigator.dart';
import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/components/loading/EmptyData.dart';
import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/category/ListIdCategory.dart';
import 'package:app_doan_flutter/screens/home/bookVaccine/data/Vaccination.dart';
import 'package:app_doan_flutter/screens/home/suggestVaccine/Vaccine.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/screens/manage/data/Hospital.dart';
import 'package:app_doan_flutter/untils/FutureHospital.dart';
import 'package:app_doan_flutter/untils/FutureVaccination.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/InputDropDown.dart';
import '../../../components/inputform/InputTextField.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureRoom.dart';
import '../../../untils/FutureUser.dart';
import '../../../untils/StorePreferences.dart';
import '../../category/ListCategory.dart';
import '../../manage/data/Room.dart';

class EditVaccination extends StatefulWidget {
  final Vaccination vaccination;
  const EditVaccination({super.key, required this.vaccination});

  @override
  State<EditVaccination> createState() => _EditVaccinationState();
}

class _EditVaccinationState extends State<EditVaccination> {
  late Future<void> _loadDataEdit;
  final FocusNode _focusGhiChu = FocusNode();
  final FocusNode _focusTienSuBenh = FocusNode();
  String textHoSo = '';
  String textBenhVien = '';
  String textPhongTiem = '';
  String textNgayTiem = '';
  String textGioTiem = '';
  String textGoiVaccine = '';

  String idBenhVien = '';
  String idPhongTiem = '';
  String idBenhNhan = '';
  String idVaccine = '';
  @override
  void initState() {
    _loadDataEdit = loadData();
    super.initState();
    _focusGhiChu.addListener(_onFocusChange);
    _focusTienSuBenh.addListener(_onFocusChange);
  }

  bool validate = false;
  DateTime dateTime = DateTime.now();
  final TextEditingController textGhiChu = TextEditingController();
  final TextEditingController textTienSuBenh = TextEditingController();

  void _onFocusChange() {
    setState(() {});
  }

  @override
  void dispose() {
    _focusGhiChu.dispose();
    _focusTienSuBenh.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: AppLocalizations.of(context)!
                  .translate("app_bar_edit_vaccine"),
              check: false,
              onPress: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: Scaffold(
            body: SingleChildScrollView(
              child: FutureBuilder(
                future: _loadDataEdit,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return CircularProgressIndicatorWidget();
                  } else if (snapshot.hasError) {
                    return EmptyData();
                  } else {
                    return Container(
                      decoration: BoxDecorationContainer(),
                      padding: const EdgeInsets.only(
                          top: 16, left: 16, right: 16, bottom: 16),
                      margin: const EdgeInsets.only(
                          top: 8, left: 16, right: 16, bottom: 0),
                      child: Column(
                        children: [
                          InputDropDown(
                              onPress: () {
                                guiNhanIDDrop(
                                    defaultHoSo,
                                    AppLocalizations.of(context)!
                                        .translate('category_title_file'));
                              },
                              value: textHoSo,
                              title: AppLocalizations.of(context)!
                                  .translate("select_file"),
                              checkRangBuoc: true,
                              icon: Icons.arrow_drop_down,
                              isError: validate && textHoSo.isEmpty),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                              onPress: () {
                                guiNhanIDDrop(
                                    defaultBenhVien,
                                    AppLocalizations.of(context)!
                                        .translate('category_title_hospital'));
                              },
                              value: textBenhVien,
                              title: AppLocalizations.of(context)!
                                  .translate("select_hospital"),
                              checkRangBuoc: true,
                              icon: Icons.arrow_drop_down,
                              isError: validate && textBenhVien.isEmpty),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                              onPress: () {
                                _show();
                              },
                              value: textNgayTiem,
                              title: AppLocalizations.of(context)!
                                  .translate("select_day_vaccination"),
                              checkRangBuoc: true,
                              icon: Icons.calendar_month,
                              isError: validate && textNgayTiem.isEmpty),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                              onPress: () {
                                guiNhanIDDrop(defaultPhongTiem, 'phòng tiêm');
                              },
                              value: textPhongTiem,
                              title: AppLocalizations.of(context)!.translate(
                                  "category_title_department_vaccine"),
                              checkRangBuoc: true,
                              icon: Icons.arrow_drop_down,
                              isError: validate && textPhongTiem.isEmpty),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                              onPress: () {
                                guiNhanDrop(
                                    defaultGioTiem,
                                    AppLocalizations.of(context)!.translate(
                                        'category_title_day_vaccination'));
                              },
                              value: textGioTiem,
                              title: AppLocalizations.of(context)!
                                  .translate("select_time_vaccination"),
                              checkRangBuoc: true,
                              icon: Icons.arrow_drop_down,
                              isError: validate && textGioTiem.isEmpty),
                          const SizedBox(
                            height: 16,
                          ),
                          InputDropDown(
                              onPress: () {
                                guiNhanVaccine(
                                    defaultGoiVaccine,
                                    AppLocalizations.of(context)!.translate(
                                        'category_title_vaccination_package'),
                                    textHoSo);
                              },
                              value: textGoiVaccine,
                              title: AppLocalizations.of(context)!
                                  .translate("select_vaccination_package"),
                              checkRangBuoc: true,
                              icon: Icons.arrow_drop_down,
                              isError: validate && textGoiVaccine.isEmpty),
                          const SizedBox(
                            height: 16,
                          ),
                          InputTextField(
                            title: AppLocalizations.of(context)!
                                .translate("content_note"),
                            checkRangBuoc: false,
                            focusNode: _focusGhiChu,
                            textController: textGhiChu,
                            onPress: () {
                              setState(() {
                                textGhiChu.clear();
                              });
                            },
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          InputTextField(
                            title: AppLocalizations.of(context)!
                                .translate("select_anamnesis"),
                            checkRangBuoc: true,
                            focusNode: _focusTienSuBenh,
                            textController: textTienSuBenh,
                            isError: validate && textTienSuBenh.text.isEmpty,
                            onPress: () {
                              setState(() {
                                textTienSuBenh.clear();
                              });
                            },
                          ),
                        ],
                      ),
                    );
                  }
                },
              ),
            ),
            bottomNavigationBar: BottomNavigator(
              widget: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: 16,
                  ),
                  Expanded(
                      child: CustomButton(
                    text: AppLocalizations.of(context)!
                        .translate("button_cancel"),
                    height: 32,
                    icon: Icons.cancel_outlined,
                    iconColor: primaryColor,
                    textColor: primaryColor,
                    background: backgroundScreenColor,
                    press: () {
                      Navigator.pop(context);
                    },
                  )),
                  Container(
                    width: 8,
                  ),
                  Expanded(
                      child: CustomButton(
                    text: AppLocalizations.of(context)!
                        .translate("button_update"),
                    icon: Icons.save,
                    height: 32,
                    press: () {
                      checkValidate();
                      if (validate == false) {
                        editVaccination();
                      }
                    },
                  )),
                  Container(
                    width: 16,
                  ),
                ],
              ),
            ),
          )),
    );
  }

  void guiNhanDrop(String loai, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      if (value != null) {
        if (loai == defaultGioTiem) {
          setState(() {
            textGioTiem = value;
          });
        }
      }
    });
  }

  void guiNhanIDDrop(String loai, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListIdCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      if (value != null) {
        if (loai == defaultHoSo) {
          setState(() {
            textHoSo = value[0];
            idBenhNhan = value[1];
          });
        } else if (loai == defaultBenhVien) {
          setState(() {
            textBenhVien = value[0];
            idBenhVien = value[1];
          });
          StorePreferences.setIdHospital(idBenhVien);
        } else if (loai == defaultPhongTiem) {
          setState(() {
            textPhongTiem = value[0];
            idPhongTiem = value[1];
          });
        }
      }
    });
  }

  void guiNhanVaccine(String loai, String titleAppbar, String idNguoiThan) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListIdCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                  idNguoiThan: idBenhNhan,
                ))).then((value) {
      if (value != null) {
        setState(() {
          textGoiVaccine = value[0];
          idVaccine = value[1];
        });
      }
    });
  }

  void _show() async {
    final DateTime? result = await showDatePicker(
      context: context,
      initialDate: dateTime,
      firstDate: DateTime(2024),
      lastDate: DateTime(2025),
      locale: const Locale('vi'),
    );
    if (result != null) {
      setState(() {
        dateTime = result;
        textNgayTiem = '${formatDate(dateTime, [dd, '-', mm, '-', yyyy])}';
      });
    }
  }

  void checkValidate() {
    if (textHoSo.isNotEmpty &&
        textBenhVien.isNotEmpty &&
        textNgayTiem.isNotEmpty &&
        textGioTiem.isNotEmpty &&
        textGoiVaccine.isNotEmpty &&
        textPhongTiem.isNotEmpty &&
        textTienSuBenh.text.isNotEmpty) {
      setState(() {
        validate = false;
      });
    } else {
      setState(() {
        validate = true;
      });
    }
  }

  Future<void> editVaccination() async {
    try {
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('RegistrationSchedule')
          .doc(StorePreferences.getIdUser());
      List<Vaccination> listVaccination =
          await readAllVaccination(StorePreferences.getIdUser());
      Vaccination vaccinationNew = Vaccination(
          id: widget.vaccination.id,
          trangThai: widget.vaccination.trangThai,
          idBenhNhan: idBenhNhan,
          idBenhVien: idBenhVien,
          idPhongTiem: idPhongTiem,
          gioTiem: textGioTiem,
          ngayTiem: textNgayTiem,
          idVaccine: idVaccine,
          ghiChu: textGhiChu.text,
          tienSuBenh: textTienSuBenh.text,
          idUser: StorePreferences.getIdUser());
      for (Vaccination e in listVaccination) {
        if (e.id == widget.vaccination.id) {
          listVaccination.remove(e);
          break;
        }
      }
      if (listVaccination.any((e) =>
              e.idVaccine == vaccinationNew.idVaccine &&
              e.idBenhVien == vaccinationNew.idBenhVien &&
              e.ngayTiem == vaccinationNew.ngayTiem &&
              e.idPhongTiem == vaccinationNew.idPhongTiem &&
              e.gioTiem == vaccinationNew.gioTiem) ==
          false) {
        listVaccination.add(vaccinationNew);
        documentReference.update({
          'vaccination': listVaccination.map((e) => e.toMap()).toList()
        }).then((value) {
          ToastCommon.showToast(AppLocalizations.of(context)!
              .translate('toast_edit_vaccination_success'));
          Navigator.pop(context);
        });
      } else {
        showDialog(
            context: context,
            builder: (context) {
              return ErrorPopup(
                message: AppLocalizations.of(context)!
                    .translate('error_book_duplicate'),
                buttonText: 'OK',
                onPress: () {
                  Navigator.pop(context);
                },
              );
            });
      }
    } on Exception catch (e) {
      print(e);
      // ignore: use_build_context_synchronously
      showDialog(
          context: context,
          builder: (context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_edit_vaccination'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          });
    }
  }

  Future<void> loadData() async {
    FamilyMember hoSo = await readIDBenhNhan(
        StorePreferences.getIdUser(), widget.vaccination.idBenhNhan);
    Room phongTiem = await readIDPhongTiem(widget.vaccination.idPhongTiem);
    Vaccine vaccine = await readIDGoiVaccine(widget.vaccination.idVaccine);
    Hospital hospital = await readIDHospital(widget.vaccination.idBenhVien);
    textHoSo = hoSo.moiQH;
    textPhongTiem = phongTiem.tenPhong;
    textBenhVien = hospital.ten;
    textNgayTiem = widget.vaccination.ngayTiem;
    textGioTiem = widget.vaccination.gioTiem;
    textGoiVaccine = vaccine.ten;
    textGhiChu.text = widget.vaccination.ghiChu;
    textTienSuBenh.text = widget.vaccination.tienSuBenh;

    idBenhVien = widget.vaccination.idBenhVien;
    idPhongTiem = widget.vaccination.idPhongTiem;
    idBenhNhan = widget.vaccination.idBenhNhan;
    idVaccine = widget.vaccination.idVaccine;
  }
}
