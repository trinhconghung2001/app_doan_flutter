import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/category/ListStatus.dart';
import 'package:app_doan_flutter/screens/home/bookVaccine/data/Vaccination.dart';
import 'package:app_doan_flutter/screens/home/suggestVaccine/Vaccine.dart';
import 'package:app_doan_flutter/screens/information/inforVaccination/DetailVaccination.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/screens/manage/data/Hospital.dart';
import 'package:app_doan_flutter/untils/FutureHospital.dart';
import 'package:app_doan_flutter/untils/FutureVaccination.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/filtersearch/RowFilter.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureUser.dart';
import '../../../untils/StorePreferences.dart';
import 'ItemVaccination.dart';

class InforVaccination extends StatefulWidget {
  const InforVaccination({super.key});

  @override
  State<InforVaccination> createState() => _InforVaccinationState();
}

class _InforVaccinationState extends State<InforVaccination> {
  late Future<void> _loadDataVaccination;
  List<Vaccination> listVaccination = [];
  List<FamilyMember> listFamily = [];
  List<Vaccine> listVaccine = [];
  List<Hospital> listHosital = [];
  String textDateRange = '';
  String textXacNhan = '';
  @override
  void initState() {
    _loadDataVaccination = loadData();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDataVaccination = loadData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: AppLocalizations.of(context)!
                .translate("information_book_vaccination"),
            check: true,
            onPress: () {
              Navigator.pop(context);
            },
            checkIconBack: false,
            icon: Icon(Icons.sync),
            onTap: () {
              loadScreen();
            },
          ),
        ),
        body: ListView.builder(
            itemCount: 2,
            itemBuilder: (BuildContext context, index) {
              if (index == 0) {
                return FilterVaccine(
                    dataSearch: searchData,
                    textDateRange: textDateRange,
                    textXacNhan: textXacNhan);
              } else {
                return FutureBuilder(
                    future: _loadDataVaccination,
                    builder: ((context, AsyncSnapshot snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return CircularProgressIndicatorWidget();
                      } else if (snapshot.hasError) {
                        return EmptyData(
                            msg: AppLocalizations.of(context)!
                                .translate('error_data_msg'));
                      } else {
                        if (listVaccination.length > 0) {
                          return Container(
                              margin: const EdgeInsets.only(top: 8),
                              child: ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: listVaccination.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        DetailVaccination(
                                                            idSend:
                                                                listVaccination[
                                                                        index]
                                                                    .id)))
                                            .then((value) => loadScreen());
                                      },
                                      child: ItemVaccination(
                                        trangThai:
                                            listVaccination[index].trangThai,
                                        benhNhan: listFamily[index].ten,
                                        benhVien: listHosital[index].ten,
                                        goiVaccine: listVaccine[index].ten,
                                        ngayTiem:
                                            listVaccination[index].ngayTiem,
                                        gioTiem: listVaccination[index].gioTiem,
                                      ));
                                },
                              ));
                        } else {
                          // Không có dữ liệu
                          return EmptyData();
                        }
                      }
                    }));
              }
            }),
      ),
    );
  }

  Future<void> loadData() async {
    //reset lai list
    textDateRange = 'Lựa chọn ngày';
    textXacNhan = 'Tất cả';
    listVaccination = await readAllVaccination(StorePreferences.getIdUser());
    listFamily = [];
    listVaccine = [];
    for (int i = 0; i < listVaccination.length; i++) {
      FamilyMember familyMember = await readIDBenhNhan(
          StorePreferences.getIdUser(), listVaccination[i].idBenhNhan);
      listFamily.add(familyMember);
      Vaccine vaccine = await readIDGoiVaccine(listVaccination[i].idVaccine);
      listVaccine.add(vaccine);
      Hospital hospital = await readIDHospital(listVaccination[i].idBenhVien);
      listHosital.add(hospital);
    }
  }

  void searchData(List<String> list) {
    setState(() {
      _loadDataVaccination = loadDataSearch(list);
      textDateRange = list[2];
      textXacNhan = list[3];
    });
  }

  Future<void> loadDataSearch(List<String> list) async {
    //reset lai list
    listVaccination = [];
    List<Vaccination> listResult =
        await readAllVaccination(StorePreferences.getIdUser());
    if (list[2] == 'Lựa chọn ngày') {
      if (list[0] != '' && list[1] != '') {
        // trạng thái = tất cả
        if (list[3] ==
            AppLocalizations.of(context)!.translate('category_select_all')) {
          listVaccination = listResult;
        }
        //trạng thái # tất cả
        else {
          for (Vaccination i in listResult) {
            if (i.trangThai == list[3]) {
              listVaccination.add(i);
            }
          }
        }
      } else {
        // trạng thái = tất cả
        if (list[3] ==
            AppLocalizations.of(context)!.translate('category_select_all')) {
          listVaccination = listResult;
        }
        //trạng thái # tất cả
        else {
          for (Vaccination i in listResult) {
            if (i.trangThai == list[3]) {
              listVaccination.add(i);
            }
          }
        }
      }
    }
    //textDateRange # lựa chọn ngày
    else {
      //trạng thái = tất cả
      if (list[3] ==
          AppLocalizations.of(context)!.translate('category_select_all')) {
        for (Vaccination i in listResult) {
          DateTime dateTime = DateFormat("dd-MM-yyyy").parse(i.ngayTiem);
          DateTime dateStart = DateFormat("dd-MM-yyyy").parse(list[0]);
          DateTime dateEnd = DateFormat("dd-MM-yyyy").parse(list[1]);
          if ((dateTime.isAfter(dateStart) == true &&
                  dateTime.isBefore(dateEnd) == true) ||
              dateTime == dateStart ||
              dateTime == dateEnd) {
            listVaccination.add(i);
          }
        }
      } else {
        for (Vaccination i in listResult) {
          DateTime dateTime = DateFormat("dd-MM-yyyy").parse(i.ngayTiem);
          DateTime dateStart = DateFormat("dd-MM-yyyy").parse(list[0]);
          DateTime dateEnd = DateFormat("dd-MM-yyyy").parse(list[1]);
          if ((dateTime.isAfter(dateStart) == true &&
                  dateTime.isBefore(dateEnd) == true) ||
              dateTime == dateStart ||
              dateTime == dateEnd) {
            if (i.trangThai == list[3]) {
              listVaccination.add(i);
            }
          }
        }
      }
    }
  }
}

// ignore: must_be_immutable
class FilterVaccine extends StatefulWidget {
  final void Function(List<String> list) dataSearch;
  String textDateRange;
  String textXacNhan;
  FilterVaccine(
      {super.key,
      required this.dataSearch,
      required this.textDateRange,
      required this.textXacNhan});

  @override
  State<FilterVaccine> createState() => _FilterVaccineState();
}

class _FilterVaccineState extends State<FilterVaccine> {
  DateTimeRange? dateTimeRange;
  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecorationContainer(color: filterColor),
        margin: const EdgeInsets.only(top: 8, left: 16, right: 16),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RowFilter(
              title: AppLocalizations.of(context)!
                  .translate('filter_search_date_range'),
              value: widget.textDateRange,
              icon: Icons.calendar_month,
              onPress: () {
                _show();
              },
            ),
            const SizedBox(height: 8),
            Row(
              children: <Widget>[
                Expanded(
                    flex: 2,
                    child: RowFilter(
                      title: AppLocalizations.of(context)!
                          .translate('filter_search_state_confirm'),
                      value: widget.textXacNhan,
                      icon: Icons.arrow_drop_down,
                      onPress: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ListStatus(
                                      loai: defaultStatusBook,
                                      titleAppbar: AppLocalizations.of(context)!
                                          .translate('category_title_status'),
                                    ))).then((value) {
                          if (value != '') {
                            setState(() {
                              widget.textXacNhan = value;
                            });
                          }
                        });
                      },
                    )),
                const SizedBox(
                  width: 8,
                ),
                Expanded(
                    flex: 2,
                    child: Column(
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            '',
                            style: TextStyle(fontSize: 12, color: textColor),
                          ),
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        CustomButton(
                            text: AppLocalizations.of(context)!
                                .translate('button_search'),
                            icon: Icons.search,
                            press: () {
                              if (dateTimeRange != null) {
                                widget.dataSearch([
                                  DateFormat('dd-MM-yyyy')
                                      .format(dateTimeRange!.start),
                                  DateFormat('dd-MM-yyyy')
                                      .format(dateTimeRange!.end),
                                  widget.textDateRange,
                                  widget.textXacNhan
                                ]);
                              } else {
                                widget.dataSearch([
                                  '',
                                  '',
                                  widget.textDateRange,
                                  widget.textXacNhan
                                ]);
                              }
                            },
                            height: 36),
                      ],
                    )),
              ],
            ),
          ],
        ));
  }

  void _show() async {
    final DateTimeRange? result = await showDateRangePicker(
      context: context,
      firstDate: DateTime(2019),
      lastDate: DateTime(2024),
      currentDate: DateTime.now(),
      saveText: 'OK',
      initialDateRange: dateTimeRange,
      locale: const Locale('vi'),
    );
    if (result != null) {
      setState(() {
        dateTimeRange = result;
        widget.textDateRange =
            DateFormat('dd-MM-yyyy').format(dateTimeRange!.start) +
                ' - ' +
                DateFormat('dd-MM-yyyy').format(dateTimeRange!.end);
      });
    }
  }
}
