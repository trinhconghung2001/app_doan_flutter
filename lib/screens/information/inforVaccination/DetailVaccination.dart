import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/home/bookVaccine/data/Vaccination.dart';
import 'package:app_doan_flutter/screens/home/suggestVaccine/Vaccine.dart';
import 'package:app_doan_flutter/screens/information/inforVaccination/EditVaccination.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/screens/manage/data/Doctor.dart';
import 'package:app_doan_flutter/screens/manage/data/Hospital.dart';
import 'package:app_doan_flutter/screens/manage/data/Room.dart';
import 'package:app_doan_flutter/untils/FutureHospital.dart';
import 'package:app_doan_flutter/untils/FutureVaccination.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/RowTextView.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureManageDoctor.dart';
import '../../../untils/FutureRoom.dart';
import '../../../untils/FutureUser.dart';
import '../../../untils/StorePreferences.dart';

class DetailVaccination extends StatefulWidget {
  final String idSend;
  const DetailVaccination({super.key, required this.idSend});

  @override
  State<DetailVaccination> createState() => _DetailVaccinationState();
}

class _DetailVaccinationState extends State<DetailVaccination> {
  Vaccination detailVaccination = Vaccination.defaultContructor();
  FamilyMember nguoiDat = FamilyMember.defaultContructor();
  Room phongTiem = Room.defaultContructor();
  Doctor bacSi = Doctor.defaultContructor();
  Vaccine goiVaccine = Vaccine.defaultContructor();
  Hospital benhVien = Hospital.defaultContructor();
  late Future<void> _loadDetailVaccination;
  List<Vaccination> listVaccination = [];
  @override
  void initState() {
    _loadDetailVaccination = loadDetail();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDetailVaccination = loadDetail();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: AppLocalizations.of(context)!
                .translate("app_bar_detail_vaccination"),
            check: false,
            onPress: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Scaffold(
          body: SingleChildScrollView(
              child: FutureBuilder(
            future: _loadDetailVaccination,
            builder: (context, AsyncSnapshot snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return CircularProgressIndicatorWidget();
              } else if (snapshot.hasError) {
                return EmptyData(
                    msg: AppLocalizations.of(context)!
                        .translate('error_data_msg'));
              } else {
                return Column(
                  children: [
                    Container(
                      decoration: BoxDecorationContainer(),
                      padding: const EdgeInsets.only(
                        top: 16,
                        left: 16,
                        right: 16,
                      ),
                      margin: const EdgeInsets.only(
                          top: 8, left: 16, right: 16, bottom: 0),
                      child: RowTextView(
                        title: AppLocalizations.of(context)!
                            .translate('title_status'),
                        content: detailVaccination.trangThai,
                        flexItemLeft: 2,
                        flexItemRight: 3,
                        color2: colorTrangThai(detailVaccination.trangThai),
                      ),
                    ),
                    Container(
                      decoration: BoxDecorationContainer(),
                      padding: const EdgeInsets.only(
                        top: 16,
                        left: 16,
                        right: 16,
                      ),
                      margin: const EdgeInsets.only(
                          top: 8, left: 16, right: 16, bottom: 0),
                      child: Column(
                        children: [
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('title_patient'),
                              content: nguoiDat.moiQH,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('select_hospital'),
                              content: benhVien.ten,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          Visibility(
                            child: RowTextView(
                                title: AppLocalizations.of(context)!
                                    .translate('category_title_doctor_vaccine'),
                                content: bacSi.ten,
                                flexItemLeft: 2,
                                flexItemRight: 3),
                            visible: detailVaccination.trangThai == status_1
                                ? false
                                : true,
                          ),
                          Visibility(
                            child: RowTextView(
                                title: AppLocalizations.of(context)!.translate(
                                    'category_title_department_vaccine'),
                                content: phongTiem.tenPhong,
                                flexItemLeft: 2,
                                flexItemRight: 3),
                            visible: detailVaccination.trangThai == status_1
                                ? false
                                : true,
                          ),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('select_day_vaccination'),
                              content: detailVaccination.ngayTiem,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('select_time_vaccination'),
                              content: detailVaccination.gioTiem,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('select_vaccination_package'),
                              content: goiVaccine.ten,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('content_note'),
                              content: detailVaccination.ghiChu,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('select_anamnesis'),
                              content: detailVaccination.tienSuBenh,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                        ],
                      ),
                    ),
                  ],
                );
              }
            },
          )),
          bottomNavigationBar: BottomNavigator(
            widget: StorePreferences.getStatus() == true
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        width: 16,
                      ),
                      Expanded(
                          child: CustomButton(
                        text: AppLocalizations.of(context)!
                            .translate("button_delete"),
                        height: 32,
                        icon: Icons.cancel_outlined,
                        iconColor: primaryColor,
                        textColor: primaryColor,
                        background: backgroundScreenColor,
                        press: () {
                          deleteDetailVaccination();
                        },
                      )),
                      Container(
                        width: 8,
                      ),
                      Expanded(
                          child: CustomButton(
                        text: AppLocalizations.of(context)!
                            .translate("button_edit"),
                        icon: Icons.edit_square,
                        height: 32,
                        press: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => EditVaccination(
                                        vaccination: detailVaccination,
                                      ))).then((value) => loadScreen());
                        },
                      )),
                      Container(
                        width: 16,
                      ),
                    ],
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        width: 16,
                      ),
                      Expanded(
                          child: CustomButton(
                        text: 'OK',
                        iconDisplay: false,
                        icon: Icons.edit_square,
                        height: 32,
                        press: () {
                          Navigator.pop(context);
                        },
                      )),
                      Container(
                        width: 16,
                      ),
                    ],
                  ),
          ),
        ));
  }

  Future<void> loadDetail() async {
    CollectionReference collectionReference =
        FirebaseFirestore.instance.collection('RegistrationSchedule');
    DocumentSnapshot documentSnapshot =
        await collectionReference.doc(StorePreferences.getIdUser()).get();
    if (documentSnapshot.exists) {
      //lay du lieu
      Map<String, dynamic> dataDetail =
          documentSnapshot.data() as Map<String, dynamic>;
      //dua kieu tra ve tu data thanh list
      List<Map<String, dynamic>> result =
          List<Map<String, dynamic>>.from(dataDetail['vaccination'] as List);
      listVaccination = [];
      for (int i = 0; i < result.length; i++) {
        listVaccination.add(Vaccination.fromMap(result[i]));
      }
      for (Vaccination i in listVaccination) {
        if (i.id == widget.idSend) {
          detailVaccination = i;
          if (i.trangThai == status_1) StorePreferences.setStatus(true);
          print(StorePreferences.getStatus());
          nguoiDat =
              await readIDBenhNhan(StorePreferences.getIdUser(), i.idBenhNhan);
          phongTiem = await readIDPhongTiem(i.idPhongTiem);
          benhVien = await readIDHospital(i.idBenhVien);
          goiVaccine = await readIDGoiVaccine(i.idVaccine);
          bacSi = await readIDDoctor(phongTiem.idBacSi);
          break;
        }
      }
    } else {
      detailVaccination = Vaccination.defaultContructor();
    }
  }

  Color colorTrangThai(String trangThai) {
    if (trangThai == status_1) {
      return primaryColor;
    } else if (trangThai == status_2) {
      return backgroudSearch;
    } else if (trangThai == status_3) {
      return warningColor;
    }
    return textColor;
  }

  Future<void> deleteDetailVaccination() async {
    try {
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('RegistrationSchedule')
          .doc(StorePreferences.getIdUser());
      DocumentSnapshot documentSnapshot = await documentReference.get();
      if (documentSnapshot.exists) {
        Map<String, dynamic> userData =
            documentSnapshot.data() as Map<String, dynamic>;
        List<Map<String, dynamic>> result =
            List<Map<String, dynamic>>.from(userData['vaccination'] as List);
        List<Vaccination> listVaccination = [];
        for (int i = 0; i < result.length; i++) {
          listVaccination.add(Vaccination.fromMap(result[i]));
        }
        for (int i = 0; i < listVaccination.length; i++) {
          if (listVaccination[i].id == widget.idSend) {
            listVaccination.removeAt(i);
            break;
          }
        }
        documentReference.update({
          'vaccination': listVaccination.map((e) => e.toMap()).toList()
        }).then((value) {
          ToastCommon.showToast(AppLocalizations.of(context)!
              .translate('toast_delete_vaccination_success'));
          Navigator.pop(context);
        });
      }
    } on Exception catch (e) {
      print(e);
      // ignore: use_build_context_synchronously
      showDialog(
          context: context,
          builder: (context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_remove_vaccination'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          });
    }
  }
}
