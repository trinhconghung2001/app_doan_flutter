import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/home/bookMedication/data/Medication.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/untils/FutureMedication.dart';
import 'package:app_doan_flutter/untils/FutureUser.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/RowTextView.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/StorePreferences.dart';
import 'EditMedication.dart';

class DetailMedication extends StatefulWidget {
  final String idSend;
  const DetailMedication({super.key, required this.idSend});

  @override
  State<DetailMedication> createState() => _DetailMedicationState();
}

class _DetailMedicationState extends State<DetailMedication> {
  Medication detailMedication = Medication.defaultContructor();
  FamilyMember nguoiUong = FamilyMember.defaultContructor();
  late Future<void> _loadDetailFuture;
  List<Medication> listMedication = [];
  String gioNhac = '';
  String loaiThuoc = '';
  String cachDung = '';
  @override
  void initState() {
    _loadDetailFuture = loadDetail();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDetailFuture = loadDetail();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: AppLocalizations.of(context)!
                .translate("app_bar_detail_medication"),
            check: false,
            onPress: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Scaffold(
          body: SingleChildScrollView(
            child: FutureBuilder(
              future: _loadDetailFuture,
              builder: ((context, AsyncSnapshot snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return CircularProgressIndicatorWidget();
                } else if (snapshot.hasError) {
                  return EmptyData(
                      msg: AppLocalizations.of(context)!
                          .translate('error_data_msg'));
                } else {
                  return Column(
                    children: [
                      Container(
                        decoration: BoxDecorationContainer(),
                        padding: const EdgeInsets.only(
                          top: 16,
                          left: 16,
                          right: 16,
                        ),
                        margin: const EdgeInsets.only(
                            top: 8, left: 16, right: 16, bottom: 0),
                        child: RowTextView(
                          title: AppLocalizations.of(context)!
                              .translate('title_status'),
                          content: detailMedication.trangThai,
                          flexItemLeft: 2,
                          flexItemRight: 3,
                          color2: colorTrangThai(detailMedication.trangThai),
                        ),
                      ),
                      Container(
                        decoration: BoxDecorationContainer(),
                        padding: const EdgeInsets.only(
                          top: 16,
                          left: 16,
                          right: 16,
                        ),
                        margin: const EdgeInsets.only(
                            top: 8, left: 16, right: 16, bottom: 0),
                        child: Column(
                          children: [
                            RowTextView(
                                title: AppLocalizations.of(context)!
                                    .translate('select_people_use'),
                                content: nguoiUong.moiQH,
                                flexItemLeft: 2,
                                flexItemRight: 3),
                            RowTextView(
                                title: AppLocalizations.of(context)!
                                    .translate('select_type_medication'),
                                content: loaiThuoc,
                                flexItemLeft: 2,
                                flexItemRight: 3),
                            RowTextView(
                                title: AppLocalizations.of(context)!
                                    .translate('select_use'),
                                content: cachDung,
                                flexItemLeft: 2,
                                flexItemRight: 3),
                            RowTextView(
                                title: AppLocalizations.of(context)!
                                    .translate('select_time_use'),
                                content: gioNhac,
                                flexItemLeft: 2,
                                flexItemRight: 3),
                            RowTextView(
                                title: AppLocalizations.of(context)!
                                    .translate('select_start_day'),
                                content: detailMedication.ngayBatDau,
                                flexItemLeft: 2,
                                flexItemRight: 3),
                            RowTextView(
                                title: AppLocalizations.of(context)!
                                    .translate('select_end_day'),
                                content: detailMedication.ngayKetThuc,
                                flexItemLeft: 2,
                                flexItemRight: 3),
                            RowTextView(
                                title: AppLocalizations.of(context)!
                                    .translate('content_note'),
                                content: detailMedication.ghiChu,
                                flexItemLeft: 2,
                                flexItemRight: 3),
                          ],
                        ),
                      ),
                    ],
                  );
                }
              }),
            ),
          ),
          bottomNavigationBar: BottomNavigator(
              widget: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                width: 16,
              ),
              Expanded(
                  child: CustomButton(
                text: AppLocalizations.of(context)!.translate("button_delete"),
                height: 32,
                icon: Icons.cancel_outlined,
                iconColor: primaryColor,
                textColor: primaryColor,
                background: backgroundScreenColor,
                press: () {
                  deleteDetailMedication();
                },
              )),
              Container(
                width: 8,
              ),
              Expanded(
                  child: CustomButton(
                text: AppLocalizations.of(context)!.translate("button_edit"),
                icon: Icons.edit_square,
                height: 32,
                press: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => EditMedication(
                                idMedication: widget.idSend,
                                moiQH: nguoiUong.moiQH,
                              ))).then((value) => loadScreen());
                },
              )),
              Container(
                width: 16,
              ),
            ],
          )),
        ));
  }

  Future<void> loadDetail() async {
    listMedication = await readListMedication(StorePreferences.getIdUser());
    for (Medication i in listMedication) {
      if (i.id == widget.idSend) {
        detailMedication = i;
        nguoiUong =
            await readIDBenhNhan(StorePreferences.getIdUser(), i.idNguoiUong);
        break;
      }
    }
    gioNhac = '';
    for (int i = 0; i < detailMedication.gioNhacNho.length; i++) {
      gioNhac += detailMedication.gioNhacNho[i];
      if (i < detailMedication.gioNhacNho.length - 1) {
        gioNhac += ', ';
      }
    }
    loaiThuoc = '';
    for (int i = 0; i < detailMedication.loaiThuoc.length; i++) {
      loaiThuoc += detailMedication.loaiThuoc[i];
      if (i < detailMedication.loaiThuoc.length - 1) {
        loaiThuoc += ', ';
      }
    }
    cachDung = '';
    for (String e in detailMedication.cachDung) {
      cachDung += e;
    }
  }

  Color colorTrangThai(String trangThai) {
    if (trangThai == status_medication_1) {
      return primaryColor;
    } else if (trangThai == status_medication_2) {
      return backgroudSearch;
    } else if (trangThai == status_medication_3) {
      return warningColor;
    }
    return textColor;
  }

  Future<void> deleteDetailMedication() async {
    try {
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('RegistrationSchedule')
          .doc(StorePreferences.getIdUser());
      DocumentSnapshot documentSnapshot = await documentReference.get();
      if (documentSnapshot.exists) {
        Map<String, dynamic> userData =
            documentSnapshot.data() as Map<String, dynamic>;
        List<Map<String, dynamic>> result =
            List<Map<String, dynamic>>.from(userData['medication'] as List);
        List<Medication> listMedication = [];
        for (int i = 0; i < result.length; i++) {
          listMedication.add(Medication.fromMap(result[i]));
        }
        for (int i = 0; i < listMedication.length; i++) {
          if (listMedication[i].id == widget.idSend) {
            listMedication.removeAt(i);
            break;
          }
        }
        documentReference.update({
          'medication': listMedication.map((e) => e.toMap()).toList()
        }).then((value) {
          ToastCommon.showToast(AppLocalizations.of(context)!
              .translate('toast_delete_medication_success'));
          Navigator.pop(context);
        });
      }
    } on Exception catch (e) {
      print(e);
      // ignore: use_build_context_synchronously
      showDialog(
          context: context,
          builder: (context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_remove_medication'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          });
    }
  }
}
