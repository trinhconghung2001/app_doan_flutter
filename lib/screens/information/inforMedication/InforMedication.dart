import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/home/bookMedication/data/Medication.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/untils/FutureMedication.dart';
import 'package:app_doan_flutter/untils/FutureUser.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/filtersearch/RowFilter.dart';
import '../../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/StorePreferences.dart';
import '../../category/ListStatus.dart';
import 'DetailMedication.dart';
import 'ItemMedication.dart';

class InforMedication extends StatefulWidget {
  const InforMedication({super.key});

  @override
  State<InforMedication> createState() => _InforMedicationState();
}

class _InforMedicationState extends State<InforMedication> {
  late Future<void> _loadDataMedication;
  List<Medication> listMedication = [];
  List<FamilyMember> listNguoiUong = [];
  List<String> listThuoc = [];
  String textDateRange = '';
  String textXacNhan = '';
  @override
  void initState() {
    _loadDataMedication = loadData();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDataMedication = loadData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: AppLocalizations.of(context)!
                .translate("information_book_medication_schedule"),
            check: true,
            onPress: () {
              Navigator.pop(context);
            },
            checkIconBack: false,
            icon: Icon(Icons.sync),
            onTap: () {
              loadScreen();
            },
          ),
        ),
        body: ListView.builder(
            itemCount: 2,
            itemBuilder: (BuildContext context, index) {
              if (index == 0) {
                return FilterMedication(
                    dataSearch: searchData,
                    textDateRange: textDateRange,
                    textXacNhan: textXacNhan);
              } else {
                return FutureBuilder(
                    future: _loadDataMedication,
                    builder: (context, AsyncSnapshot snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return CircularProgressIndicatorWidget();
                      } else if (snapshot.hasError) {
                        return EmptyData(
                            msg: AppLocalizations.of(context)!
                                .translate('error_data_msg'));
                      } else {
                        if (listMedication.length > 0) {
                          return Container(
                              margin: const EdgeInsets.only(top: 8),
                              child: ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: listMedication.length,
                                itemBuilder: (BuildContext context, int index) {
                                  String cachDung = '';
                                  for(String e in listMedication[index].cachDung){
                                    cachDung+= e;
                                  }
                                  return GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetailMedication(
                                                      idSend:
                                                          listMedication[index]
                                                              .id,
                                                    ))).then(
                                            (value) => loadScreen());
                                      },
                                      child: ItemMedication(
                                        trangThai:
                                            listMedication[index].trangThai,
                                        nguoiUong: listNguoiUong[index].ten,
                                        loaiThuoc: listThuoc[index],
                                        cachDung: cachDung,
                                        ngayBatDau:
                                            listMedication[index].ngayBatDau,
                                        ngayKetThuc:
                                            listMedication[index].ngayKetThuc,
                                      ));
                                },
                              ));
                        } else {
                          // Không có dữ liệu
                          return EmptyData();
                        }
                      }
                    });
              }
            }),
      ),
    );
  }

  void searchData(List<String> list) {
    setState(() {
      _loadDataMedication = loadDataSearch(list);
      textDateRange = list[2];
      textXacNhan = list[3];
    });
  }

  Future<void> loadDataSearch(List<String> list) async {
    print("có vào loadDataSearch");
    print(list[0] + " + " + list[1] + ' + ' + list[2] + ' + ' + list[3]);
    CollectionReference collectionReference =
        FirebaseFirestore.instance.collection('RegistrationSchedule');
    DocumentSnapshot documentSnapshot =
        await collectionReference.doc(StorePreferences.getIdUser()).get();
    if (documentSnapshot.exists) {
      //lay du lieu
      Map<String, dynamic> userData =
          documentSnapshot.data() as Map<String, dynamic>;
      //convert array data -> List
      List<Map<String, dynamic>> result =
          List<Map<String, dynamic>>.from(userData['medication'] as List);
      //reset lai list
      listMedication = [];
      List<Medication> listResult = [];
      for (int i = 0; i < result.length; i++) {
        listResult.add(Medication.fromMap(result[i]));
      }
      if (list[2] == 'Lựa chọn ngày') {
        if (list[0] != '' && list[1] != '') {
          // trạng thái = tất cả
          if (list[3] ==
              AppLocalizations.of(context)!.translate('category_select_all')) {
            listMedication = listResult;
          }
          //trạng thái # tất cả
          else {
            for (Medication i in listResult) {
              if (i.trangThai == list[3]) {
                listMedication.add(i);
              }
            }
          }
        } else {
          // trạng thái = tất cả
          if (list[3] ==
              AppLocalizations.of(context)!.translate('category_select_all')) {
            listMedication = listResult;
          }
          //trạng thái # tất cả
          else {
            for (Medication i in listResult) {
              if (i.trangThai == list[3]) {
                listMedication.add(i);
              }
            }
          }
        }
      }
      //textDateRange # lựa chọn ngày
      else {
        //trạng thái = tất cả
        if (list[3] ==
            AppLocalizations.of(context)!.translate('category_select_all')) {
          for (Medication i in listResult) {
            DateTime dateMedicationStart =
                DateFormat("dd-MM-yyyy").parse(i.ngayBatDau);
            DateTime dateMedicationEnd =
                DateFormat("dd-MM-yyyy").parse(i.ngayKetThuc);
            DateTime dateStart = DateFormat("dd-MM-yyyy").parse(list[0]);
            DateTime dateEnd = DateFormat("dd-MM-yyyy").parse(list[1]);
            if ((dateMedicationStart.isAfter(dateStart) == true &&
                    dateMedicationEnd.isBefore(dateEnd) == true) ||
                (dateMedicationStart == dateStart &&
                    dateMedicationEnd == dateEnd)) {
              listMedication.add(i);
            }
          }
        } else {
          for (Medication i in listResult) {
            DateTime dateMedicationStart =
                DateFormat("dd-MM-yyyy").parse(i.ngayBatDau);
            DateTime dateMedicationEnd =
                DateFormat("dd-MM-yyyy").parse(i.ngayKetThuc);
            DateTime dateStart = DateFormat("dd-MM-yyyy").parse(list[0]);
            DateTime dateEnd = DateFormat("dd-MM-yyyy").parse(list[1]);
            if ((dateMedicationStart.isAfter(dateStart) == true &&
                    dateMedicationEnd.isBefore(dateEnd) == true) ||
                (dateMedicationStart == dateStart &&
                    dateMedicationEnd == dateEnd)) {
              if (i.trangThai == list[3]) {
                listMedication.add(i);
              }
            }
          }
        }
      }
    } else {
      listMedication = [];
    }
  }

  Future<void> loadData() async {
    textDateRange = 'Lựa chọn ngày';
    textXacNhan = 'Tất cả';
    listMedication = await readListMedication(StorePreferences.getIdUser());
    listThuoc = [];
    for (int i = 0; i < listMedication.length; i++) {
      listThuoc.add(convertList(listMedication[i].loaiThuoc));
      FamilyMember nguoiUong = await readIDBenhNhan(
          StorePreferences.getIdUser(), listMedication[i].idNguoiUong);
      listNguoiUong.add(nguoiUong);
    }
  }

  String convertList(List<dynamic> list) {
    String loaiThuoc = '';
    List<String> listString = list.map((e) => e.toString()).toList();
    for (int i = 0; i < listString.length; i++) {
      loaiThuoc += listString[i];
      if (i < listString.length - 1) {
        loaiThuoc += ', ';
      }
    }
    return loaiThuoc;
  }
}

// ignore: must_be_immutable
class FilterMedication extends StatefulWidget {
  final void Function(List<String> list) dataSearch;
  String textDateRange;
  String textXacNhan;
  FilterMedication(
      {super.key,
      required this.dataSearch,
      required this.textDateRange,
      required this.textXacNhan});

  @override
  State<FilterMedication> createState() => _FilterMedicationState();
}

class _FilterMedicationState extends State<FilterMedication> {
  DateTimeRange? dateTimeRange;
  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecorationContainer(color: filterColor),
        margin: const EdgeInsets.only(top: 8, left: 16, right: 16),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RowFilter(
              title: AppLocalizations.of(context)!
                  .translate('filter_search_date_range'),
              value: widget.textDateRange,
              icon: Icons.calendar_month,
              onPress: () {
                _show();
              },
            ),
            const SizedBox(height: 8),
            Row(
              children: <Widget>[
                Expanded(
                    flex: 2,
                    child: RowFilter(
                      title: AppLocalizations.of(context)!
                          .translate('filter_search_state_drink'),
                      value: widget.textXacNhan,
                      icon: Icons.arrow_drop_down,
                      onPress: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ListStatus(
                                      loai: defaultStatusMedication,
                                      titleAppbar: AppLocalizations.of(context)!
                                          .translate('category_title_status'),
                                    ))).then((value) {
                          setState(() {
                            widget.textXacNhan = value;
                          });
                        });
                      },
                    )),
                const SizedBox(
                  width: 8,
                ),
                Expanded(
                    flex: 2,
                    child: Column(
                      children: [
                        const Padding(
                          padding: EdgeInsets.only(
                            left: 8,
                            right: 8,
                          ),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              '',
                              style: TextStyle(fontSize: 12, color: textColor),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        CustomButton(
                            text: AppLocalizations.of(context)!
                                .translate('button_search'),
                            icon: Icons.search,
                            press: () {
                              if (dateTimeRange != null) {
                                widget.dataSearch([
                                  DateFormat('dd-MM-yyyy')
                                      .format(dateTimeRange!.start),
                                  DateFormat('dd-MM-yyyy')
                                      .format(dateTimeRange!.end),
                                  widget.textDateRange,
                                  widget.textXacNhan
                                ]);
                              } else {
                                widget.dataSearch([
                                  '',
                                  '',
                                  widget.textDateRange,
                                  widget.textXacNhan
                                ]);
                              }
                            },
                            height: 36),
                      ],
                    )),
              ],
            ),
          ],
        ));
  }

  void _show() async {
    final DateTimeRange? result = await showDateRangePicker(
      context: context,
      firstDate: DateTime(2019),
      lastDate: DateTime(2024),
      currentDate: DateTime.now(),
      saveText: 'OK',
      initialDateRange: dateTimeRange,
      locale: const Locale('vi'),
    );
    if (result != null) {
      setState(() {
        dateTimeRange = result;
        dateTimeRange = result;
        widget.textDateRange =
            DateFormat('dd-MM-yyyy').format(dateTimeRange!.start) +
                ' - ' +
                DateFormat('dd-MM-yyyy').format(dateTimeRange!.end);
      });
    }
  }
}
