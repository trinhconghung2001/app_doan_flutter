import 'package:app_doan_flutter/config/styles/Dimens.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:flutter/material.dart';

import '../../../components/inputform/RowTextView.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../item/ItemInformation.dart';
import '../../../untils/AppLocalizations.dart';

class ItemMedication extends StatelessWidget {
  final String trangThai;
  final String nguoiUong;
  final String loaiThuoc;
  final String cachDung;
  final String ngayBatDau;
  final String ngayKetThuc;
  const ItemMedication(
      {super.key,
      required this.trangThai,
      required this.nguoiUong,
      required this.loaiThuoc,
      required this.cachDung,
      required this.ngayBatDau,
      required this.ngayKetThuc});

  @override
  Widget build(BuildContext context) {
    Color? titleColor;
    if (trangThai == status_medication_1) {
      titleColor = primaryColor;
    } else if (trangThai == status_medication_2) {
      titleColor = backgroudSearch;
    } else if (trangThai == status_medication_3) {
      titleColor = warningColor;
    }
    return ItemInformation(
      widgetBottom: Column(
        children: [
          RowTextView(
            title: AppLocalizations.of(context)!.translate('select_people_use'),
            content: nguoiUong,
            flexItemLeft: 2,
            flexItemRight: 4,
          ),
          RowTextView(
            title: AppLocalizations.of(context)!
                .translate('select_type_medication'),
            content: loaiThuoc,
            flexItemLeft: 2,
            flexItemRight: 4,
          ),
          RowTextView(
            title: AppLocalizations.of(context)!.translate('select_use'),
            content: cachDung,
            flexItemLeft: 2,
            flexItemRight: 4,
          ),
          RowTextView(
            title: AppLocalizations.of(context)!.translate('select_start_day'),
            content: ngayBatDau,
            flexItemLeft: 2,
            flexItemRight: 4,
          ),
          RowTextView(
            title: AppLocalizations.of(context)!.translate('select_end_day'),
            content: ngayKetThuc,
            flexItemLeft: 2,
            flexItemRight: 4,
            paddingBottom: defaultPaddingMin,
          ),
        ],
      ),
      widgetTop: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Text(
              trangThai,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
      titleColor: titleColor,
    );
  }
}
