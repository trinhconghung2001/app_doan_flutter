import 'package:flutter/material.dart';

import '../../../config/styles/CustomColor.dart';
import '../../../config/styles/Dimens.dart';

class Body extends StatelessWidget {
  final String title;
  final IconData icon;
  final VoidCallback onpress;
  const Body(
      {super.key,
      required this.title,
      required this.icon,
      required this.onpress});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onpress,
      child: Container(
        decoration: BoxDecoration(
          color: backgroundScreenColor,
          borderRadius: BorderRadius.circular(16),
          boxShadow: [
            BoxShadow(
              color: primaryColor.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 2,
              offset: const Offset(0, 2), // changes position of shadow
            ),
          ],
        ),
        padding: const EdgeInsets.only(top: 16, left: 16, right: 0, bottom: 16),
        margin: const EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
                flex: 9,
                child: Text(
                  title,
                  style: const TextStyle(
                    color: textColor,
                    fontSize: defaultButtonText,
                    fontWeight: FontWeight.w400,
                  ),
                )),
            Expanded(
              flex: 1,
              child: Icon(
                icon,
                color: textColor,
                size: 14,
              ),
            )
          ],
        ),
      ),
    );
  }
}
