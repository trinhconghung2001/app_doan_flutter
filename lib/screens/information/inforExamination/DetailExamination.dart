import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/home/bookExamination/data/Examination.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/screens/manage/data/Doctor.dart';
import 'package:app_doan_flutter/screens/manage/data/Hospital.dart';
import 'package:app_doan_flutter/screens/manage/data/Room.dart';
import 'package:app_doan_flutter/untils/FutureHospital.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/RowTextView.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureExamination.dart';
import '../../../untils/FutureManageDoctor.dart';
import '../../../untils/FutureRoom.dart';
import '../../../untils/FutureUser.dart';
import '../../../untils/StorePreferences.dart';
import 'EditExamination.dart';

class DetailExamination extends StatefulWidget {
  final String idSend;
  const DetailExamination({super.key, required this.idSend});

  @override
  State<DetailExamination> createState() => _DetailExaminationState();
}

class _DetailExaminationState extends State<DetailExamination> {
  Examination detailExamination = Examination.defaultContructor();
  FamilyMember benhNhan = FamilyMember.defaultContructor();
  Room phongKham = Room.defaultContructor();
  Doctor bacSi = Doctor.defaultContructor();
  Hospital hospital = Hospital.defaultContructor();
  late Future<void> _loadDetailExamination;
  String trieuChung = '';
  @override
  void initState() {
    _loadDetailExamination = loadDetail();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDetailExamination = loadDetail();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: AppLocalizations.of(context)!
                .translate("app_bar_detail_examination"),
            check: false,
            onPress: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Scaffold(
          body: SingleChildScrollView(
              child: FutureBuilder(
            future: _loadDetailExamination,
            builder: ((context, AsyncSnapshot snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return CircularProgressIndicatorWidget();
              } else if (snapshot.hasError) {
                return EmptyData(
                    msg: AppLocalizations.of(context)!
                        .translate('error_data_msg'));
              } else {
                return Column(
                  children: [
                    Container(
                      decoration: BoxDecorationContainer(),
                      padding: const EdgeInsets.only(
                        top: 16,
                        left: 16,
                        right: 16,
                      ),
                      margin: const EdgeInsets.only(
                          top: 8, left: 16, right: 16, bottom: 0),
                      child: RowTextView(
                        title: AppLocalizations.of(context)!
                            .translate('title_status'),
                        content: detailExamination.trangThai,
                        flexItemLeft: 2,
                        flexItemRight: 3,
                        color2: colorTrangThai(detailExamination.trangThai),
                      ),
                    ),
                    Container(
                      decoration: BoxDecorationContainer(),
                      padding: const EdgeInsets.only(
                        top: 16,
                        left: 16,
                        right: 16,
                      ),
                      margin: const EdgeInsets.only(
                          top: 8, left: 16, right: 16, bottom: 0),
                      child: Column(
                        children: [
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('title_patient'),
                              content: benhNhan.moiQH,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('select_hospital'),
                              content: hospital.ten,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('select_type_examination'),
                              content: detailExamination.loaiKham,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          Visibility(
                            child: RowTextView(
                                title: AppLocalizations.of(context)!
                                    .translate('category_title_doctor_advise'),
                                content: bacSi.ten,
                                flexItemLeft: 2,
                                flexItemRight: 3),
                            visible: detailExamination.trangThai == status_1
                                ? false
                                : true,
                          ),
                          Visibility(
                            child: RowTextView(
                                title: AppLocalizations.of(context)!.translate(
                                    'category_title_room_examination'),
                                content: phongKham.tenPhong,
                                flexItemLeft: 2,
                                flexItemRight: 3),
                            visible: detailExamination.trangThai == status_1
                                ? false
                                : true,
                          ),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('select_day_book_examination'),
                              content: detailExamination.ngayKham,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('select_time_examination'),
                              content: detailExamination.gioKham,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('content_examination'),
                              content: detailExamination.noiDungKham,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('select_symptom'),
                              content: trieuChung,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                          RowTextView(
                              title: AppLocalizations.of(context)!
                                  .translate('select_chronic_diseases'),
                              content: detailExamination.benhManTinh,
                              flexItemLeft: 2,
                              flexItemRight: 3),
                        ],
                      ),
                    ),
                  ],
                );
              }
            }),
          )),
          bottomNavigationBar: BottomNavigator(
            widget: StorePreferences.getStatus() == true
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        width: 16,
                      ),
                      Expanded(
                          child: CustomButton(
                        text: AppLocalizations.of(context)!
                            .translate("button_delete"),
                        height: 32,
                        icon: Icons.cancel_outlined,
                        iconColor: primaryColor,
                        textColor: primaryColor,
                        background: backgroundScreenColor,
                        press: () {
                          deleteExamination();
                        },
                      )),
                      Container(
                        width: 8,
                      ),
                      Expanded(
                          child: CustomButton(
                        text: AppLocalizations.of(context)!
                            .translate("button_edit"),
                        icon: Icons.edit_square,
                        height: 32,
                        press: () {
                          Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => EditExamination(
                                          detailExamination:
                                              detailExamination)))
                              .then((value) => loadScreen());
                        },
                      )),
                      Container(
                        width: 16,
                      ),
                    ],
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        width: 16,
                      ),
                      Expanded(
                          child: CustomButton(
                        text: 'OK',
                        iconDisplay: false,
                        icon: Icons.edit_square,
                        height: 32,
                        press: () {
                          Navigator.pop(context);
                        },
                      )),
                      Container(
                        width: 16,
                      ),
                    ],
                  ),
          ),
        ));
  }

  Future<void> loadDetail() async {
    List<Examination> listExamination =
        await readAllExamination(StorePreferences.getIdUser());
    for (Examination i in listExamination) {
      if (i.id == widget.idSend) {
        detailExamination = i;
        benhNhan =
            await readIDBenhNhan(StorePreferences.getIdUser(), i.idBenhNhan);
        hospital = await readIDHospital(i.idBenhVien);
        if (i.idKhoa != '' && i.idPhong != '') {
          phongKham = await readIDPhongKham(i.idKhoa, i.idPhong);
          bacSi = await readIDDoctor(phongKham.idBacSi);
        }
        break;
      }
    }
    trieuChung = '';
    for (int i = 0; i < detailExamination.trieuChung.length; i++) {
      trieuChung += detailExamination.trieuChung[i];
      if (i < detailExamination.trieuChung.length - 1) {
        trieuChung += ', ';
      }
    }
  }

  Color colorTrangThai(String trangThai) {
    if (trangThai == status_1) {
      return primaryColor;
    } else if (trangThai == status_2) {
      return backgroudSearch;
    } else if (trangThai == status_3) {
      return warningColor;
    } else if (trangThai == status_4) {
      return statusColor;
    }
    return textColor;
  }

  Future<void> deleteExamination() async {
    try {
      List<Examination> listExamination =
          await readAllExamination(StorePreferences.getIdUser());
      for (int i = 0; i < listExamination.length; i++) {
        if (listExamination[i].id == widget.idSend) {
          listExamination.removeAt(i);
          break;
        }
      }
      FirebaseFirestore.instance
          .collection('RegistrationSchedule')
          .doc(StorePreferences.getIdUser())
          .update({
        'examination': listExamination.map((e) => e.toMap()).toList()
      }).then((value) {
        ToastCommon.showToast(AppLocalizations.of(context)!
            .translate('toast_delete_examination_success'));
        Navigator.pop(context);
      });
    } on Exception catch (e) {
      print(e);
      // ignore: use_build_context_synchronously
      showDialog(
          context: context,
          builder: (context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_remove_examination'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          });
    }
  }
}
