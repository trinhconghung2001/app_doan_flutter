import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/screens/home/bookExamination/data/Examination.dart';
import 'package:app_doan_flutter/screens/information/inforExamination/ItemExamination.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/untils/FutureExamination.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/filtersearch/RowFilter.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../config/styles/KeyValue.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureUser.dart';
import '../../../untils/StorePreferences.dart';
import '../../category/ListStatus.dart';
import 'DetailExamination.dart';

class InfoExamination extends StatefulWidget {
  const InfoExamination({super.key});

  @override
  State<InfoExamination> createState() => _InfoExaminationState();
}

class _InfoExaminationState extends State<InfoExamination> {
  late Future<void> _loadDataExamination;
  List<Examination> listExamination = [];
  List<FamilyMember> listFamily = [];
  String textDateRange = '';
  String textXacNhan = '';
  @override
  void initState() {
    _loadDataExamination = loadData();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDataExamination = loadData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: AppLocalizations.of(context)!
                .translate("information_book_examination"),
            check: true,
            onPress: () {
              Navigator.pop(context);
            },
            checkIconBack: false,
            icon: Icon(Icons.sync),
            onTap: () {
              loadScreen();
            },
          ),
        ),
        body: ListView.builder(
            itemCount: 2,
            itemBuilder: (BuildContext context, index) {
              if (index == 0) {
                return FiltterExamination(
                    dataSearch: searchData,
                    textDateRange: textDateRange,
                    textXacNhan: textXacNhan);
              } else {
                return FutureBuilder(
                    future: _loadDataExamination,
                    builder: ((context, AsyncSnapshot snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return CircularProgressIndicatorWidget();
                      } else if (snapshot.hasError) {
                        return EmptyData(
                            msg: AppLocalizations.of(context)!
                                .translate('error_data_msg'));
                      } else {
                        if (listExamination.length > 0) {
                          return Container(
                              margin: const EdgeInsets.only(top: 8),
                              child: ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: listExamination.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return GestureDetector(
                                      onTap: () {
                                        if (listExamination[index].trangThai ==
                                            status_1) {
                                          StorePreferences.setStatus(true);
                                        } else {
                                          StorePreferences.setStatus(false);
                                        }
                                        Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        DetailExamination(
                                                            idSend:
                                                                listExamination[
                                                                        index]
                                                                    .id)))
                                            .then((value) => loadScreen());
                                      },
                                      child: ItemExamination(
                                          trangThai:
                                              listExamination[index].trangThai,
                                          benhNhan: listFamily[index].ten,
                                          noiDungKham: listExamination[index]
                                              .noiDungKham,
                                          thoiGian:
                                              listExamination[index].ngayKham));
                                },
                              ));
                        } else {
                          // Không có dữ liệu
                          return EmptyData();
                        }
                      }
                    }));
              }
            }),
      ),
    );
  }

  void searchData(List<String> list) {
    setState(() {
      _loadDataExamination = loadDataSearch(list);
      textDateRange = list[2];
      textXacNhan = list[3];
    });
  }

  Future<void> loadDataSearch(List<String> list) async {
    listExamination = [];
    List<Examination> listResult =
        await readAllExamination(StorePreferences.getIdUser());
    if (list[2] == 'Lựa chọn ngày') {
      if (list[0] != '' && list[1] != '') {
        // trạng thái = tất cả
        if (list[3] ==
            AppLocalizations.of(context)!.translate('category_select_all')) {
          listExamination = listResult;
        }
        //trạng thái # tất cả
        else {
          for (Examination i in listResult) {
            if (i.trangThai == list[3]) {
              listExamination.add(i);
            }
          }
        }
      } else {
        // trạng thái = tất cả
        if (list[3] ==
            AppLocalizations.of(context)!.translate('category_select_all')) {
          listExamination = listResult;
        }
        //trạng thái # tất cả
        else {
          for (Examination i in listResult) {
            if (i.trangThai == list[3]) {
              listExamination.add(i);
            }
          }
        }
      }
    }
    //textDateRange # lựa chọn ngày
    else {
      //trạng thái = tất cả
      if (list[3] ==
          AppLocalizations.of(context)!.translate('category_select_all')) {
        for (Examination i in listResult) {
          DateTime dateTime = DateFormat("dd-MM-yyyy").parse(i.ngayKham);
          DateTime dateStart = DateFormat("dd-MM-yyyy").parse(list[0]);
          DateTime dateEnd = DateFormat("dd-MM-yyyy").parse(list[1]);
          if ((dateTime.isAfter(dateStart) == true &&
                  dateTime.isBefore(dateEnd) == true) ||
              dateTime == dateStart ||
              dateTime == dateEnd) {
            listExamination.add(i);
          }
        }
      } else {
        for (Examination i in listResult) {
          DateTime dateTime = DateFormat("dd-MM-yyyy").parse(i.ngayKham);
          DateTime dateStart = DateFormat("dd-MM-yyyy").parse(list[0]);
          DateTime dateEnd = DateFormat("dd-MM-yyyy").parse(list[1]);
          if ((dateTime.isAfter(dateStart) == true &&
                  dateTime.isBefore(dateEnd) == true) ||
              dateTime == dateStart ||
              dateTime == dateEnd) {
            if (i.trangThai == list[3]) {
              listExamination.add(i);
            }
          }
        }
      }
    }
  }

  Future<void> loadData() async {
    textDateRange = 'Lựa chọn ngày';
    textXacNhan = 'Tất cả';
    listExamination = await readAllExamination(StorePreferences.getIdUser());
    listFamily = [];
    for (int i = 0; i < listExamination.length; i++) {
      FamilyMember familyMember = await readIDBenhNhan(
          StorePreferences.getIdUser(), listExamination[i].idBenhNhan);
      listFamily.add(familyMember);
    }
  }
}

// ignore: must_be_immutable
class FiltterExamination extends StatefulWidget {
  final void Function(List<String> list) dataSearch;
  String textDateRange;
  String textXacNhan;
  FiltterExamination(
      {super.key,
      required this.dataSearch,
      required this.textDateRange,
      required this.textXacNhan});

  @override
  State<FiltterExamination> createState() => _FiltterExaminationState();
}

class _FiltterExaminationState extends State<FiltterExamination> {
  DateTimeRange? dateTimeRange;
  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecorationContainer(color: filterColor),
        margin: const EdgeInsets.only(top: 8, left: 16, right: 16),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RowFilter(
              title: AppLocalizations.of(context)!
                  .translate('filter_search_date_range'),
              value: widget.textDateRange,
              icon: Icons.calendar_month,
              onPress: () {
                _show();
              },
            ),
            const SizedBox(height: 8),
            Row(
              children: <Widget>[
                Expanded(
                    flex: 2,
                    child: RowFilter(
                      title: AppLocalizations.of(context)!
                          .translate('filter_search_state_confirm'),
                      value: widget.textXacNhan,
                      icon: Icons.arrow_drop_down,
                      onPress: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ListStatus(
                                      loai: defaultStatusBook,
                                      titleAppbar: AppLocalizations.of(context)!
                                          .translate('category_title_status'),
                                    ))).then((value) {
                          if (value != '') {
                            setState(() {
                              widget.textXacNhan = value;
                            });
                          }
                        });
                      },
                    )),
                const SizedBox(
                  width: 8,
                ),
                Expanded(
                    flex: 2,
                    child: Column(
                      children: [
                        const Padding(
                          padding: EdgeInsets.only(
                            left: 8,
                            right: 8,
                          ),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              '',
                              style: TextStyle(fontSize: 12, color: textColor),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        CustomButton(
                            text: AppLocalizations.of(context)!
                                .translate('button_search'),
                            icon: Icons.search,
                            press: () {
                              if (dateTimeRange != null) {
                                widget.dataSearch([
                                  DateFormat('dd-MM-yyyy')
                                      .format(dateTimeRange!.start),
                                  DateFormat('dd-MM-yyyy')
                                      .format(dateTimeRange!.end),
                                  widget.textDateRange,
                                  widget.textXacNhan
                                ]);
                              } else {
                                widget.dataSearch([
                                  '',
                                  '',
                                  widget.textDateRange,
                                  widget.textXacNhan
                                ]);
                              }
                            },
                            height: 36),
                      ],
                    )),
              ],
            ),
          ],
        ));
  }

  void _show() async {
    final DateTimeRange? result = await showDateRangePicker(
      context: context,
      firstDate: DateTime(2019),
      lastDate: DateTime(2024),
      currentDate: DateTime.now(),
      saveText: 'OK',
      initialDateRange: dateTimeRange,
      locale: const Locale('vi'),
    );
    if (result != null) {
      setState(() {
        dateTimeRange = result;
        dateTimeRange = result;
        widget.textDateRange =
            DateFormat('dd-MM-yyyy').format(dateTimeRange!.start) +
                ' - ' +
                DateFormat('dd-MM-yyyy').format(dateTimeRange!.end);
      });
    }
  }
}
