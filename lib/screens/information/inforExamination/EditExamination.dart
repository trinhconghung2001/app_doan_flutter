import 'package:app_doan_flutter/components/bottomnavigator/BottomNavigator.dart';
import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/components/loading/EmptyData.dart';
import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/category/ListIdCategory.dart';
import 'package:app_doan_flutter/screens/home/bookExamination/data/Examination.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/screens/manage/data/Hospital.dart';
import 'package:app_doan_flutter/untils/FutureHospital.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/InputDropDown.dart';
import '../../../components/inputform/InputTextField.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureExamination.dart';
import '../../../untils/FutureUser.dart';
import '../../../untils/StorePreferences.dart';
import '../../category/ListAddCategory.dart';
import '../../category/ListCategory.dart';

class EditExamination extends StatefulWidget {
  final Examination detailExamination;
  const EditExamination({super.key, required this.detailExamination});

  @override
  State<EditExamination> createState() => _EditExaminationState();
}

class _EditExaminationState extends State<EditExamination> {
  late Future<void> _loadDataEdit;
  final FocusNode _focusNoiDungKham = FocusNode();
  final FocusNode _focusBenhManTinh = FocusNode();
  String textHoSo = '';
  String textBenhVien = '';
  String textNgayKham = '';
  String textGioKham = '';
  String textLoaiKham = '';
  String textTrieuChung = '';
  List<String> listTrieuChung = [];

  String idNguoiDat = '';
  String idBenhVien = '';
  bool validate = false;
  DateTime dateTime = DateTime.now();
  final TextEditingController textNoiDungKham = TextEditingController();
  final TextEditingController textBenhManTinh = TextEditingController();

  @override
  void initState() {
    readTrieuChung();
    _loadDataEdit = loadData();
    super.initState();
    _focusNoiDungKham.addListener(_onFocusChange);
    _focusBenhManTinh.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    setState(() {});
  }

  @override
  void dispose() {
    _focusNoiDungKham.dispose();
    _focusBenhManTinh.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: AppLocalizations.of(context)!
                  .translate("app_bar_edit_examination"),
              check: false,
              onPress: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: Scaffold(
            body: SingleChildScrollView(
                child: FutureBuilder(
              future: _loadDataEdit,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return CircularProgressIndicatorWidget();
                } else if (snapshot.hasError) {
                  return EmptyData();
                } else {
                  return Container(
                    decoration: BoxDecorationContainer(),
                    padding: const EdgeInsets.only(
                        top: 16, left: 16, right: 16, bottom: 16),
                    margin: const EdgeInsets.only(
                        top: 8, left: 16, right: 16, bottom: 0),
                    child: Column(
                      children: [
                        InputDropDown(
                            onPress: () {
                              guiNhanIDDrop(
                                  defaultHoSo,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_file'));
                            },
                            value: textHoSo,
                            title: AppLocalizations.of(context)!
                                .translate("select_file"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textHoSo.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanIDDrop(
                                  defaultBenhVien,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_hospital'));
                            },
                            value: textBenhVien,
                            title: AppLocalizations.of(context)!
                                .translate("select_hospital"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textBenhVien.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              _show();
                            },
                            value: textNgayKham,
                            title: AppLocalizations.of(context)!
                                .translate("select_day_book_examination"),
                            checkRangBuoc: true,
                            icon: Icons.calendar_month,
                            isError: validate && textNgayKham.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultGioKham,
                                  AppLocalizations.of(context)!.translate(
                                      'category_title_time_examination'));
                            },
                            value: textGioKham,
                            title: AppLocalizations.of(context)!
                                .translate("select_time_examination"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textGioKham.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultLoaiKham,
                                  AppLocalizations.of(context)!.translate(
                                      'category_title_type_examination'));
                            },
                            value: textLoaiKham,
                            title: AppLocalizations.of(context)!
                                .translate("select_type_examination"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textGioKham.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("content_examination"),
                          checkRangBuoc: true,
                          focusNode: _focusNoiDungKham,
                          textController: textNoiDungKham,
                          onPress: () {
                            setState(() {
                              textNoiDungKham.clear();
                            });
                          },
                          isError: validate && textNoiDungKham.text.isEmpty,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiTrieuChung(
                                  widget.detailExamination.trieuChung
                                      .map((e) => e.toString())
                                      .toList(),
                                  AppLocalizations.of(context)!
                                      .translate('category_title_symptom'));
                            },
                            value: textTrieuChung,
                            title: AppLocalizations.of(context)!
                                .translate("select_symptom"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textTrieuChung.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("select_chronic_diseases"),
                          checkRangBuoc: true,
                          focusNode: _focusBenhManTinh,
                          textController: textBenhManTinh,
                          onPress: () {
                            setState(() {
                              textBenhManTinh.clear();
                            });
                          },
                          isError: validate && textBenhManTinh.text.isEmpty,
                        ),
                      ],
                    ),
                  );
                }
              },
            )),
            bottomNavigationBar: BottomNavigator(
              widget: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: 16,
                  ),
                  Expanded(
                      child: CustomButton(
                    text: AppLocalizations.of(context)!
                        .translate("button_cancel"),
                    height: 32,
                    icon: Icons.cancel_outlined,
                    iconColor: primaryColor,
                    textColor: primaryColor,
                    background: backgroundScreenColor,
                    press: () {
                      Navigator.pop(context);
                    },
                  )),
                  Container(
                    width: 8,
                  ),
                  Expanded(
                      child: CustomButton(
                    text: AppLocalizations.of(context)!
                        .translate("button_update"),
                    icon: Icons.save,
                    height: 32,
                    press: () {
                      checkValidate();
                      if (validate == false) {
                        editExamination();
                      }
                    },
                  )),
                  Container(
                    width: 16,
                  ),
                ],
              ),
            ),
          )),
    );
  }

  void guiNhanDrop(String loai, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      if (value != null) {
        if (loai == defaultGioKham) {
          setState(() {
            textGioKham = value;
          });
        } else if (loai == defaultLoaiKham) {
          setState(() {
            textLoaiKham = value;
          });
        }
      }
    });
  }

  void guiNhanIDDrop(String loai, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListIdCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      if (value != null) {
        if (loai == defaultHoSo) {
          setState(() {
            textHoSo = value[0];
            idNguoiDat = value[1];
          });
        } else if (loai == defaultBenhVien) {
          setState(() {
            textBenhVien = value[0];
            idBenhVien = value[1];
          });
          StorePreferences.setIdHospital(idBenhVien);
        }
      }
    });
  }

  void _show() async {
    final DateTime? result = await showDatePicker(
      context: context,
      initialDate: dateTime,
      firstDate: DateTime(2024),
      lastDate: DateTime(2025),
      locale: const Locale('vi'),
    );
    if (result != null) {
      setState(() {
        dateTime = result;
        textNgayKham = '${formatDate(dateTime, [dd, '-', mm, '-', yyyy])}';
      });
    }
  }

  void checkValidate() {
    if (textHoSo.isNotEmpty &&
        textBenhVien.isNotEmpty &&
        textNgayKham.isNotEmpty &&
        textGioKham.isNotEmpty &&
        textBenhManTinh.text.isNotEmpty &&
        textNoiDungKham.text.isNotEmpty &&
        textTrieuChung.isNotEmpty) {
      setState(() {
        validate = false;
      });
    } else {
      setState(() {
        validate = true;
      });
    }
  }

  Future<void> readTrieuChung() async {
    DocumentReference documentReference = FirebaseFirestore.instance
        .collection('Categorys')
        .doc('8nDu9e5HN5HkdyhmO4Lz');
    DocumentSnapshot documentSnapshot = await documentReference.get();

    if (documentSnapshot.exists) {
      Map<String, dynamic> dataCategory =
          documentSnapshot.data() as Map<String, dynamic>;
      StorePreferences.setListTrieuChung(
          List<String>.from(dataCategory['trieuChung'] as List));
    } else {
      StorePreferences.setListTrieuChung([]);
    }
  }

  void guiTrieuChung(List<String> list, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListAddCategory(
                  type: defaultTrieuChung,
                  list: list,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      String text = '';
      for (int i = 0; i < value.length; i++) {
        text += value[i];
        if (i < value.length - 1) {
          text += ', ';
        }
      }
      setState(() {
        listTrieuChung = value;
        textTrieuChung = text;
      });
    });
  }

  Future<void> editExamination() async {
    try {
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('RegistrationSchedule')
          .doc(StorePreferences.getIdUser());
      List<Examination> listExamination =
          await readAllExamination(StorePreferences.getIdUser());
      Examination examinationNew = Examination(
          id: widget.detailExamination.id,
          idUser: StorePreferences.getIdUser(),
          trangThai: widget.detailExamination.trangThai,
          idBenhNhan: idNguoiDat,
          idKhoa: widget.detailExamination.idKhoa,
          idPhong: widget.detailExamination.idPhong,
          idKetQua: widget.detailExamination.idKetQua,
          loaiKham: textLoaiKham,
          ngayKham: textNgayKham,
          gioKham: textGioKham,
          noiDungKham: textNoiDungKham.text,
          benhManTinh: textBenhManTinh.text,
          idBenhVien: idBenhVien,
          trieuChung: listTrieuChung);
      for (Examination e in listExamination) {
        if (e.id == widget.detailExamination.id) {
          listExamination.remove(e);
          break;
        }
      }
      if (listExamination.any((e) =>
              e.idPhong == examinationNew.idPhong &&
              e.idKhoa == examinationNew.idKhoa &&
              e.ngayKham == examinationNew.ngayKham &&
              e.gioKham == examinationNew.gioKham &&
              e.idBenhVien == examinationNew.idBenhVien) ==
          false) {
        listExamination.add(examinationNew);
        documentReference.update({
          'examination': listExamination.map((e) => e.toMap()).toList()
        }).then((value) {
          ToastCommon.showToast(AppLocalizations.of(context)!
              .translate('toast_edit_examination_success'));
          Navigator.pop(context);
        });
      } else {
        showDialog(
            context: context,
            builder: (context) {
              return ErrorPopup(
                message: AppLocalizations.of(context)!
                    .translate('error_book_duplicate'),
                buttonText: 'OK',
                onPress: () {
                  Navigator.pop(context);
                },
              );
            });
      }
    } on Exception catch (e) {
      print(e);
      // ignore: use_build_context_synchronously
      showDialog(
          context: context,
          builder: (context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_edit_examination'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          });
    }
  }

  Future<void> loadData() async {
    FamilyMember hoSo = await readIDBenhNhan(
        StorePreferences.getIdUser(), widget.detailExamination.idBenhNhan);
    Hospital hospital =
        await readIDHospital(widget.detailExamination.idBenhVien);
    textHoSo = hoSo.moiQH;
    textBenhVien = hospital.ten;
    textNgayKham = widget.detailExamination.ngayKham;
    textGioKham = widget.detailExamination.gioKham;
    textLoaiKham = widget.detailExamination.loaiKham;
    textNoiDungKham.text = widget.detailExamination.noiDungKham;
    textBenhManTinh.text = widget.detailExamination.benhManTinh;
    listTrieuChung =
        widget.detailExamination.trieuChung.map((e) => e.toString()).toList();
    for (int i = 0; i < listTrieuChung.length; i++) {
      textTrieuChung += listTrieuChung[i];
      if (i < listTrieuChung.length - 1) {
        textTrieuChung += ', ';
      }
    }
    idBenhVien = widget.detailExamination.idBenhVien;
    idNguoiDat = widget.detailExamination.idBenhNhan;
  }
}
