import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/main.dart';
import 'package:app_doan_flutter/screens/home/bookExamination/data/Examination.dart';
import 'package:app_doan_flutter/screens/home/bookVaccine/data/Vaccination.dart';
import 'package:app_doan_flutter/screens/homeDoctor/resultExamination/Result.dart';
import 'package:app_doan_flutter/screens/login/LoginDoctor.dart';
import 'package:app_doan_flutter/screens/login/LoginReception.dart';
import 'package:app_doan_flutter/screens/login/LoginScreen.dart';
import 'package:app_doan_flutter/screens/setting/changePassword/ChangePassword.dart';
import 'package:app_doan_flutter/screens/setting/components/Body.dart';
import 'package:app_doan_flutter/screens/setting/termService/TermService.dart';
import 'package:app_doan_flutter/untils/FutureExamination.dart';
import 'package:app_doan_flutter/untils/FutureMessage.dart';
import 'package:app_doan_flutter/untils/FutureResult.dart';
import 'package:app_doan_flutter/untils/FutureVaccination.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../components/appbar/CustomAppBar.dart';
import '../../components/button/CustomButton.dart';
import '../../components/popup/ErrorPopup.dart';
import '../../components/toast/ToastCommon.dart';
import '../../config/styles/Dimens.dart';
import '../../untils/AppLocalizations.dart';

class SettingScreen extends StatefulWidget {
  final String accountType;
  const SettingScreen({super.key, required this.accountType});

  @override
  State<SettingScreen> createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: AppLocalizations.of(context)!
                .translate("bottom_navigation_bar_setting_title"),
            check: false,
            checkIconBack: true,
            onPress: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Column(
          children: [
            Body(
              title: AppLocalizations.of(context)!
                  .translate('setting_change_password'),
              icon: Icons.arrow_forward_ios,
              onpress: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ChangePassword(
                              accountType: widget.accountType,
                            )));
              },
            ),
            const SizedBox(
              height: 8,
            ),
            Body(
              title: AppLocalizations.of(context)!.translate('setting_logout'),
              icon: Icons.arrow_forward_ios,
              onpress: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return ErrorPopup(
                        message: AppLocalizations.of(context)!
                            .translate('popup_logout'),
                        buttonText: AppLocalizations.of(context)!
                            .translate('popup_button_confirm'),
                        onPress: () {
                          logout(widget.accountType);
                        },
                      );
                    });
              },
            ),
            const SizedBox(
              height: 8,
            ),
            Body(
              title: AppLocalizations.of(context)!
                  .translate('setting_terms_services'),
              icon: Icons.arrow_forward_ios,
              onpress: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => TermService()));
              },
            ),
            const SizedBox(
              height: 8,
            ),
            Body(
              title: AppLocalizations.of(context)!.translate('change_languge'),
              icon: Icons.arrow_forward_ios,
              onpress: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return Dialog(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0)),
                          child: Container(
                            height: MediaQuery.of(context).size.height / 5,
                            padding: EdgeInsets.all(defaultPadding),
                            width: MediaQuery.of(context).size.width / 1.5,
                            child: Column(
                              children: [
                                Expanded(
                                    child: CustomButton(
                                  text: AppLocalizations.of(context)!
                                      .translate("language_vietnamese"),
                                  height: 32,
                                  icon: Icons.cancel_outlined,
                                  iconDisplay: false,
                                  press: () {
                                    MyApp.setLocale(
                                        context, Locale('vi', "VN"));
                                    Navigator.pop(context);
                                    // changeLanguage(Locale('vi', "VN"));
                                  },
                                )),
                                Expanded(
                                    child: CustomButton(
                                  text: AppLocalizations.of(context)!
                                      .translate("language_english"),
                                  height: 32,
                                  icon: Icons.cancel_outlined,
                                  iconDisplay: false,
                                  press: () {
                                    MyApp.setLocale(
                                        context, Locale('en', "US"));
                                    Navigator.pop(context);
                                    // changeLanguage(Locale('en', "US"));
                                  },
                                )),
                              ],
                            ),
                          ));
                    });
              },
            ),
            const SizedBox(
              height: 8,
            ),
            Visibility(
              visible: widget.accountType == key_account_user ? true : false,
              child: Body(
                title: AppLocalizations.of(context)!
                    .translate('setting_remove_account'),
                icon: Icons.arrow_forward_ios,
                onpress: () {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return ErrorPopup(
                          message: AppLocalizations.of(context)!
                              .translate('popup_remove_account'),
                          buttonText: AppLocalizations.of(context)!
                              .translate('popup_button_confirm'),
                          onPress: () {
                            removeAccount();
                          },
                        );
                      });
                },
              ),
            ),
          ],
        ));
  }

  void logout(String accountType) async {
    try {
      await FirebaseAuth.instance.signOut();
      ToastCommon.showToast(
          AppLocalizations.of(context)!.translate('toast_logout_success'));
      if (accountType == key_account_user) {
        updateStatusOnlineUser(StorePreferences.getIdUser(), false);
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => LoginScreen()));
      } else if (accountType == key_account_reception) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => LoginReception()));
      } else if (accountType == key_account_doctor) {
        updateStatusOnlineDoctor(StorePreferences.getIdDoctor(), false);
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => LoginDoctor()));
      }
      StorePreferences.setListTime([]);
      StorePreferences.setListTrieuChung([]);
    } catch (e) {
      ToastCommon.showToast(e.toString());
      Navigator.pop(context);
    }
  }

  void removeAccount() async {
    List<Result> listResult =
        await readListResult(StorePreferences.getIdUser());
    List<Examination> listExamination =
        await readAllExamination(StorePreferences.getIdUser());
    List<Vaccination> listVaccination =
        await readAllVaccination(StorePreferences.getIdUser());
    if (listResult.length != 0 ||
        listExamination.length != 0 ||
        listVaccination.length != 0) {
      ToastCommon.showToast(
          AppLocalizations.of(context)!.translate('error_remove_accout_exits'));
    } else {
      // Lấy tài khoản người dùng hiện tại
      User user = FirebaseAuth.instance.currentUser!;
      // Đổi mật khẩu
      try {
        await user.delete();
        // Xóa tài khoản thành công
        await FirebaseFirestore.instance
            .collection('Users')
            .doc(StorePreferences.getIdUser())
            .delete();
        await FirebaseFirestore.instance
            .collection('RegistrationSchedule')
            .doc(StorePreferences.getIdUser())
            .delete();
        StorePreferences.setEmail('');
        ToastCommon.showToast(AppLocalizations.of(context)!
            .translate('toast_remove_account_succes'));
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => LoginScreen()));
      } catch (e) {
        // Xử lý khi có lỗi xảy ra trong quá trình xóa tài khoản
        ToastCommon.showToast(AppLocalizations.of(context)!
            .translate('toast_remove_account_faild'));
        Navigator.pop(context);
      }
    }
  }

  Future<void> changeLanguage(Locale locale) async {
    // AppLocalizations appLocalizations = AppLocalizations(locale);
    // appLocalizations.load().then((_) {
    //   // Reload the UI with the new language
    //   Navigator.pop(context);
    //   // Navigator.pushReplacement(
    //   //     context, MaterialPageRoute(builder: (context) => FirstScreen()));
    // });
    MyApp.setLocale(context, locale);
    Navigator.pop(context);
  }
}
