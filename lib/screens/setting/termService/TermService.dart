import 'package:app_doan_flutter/config/styles/Dimens.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../untils/AppLocalizations.dart';

class TermService extends StatelessWidget {
  const TermService({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text:
                AppLocalizations.of(context)!.translate("app_bar_term_service"),
            check: false,
            onPress: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Scaffold(
          body: SingleChildScrollView(
            child: Padding(
              padding:
                  const EdgeInsets.only(top: 8, bottom: 8, right: 8, left: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .translate('title_use_service'),
                    style: const TextStyle(
                      color: primaryColor,
                      fontSize: 16,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Container(
                    decoration: BoxDecorationContainer(),
                    padding: const EdgeInsets.only(
                        left: 8, right: 8, top: 8, bottom: 8),
                    margin: const EdgeInsets.only(top: 8, bottom: 0),
                    child: Text(
                      AppLocalizations.of(context)!
                          .translate('content_use_service'),
                      style: const TextStyle(
                        color: textColor,
                        fontSize: 14,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: defaultPadding,
                  ),
                  Text(
                    AppLocalizations.of(context)!
                        .translate('title_healthy_service'),
                    style: const TextStyle(
                      color: primaryColor,
                      fontSize: 16,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Container(
                    decoration: BoxDecorationContainer(),
                    padding: const EdgeInsets.only(
                        left: 8, right: 8, top: 8, bottom: 8),
                    margin: const EdgeInsets.only(top: 8, bottom: 0),
                    child: Text(
                      AppLocalizations.of(context)!
                          .translate('content_healthy_service'),
                      style: const TextStyle(
                        color: textColor,
                        fontSize: 14,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: defaultPadding,
                  ),
                  Text(
                    AppLocalizations.of(context)!
                        .translate('title_content_service'),
                    style: const TextStyle(
                      color: primaryColor,
                      fontSize: 16,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Container(
                    decoration: BoxDecorationContainer(),
                    padding: const EdgeInsets.only(
                        left: 8, right: 8, top: 8, bottom: 8),
                    margin: const EdgeInsets.only(top: 8, bottom: 0),
                    child: Text(
                      AppLocalizations.of(context)!
                          .translate('content_service'),
                      style: const TextStyle(
                        color: textColor,
                        fontSize: 14,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: defaultPadding,
                  ),
                  Text(
                    AppLocalizations.of(context)!
                        .translate('title_commit_service'),
                    style: const TextStyle(
                      color: primaryColor,
                      fontSize: 16,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Container(
                    decoration: BoxDecorationContainer(),
                    padding: const EdgeInsets.only(
                        left: 8, right: 8, top: 8, bottom: 8),
                    margin: const EdgeInsets.only(top: 8, bottom: 0),
                    child: Text(
                      AppLocalizations.of(context)!
                          .translate('content_commit_service'),
                      style: const TextStyle(
                        color: textColor,
                        fontSize: 14,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          bottomNavigationBar: BottomNavigator(
            widget: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  width: 16,
                ),
                Expanded(
                    child: CustomButton(
                  text:
                      AppLocalizations.of(context)!.translate("button_confirm"),
                  icon: Icons.save_alt,
                  height: 32,
                  press: () {
                    Navigator.pop(context);
                  },
                )),
                Container(
                  width: 16,
                ),
              ],
            ),
          ),
        ));
  }
}
