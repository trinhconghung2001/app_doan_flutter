import 'package:app_doan_flutter/config/styles/Dimens.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/SecurityInputText.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../untils/AppLocalizations.dart';
import '../../login/LoginDoctor.dart';
import '../../login/LoginReception.dart';
import '../../login/LoginScreen.dart';

class ChangePassword extends StatefulWidget {
  final String accountType;
  const ChangePassword({super.key, required this.accountType});

  @override
  State<ChangePassword> createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  String passCu = '';
  String passMoi = '';
  String confirmPassMoi = '';
  bool hidePasswordCu = true;
  bool hidePasswordMoi = true;
  bool hideConfirmPassMoi = true;
  bool isInvalid = false;
  String errorMsg = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: AppLocalizations.of(context)!
                .translate("setting_change_password"),
            check: false,
            onPress: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Scaffold(
          body: SingleChildScrollView(
            child: Container(
              decoration: BoxDecorationContainer(),
              padding: const EdgeInsets.only(top: 16, bottom: 16),
              margin:
                  const EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 0),
              child: Column(
                children: [
                  Visibility(
                    // ignore: sort_child_properties_last
                    child: Text(
                      errorMsg,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        color: Color(0xFFB43939),
                        fontSize: 14,
                      ),
                    ),
                    visible: isInvalid,
                  ),
                  SecurityInputText(
                    colorText: textColor,
                    focusedBorderColor: textColor,
                    hintText: AppLocalizations.of(context)!
                        .translate('password_old_hint'),
                    labelText: AppLocalizations.of(context)!
                        .translate('phone_old_number'),
                    value: passCu,
                    isHidePassword: hidePasswordCu,
                    onChanged: (value) => setState(() => passCu = value),
                    onClickIcon: () =>
                        setState(() => hidePasswordCu = !hidePasswordCu),
                  ),
                  const SizedBox(
                    height: defaultPadding,
                  ),
                  SecurityInputText(
                    colorText: textColor,
                    focusedBorderColor: textColor,
                    hintText: AppLocalizations.of(context)!
                        .translate('password_new_hint'),
                    labelText: AppLocalizations.of(context)!
                        .translate('phone_new_number'),
                    value: passMoi,
                    isHidePassword: hidePasswordMoi,
                    onChanged: (value) => setState(() => passMoi = value),
                    onClickIcon: () =>
                        setState(() => hidePasswordMoi = !hidePasswordMoi),
                  ),
                  const SizedBox(
                    height: defaultPadding,
                  ),
                  SecurityInputText(
                    colorText: textColor,
                    focusedBorderColor: textColor,
                    hintText: AppLocalizations.of(context)!
                        .translate('password_new_hint'),
                    labelText: AppLocalizations.of(context)!
                        .translate('confirm_phone_new_number'),
                    value: confirmPassMoi,
                    isHidePassword: hideConfirmPassMoi,
                    onChanged: (value) =>
                        setState(() => confirmPassMoi = value),
                    onClickIcon: () => setState(
                        () => hideConfirmPassMoi = !hideConfirmPassMoi),
                  ),
                  const SizedBox(
                    height: defaultPadding,
                  ),
                ],
              ),
            ),
          ),
          bottomNavigationBar: BottomNavigator(
            widget: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  width: 16,
                ),
                Expanded(
                    child: CustomButton(
                  text: AppLocalizations.of(context)!
                      .translate("setting_change_password"),
                  icon: Icons.save,
                  height: 32,
                  press: () {
                    if (passCu.trim().isEmpty ||
                        passMoi.trim().isEmpty ||
                        confirmPassMoi.trim().isEmpty) {
                      setState(() {
                        isInvalid = true;
                        errorMsg = AppLocalizations.of(context)!
                            .translate('error_text_empty');
                      });
                    } else if (isAtLeastSixCharacters(passMoi) == false) {
                      setState(() {
                        isInvalid = true;
                        errorMsg = AppLocalizations.of(context)!
                            .translate('error_text_min_password');
                      });
                    } else {
                      changePassword(passMoi);
                    }
                  },
                )),
                Container(
                  width: 16,
                ),
              ],
            ),
          ),
        ));
  }

  bool isAtLeastSixCharacters(String input) {
    // Biểu thức chính quy kiểm tra xem chuỗi có ít nhất 6 ký tự không
    final RegExp regex = RegExp(r'^.{6,}$');
    return regex.hasMatch(input);
  }

  void changePassword(String password) async {
    try {
      // Lấy tài khoản người dùng hiện tại
      User user = FirebaseAuth.instance.currentUser!;
      // Đổi mật khẩu
      String pass = '';
      if (widget.accountType == key_account_user) {
        pass = StorePreferences.getPassword();
      } else if (widget.accountType == key_account_reception) {
        pass = StorePreferences.getPassReception();
      } else if (widget.accountType == key_account_doctor) {
        pass = StorePreferences.getPassDoctor();
      }
      if (passCu != pass) {
        setState(() {
          isInvalid = true;
          errorMsg = AppLocalizations.of(context)!
              .translate('error_text_old_password_not_right');
        });
      } else {
        if (passMoi != confirmPassMoi) {
          setState(() {
            isInvalid = true;
            errorMsg = AppLocalizations.of(context)!
                .translate('error_text_password_confirm');
          });
        } else {
          await user.updatePassword(password).then((value) {
            ToastCommon.showToast(AppLocalizations.of(context)!
                .translate('toast_changePass_success'));
            if (widget.accountType == key_account_user) {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()));
            } else if (widget.accountType == key_account_reception) {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginReception()));
            } else if (widget.accountType == key_account_doctor) {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginDoctor()));
            }
          }).catchError((onError) {
            ToastCommon.showToast(AppLocalizations.of(context)!
                .translate('toast_changePass_faild'));
          });
        }
      }
    } catch (e) {
      ToastCommon.showToast(
          AppLocalizations.of(context)!.translate('toast_changePass_faild'));
    }
  }
}
