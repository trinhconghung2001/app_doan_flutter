import 'dart:io';

import 'package:app_doan_flutter/screens/homeRecption/components/Body.dart';
import 'package:app_doan_flutter/screens/manage/manageDoctor/ManageDoctor.dart';
import 'package:app_doan_flutter/screens/manage/manageExamination/ManageExamination.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../config/styles/Dimens.dart';
import '../../untils/AppLocalizations.dart';
import '../../untils/StorePreferences.dart';
import '../home/components/BackgroundHomeScreen.dart';

class HomeReception extends StatelessWidget {
  const HomeReception({super.key});

  @override
  Widget build(BuildContext context) {
    List<String> listTitle = [
      AppLocalizations.of(context)!.translate('app_bar_manage_doctor'),
      AppLocalizations.of(context)!.translate('app_bar_manage_examination'),
      // AppLocalizations.of(context)!.translate('app_bar_manage_vaccination')
    ];
    List<SvgPicture> listImg = [
      SvgPicture.asset(
        'assets/icons/home_reception/AddDoctor.svg',
        height: 120,
      ),
      SvgPicture.asset(
        'assets/icons/home_reception/HomeExamination.svg',
        height: 120,
      ),
      // SvgPicture.asset(
      //   'assets/icons/home_reception/HomeVaccination.svg',
      //   height: 120,
      // )
    ];
    Size size = MediaQuery.of(context).size;
    double deviceHeight = MediaQuery.of(context).size.height;
    final isSmallMobile =
        Platform.isAndroid ? deviceHeight < 600 : deviceHeight < 700;
    final isMediumMobile =
        Platform.isAndroid ? deviceHeight < 1200 : deviceHeight < 1000;
    // This size provide us total height and width of our screen
    return BackgroundHomeScreen(
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: isSmallMobile ? size.height * 0.4 : size.height * 0.4,
              width: size.width,
              color: Colors.transparent,
              child: Container(
                  decoration: const BoxDecoration(
                      color: Color(0xFFFDFDFD),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40.0),
                        topRight: Radius.circular(40.0),
                      )),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: defaultPaddingMin, right: defaultPaddingMin),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: isSmallMobile
                              ? size.height * 0.02
                              : size.height * 0.025,
                        ),
                        Text(
                          AppLocalizations.of(context)!
                              .translate('home_card_title'),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: isMediumMobile ? 20 : 36,
                            shadows: const <Shadow>[
                              Shadow(
                                offset: Offset(0.0, 4.0),
                                blurRadius: 4.0,
                                color: Color.fromARGB(0, 0, 0, 1),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.only(top: defaultPadding),
                            child: Container(
                              height: isSmallMobile
                                  ? size.height * 0.3
                                  : size.height * 0.32,
                              margin: EdgeInsets.symmetric(horizontal: 8),
                              child: ListView.builder(
                                itemCount: 2,
                                scrollDirection: Axis.vertical,
                                itemBuilder: (BuildContext context, int index) {
                                  return GestureDetector(
                                    onTap: () {
                                      if (index == 0) {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    ManageDoctor()));
                                      } else if (index == 1) {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    ManageExamination()));
                                      }
                                      // else {
                                      //   Navigator.push(
                                      //       context,
                                      //       MaterialPageRoute(
                                      //           builder: (context) =>
                                      //               ManageVaccination()));
                                      // }
                                    },
                                    child: Body(
                                      text: listTitle[index],
                                      svgPicture: listImg[index],
                                    ),
                                  );
                                },
                              ),
                            )),
                      ],
                    ),
                  )),
            ),
          ),
        ],
      ),
      userName: StorePreferences.getNameReception(),
    );
  }
}
