import 'package:app_doan_flutter/screens/home/bookVaccine/data/Vaccination.dart';
import 'package:app_doan_flutter/screens/home/suggestVaccine/Vaccine.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/screens/manage/data/Doctor.dart';
import 'package:app_doan_flutter/screens/manage/data/Room.dart';
import 'package:app_doan_flutter/untils/FutureManageDoctor.dart';
import 'package:app_doan_flutter/untils/FutureRoom.dart';
import 'package:app_doan_flutter/untils/FutureVaccination.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/InputDropDown.dart';
import '../../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../config/styles/KeyValue.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/StorePreferences.dart';
import '../../category/ListCategory.dart';

// ignore: must_be_immutable
class DoctorConfirmVaccination extends StatefulWidget {
  Vaccination lichTiem;
  FamilyMember benhNhan;
  DoctorConfirmVaccination(
      {super.key, required this.lichTiem, required this.benhNhan});

  @override
  State<DoctorConfirmVaccination> createState() =>
      _DoctorConfirmVaccinationState();
}

class _DoctorConfirmVaccinationState extends State<DoctorConfirmVaccination> {
  Room phongTiem = Room.defaultContructor();
  Doctor bacSi = Doctor.defaultContructor();
  Vaccine goiVaccine = Vaccine.defaultContructor();
  String textTrangThai = '';
  bool validate = false;
  late Future<void> _loadDetailVaccination;
  @override
  void initState() {
    _loadDetailVaccination = loadDetail();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: CustomAppBar(
          text: AppLocalizations.of(context)!
              .translate("app_bar_confirm_vaccination"),
          check: false,
          onPress: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Scaffold(
          body: SingleChildScrollView(
              child: FutureBuilder(
        future: _loadDetailVaccination,
        builder: ((context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return CircularProgressIndicatorWidget();
          } else if (snapshot.hasError) {
            return EmptyData(
                msg: AppLocalizations.of(context)!.translate('error_data_msg'));
          } else {
            return Column(
              children: [
                Container(
                  decoration: BoxDecorationContainer(),
                  padding: const EdgeInsets.only(
                      top: 16, left: 16, right: 16, bottom: 16),
                  margin: const EdgeInsets.only(
                      top: 8, left: 16, right: 16, bottom: 0),
                  child: Column(
                    children: [
                      InputDropDown(
                          title: AppLocalizations.of(context)!
                              .translate('title_status'),
                          value: textTrangThai,
                          onPress: () {
                            guiNhanDrop(
                                defaultStatusBook,
                                AppLocalizations.of(context)!
                                    .translate('category_title_status'));
                          },
                          checkRangBuoc: true,
                          icon: Icons.arrow_drop_down,
                          isError: validate && textTrangThai.isEmpty),
                      const SizedBox(
                        height: 16,
                      ),
                      InputDropDown(
                        title: AppLocalizations.of(context)!
                            .translate('title_patient'),
                        value: widget.benhNhan.ten,
                        checkRangBuoc: false,
                        icon: Icons.arrow_drop_down,
                        showIcon: false,
                        offEdit: true,
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      InputDropDown(
                        title: AppLocalizations.of(context)!
                            .translate('category_title_doctor_vaccine'),
                        value: bacSi.ten,
                        checkRangBuoc: false,
                        icon: Icons.arrow_drop_down,
                        showIcon: false,
                        offEdit: true,
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      InputDropDown(
                        title: AppLocalizations.of(context)!
                            .translate('category_title_department_vaccine'),
                        value: phongTiem.tenPhong,
                        checkRangBuoc: false,
                        icon: Icons.arrow_drop_down,
                        showIcon: false,
                        offEdit: true,
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      InputDropDown(
                        title: AppLocalizations.of(context)!
                            .translate('select_day_vaccination'),
                        value: widget.lichTiem.ngayTiem,
                        checkRangBuoc: false,
                        icon: Icons.arrow_drop_down,
                        showIcon: false,
                        offEdit: true,
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      InputDropDown(
                        title: AppLocalizations.of(context)!
                            .translate('select_time_vaccination'),
                        value: widget.lichTiem.gioTiem,
                        checkRangBuoc: false,
                        icon: Icons.arrow_drop_down,
                        showIcon: false,
                        offEdit: true,
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      InputDropDown(
                        title: AppLocalizations.of(context)!
                            .translate('select_vaccination_package'),
                        value: goiVaccine.ten,
                        checkRangBuoc: false,
                        icon: Icons.arrow_drop_down,
                        showIcon: false,
                        offEdit: true,
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      InputDropDown(
                        title: AppLocalizations.of(context)!
                            .translate('content_note'),
                        value: widget.lichTiem.ghiChu,
                        checkRangBuoc: false,
                        icon: Icons.arrow_drop_down,
                        showIcon: false,
                        offEdit: true,
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      InputDropDown(
                        title: AppLocalizations.of(context)!
                            .translate('select_anamnesis'),
                        value: widget.lichTiem.tienSuBenh,
                        checkRangBuoc: false,
                        icon: Icons.arrow_drop_down,
                        showIcon: false,
                        offEdit: true,
                      ),
                    ],
                  ),
                ),
              ],
            );
          }
        }),
      ))),
      bottomNavigationBar: BottomNavigator(
        widget: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              width: 16,
            ),
            Expanded(
                child: CustomButton(
              text: AppLocalizations.of(context)!.translate("button_cancel"),
              height: 32,
              icon: Icons.cancel_outlined,
              iconColor: primaryColor,
              textColor: primaryColor,
              background: backgroundScreenColor,
              press: () {
                Navigator.pop(context);
              },
            )),
            Container(
              width: 8,
            ),
            Expanded(
                child: CustomButton(
              text: AppLocalizations.of(context)!.translate("button_agree"),
              icon: Icons.save,
              height: 32,
              press: () {
                checkValidate();
                if (validate == false) {
                  confirmExamination();
                }
              },
            )),
            Container(
              width: 16,
            ),
          ],
        ),
      ),
    );
  }

  Future<void> loadDetail() async {
    phongTiem = await readIDPhongTiem(widget.lichTiem.idPhongTiem);
    goiVaccine = await readIDGoiVaccine(widget.lichTiem.idVaccine);
    bacSi = await readIDDoctor(phongTiem.idBacSi);
    textTrangThai = widget.lichTiem.trangThai;
  }

  void checkValidate() {
    if (textTrangThai.isNotEmpty) {
      setState(() {
        validate = false;
      });
    } else {
      setState(() {
        validate = true;
      });
    }
  }

  void guiNhanDrop(String loai, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      if (value != null) {
        if (loai == defaultStatusBook) {
          setState(() {
            if (value == status_confirm_1) {
              textTrangThai = status_2;
            } else if (value == status_confirm_2) {
              textTrangThai = status_3;
            }
          });
        }
      }
    });
  }

  Future<void> confirmExamination() async {
    try {
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('RegistrationSchedule')
          .doc(StorePreferences.getIdUser());
      List<Vaccination> listVaccination =
          await readAllVaccination(StorePreferences.getIdUser());
      Vaccination vaccinationNew = Vaccination(
          id: widget.lichTiem.id,
          idUser: StorePreferences.getIdUser(),
          trangThai: textTrangThai,
          idBenhNhan: widget.benhNhan.id,
          idPhongTiem: widget.lichTiem.idPhongTiem,
          ngayTiem: widget.lichTiem.ngayTiem,
          gioTiem: widget.lichTiem.gioTiem,
          ghiChu: widget.lichTiem.ghiChu,
          tienSuBenh: widget.lichTiem.tienSuBenh,
          idBenhVien: widget.lichTiem.idBenhVien,
          idVaccine: widget.lichTiem.idVaccine);
      for (Vaccination e in listVaccination) {
        if (e.id == widget.lichTiem.id) {
          listVaccination.remove(e);
          break;
        }
      }
      if (listVaccination.any((e) =>
              e.idVaccine == vaccinationNew.idVaccine &&
              e.idBenhVien == vaccinationNew.idBenhVien &&
              e.ngayTiem == vaccinationNew.ngayTiem &&
              e.idPhongTiem == vaccinationNew.idPhongTiem &&
              e.gioTiem == vaccinationNew.gioTiem) ==
          false) {
        listVaccination.add(vaccinationNew);
        documentReference.update({
          'vaccination': listVaccination.map((e) => e.toMap()).toList()
        }).then((value) {
          ToastCommon.showToast(AppLocalizations.of(context)!
              .translate('toast_confirm_vaccination_success'));
          Navigator.pop(context);
        }).catchError((e) {
          showDialog(
              context: context,
              builder: (context) {
                return ErrorPopup(
                  message: AppLocalizations.of(context)!
                      .translate('error_confirm_vaccination'),
                  buttonText: 'OK',
                  onPress: () {
                    Navigator.pop(context);
                  },
                );
              });
        });
      } else {
        showDialog(
            context: context,
            builder: (context) {
              return ErrorPopup(
                message: AppLocalizations.of(context)!
                    .translate('error_book_duplicate'),
                buttonText: 'OK',
                onPress: () {
                  Navigator.pop(context);
                },
              );
            });
      }
    } on Exception catch (e) {
      print(e);
      // ignore: use_build_context_synchronously
      showDialog(
          context: context,
          builder: (context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_confirm_examination'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          });
    }
  }
}
