import 'package:app_doan_flutter/screens/homeDoctor/calendarAdvise/ChatScreenDoctor.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/loading/CircularProgressIndicatorWidget.dart';
import '../../../components/loading/EmptyData.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureMessage.dart';
import '../../../untils/FutureUser.dart';
import '../../../untils/StorePreferences.dart';
import '../../home/bookAdvise/components/ItemDoctorAdvise.dart';
import '../../login/data/User.dart';

class MessageDoctor extends StatefulWidget {
  const MessageDoctor({super.key});

  @override
  State<MessageDoctor> createState() => _MessageDoctorState();
}

class _MessageDoctorState extends State<MessageDoctor> {
  late Future<void> _loadDataUser;
  List<Users> listUser = [];
  @override
  void initState() {
    _loadDataUser = loadData();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDataUser = loadData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: CustomAppBar(
          text:
              AppLocalizations.of(context)!.translate('app_bar_message_advise'),
          check: false,
          checkIconBack: false,
          onPress: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: FutureBuilder(
        future: _loadDataUser,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return CircularProgressIndicatorWidget();
          } else if (snapshot.hasError) {
            return EmptyData(
                msg: AppLocalizations.of(context)!.translate('error_data_msg'));
          } else {
            if (listUser.length > 0) {
              return Container(
                  margin: const EdgeInsets.only(top: 8),
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: listUser.length,
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ChatScreenDoctor(
                                          users: listUser[index],
                                        ))).then((value) => loadScreen());
                          },
                          child: ItemDoctorAdvise(
                              urlImg: listUser[index].imgURL,
                              hoTen: listUser[index].ten,
                              idSend: StorePreferences.getIdDoctor(),
                              idReceive: listUser[index].id,
                              online: listUser[index].online == true));
                    },
                  ));
            } else {
              // Không có dữ liệu
              return EmptyData();
            }
          }
        },
      ),
    );
  }

  Future<void> loadData() async {
    List<Users> listUsers = await getAllUser();
    listUser = listUsers;
    for (Users e in listUsers) {
      int soPhanTu =
          await readLengthMessage(StorePreferences.getIdDoctor(), e.id);
      if (soPhanTu < 0) {
        listUser.remove(e);
      }
    }
  }
}
