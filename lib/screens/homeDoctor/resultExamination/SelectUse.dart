import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/InputDropDown.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../untils/AppLocalizations.dart';
import '../../category/ListCategory.dart';

class SelectUse extends StatefulWidget {
  final List<String> listThuoc;
  final List<String> cachDung;
  const SelectUse({super.key, required this.listThuoc, required this.cachDung});

  @override
  State<SelectUse> createState() => _SelectUseState();
}

class _SelectUseState extends State<SelectUse> {
  bool validate = false;
  List<String> listCachDung = [];
  @override
  void initState() {
    //tách list chuỗi cách dùng
    for (String item in widget.cachDung) {
      final startIndex = item.indexOf(': ');
      if (startIndex != -1) {
        final extractedPart = item.substring(startIndex + 2);
        listCachDung.add(extractedPart);
      } else {
        listCachDung = widget.cachDung.map((e) => '').toList();
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: CustomAppBar(
          text: AppLocalizations.of(context)!.translate("select_use"),
          check: false,
          onPress: () {
            Navigator.pop(context, widget.cachDung);
          },
        ),
      ),
      body: Scaffold(
        body: SingleChildScrollView(
            child: Container(
          height: MediaQuery.of(context).size.height * 0.81,
          decoration: BoxDecorationContainer(),
          padding:
              const EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 16),
          margin: const EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 0),
          child: ListView.builder(
              itemCount: widget.listThuoc.length,
              itemBuilder: (BuildContext context, int index) {
                print(listCachDung);
                return Column(
                  children: [
                    InputDropDown(
                        onPress: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ListCategory(
                                        loai: defaultCachDung,
                                        titleAppbar:
                                            AppLocalizations.of(context)!
                                                .translate('select_use'),
                                      ))).then((value) {
                            if (value != null) {
                              setState(() {
                                listCachDung[index] = value;
                              });
                            }
                          });
                        },
                        value: listCachDung[index],
                        title: widget.listThuoc[index],
                        checkRangBuoc: true,
                        icon: Icons.arrow_drop_down,
                        isError: validate && listCachDung[index].isEmpty),
                    const SizedBox(
                      height: 16,
                    ),
                  ],
                );
              }),
        )),
      ),
      bottomNavigationBar: BottomNavigator(
        widget: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              width: 16,
            ),
            Expanded(
                child: CustomButton(
              text: AppLocalizations.of(context)!.translate("button_cancel"),
              height: 32,
              icon: Icons.cancel_outlined,
              iconColor: primaryColor,
              textColor: primaryColor,
              background: backgroundScreenColor,
              press: () {
                Navigator.pop(context, widget.cachDung);
              },
            )),
            Container(
              width: 8,
            ),
            Expanded(
                child: CustomButton(
              text: 'OK',
              icon: Icons.save,
              height: 32,
              press: () {
                checkValidate();
                if (validate == false) {
                  List<String> listSend = [];
                  for (int i = 0; i < widget.listThuoc.length; i++) {
                    listSend.add(widget.listThuoc[i] + ': ' + listCachDung[i]);
                  }
                  Navigator.pop(context, listSend);
                }
              },
            )),
            Container(
              width: 16,
            ),
          ],
        ),
      ),
    );
  }

  void checkValidate() {
    for (String i in listCachDung) {
      if (i.isNotEmpty) {
        setState(() {
          validate = false;
        });
      } else {
        setState(() {
          validate = true;
        });
      }
    }
  }
}
