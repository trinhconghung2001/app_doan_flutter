import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:flutter/material.dart';

import '../../../components/inputform/RowTextView.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../config/styles/Dimens.dart';
import '../../../item/ItemInformation.dart';
import '../../../untils/AppLocalizations.dart';

class ItemResultExamination extends StatelessWidget {
  final String? idKetQua;
  final String benhNhan;
  final String noiDungKham;
  final String thoiGian;
  const ItemResultExamination(
      {super.key,
      this.idKetQua,
      required this.benhNhan,
      required this.noiDungKham,
      required this.thoiGian});

  @override
  Widget build(BuildContext context) {
    Color? titleColor;
    String titleTrangThai;
    if (idKetQua!.isEmpty) {
      titleColor = warningColor;
      titleTrangThai = status_result_2;
    } else {
      titleColor = backgroudSearch;
      titleTrangThai = status_result_1;
    }
    return ItemInformation(
      widgetBottom: Column(
        children: [
          RowTextView(
            title: AppLocalizations.of(context)!.translate('title_patient'),
            content: benhNhan,
            flexItemLeft: 2,
            flexItemRight: 4,
          ),
          RowTextView(
            title:
                AppLocalizations.of(context)!.translate('content_examination'),
            content: noiDungKham,
            flexItemLeft: 2,
            flexItemRight: 4,
          ),
          RowTextView(
            title: AppLocalizations.of(context)!
                .translate('select_day_book_examination'),
            content: thoiGian,
            flexItemLeft: 2,
            flexItemRight: 4,
            paddingBottom: defaultPaddingMin,
          ),
        ],
      ),
      widgetTop: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Text(
              titleTrangThai,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
      titleColor: titleColor,
    );
  }
}
