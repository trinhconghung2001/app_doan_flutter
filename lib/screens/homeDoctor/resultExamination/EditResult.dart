import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/components/loading/EmptyData.dart';
import 'package:app_doan_flutter/screens/homeDoctor/resultExamination/Result.dart';
import 'package:app_doan_flutter/screens/homeDoctor/resultExamination/SelectUse.dart';
import 'package:app_doan_flutter/untils/FutureResult.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/bottomnavigator/BottomNavigator.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/InputDropDown.dart';
import '../../../components/inputform/InputTextField.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../config/styles/CustomColor.dart';
import '../../../config/styles/KeyValue.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/StorePreferences.dart';
import '../../category/ListAddCategory.dart';

class EditResult extends StatefulWidget {
  final String idKetQua;
  const EditResult({super.key, required this.idKetQua});

  @override
  State<EditResult> createState() => _EditResultState();
}

class _EditResultState extends State<EditResult> {
  late Future<void> _loadDataEdit;
  final FocusNode _focusChuanDoan = FocusNode();
  final FocusNode _focusHuongDieuTri = FocusNode();
  final FocusNode _focusKieng = FocusNode();
  String textNgayBatDau = '';
  String textNgayKetThuc = '';
  String textThuoc = '';
  String textCachDung = '';
  List<String> listCachDung = [];
  List<String> listThuoc = [];

  bool validate = false;
  DateTime dateStart = DateTime.now();
  DateTime dateEnd = DateTime.now();

  final TextEditingController textChuanDoan = TextEditingController();
  final TextEditingController textHuongDieuTri = TextEditingController();
  final TextEditingController textKieng = TextEditingController();

  @override
  void initState() {
    readThuoc();
    _loadDataEdit = loadData();
    super.initState();
    _focusChuanDoan.addListener(_onFocusChange);
    _focusHuongDieuTri.addListener(_onFocusChange);
    _focusKieng.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    setState(() {});
  }

  @override
  void dispose() {
    _focusChuanDoan.dispose();
    _focusHuongDieuTri.dispose();
    _focusKieng.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: AppLocalizations.of(context)!
                  .translate("app_bar_edit_result_examination"),
              check: false,
              onPress: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: Scaffold(
            body: SingleChildScrollView(
                child: FutureBuilder(
              future: _loadDataEdit,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return CircularProgressIndicatorWidget();
                } else if (snapshot.hasError) {
                  return EmptyData();
                } else {
                  return Container(
                    decoration: BoxDecorationContainer(),
                    padding: const EdgeInsets.only(
                        top: 16, left: 16, right: 16, bottom: 16),
                    margin: const EdgeInsets.only(
                        top: 8, left: 16, right: 16, bottom: 0),
                    child: Column(
                      children: [
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("title_result_diagnosis"),
                          checkRangBuoc: true,
                          isError: validate && textChuanDoan.text.isEmpty,
                          focusNode: _focusChuanDoan,
                          textController: textChuanDoan,
                          onPress: () {
                            setState(() {
                              textChuanDoan.clear();
                            });
                          },
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("title_result_treatment_direction"),
                          checkRangBuoc: true,
                          isError: validate && textHuongDieuTri.text.isEmpty,
                          focusNode: _focusHuongDieuTri,
                          textController: textHuongDieuTri,
                          onPress: () {
                            setState(() {
                              textHuongDieuTri.clear();
                            });
                          },
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputTextField(
                          title: AppLocalizations.of(context)!
                              .translate("title_result_abstain"),
                          checkRangBuoc: true,
                          isError: validate && textKieng.text.isEmpty,
                          focusNode: _focusKieng,
                          textController: textKieng,
                          onPress: () {
                            setState(() {
                              textKieng.clear();
                            });
                          },
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              _showStart();
                            },
                            value: textNgayBatDau,
                            title: AppLocalizations.of(context)!
                                .translate("select_start_day"),
                            checkRangBuoc: true,
                            icon: Icons.calendar_month,
                            isError: validate && textNgayBatDau.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              _showEnd();
                            },
                            value: textNgayKetThuc,
                            title: AppLocalizations.of(context)!
                                .translate("select_end_day"),
                            checkRangBuoc: true,
                            icon: Icons.calendar_month,
                            isError: validate && textNgayKetThuc.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiThuoc(
                                  listThuoc,
                                  AppLocalizations.of(context)!
                                      .translate('category_type_medication'));
                            },
                            maxLines: 6,
                            value: textThuoc,
                            title: AppLocalizations.of(context)!
                                .translate("select_type_medication"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textThuoc.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => SelectUse(
                                              listThuoc: listThuoc,
                                              cachDung: listCachDung)))
                                  .then((value) {
                                if (value != listCachDung) {
                                  String textCachDungChon = '';
                                  for (int i = 0; i < value.length; i++) {
                                    textCachDungChon += value[i];
                                    if (i < value.length - 1) {
                                      textCachDungChon += ', ';
                                    }
                                  }
                                  setState(() {
                                    listCachDung = value;
                                    textCachDung = textCachDungChon;
                                  });
                                }
                              });
                            },
                            maxLines: 6,
                            value: textCachDung,
                            title: AppLocalizations.of(context)!
                                .translate("select_use"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textCachDung.isEmpty),
                      ],
                    ),
                  );
                }
              },
            )),
          ),
          bottomNavigationBar: BottomNavigator(
            widget: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  width: 16,
                ),
                Expanded(
                    child: CustomButton(
                  text:
                      AppLocalizations.of(context)!.translate("button_cancel"),
                  height: 32,
                  icon: Icons.cancel_outlined,
                  iconColor: primaryColor,
                  textColor: primaryColor,
                  background: backgroundScreenColor,
                  press: () {
                    Navigator.pop(context);
                  },
                )),
                Container(
                  width: 8,
                ),
                Expanded(
                    child: CustomButton(
                  text: AppLocalizations.of(context)!
                      .translate("button_edit_result"),
                  icon: Icons.save,
                  height: 32,
                  press: () {
                    checkValidate();
                    if (validate == false) {
                      editResult();
                    }
                  },
                )),
                Container(
                  width: 16,
                ),
              ],
            ),
          ),
        ));
  }

  void checkValidate() {
    if (textChuanDoan.text.isNotEmpty &&
        textHuongDieuTri.text.isNotEmpty &&
        textKieng.text.isNotEmpty &&
        textNgayBatDau.isNotEmpty &&
        textNgayKetThuc.isNotEmpty &&
        textCachDung.isNotEmpty &&
        textThuoc.isNotEmpty) {
      setState(() {
        validate = false;
      });
    } else {
      setState(() {
        validate = true;
      });
    }
  }

  void _showStart() async {
    final DateTime? result = await showDatePicker(
      context: context,
      initialDate: dateStart,
      firstDate: DateTime(2024),
      lastDate: DateTime(2025),
      locale: const Locale('vi'),
    );
    if (result != null) {
      bool checkDateStart = result.isBefore(dateEnd);
      if (checkDateStart == true) {
        setState(() {
          dateStart = result;
          textNgayBatDau = '${formatDate(dateStart, [dd, '-', mm, '-', yyyy])}';
        });
      } else {
        // ignore: use_build_context_synchronously
        showDialog(
            context: context,
            builder: (context) {
              return ErrorPopup(
                message: AppLocalizations.of(context)!
                    .translate('error_popup_start_end_day_2'),
                buttonText: 'OK',
                title: AppLocalizations.of(context)!
                    .translate('notification_error'),
                onPress: () {
                  Navigator.pop(context);
                },
              );
            });
      }
    }
  }

  void _showEnd() async {
    final DateTime? result = await showDatePicker(
      context: context,
      initialDate: dateEnd,
      firstDate: DateTime(2024),
      lastDate: DateTime(2025),
      locale: const Locale('vi'),
    );
    if (result != null) {
      bool checkDateRange = result.isAfter(dateStart);
      if (checkDateRange == true) {
        setState(() {
          dateEnd = result;
          textNgayKetThuc = '${formatDate(dateEnd, [dd, '-', mm, '-', yyyy])}';
        });
      } else if (checkDateRange == false) {
        // ignore: use_build_context_synchronously
        showDialog(
            context: context,
            builder: (context) {
              return ErrorPopup(
                message: AppLocalizations.of(context)!
                    .translate('error_popup_start_end_day'),
                buttonText: 'OK',
                title: AppLocalizations.of(context)!
                    .translate('notification_error'),
                onPress: () {
                  Navigator.pop(context);
                },
              );
            });
      }
    }
  }

  void guiThuoc(List<String> list, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListAddCategory(
                  type: defaultTypeMedication,
                  list: list,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      String textThuocChon = '';
      for (int i = 0; i < value.length; i++) {
        textThuocChon += value[i];
        if (i < value.length - 1) {
          textThuocChon += ', ';
        }
      }
      setState(() {
        listThuoc = value;
        textThuoc = textThuocChon;
      });
    });
  }

  Future<void> readThuoc() async {
    DocumentReference documentReference = FirebaseFirestore.instance
        .collection('Categorys')
        .doc('8nDu9e5HN5HkdyhmO4Lz');
    DocumentSnapshot documentSnapshot = await documentReference.get();

    if (documentSnapshot.exists) {
      Map<String, dynamic> dataCategory =
          documentSnapshot.data() as Map<String, dynamic>;
      StorePreferences.setListThuoc(
          List<String>.from(dataCategory['thuoc'] as List));
    } else {
      StorePreferences.setListThuoc([]);
    }
  }

  Future<void> editResult() async {
    print(widget.idKetQua);
    try {
      DocumentReference documentReference = await FirebaseFirestore.instance
          .collection('RegistrationSchedule')
          .doc(StorePreferences.getIdUser());
      List<Result> listResult =
          await readListResult(StorePreferences.getIdUser());
      Result ketQuaNew = Result(
          chuanDoan: textChuanDoan.text,
          huongDieuTri: textHuongDieuTri.text,
          cachDung: listCachDung,
          id: widget.idKetQua,
          kieng: textKieng.text,
          ngayBatDau: textNgayBatDau,
          ngayKetThuc: textNgayKetThuc,
          thuoc: listThuoc);
      for (Result e in listResult) {
        if (e.id == widget.idKetQua) {
          listResult.remove(e);
          break;
          // e = ketQuaNew;
        }
      }
      listResult.add(ketQuaNew);
      await documentReference.update(
          {'resultExamination': listResult.map((e) => e.toMap()).toList()});
      ToastCommon.showToast(AppLocalizations.of(context)!
          .translate('toast_edit_result_examination_success'));
      Navigator.pop(context);
    } catch (e) {
      ToastCommon.showToast(AppLocalizations.of(context)!
          .translate('toast_edit_result_examination_fail'));
    }
  }

  Future<void> loadData() async {
    List<Result> listKetQua =
        await readListResult(StorePreferences.getIdUser());
    Result ketQua = Result.defaultContructor();
    for (Result i in listKetQua) {
      if (i.id == widget.idKetQua) {
        ketQua = i;
        break;
      }
    }
    textChuanDoan.text = ketQua.chuanDoan;
    textHuongDieuTri.text = ketQua.huongDieuTri;
    listCachDung = ketQua.cachDung.map((e) => e.toString()).toList();
    for (int i = 0; i < listCachDung.length; i++) {
      textCachDung += listCachDung[i];
      if (i < listCachDung.length - 1) {
        textCachDung += ', ';
      }
    }
    textKieng.text = ketQua.kieng;
    listThuoc = ketQua.thuoc.map((e) => e.toString()).toList();
    textNgayBatDau = ketQua.ngayBatDau;
    textNgayKetThuc = ketQua.ngayKetThuc;
    dateStart = DateFormat("dd-MM-yyyy").parse(ketQua.ngayBatDau);
    dateEnd = DateFormat("dd-MM-yyyy").parse(ketQua.ngayKetThuc);
    for (int i = 0; i < listThuoc.length; i++) {
      textThuoc += listThuoc[i];
      if (i < listThuoc.length - 1) {
        textThuoc += ', ';
      }
    }
  }
}
