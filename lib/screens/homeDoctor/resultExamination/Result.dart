class Result {
  final String chuanDoan;
  final String huongDieuTri;
  final String kieng;
  final String id;
  final String ngayBatDau;
  final String ngayKetThuc;
  final List<dynamic> thuoc;
  final List<dynamic> cachDung;

  Result({
    required this.chuanDoan,
    required this.huongDieuTri,
    required this.cachDung,
    required this.id,
    required this.kieng,
    required this.ngayBatDau,
    required this.ngayKetThuc,
    required this.thuoc,
  });

  factory Result.defaultContructor() {
    return Result(
        chuanDoan: '',
        huongDieuTri: '',
        cachDung: [],
        kieng: '',
        id: '',
        ngayBatDau: '',
        ngayKetThuc: '',
        thuoc: []);
  }

  Map<String, dynamic> toMap() {
    return {
      'chuanDoan': chuanDoan,
      'huongDieuTri': huongDieuTri,
      'kieng': kieng,
      'id': id,
      'cachDung': cachDung,
      'ngayBatDau': ngayBatDau,
      'ngayKetThuc': ngayKetThuc,
      'thuoc': thuoc
    };
  }

  factory Result.fromMap(Map<String, dynamic> map) {
    return Result(
        chuanDoan: map['chuanDoan'],
        kieng: map['kieng'],
        huongDieuTri: map['huongDieuTri'],
        id: map['id'],
        cachDung: map['cachDung'],
        ngayBatDau: map['ngayBatDau'],
        ngayKetThuc: map['ngayKetThuc'],
        thuoc: map['thuoc']);
  }
}
