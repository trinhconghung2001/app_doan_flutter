import 'package:app_doan_flutter/components/bottomnavigator/BottomNavigator.dart';
import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/components/loading/EmptyData.dart';
import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/home/bookExamination/data/Examination.dart';
import 'package:app_doan_flutter/screens/homeDoctor/resultExamination/AddResult.dart';
import 'package:app_doan_flutter/screens/homeDoctor/resultExamination/EditResult.dart';
import 'package:app_doan_flutter/screens/homeDoctor/resultExamination/Result.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/screens/manage/data/Department.dart';
import 'package:app_doan_flutter/screens/manage/data/Hospital.dart';
import 'package:app_doan_flutter/screens/manage/data/Room.dart';
import 'package:app_doan_flutter/untils/FutureHospital.dart';
import 'package:app_doan_flutter/untils/FutureResult.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/RowTextView.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureDepartment.dart';
import '../../../untils/FutureExamination.dart';
import '../../../untils/FutureUser.dart';
import '../../../untils/StorePreferences.dart';

class DetatilResultExamination extends StatefulWidget {
  final String idExamination;
  final String idFamily;
  final String idBenhNhan;

  const DetatilResultExamination({
    super.key,
    required this.idExamination,
    required this.idFamily,
    required this.idBenhNhan,
  });

  @override
  State<DetatilResultExamination> createState() =>
      _DetatilResultExaminationState();
}

class _DetatilResultExaminationState extends State<DetatilResultExamination> {
  FamilyMember hoSo = FamilyMember.defaultContructor();
  late Future<void> _loadDataEdit;
  Examination lichKham = Examination.defaultContructor();
  Hospital benhVien = Hospital.defaultContructor();
  Department khoa = Department.defaultContructor();
  Room phong = Room.defaultContructor();
  Result ketQua = Result.defaultContructor();
  bool validate = false;
  DateTime dateTime = DateTime.now();

  String trieuChung = '';
  String trangThai = '';

  String loaiThuoc = '';
  String cachDung = '';

  @override
  void initState() {
    readTrieuChung();
    _loadDataEdit = loadData();
    super.initState();
  }

  void loadScreen() {
    setState(() {
      _loadDataEdit = loadData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: AppLocalizations.of(context)!
                  .translate("app_bar_result_detail_examination"),
              check: false,
              onPress: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: FutureBuilder(
            future: _loadDataEdit,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return CircularProgressIndicatorWidget();
              } else if (snapshot.hasError) {
                return EmptyData(
                    msg: AppLocalizations.of(context)!
                        .translate('error_data_msg'));
              } else {
                return Scaffold(
                  body: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          decoration: BoxDecorationContainer(),
                          padding: const EdgeInsets.only(
                            top: 16,
                            left: 16,
                            right: 16,
                          ),
                          margin: const EdgeInsets.only(
                              top: 8, left: 16, right: 16, bottom: 0),
                          child: RowTextView(
                            title: AppLocalizations.of(context)!
                                .translate('title_status'),
                            content: trangThai,
                            flexItemLeft: 2,
                            flexItemRight: 3,
                            color2: lichKham.idKetQua.isEmpty
                                ? warningColor
                                : backgroudSearch,
                          ),
                        ),
                        Container(
                          decoration: BoxDecorationContainer(),
                          padding: const EdgeInsets.only(
                              top: 16, left: 16, right: 16, bottom: 0),
                          margin: const EdgeInsets.only(
                              top: 8, left: 16, right: 16, bottom: 0),
                          child: Column(
                            children: [
                              RowTextView(
                                content: hoSo.ten,
                                title: AppLocalizations.of(context)!
                                    .translate("title_patient"),
                                flexItemLeft: 2,
                                flexItemRight: 3,
                              ),
                              RowTextView(
                                content: benhVien.ten,
                                title: AppLocalizations.of(context)!
                                    .translate("select_hospital"),
                                flexItemLeft: 2,
                                flexItemRight: 3,
                              ),
                              RowTextView(
                                content: lichKham.ngayKham,
                                title: AppLocalizations.of(context)!
                                    .translate("select_day_book_examination"),
                                flexItemLeft: 2,
                                flexItemRight: 3,
                              ),
                              RowTextView(
                                content: lichKham.gioKham,
                                title: AppLocalizations.of(context)!
                                    .translate("select_time_examination"),
                                flexItemLeft: 2,
                                flexItemRight: 3,
                              ),
                              RowTextView(
                                content: khoa.ten,
                                title: AppLocalizations.of(context)!
                                    .translate("category_title_department"),
                                flexItemLeft: 2,
                                flexItemRight: 3,
                              ),
                              RowTextView(
                                content: phong.tenPhong,
                                title: AppLocalizations.of(context)!.translate(
                                    "category_title_room_examination"),
                                flexItemLeft: 2,
                                flexItemRight: 3,
                              ),
                              RowTextView(
                                content: lichKham.loaiKham,
                                title: AppLocalizations.of(context)!
                                    .translate("select_type_examination"),
                                flexItemLeft: 2,
                                flexItemRight: 3,
                              ),
                              RowTextView(
                                content: lichKham.noiDungKham,
                                title: AppLocalizations.of(context)!
                                    .translate("content_examination"),
                                flexItemLeft: 2,
                                flexItemRight: 3,
                              ),
                              RowTextView(
                                content: trieuChung,
                                title: AppLocalizations.of(context)!
                                    .translate("select_symptom"),
                                flexItemLeft: 2,
                                flexItemRight: 3,
                              ),
                              RowTextView(
                                content: lichKham.benhManTinh,
                                title: AppLocalizations.of(context)!
                                    .translate("select_chronic_diseases"),
                                flexItemLeft: 2,
                                flexItemRight: 3,
                              ),
                            ],
                          ),
                        ),
                        Visibility(
                          visible: StorePreferences.getStatus() == false,
                          child: Container(
                              decoration: BoxDecorationContainer(),
                              padding: const EdgeInsets.only(
                                  top: 16, left: 16, right: 16, bottom: 0),
                              margin: const EdgeInsets.only(
                                  top: 8, left: 16, right: 16, bottom: 0),
                              child: Column(
                                children: [
                                  RowTextView(
                                    content: ketQua.chuanDoan,
                                    title: AppLocalizations.of(context)!
                                        .translate("title_result_diagnosis"),
                                    flexItemLeft: 2,
                                    flexItemRight: 3,
                                  ),
                                  RowTextView(
                                    content: ketQua.huongDieuTri,
                                    title: AppLocalizations.of(context)!
                                        .translate(
                                            "title_result_treatment_direction"),
                                    flexItemLeft: 2,
                                    flexItemRight: 3,
                                  ),
                                  RowTextView(
                                    content: ketQua.kieng,
                                    title: AppLocalizations.of(context)!
                                        .translate("title_result_abstain"),
                                    flexItemLeft: 2,
                                    flexItemRight: 3,
                                  ),
                                  RowTextView(
                                    content: ketQua.ngayBatDau,
                                    title: AppLocalizations.of(context)!
                                        .translate("select_start_day"),
                                    flexItemLeft: 2,
                                    flexItemRight: 3,
                                  ),
                                  RowTextView(
                                    content: ketQua.ngayKetThuc,
                                    title: AppLocalizations.of(context)!
                                        .translate("select_end_day"),
                                    flexItemLeft: 2,
                                    flexItemRight: 3,
                                  ),
                                  RowTextView(
                                    content: loaiThuoc,
                                    title: AppLocalizations.of(context)!
                                        .translate("select_type_medication"),
                                    flexItemLeft: 2,
                                    flexItemRight: 3,
                                  ),
                                  RowTextView(
                                    content: cachDung,
                                    title: AppLocalizations.of(context)!
                                        .translate("select_use"),
                                    flexItemLeft: 2,
                                    flexItemRight: 3,
                                  ),
                                ],
                              )),
                        )
                      ],
                    ),
                  ),
                  bottomNavigationBar: BottomNavigator(
                    widget: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: 16,
                        ),
                        Expanded(
                            child: CustomButton(
                          text: AppLocalizations.of(context)!
                              .translate("button_cancel"),
                          height: 32,
                          icon: Icons.cancel_outlined,
                          iconColor: primaryColor,
                          textColor: primaryColor,
                          background: backgroundScreenColor,
                          press: () {
                            Navigator.pop(context);
                          },
                        )),
                        Container(
                          width: 8,
                        ),
                        Expanded(
                            child: StorePreferences.getStatus() == false
                                ? CustomButton(
                                    text: AppLocalizations.of(context)!
                                        .translate("button_edit_result"),
                                    icon: Icons.edit_document,
                                    height: 32,
                                    press: () {
                                      Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      EditResult(
                                                          idKetQua: ketQua.id)))
                                          .then((value) => loadScreen());
                                    },
                                  )
                                : CustomButton(
                                    text: AppLocalizations.of(context)!
                                        .translate("button_return_result"),
                                    icon: Icons.save,
                                    height: 32,
                                    press: () {
                                      Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      AddResult(
                                                          idLichKham:
                                                              lichKham.id)))
                                          .then((value) => loadScreen());
                                    },
                                  )),
                        Container(
                          width: 16,
                        ),
                      ],
                    ),
                  ),
                );
              }
            },
          )),
    );
  }

  Future<void> readTrieuChung() async {
    DocumentReference documentReference = FirebaseFirestore.instance
        .collection('Categorys')
        .doc('8nDu9e5HN5HkdyhmO4Lz');
    DocumentSnapshot documentSnapshot = await documentReference.get();

    if (documentSnapshot.exists) {
      Map<String, dynamic> dataCategory =
          documentSnapshot.data() as Map<String, dynamic>;
      StorePreferences.setListTrieuChung(
          List<String>.from(dataCategory['trieuChung'] as List));
    } else {
      StorePreferences.setListTrieuChung([]);
    }
  }

  Future<void> loadData() async {
    // StorePreferences.setIdUser(widget.detailExamination)
    lichKham = await readExamination_byIDUser_byIDExmaination(
        StorePreferences.getIdUser(), widget.idExamination);
    hoSo =
        await readIDBenhNhan(StorePreferences.getIdUser(), widget.idBenhNhan);
    benhVien = await readIDHospital(StorePreferences.getIdHospital());
    khoa = await readIDKhoaKham(lichKham.idKhoa);
    ketQua = await readResult(StorePreferences.getIdUser(), lichKham.idKetQua);
    List<Room> listPhong = khoa.phongKham;
    phong = Room.defaultContructor();
    for (Room i in listPhong) {
      if (i.id == lichKham.idPhong) {
        phong = i;
        break;
      }
    }
    if (lichKham.idKetQua.isEmpty) {
      trangThai = status_result_2;
      //set trạng thái để tí check button sửa hoặc trả kết quả
      //chua tra ket qua
      StorePreferences.setStatus(true);
    } else {
      trangThai = status_result_1;
      StorePreferences.setStatus(false);
    }
    List<String> listTrieuChung =
        lichKham.trieuChung.map((e) => e.toString()).toList();
    trieuChung = '';
    for (int i = 0; i < listTrieuChung.length; i++) {
      trieuChung += listTrieuChung[i];
      if (i < listTrieuChung.length - 1) {
        trieuChung += ', ';
      }
    }
    loaiThuoc = '';
    for (int i = 0; i < ketQua.thuoc.length; i++) {
      loaiThuoc += ketQua.thuoc[i];
      if (i < ketQua.thuoc.length - 1) {
        loaiThuoc += ', ';
      }
    }
    cachDung = '';
    for (int i = 0; i < ketQua.cachDung.length; i++) {
      cachDung += ketQua.cachDung[i];
      if (i < ketQua.cachDung.length - 1) {
        cachDung += ', ';
      }
    }
  }
}
