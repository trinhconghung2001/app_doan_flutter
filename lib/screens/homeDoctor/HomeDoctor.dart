import 'dart:io';

import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/home/bookAdvise/notification_service/NotificationDoctor.dart';
import 'package:app_doan_flutter/screens/homeDoctor/calendarAdvise/MessageDoctor.dart';
import 'package:app_doan_flutter/screens/homeDoctor/calendarExamination/CalendarExamination.dart';
import 'package:app_doan_flutter/screens/homeDoctor/components/Body.dart';
import 'package:app_doan_flutter/screens/homeDoctor/resultExamination/ResultExamination.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../config/styles/Dimens.dart';
import '../../untils/AppLocalizations.dart';
import '../../untils/StorePreferences.dart';
import '../home/components/BackgroundHomeScreen.dart';

class HomeDoctor extends StatefulWidget {
  const HomeDoctor({super.key});

  @override
  State<HomeDoctor> createState() => _HomeDoctorState();
}

class _HomeDoctorState extends State<HomeDoctor> {
  NotificationDoctor _notificationDoctor = NotificationDoctor();
  @override
  void initState() {
    // TODO: implement initState
    _notificationDoctor.requestNotificationPermissions();
    _notificationDoctor.forgroundMessages();
    _notificationDoctor.firebaseInt(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<String> listTitleExamination = [
      AppLocalizations.of(context)!.translate('app_bar_calendar_examination'),
      AppLocalizations.of(context)!.translate('app_bar_result_examination'),
    ];
    // List<String> listTitleVaccination = [
    //   AppLocalizations.of(context)!.translate('app_bar_calendar_vaccination')
    // ];
    List<String> listTitleAdvise = [
      AppLocalizations.of(context)!.translate('app_bar_message_advise'),
    ];
    List<SvgPicture> listImgExamination = [
      SvgPicture.asset(
        'assets/icons/home_doctor/DoctorExamination.svg',
        height: 120,
      ),
      SvgPicture.asset(
        'assets/icons/home_doctor/ResultExamination.svg',
        height: 120,
      ),
    ];
    // List<SvgPicture> listImgVaccination = [
    //   SvgPicture.asset(
    //     'assets/icons/home_doctor/DoctorVaccination.svg',
    //     height: 120,
    //   ),
    // ];
    List<SvgPicture> listImgAdvise = [
      SvgPicture.asset(
        'assets/icons/home_doctor/DoctorAdvise.svg',
        height: 120,
      ),
    ];
    List<String> listTitle = [];
    List<SvgPicture> listImg = [];
    if (StorePreferences.getTypeDoctor() == key_role_doctor_examination) {
      listTitle = listTitleExamination;
      listImg = listImgExamination;
    }
    // else if (StorePreferences.getTypeDoctor() ==
    //     key_role_doctor_vaccination) {
    //   listTitle = listTitleVaccination;
    //   listImg = listImgVaccination;
    // }
    else if (StorePreferences.getTypeDoctor() == key_role_doctor_advise) {
      listTitle = listTitleAdvise;
      listImg = listImgAdvise;
    }
    Size size = MediaQuery.of(context).size;
    double deviceHeight = MediaQuery.of(context).size.height;
    final isSmallMobile =
        Platform.isAndroid ? deviceHeight < 600 : deviceHeight < 700;
    final isMediumMobile =
        Platform.isAndroid ? deviceHeight < 1200 : deviceHeight < 1000;
    // This size provide us total height and width of our screen
    return BackgroundHomeScreen(
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: isSmallMobile ? size.height * 0.4 : size.height * 0.4,
              width: size.width,
              color: Colors.transparent,
              child: Container(
                  decoration: const BoxDecoration(
                      color: Color(0xFFFDFDFD),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40.0),
                        topRight: Radius.circular(40.0),
                      )),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: defaultPaddingMin, right: defaultPaddingMin),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: isSmallMobile
                              ? size.height * 0.02
                              : size.height * 0.025,
                        ),
                        Text(
                          AppLocalizations.of(context)!
                              .translate('home_card_title'),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: isMediumMobile ? 20 : 36,
                            shadows: const <Shadow>[
                              Shadow(
                                offset: Offset(0.0, 4.0),
                                blurRadius: 4.0,
                                color: Color.fromARGB(0, 0, 0, 1),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.only(top: defaultPadding),
                            child: Container(
                              height: isSmallMobile
                                  ? size.height * 0.3
                                  : size.height * 0.32,
                              margin: EdgeInsets.symmetric(horizontal: 8),
                              child: ListView.builder(
                                  itemCount: listTitle.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    if (StorePreferences.getTypeDoctor() ==
                                        key_role_doctor_examination) {
                                      return Column(
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              if (index == 0) {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            CalendarExamination()));
                                              } else if (index == 1) {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            ResultExamination()));
                                              }
                                            },
                                            child: Body(
                                              text: listTitle[index],
                                              svgPicture: listImg[index],
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 16,
                                          )
                                        ],
                                      );
                                    } else if (StorePreferences
                                            .getTypeDoctor() ==
                                        key_role_doctor_advise) {
                                      return GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      MessageDoctor()));
                                        },
                                        child: Body(
                                          text: listTitle[index],
                                          svgPicture: listImg[index],
                                        ),
                                      );
                                    }
                                    // else if (StorePreferences
                                    //         .getTypeDoctor() ==
                                    //     key_role_doctor_vaccination) {
                                    //   return GestureDetector(
                                    //     onTap: () {
                                    //       Navigator.push(
                                    //           context,
                                    //           MaterialPageRoute(
                                    //               builder: (context) =>
                                    //                   CalendarVaccination()));
                                    //     },
                                    //     child: Body(
                                    //       text: listTitle[index],
                                    //       svgPicture: listImg[index],
                                    //     ),
                                    //   );
                                    // }
                                    return null;
                                  }),
                            )),
                      ],
                    ),
                  )),
            ),
          ),
        ],
      ),
      userName: StorePreferences.getNameDoctor(),
    );
  }
}
