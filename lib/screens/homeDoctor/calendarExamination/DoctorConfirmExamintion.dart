import 'package:app_doan_flutter/components/bottomnavigator/BottomNavigator.dart';
import 'package:app_doan_flutter/components/loading/CircularProgressIndicatorWidget.dart';
import 'package:app_doan_flutter/components/loading/EmptyData.dart';
import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/category/ListCategory.dart';
import 'package:app_doan_flutter/screens/home/bookExamination/data/Examination.dart';
import 'package:app_doan_flutter/screens/login/data/FamilyMember.dart';
import 'package:app_doan_flutter/screens/manage/data/Department.dart';
import 'package:app_doan_flutter/screens/manage/data/Hospital.dart';
import 'package:app_doan_flutter/screens/manage/data/Room.dart';
import 'package:app_doan_flutter/untils/FutureHospital.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../../components/appbar/CustomAppBar.dart';
import '../../../components/button/CustomButton.dart';
import '../../../components/container/Decoration.dart';
import '../../../components/inputform/InputDropDown.dart';
import '../../../components/popup/ErrorPopup.dart';
import '../../../components/toast/ToastCommon.dart';
import '../../../untils/AppLocalizations.dart';
import '../../../untils/FutureDepartment.dart';
import '../../../untils/FutureExamination.dart';
import '../../../untils/FutureUser.dart';
import '../../../untils/StorePreferences.dart';
import '../../category/ListAddCategory.dart';

class DoctorConfirmExamination extends StatefulWidget {
  final Examination detailExamination;
  const DoctorConfirmExamination({super.key, required this.detailExamination});

  @override
  State<DoctorConfirmExamination> createState() =>
      _DoctorConfirmExaminationState();
}

class _DoctorConfirmExaminationState extends State<DoctorConfirmExamination> {
  FamilyMember hoSo = FamilyMember.defaultContructor();
  late Future<void> _loadDataEdit;
  String textHoSo = '';
  String textBenhVien = '';
  String textNgayKham = '';
  String textGioKham = '';
  String textLoaiKham = '';
  String textTrieuChung = '';
  String textNoiDungKham = '';
  String textBenhManTinh = '';
  String textKhoa = '';
  String textPhong = '';
  String textTrangThai = '';
  List<String> listTrieuChung = [];

  String idKhoa = '';
  String idPhong = '';

  bool validate = false;
  DateTime dateTime = DateTime.now();

  @override
  void initState() {
    readTrieuChung();
    _loadDataEdit = loadData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: AppLocalizations.of(context)!
                  .translate("app_bar_confirm_examination"),
              check: false,
              onPress: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: Scaffold(
            body: SingleChildScrollView(
                child: FutureBuilder(
              future: _loadDataEdit,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return CircularProgressIndicatorWidget();
                } else if (snapshot.hasError) {
                  return EmptyData();
                } else {
                  return Container(
                    decoration: BoxDecorationContainer(),
                    padding: const EdgeInsets.only(
                        top: 16, left: 16, right: 16, bottom: 16),
                    margin: const EdgeInsets.only(
                        top: 8, left: 16, right: 16, bottom: 0),
                    child: Column(
                      children: [
                        InputDropDown(
                          value: textHoSo,
                          title: AppLocalizations.of(context)!
                              .translate("title_patient"),
                          checkRangBuoc: false,
                          icon: Icons.arrow_drop_down,
                          showIcon: false,
                          offEdit: true,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                          value: textBenhVien,
                          title: AppLocalizations.of(context)!
                              .translate("select_hospital"),
                          checkRangBuoc: false,
                          icon: Icons.arrow_drop_down,
                          showIcon: false,
                          offEdit: true,
                          maxLines: 2,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                          value: textNgayKham,
                          title: AppLocalizations.of(context)!
                              .translate("select_day_book_examination"),
                          checkRangBuoc: false,
                          showIcon: false,
                          icon: Icons.calendar_month,
                          offEdit: true,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                          value: textGioKham,
                          title: AppLocalizations.of(context)!
                              .translate("select_time_examination"),
                          checkRangBuoc: false,
                          showIcon: false,
                          icon: Icons.arrow_drop_down,
                          offEdit: true,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                          value: textKhoa,
                          title: AppLocalizations.of(context)!
                              .translate("category_title_department"),
                          checkRangBuoc: false,
                          icon: Icons.arrow_drop_down,
                          offEdit: true,
                          showIcon: false,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                          value: textPhong,
                          title: AppLocalizations.of(context)!
                              .translate("category_title_room_examination"),
                          checkRangBuoc: false,
                          icon: Icons.arrow_drop_down,
                          showIcon: false,
                          offEdit: true,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                            onPress: () {
                              guiNhanDrop(
                                  defaultStatusBook,
                                  AppLocalizations.of(context)!
                                      .translate('category_title_status'));
                            },
                            value: textTrangThai,
                            title: AppLocalizations.of(context)!
                                .translate("title_status"),
                            checkRangBuoc: true,
                            icon: Icons.arrow_drop_down,
                            isError: validate && textTrangThai.isEmpty),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                          value: textLoaiKham,
                          title: AppLocalizations.of(context)!
                              .translate("select_type_examination"),
                          checkRangBuoc: false,
                          icon: Icons.arrow_drop_down,
                          showIcon: false,
                          offEdit: true,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                          value: textNoiDungKham,
                          title: AppLocalizations.of(context)!
                              .translate("content_examination"),
                          checkRangBuoc: false,
                          icon: Icons.arrow_drop_down,
                          showIcon: false,
                          offEdit: true,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                          value: textTrieuChung,
                          title: AppLocalizations.of(context)!
                              .translate("select_symptom"),
                          checkRangBuoc: false,
                          showIcon: false,
                          icon: Icons.arrow_drop_down,
                          offEdit: true,
                          maxLines: 2,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        InputDropDown(
                          value: textBenhManTinh,
                          title: AppLocalizations.of(context)!
                              .translate("select_chronic_diseases"),
                          checkRangBuoc: false,
                          icon: Icons.arrow_drop_down,
                          showIcon: false,
                          offEdit: true,
                        ),
                      ],
                    ),
                  );
                }
              },
            )),
            bottomNavigationBar: BottomNavigator(
              widget: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: 16,
                  ),
                  Expanded(
                      child: CustomButton(
                    text: AppLocalizations.of(context)!
                        .translate("button_cancel"),
                    height: 32,
                    icon: Icons.cancel_outlined,
                    iconColor: primaryColor,
                    textColor: primaryColor,
                    background: backgroundScreenColor,
                    press: () {
                      Navigator.pop(context);
                    },
                  )),
                  Container(
                    width: 8,
                  ),
                  Expanded(
                      child: CustomButton(
                    text: AppLocalizations.of(context)!
                        .translate("button_agree"),
                    icon: Icons.save,
                    height: 32,
                    press: () {
                      checkValidate();
                      if (validate == false) {
                        editExamination();
                      }
                    },
                  )),
                  Container(
                    width: 16,
                  ),
                ],
              ),
            ),
          )),
    );
  }

  void guiNhanDrop(String loai, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListCategory(
                  loai: loai,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      if (value != null) {
        if (loai == defaultStatusBook) {
          setState(() {
            if (value == status_confirm_1) {
              textTrangThai = status_2;
            } else if (value == status_confirm_2) {
              textTrangThai = status_5;
            }
          });
        }
      }
    });
  }

  void checkValidate() {
    if (textKhoa.isNotEmpty &&
        textPhong.isNotEmpty &&
        textTrangThai.isNotEmpty) {
      setState(() {
        validate = false;
      });
    } else {
      setState(() {
        validate = true;
      });
    }
  }

  Future<void> readTrieuChung() async {
    DocumentReference documentReference = FirebaseFirestore.instance
        .collection('Categorys')
        .doc('8nDu9e5HN5HkdyhmO4Lz');
    DocumentSnapshot documentSnapshot = await documentReference.get();

    if (documentSnapshot.exists) {
      Map<String, dynamic> dataCategory =
          documentSnapshot.data() as Map<String, dynamic>;
      StorePreferences.setListTrieuChung(
          List<String>.from(dataCategory['trieuChung'] as List));
    } else {
      StorePreferences.setListTrieuChung([]);
    }
  }

  void guiTrieuChung(List<String> list, String titleAppbar) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ListAddCategory(
                  type: defaultTrieuChung,
                  list: list,
                  titleAppbar: titleAppbar,
                ))).then((value) {
      String text = '';
      for (int i = 0; i < value.length; i++) {
        text += value[i];
        if (i < value.length - 1) {
          text += ', ';
        }
      }
      setState(() {
        listTrieuChung = value;
        textTrieuChung = text;
      });
    });
  }

  Future<void> editExamination() async {
    try {
      DocumentReference documentReference = FirebaseFirestore.instance
          .collection('RegistrationSchedule')
          .doc(StorePreferences.getIdUser());
      List<Examination> listExamination =
          await readAllExamination(StorePreferences.getIdUser());
      Examination examinationNew = Examination(
          id: widget.detailExamination.id,
          idUser: StorePreferences.getIdUser(),
          trangThai: textTrangThai,
          idBenhNhan: hoSo.id,
          idKhoa: widget.detailExamination.idKhoa,
          idPhong: widget.detailExamination.idPhong,
          idKetQua: widget.detailExamination.idKetQua,
          loaiKham: textLoaiKham,
          ngayKham: textNgayKham,
          gioKham: textGioKham,
          noiDungKham: textNoiDungKham,
          benhManTinh: textBenhManTinh,
          idBenhVien: widget.detailExamination.idBenhVien,
          trieuChung: listTrieuChung);
      for (Examination e in listExamination) {
        if (e.id == widget.detailExamination.id) {
          listExamination.remove(e);
          break;
        }
      }
      if (listExamination.any((e) =>
              e.idPhong == examinationNew.idPhong &&
              e.idKhoa == examinationNew.idKhoa &&
              e.ngayKham == examinationNew.ngayKham &&
              e.gioKham == examinationNew.gioKham &&
              e.idBenhVien == examinationNew.idBenhVien) ==
          false) {
        listExamination.add(examinationNew);
        print(examinationNew);
        documentReference.update({
          'examination': listExamination.map((e) => e.toMap()).toList()
        }).then((value) {
          ToastCommon.showToast(AppLocalizations.of(context)!
              .translate('toast_confirm_examination_success'));
          Navigator.pop(context);
        }).catchError((e) {
          showDialog(
              context: context,
              builder: (context) {
                return ErrorPopup(
                  message: AppLocalizations.of(context)!
                      .translate('error_confirm_examination'),
                  buttonText: 'OK',
                  onPress: () {
                    Navigator.pop(context);
                  },
                );
              });
        });
      } else {
        showDialog(
            context: context,
            builder: (context) {
              return ErrorPopup(
                message: AppLocalizations.of(context)!
                    .translate('error_book_duplicate'),
                buttonText: 'OK',
                onPress: () {
                  Navigator.pop(context);
                },
              );
            });
      }
    } on Exception catch (e) {
      print(e);
      // ignore: use_build_context_synchronously
      showDialog(
          context: context,
          builder: (context) {
            return ErrorPopup(
              message: AppLocalizations.of(context)!
                  .translate('error_confirm_examination'),
              buttonText: 'OK',
              onPress: () {
                Navigator.pop(context);
              },
            );
          });
    }
  }

  Future<void> loadData() async {
    // StorePreferences.setIdUser(widget.detailExamination)
    hoSo = await readIDBenhNhan(
        StorePreferences.getIdUser(), widget.detailExamination.idBenhNhan);
    Hospital hospital = await readIDHospital(StorePreferences.getIdHospital());
    Department khoa = await readIDKhoaKham(widget.detailExamination.idKhoa);
    List<Room> listPhong = khoa.phongKham;
    Room phong = Room.defaultContructor();
    for (Room i in listPhong) {
      if (i.id == widget.detailExamination.idPhong) {
        phong = i;
        break;
      }
    }
    textKhoa = khoa.ten;
    textPhong = phong.tenPhong;
    textTrangThai = widget.detailExamination.trangThai;
    textHoSo = hoSo.ten;
    textBenhVien = hospital.ten;
    textNgayKham = widget.detailExamination.ngayKham;
    textGioKham = widget.detailExamination.gioKham;
    textLoaiKham = widget.detailExamination.loaiKham;
    textNoiDungKham = widget.detailExamination.noiDungKham;
    textBenhManTinh = widget.detailExamination.benhManTinh;
    listTrieuChung =
        widget.detailExamination.trieuChung.map((e) => e.toString()).toList();
    for (int i = 0; i < listTrieuChung.length; i++) {
      textTrieuChung += listTrieuChung[i];
      if (i < listTrieuChung.length - 1) {
        textTrieuChung += ', ';
      }
    }
  }
}
