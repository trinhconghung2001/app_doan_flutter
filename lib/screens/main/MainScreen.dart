import 'package:app_doan_flutter/config/styles/CustomColor.dart';
import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/untils/AppLocalizations.dart';
import 'package:flutter/material.dart';

import '../account/AccountScreen.dart';
import '../home/HomeScreen.dart';
import '../information/InformationScreen.dart';
import '../setting/SettingScreen.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> with WidgetsBindingObserver {
  int pageIndex = 0;
  List<Widget> pageList = <Widget>[
    HomeScreen(),
    InformationScreen(),
    AccountScreen(),
    SettingScreen(accountType: key_account_user,),  
  ];
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pageList[pageIndex],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        selectedIconTheme: const IconThemeData(color: primaryColor),
        selectedItemColor: const Color(0xFF292D34),
        selectedLabelStyle:
            const TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
        unselectedItemColor: Color(0xFF82858A),
        unselectedLabelStyle:
            const TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
        currentIndex: pageIndex,
        onTap: (value) {
          setState(() {
            pageIndex = value;
          });
        },
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: const Stack(
              children: <Widget>[
                Icon(
                  Icons.home,
                  size: 20,
                ),
              ],
            ),
            label: AppLocalizations.of(context)!
                .translate('bottom_navigation_bar_home_title'),
          ),
          BottomNavigationBarItem(
            icon: const Stack(
              children: <Widget>[
                 Icon(
                  Icons.menu_book_sharp,
                  size: 20,
                ),
              ],
            ),
            label: AppLocalizations.of(context)!
                .translate('bottom_navigation_bar_information_title'),
          ),
          BottomNavigationBarItem(
            icon: const Stack(
              children: <Widget>[
                Icon(
                  Icons.person,
                  size: 20,
                ),
              ],
            ),
            label: AppLocalizations.of(context)!
                .translate('bottom_navigation_bar_profile_title'),
          ), 
          BottomNavigationBarItem(
            icon: const Stack(
              children: <Widget>[
                Icon(
                  Icons.settings,
                  size: 20,
                ),
              ],
            ),
            label: AppLocalizations.of(context)!
                .translate('bottom_navigation_bar_setting_title'),
          ),
        ],
      ),
    );
  }
}