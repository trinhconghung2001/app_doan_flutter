import 'dart:io';

import 'package:app_doan_flutter/untils/AppLocalizations.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

void showBottom(
    BuildContext context, VoidCallback clickPicture, VoidCallback clickCamera) {
  showModalBottomSheet(
    context: context,
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20), topRight: Radius.circular(20))),
    builder: (context) {
      return ListView(
          padding: EdgeInsets.symmetric(vertical: 16),
          shrinkWrap: true,
          children: [
            Text(
              AppLocalizations.of(context)!.translate('popup_select_picture'),
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: GestureDetector(
                    onTap: clickPicture,
                    child: Container(
                        height: 90,
                        width: 90,
                        child: SvgPicture.asset(
                            'assets/icons/popupPicture/addPicture.svg')),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: clickCamera,
                    child: Container(
                        height: 90,
                        width: 90,
                        child: SvgPicture.asset(
                            'assets/icons/popupPicture/camera.svg')),
                  ),
                ),
              ],
            )
          ]);
    },
  );
}

Future<String> updateProfilePicture(String idUser, File file) async {
  final ext = file.path.split('.').last;
  final ref =
      FirebaseStorage.instance.ref().child('profile_picture/$idUser.$ext');
  await ref.putFile(file, SettableMetadata(contentType: 'image/$ext'));
  String imgURL = '';
  imgURL = await ref.getDownloadURL();
  return imgURL;
}
