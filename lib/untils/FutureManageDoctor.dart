import 'package:cloud_firestore/cloud_firestore.dart';

import '../screens/manage/data/Doctor.dart';

Future<List<Doctor>> readAllDoctor() async {
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('Doctors');
  List<Doctor> listDoctor = [];
  QuerySnapshot querySnapshot = await collectionReference.get();
  for (QueryDocumentSnapshot doc in querySnapshot.docs) {
    DocumentSnapshot documentSnapshot =
        await collectionReference.doc(doc.id).get();
    if (documentSnapshot.exists) {
      Map<String, dynamic> result =
          documentSnapshot.data() as Map<String, dynamic>;
      listDoctor.add(Doctor.fromMap(result));
    }
  }
  return listDoctor;
}

Future<List<Doctor>> readAllDoctor_IdHosital(String idBenhVien) async {
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('Doctors');
  List<Doctor> listDoctor = [];
  QuerySnapshot querySnapshot = await collectionReference.get();
  for (QueryDocumentSnapshot doc in querySnapshot.docs) {
    DocumentSnapshot documentSnapshot =
        await collectionReference.doc(doc.id).get();
    if (documentSnapshot.exists) {
      Map<String, dynamic> result =
          documentSnapshot.data() as Map<String, dynamic>;
      listDoctor.add(Doctor.fromMap(result));
    }
  }
  List<Doctor> list = [];
  for (Doctor i in listDoctor) {
    if (i.idBenhVien == idBenhVien) {
      list.add(i);
    }
  }
  return list;
}

Future<Doctor> readIDDoctor(String idBacSi) async {
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('Doctors');
  DocumentSnapshot documentSnapshot =
      await collectionReference.doc(idBacSi).get();
  if (documentSnapshot.exists) {
    Map<String, dynamic> result =
        documentSnapshot.data() as Map<String, dynamic>;
    return Doctor.fromMap(result);
  }
  return Doctor.defaultContructor();
}
