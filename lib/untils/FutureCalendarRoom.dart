import 'package:cloud_firestore/cloud_firestore.dart';

Future<bool> readCalendarRoom(String date, String time, String idRoom) async {
  checkDate(date, time, idRoom);
  DocumentReference documentReference =
      FirebaseFirestore.instance.collection('CalendarRoom').doc(idRoom);
  DocumentSnapshot documentSnapshot = await documentReference.get();
  if (documentSnapshot.exists) {
    Map<String, dynamic> data = documentSnapshot.data() as Map<String, dynamic>;
    List<dynamic> list = [];
    list = data[date];
    List<String> listTime = list.map((e) => e.toString()).toList();
    for (String i in listTime) {
      if (time.trim() == i.trim()) {
        return false;
      }
    }
  }
  return true;
}

Future<void> checkDate(String date, String time, String idRoom) async {
  DocumentReference documentReference =
      FirebaseFirestore.instance.collection('CalendarRoom').doc(idRoom);
  DocumentSnapshot documentSnapshot = await documentReference.get();
  if (documentSnapshot.exists) {
    Map<String, dynamic> data = documentSnapshot.data() as Map<String, dynamic>;
    List<dynamic> list = [];
    try {
      list = data[date];
    } catch (e) {
      // TODO
      DocumentReference document =
          FirebaseFirestore.instance.collection('CalendarRoom').doc(idRoom);
      document.update({date: []});
    }
    print(list);
  }
}
