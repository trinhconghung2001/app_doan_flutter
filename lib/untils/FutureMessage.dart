//tạo  hàm cđổi 2 id người dùng luôn theo ttự mã băm nhỏ đứng trước
//=> 2 người luôn có 1 cuộc trò chuyện chung được lưu trong 1 doccument duy nhất
import 'dart:io';

import 'package:app_doan_flutter/screens/home/bookAdvise/data/Message.dart';
import 'package:app_doan_flutter/untils/AppLocalizations.dart';
import 'package:app_doan_flutter/untils/FutureNotification.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

String convertTimeOnline(BuildContext context, String hoatDongCuoi) {
  final int i = int.tryParse(hoatDongCuoi) ?? -1;

  //neu tgian k hop le
  if (i == -1) return AppLocalizations.of(context)!.translate('title_no_online');
  //neu hop le
  DateTime time = DateTime.fromMillisecondsSinceEpoch(i);
  DateTime now = DateTime.now();

  String formatTime = TimeOfDay.fromDateTime(time).format(context);
  if (time.day == now.day && time.month == now.month && time.year == now.year) {
    return "${AppLocalizations.of(context)!.translate('title_time_online')} $formatTime";
  }
  if ((now.difference(time).inHours / 24).round() == 1) {
    return "${AppLocalizations.of(context)!.translate('title_time_yesterday_online')} $formatTime";
  }
  String month = _getMonth(time);
  return "${AppLocalizations.of(context)!.translate('title_day_online')} ${time.day} $month ${AppLocalizations.of(context)!.translate('title_at')} $formatTime";
}

String _getMonth(DateTime date) {
  switch (date.month) {
    case 1:
      return 'Jan';
    case 2:
      return 'Feb';
    case 3:
      return 'Mar';
    case 4:
      return 'Apr';
    case 5:
      return 'May';
    case 6:
      return 'Jun';
    case 7:
      return 'Jul';
    case 8:
      return 'Aug';
    case 9:
      return 'Sept';
    case 10:
      return 'Oct';
    case 11:
      return 'Nov';
    case 12:
      return 'Dec';
  }
  return 'NA';
}

String getConverID(String idSend, String idReceive) {
  if (idSend.hashCode <= idReceive.hashCode) {
    return '${idSend}_${idReceive}';
  } else {
    return '${idReceive}_${idSend}';
  }
}

Stream<QuerySnapshot<Map<String, dynamic>>> readAllMessage(
    String idSend, String idReceive) {
  return FirebaseFirestore.instance
      .collection('Chats/${getConverID(idSend, idReceive)}/messages/')
      .orderBy('sent', descending: true)
      .snapshots();
}

Future<void> sendMessage(
    String idSend, String idReceive, String msg, Type type) async {
  String time = DateTime.now().millisecondsSinceEpoch.toString();
  Message message = Message(
      senderId: idSend,
      read: '',
      receiverId: idReceive,
      message: msg,
      sent: time,
      type: type);
  CollectionReference collectionReference = FirebaseFirestore.instance
      .collection('Chats/${getConverID(idSend, idReceive)}/messages/');
  DocumentReference documentReference = await collectionReference.doc(time);
  await documentReference.set(message.toMap()).then((value) {
    if (idSend == StorePreferences.getIdUser()) {
      sendPushNotificationByUser(
          idSend, type == Type.text ? msg : 'image', idReceive);
    } else if(idSend == StorePreferences.getIdDoctor()){
      sendPushNotificationByDoctor(
          idSend, type == Type.text ? msg : 'image', idReceive);
    }
  });
}

// hàm convert kiểu string miliSecondsSinceEpochs trường sent, readthành kiểu time
String getConvertTime(BuildContext context, String text) {
  if (text.isNotEmpty) {
    final date = DateTime.fromMillisecondsSinceEpoch(int.parse(text));
    return TimeOfDay.fromDateTime(date).format(context);
  }
  return '';
}

Future<void> updateMessageReadStatus(Message message) async {
  CollectionReference collectionReference = FirebaseFirestore.instance.collection(
      'Chats/${getConverID(message.senderId, message.receiverId)}/messages/');
  DocumentReference documentReference =
      await collectionReference.doc(message.sent);
  await documentReference
      .update({'read': DateTime.now().millisecondsSinceEpoch.toString()});
}

Future<void> sendPicture(String idSend, String idReceive, File file) async {
  final ext = file.path.split('.').last;
  final ref = FirebaseStorage.instance.ref().child(
      'images/${getConverID(idSend, idReceive)}/${DateTime.now().millisecondsSinceEpoch.toString()}.$ext');
  await ref.putFile(file, SettableMetadata(contentType: 'image/$ext'));
  String imgURL = '';
  imgURL = await ref.getDownloadURL();
  await sendMessage(idSend, idReceive, imgURL, Type.image);
}

Stream<QuerySnapshot<Map<String, dynamic>>> readDoctorInfo(String idDoctor) {
  return FirebaseFirestore.instance
      .collection('Doctors')
      .where('id', isEqualTo: idDoctor)
      .snapshots();
}

// update trạng thái online tkhoan doctor
Future<void> updateStatusOnlineDoctor(String idDoctor, bool isOnline) async {
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('Doctors');
  DocumentReference documentReference = collectionReference.doc(idDoctor);
  String token = await refreshFirebaseMessagingTokenDoctor();
  await documentReference.update({
    'online': isOnline,
    'hoatDongCuoi': DateTime.now().millisecondsSinceEpoch.toString(),
    'pushToken': token
  });
}

// update trạng thái online tkhoan user
Future<void> updateStatusOnlineUser(String idUser, bool isOnline) async {
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('Users');
  DocumentReference documentReference = collectionReference.doc(idUser);
  String token = await refreshFirebaseMessagingTokenUser();
  print(token + '..................');
  await documentReference.update({
    'online': isOnline,
    'hoatDongCuoi': DateTime.now().millisecondsSinceEpoch.toString(),
    'pushToken': token
  });
}

Stream<QuerySnapshot<Map<String, dynamic>>> readUsersInfo(String idUsers) {
  return FirebaseFirestore.instance
      .collection('Users')
      .where('id', isEqualTo: idUsers)
      .snapshots();
}

Stream<QuerySnapshot<Map<String, dynamic>>> readLastAllMessage(
    String idSend, String idReceive) {
  return FirebaseFirestore.instance
      .collection('Chats/${getConverID(idSend, idReceive)}/messages/')
      .limit(2)
      .orderBy('sent', descending: true)
      .snapshots();
}

Future<int> readLengthMessage(String idSend, String idReceive) async {
  List<Message> listMessage = [];
  CollectionReference collectionReference = FirebaseFirestore.instance
      .collection('Chats/${getConverID(idSend, idReceive)}/messages/');
  QuerySnapshot querySnapshot = await collectionReference.get();
  for (QueryDocumentSnapshot doc in querySnapshot.docs) {
    DocumentSnapshot documentSnapshot =
        await collectionReference.doc(doc.id).get();
    if (documentSnapshot.exists) {
      //lay du lieu
      Map<String, dynamic> message =
          documentSnapshot.data() as Map<String, dynamic>;
      listMessage.add(Message.fromMap(message));
    }
  }
  return listMessage.length;
}
