import 'package:cloud_firestore/cloud_firestore.dart';

import '../config/styles/KeyValue.dart';

Future<Map<String, dynamic>> readCalo(String idCalo) async {
  print('Load id: ${idCalo}');
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection("Calo");
  DocumentReference documentReference = collectionReference.doc(idCalo);
  DocumentSnapshot documentSnapshot = await documentReference.get();
  if (documentSnapshot.exists) {
    Map<String, dynamic> result =
        documentSnapshot.data() as Map<String, dynamic>;
    print(result);
    return result;
  } else {
    return {};
  }
}

double calculateBMR(double weight, double height, int age, bool isMale) {
  double bmr;
  // Tính BMR dựa trên công thức Mifflin - St Jeor
  //là nam
  if (isMale) {
    bmr = (6.25 * height * 100) + (10 * weight) - (5 * age) + 5;
  } else {
    bmr = (6.25 * height * 100) + (10 * weight) - (5 * age) - 161;
  }
  return bmr;
}

double calculateActivityFactor(String keyActivity) {
  if (keyActivity == key_title_few) {
    return 1.2;
  } else if (keyActivity == key_title_little) {
    return 1.375;
  } else if (keyActivity == key_title_medium) {
    return 1.55;
  } else if (keyActivity == key_title_high) {
    return 1.725;
  } else if (keyActivity == key_title_very_high) {
    return 1.9;
  }
  return 0;
}

//lượng calo cần thiết để nạp trong ngày
double caclulateTDEE(double bmr, double activityFactor) {
  return double.parse((bmr * activityFactor).toStringAsFixed(2));
}

//lượng calo 1 ngày cần để giảm cân
//Một kilogram (kg) mỡ cơ thể thường tương đương với khoảng 7700 calo.
//Mức giảm này phụ thuộc vào mức độ giảm cân mong muốn và tốc độ giảm cân an toàn
//(thường là khoảng 500 calo dưới TDEE mỗi ngày để giảm khoảng 0.5 kg mỗi tuần).
double caculateWeightLoss(double TDEE, double weightLoss) {
  return TDEE - weightLoss * 2 * 500;
}

//tính lượng calo thặng dư để tăng cân
//Quy tắc chung là một pound (khoảng 0.45 kg) mất/giảm cân tương đương với khoảng 3,500 calo. Do đó, nếu bạn muốn tăng 0.5 kg mỗi tuần
//cần tạo ra một lượng calo thặng dư là 0.5kg/tuần x 3500calo/kg : 7ngày/tuần
double caculateSurplusCalo(double weightGain) {
  return double.parse(((weightGain * 3500) / 7).toStringAsFixed(2));
}

//lượng calo 1 ngày cần để tăng cân
double caculateWeightGain(double TDEE, double weightGain) {
  return TDEE + caculateSurplusCalo(weightGain);
}

//tính lượng tinh bột cần cho lượng lượng calo trong ngày
//Giảm cân: 40/40/20 (carbohydrate/protein/chất béo)
// Tăng cân: 40/30/30 (carbohydrate/protein/chất béo)
// Duy trì cân nặng: 40/30/30 (carbohydrate/protein/chất béo)
//1g Chất đạm = 4 calo
//1g Tinh bột = 4 calo
//1g Chất béo = 9 calo
double calculateCarbs(double caloDay) {
  return double.parse((caloDay * 0.4 / 4).toStringAsFixed(2));
}

double calculateProtein(String textKetLuan, double caloDay) {
  if (textKetLuan == conclusion_underweight ||
      textKetLuan == conclusion_normal) {
    return double.parse((caloDay * 0.3 / 4).toStringAsFixed(2));
  } else {
    return double.parse((caloDay * 0.4 / 4).toStringAsFixed(2));
  }
}

double calculateFat(String textKetLuan, double caloDay) {
  if (textKetLuan == conclusion_underweight ||
      textKetLuan == conclusion_normal) {
    return double.parse((caloDay * 0.3 / 9).toStringAsFixed(2));
  } else {
    return double.parse((caloDay * 0.2 / 9).toStringAsFixed(2));
  }
}
