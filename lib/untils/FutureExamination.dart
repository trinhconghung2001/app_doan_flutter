import 'package:app_doan_flutter/screens/home/bookExamination/data/Examination.dart';
import 'package:app_doan_flutter/screens/manage/data/Hospital.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'FutureHospital.dart';

Future<List<Examination>> readAllExamination(String idUser) async {
  DocumentReference documentReference =
      FirebaseFirestore.instance.collection('RegistrationSchedule').doc(idUser);
  DocumentSnapshot documentSnapshot = await documentReference.get();
  if (documentSnapshot.exists) {
    Map<String, dynamic> userData =
        documentSnapshot.data() as Map<String, dynamic>;
    List<Map<String, dynamic>> result = [];
    try {
      result = List<Map<String, dynamic>>.from(userData['examination'] as List);
    } catch (e) {
      return [];
    }
    List<Examination> listExamination = [];
    for (int i = 0; i < result.length; i++) {
      listExamination.add(Examination.fromMap(result[i]));
    }
    return listExamination;
  }
  return [];
}

Future<List<Examination>> readAllExamination_byIDHosital(
    String idBenhVien) async {
  Hospital hospital = await readIDHospital(idBenhVien);
  List<Examination> listExamination = [];
  List<Map<String, dynamic>> listKham = hospital.lichKham;
  for (int i = 0; i < listKham.length; i++) {
    String idUser = listKham[i]['idUser'];
    String idLichKham = listKham[i]['idLichKham'];
    Examination examination =
        await readExamination_byIDUser_byIDExmaination(idUser, idLichKham);
    listExamination.add(examination);
  }
  return listExamination;
}

Future<Examination> readExamination_byIDUser_byIDExmaination(
    String idUser, String idLichKham) async {
  List<Examination> list = await readAllExamination(idUser);
  for (Examination i in list) {
    if (i.id == idLichKham) {
      return i;
    }
  }
  return Examination.defaultContructor();
}

Future<List<Examination>> readAllExamination_ByIdBenhNhan(
    String idUser, String idBenhNhan) async {
  DocumentReference documentReference =
      FirebaseFirestore.instance.collection('RegistrationSchedule').doc(idUser);
  DocumentSnapshot documentSnapshot = await documentReference.get();
  if (documentSnapshot.exists) {
    Map<String, dynamic> userData =
        documentSnapshot.data() as Map<String, dynamic>;
    List<Map<String, dynamic>> result = [];
    try {
      result = List<Map<String, dynamic>>.from(userData['examination'] as List);
    } catch (e) {
      return [];
    }
    List<Examination> listExamination = [];
    for (int i = 0; i < result.length; i++) {
      Examination e = Examination.fromMap(result[i]);
      if(e.idBenhNhan == idBenhNhan){
        listExamination.add(e);
      }
      
    }
    return listExamination;
  }
  return [];
}
