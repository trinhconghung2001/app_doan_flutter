import 'dart:convert';

import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StorePreferences {
  static SharedPreferences? _preferences;
  static const _keyEmail = 'email';
  static const _keyPassword = 'password';
  static const _keyIdUser = 'id_user';
  static const _keyName = 'user_name';
  static const _keyBHYTUser = 'bhyt_user';
  static const _keyProvince = 'key_province';
  static const _keyDistricts = 'key_districts';
  static const _keyListTime = 'key_list_time';
  static const _keyListTrieuChung = 'key_list_trieu_chung';
  static const _keyStatus = 'key_status';
  static const _keyEmailReception = 'email_reception';
  static const _keyIdReception = 'id_reception';
  static const _keyPassReception = 'pass_reception';
  static const _keyNameReception = 'name_reception';
  static const _keyIdHospital = 'id_hospital';
  static const _keyIdDepartment = 'id_department';
  static const _keyEmailDoctor = 'email_doctor';
  static const _keyIdDoctor = 'id_doctor';
  static const _keyPassDoctor = 'pass_doctor';
  static const _keyNameDoctor = 'name_doctor';
  static const _keyTypeDoctor = 'type_account';
  static const _keyListMedication = 'key_list_thuoc';
  static const _keyListUse = 'key_list_use';
  static const _keyDate = 'key_date';
  static const _keyTime = 'key_time';
  static const _keyIdFamily = 'key_id_family';
  static const _key_Food = 'key_food';
  static const _key_start_date = 'key_start_date';
  static const _key_end_date = 'key_end_date';

  static Future init() async =>
      _preferences = await SharedPreferences.getInstance();
  static Future setIdUser(String id) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyIdUser, id);
    }
  }

  static String getIdUser() {
    if (_preferences != null) {
      return _preferences!.getString(_keyIdUser) ?? "";
    }
    return "";
  }

  static Future setName(String name) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyName, name);
    }
  }

  static String getName() {
    if (_preferences != null) {
      return _preferences!.getString(_keyName) ?? "";
    }
    return "";
  }

  static Future setBHYT_User(String bhyt) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyBHYTUser, bhyt);
    }
  }

  static String getBHYT_User() {
    if (_preferences != null) {
      return _preferences!.getString(_keyBHYTUser) ?? "";
    }
    return "";
  }

  static Future setKeyProvince(String province) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyProvince, province);
    }
  }

  static String getKeyProvince() {
    if (_preferences != null) {
      return _preferences!.getString(_keyProvince) ?? "";
    }
    return "";
  }

  static Future setKeyDistricts(String district) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyDistricts, district);
    }
  }

  static String getKeyDistricts() {
    if (_preferences != null) {
      return _preferences!.getString(_keyDistricts) ?? "";
    }
    return "";
  }

  static String getEmail() {
    if (_preferences != null) {
      return _preferences!.getString(_keyEmail) ?? "";
    }
    return "";
  }

  static Future setEmail(String email) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyEmail, email);
    }
  }

  static String getPassword() {
    if (_preferences != null) {
      return _preferences!.getString(_keyPassword) ?? "";
    }
    return "";
  }

  static Future setPassword(String pass) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyPassword, pass);
    }
  }

  //tạo get set lưu list giờ nhắc nhở lịch dùng thuốc
  static List<String> getListTime() {
    if (_preferences != null) {
      final jsonList = _preferences!.getString(_keyListTime) ?? '[]';
      return (json.decode(jsonList) as List).cast<String>();
    }
    return [];
  }

  static Future setListTime(List<String> list) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyListTime, json.encode(list));
    }
  }

  //tạo get set lưu list triệu chứng
  static List<String> getListTrieuChung() {
    if (_preferences != null) {
      final jsonList = _preferences!.getString(_keyListTrieuChung) ?? '[]';
      return (json.decode(jsonList) as List).cast<String>();
    }
    return [];
  }

  static Future setListTrieuChung(List<String> list) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyListTrieuChung, json.encode(list));
    }
  }

  //taoj get set lưu trạng thái lịch
  static bool getStatus() {
    if (_preferences != null) {
      return _preferences!.getBool(_keyStatus) ?? false;
    }
    return false;
  }

  static Future setStatus(bool a) async {
    if (_preferences != null) {
      await _preferences!.setBool(_keyStatus, a);
    }
  }

  static String getEmailReception() {
    if (_preferences != null) {
      return _preferences!.getString(_keyEmailReception) ?? "";
    }
    return "";
  }

  static Future setEmailReception(String email) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyEmailReception, email);
    }
  }

  static Future setIdReception(String id) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyIdReception, id);
    }
  }

  static String getIdReception() {
    if (_preferences != null) {
      return _preferences!.getString(_keyIdReception) ?? "";
    }
    return "";
  }

  static Future setNameReception(String name) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyNameReception, name);
    }
  }

  static String getNameReception() {
    if (_preferences != null) {
      return _preferences!.getString(_keyNameReception) ?? "";
    }
    return "";
  }

  static String getPassReception() {
    if (_preferences != null) {
      return _preferences!.getString(_keyPassReception) ?? "";
    }
    return "";
  }

  static Future setPassReception(String pass) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyPassReception, pass);
    }
  }

  static String getIdHospital() {
    if (_preferences != null) {
      return _preferences!.getString(_keyIdHospital) ?? "";
    }
    return "";
  }

  static Future setIdHospital(String id) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyIdHospital, id);
    }
  }

  static String getIdDepartment() {
    if (_preferences != null) {
      return _preferences!.getString(_keyIdDepartment) ?? "";
    }
    return "";
  }

  static Future setIdDepartment(String id) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyIdDepartment, id);
    }
  }

  static String getEmailDoctor() {
    if (_preferences != null) {
      return _preferences!.getString(_keyEmailDoctor) ?? "";
    }
    return "";
  }

  static Future setEmailDoctor(String email) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyEmailDoctor, email);
    }
  }

  static Future setIdDoctor(String id) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyIdDoctor, id);
    }
  }

  static String getIdDoctor() {
    if (_preferences != null) {
      return _preferences!.getString(_keyIdDoctor) ?? "";
    }
    return "";
  }

  static Future setNameDoctor(String name) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyNameDoctor, name);
    }
  }

  static String getNameDoctor() {
    if (_preferences != null) {
      return _preferences!.getString(_keyNameDoctor) ?? "";
    }
    return "";
  }

  static String getPassDoctor() {
    if (_preferences != null) {
      return _preferences!.getString(_keyPassDoctor) ?? "";
    }
    return "";
  }

  static Future setPassDoctor(String pass) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyPassDoctor, pass);
    }
  }

  static String getTypeDoctor() {
    if (_preferences != null) {
      return _preferences!.getString(_keyTypeDoctor) ?? "";
    }
    return "";
  }

  static Future setTypeDoctor(String type) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyTypeDoctor, type);
    }
  }

  //tạo get set lưu list triệu chứng
  static List<String> getListThuoc() {
    if (_preferences != null) {
      final jsonList = _preferences!.getString(_keyListMedication) ?? '[]';
      return (json.decode(jsonList) as List).cast<String>();
    }
    return [];
  }

  static Future setListThuoc(List<String> list) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyListMedication, json.encode(list));
    }
  }

  static List<String> getListCachDung() {
    if (_preferences != null) {
      final jsonList = _preferences!.getString(_keyListUse) ?? '[]';
      return (json.decode(jsonList) as List).cast<String>();
    }
    return [];
  }

  static Future setListCachDung(List<String> list) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyListUse, json.encode(list));
    }
  }

  static String getDate() {
    if (_preferences != null) {
      return _preferences!.getString(_keyDate) ?? "";
    }
    return "";
  }

  static Future setDate(DateTime date) async {
    String textDate = DateFormat('dd-MM-yyyy').format(date);
    if (_preferences != null) {
      await _preferences!.setString(_keyDate, textDate);
    }
  }

  static String getTime() {
    if (_preferences != null) {
      return _preferences!.getString(_keyTime) ?? "";
    }
    return "";
  }

  static Future setTime(String time) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyTime, time);
    }
  }

  static String getIdFamily() {
    if (_preferences != null) {
      return _preferences!.getString(_keyIdFamily) ?? "";
    }
    return "";
  }

  static Future setIdFamily(String idFamily) async {
    if (_preferences != null) {
      await _preferences!.setString(_keyIdFamily, idFamily);
    }
  }

  static String getFoodName() {
    if (_preferences != null) {
      return _preferences!.getString(_key_Food) ?? "";
    }
    return "";
  }

  static Future setFoodName(String food) async {
    if (_preferences != null) {
      await _preferences!.setString(_key_Food, food);
    }
  }
  static String getStartDate() {
    if (_preferences != null) {
      return _preferences!.getString(_key_start_date) ?? "";
    }
    return "";
  }

  static Future setStartDate(String date) async {
    if (_preferences != null) {
      await _preferences!.setString(_key_start_date, date);
    }
  }
  static String getEndDate() {
    if (_preferences != null) {
      return _preferences!.getString(_key_end_date) ?? "";
    }
    return "";
  }

  static Future setEndDate(String date) async {
    if (_preferences != null) {
      await _preferences!.setString(_key_end_date, date);
    }
  }
}
