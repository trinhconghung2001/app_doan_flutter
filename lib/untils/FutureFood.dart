import 'package:app_doan_flutter/config/styles/KeyValue.dart';
import 'package:app_doan_flutter/screens/home/calcularCalo/data/Food.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'FutureCalo.dart';

Future<List<Food>> readCategoryFood() async {
  DocumentReference documentReference = FirebaseFirestore.instance
      .collection('Categorys')
      .doc('8nDu9e5HN5HkdyhmO4Lz');
  DocumentSnapshot documentSnapshot = await documentReference.get();
  if (documentSnapshot.exists) {
    Map<String, dynamic> dataCategory =
        documentSnapshot.data() as Map<String, dynamic>;
    List<dynamic> list = dataCategory['food'];
    List<Food> listFood = list.map((e) => Food.fromMap(e)).toList();
    return listFood;
  }
  return [];
}

Future<List<Food>> readCategoryFoodUser(String idUser) async {
  DocumentReference documentReference =
      FirebaseFirestore.instance.collection('Food').doc(idUser);
  DocumentSnapshot documentSnapshot = await documentReference.get();
  if (documentSnapshot.exists) {
    Map<String, dynamic> dataFood =
        documentSnapshot.data() as Map<String, dynamic>;
    try {
      List<dynamic> list = dataFood['food'];
      List<Food> listFood = list.map((e) => Food.fromMap(e)).toList();
      return listFood;
    } catch (e) {
      // TODO
      return [];
    }
  }
  return [];
}

Future<List<Food>> readListAllFood(String idUser) async {
  List<Food> listUser = await readCategoryFoodUser(idUser);
  List<Food> listCategory = await readCategoryFood();
  List<Food> listAll = [];
  listAll = listUser + listCategory;
  return listAll;
}

Future<List<Food>> readListFoodMeal(
    String idUser, String date, String meal) async {
  Map<String, dynamic> resultCalo = await readCalo(idUser);
  Map<String, dynamic> mapCalo = resultCalo[date] as Map<String, dynamic>;
  List<dynamic> listDynamic = [];
  if (meal == food_breakfase) {
    listDynamic = mapCalo['buaSang'];
  } else if (meal == food_lunch) {
    listDynamic = mapCalo['buaTrua'];
  } else if (meal == food_dinner) {
    listDynamic = mapCalo['buaToi'];
  } else if (meal == food_snacks) {
    listDynamic = mapCalo['buaPhu'];
  }
  List<Food> listFood = [];
  listFood = listDynamic.map((e) => Food.fromMap(e)).toList();
  return listFood;
}
