import 'package:app_doan_flutter/screens/manage/data/DepartmentAdvise.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../screens/manage/data/Department.dart';
import '../screens/manage/data/Hospital.dart';
import 'FutureHospital.dart';

Future<List<Department>> readAllDepartment_ByIdHospital(
    String idHosital) async {
  Hospital hospital = await readIDHospital(idHosital);
  List<Department> listDepartment = [];
  listDepartment = hospital.khoa;
  return listDepartment;
}
Future<List<DepartmentAdvise>> readAllDepartmentAdvise_ByIdHospital(
    String idHosital) async {
  Hospital hospital = await readIDHospital(idHosital);
  List<DepartmentAdvise> listDepartment = [];
  listDepartment = hospital.khoaTuVan;
  return listDepartment;
}

Future<Department> readIDKhoaKham(String idKhoa) async {
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('Hospital');
  QuerySnapshot querySnapshot = await collectionReference.get();
  Department khoaKham = Department.defaultContructor();
  for (QueryDocumentSnapshot doc in querySnapshot.docs) {
    DocumentSnapshot documentSnapshot =
        await collectionReference.doc(doc.id).get();
    if (documentSnapshot.exists) {
      Map<String, dynamic> hospitalData =
          documentSnapshot.data() as Map<String, dynamic>;
      Hospital hospital = Hospital.fromMap(hospitalData);
      List<Department> listKhoa = hospital.khoa;
      for (Department i in listKhoa) {
        if (i.id == idKhoa) {
          khoaKham = i;
          return khoaKham;
        }
      }
    }
  }
  return Department.defaultContructor();
}