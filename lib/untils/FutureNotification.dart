import 'dart:convert';
import 'dart:io';

import 'package:app_doan_flutter/untils/FutureManageDoctor.dart';
import 'package:app_doan_flutter/untils/FutureUser.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:http/http.dart';

import '../screens/login/data/User.dart';
import '../screens/manage/data/Doctor.dart';

FirebaseMessaging messageUser = FirebaseMessaging.instance;
FirebaseMessaging messageDoctor = FirebaseMessaging.instance;
// Future<String> getFirebaseMessagingTokenUsers() async {
//   print('Co vao get');
//   NotificationSettings settings = await messageUser.requestPermission(
//     alert: true,
//     announcement: false,
//     badge: true,
//     carPlay: false,
//     criticalAlert: false,
//     provisional: false,
//     sound: true,
//   );

//   if (settings.authorizationStatus == AuthorizationStatus.authorized) {
//     print('User granted permission');
//   } else if (settings.authorizationStatus == AuthorizationStatus.provisional) {
//     print('User granted provisional permission');
//   } else {
//     print('User declined or has not accepted permission');
//   }
//   String? fcmToken = '';
//   fcmToken = await messageUser.getToken();
//   print(fcmToken);
//   return fcmToken!;
// }

Future<String> refreshFirebaseMessagingTokenUser() async {
  // Delete the existing FCM token
  await messageUser.deleteToken();

  // Get the new FCM token
  String? newToken = await messageUser.getToken();
  print('Refreshed FCM Token: $newToken');
  return newToken!;
}

Future<String> refreshFirebaseMessagingTokenDoctor() async {
  // Delete the existing FCM token
  await messageDoctor.deleteToken();

  // Get the new FCM token
  String? newToken = await messageDoctor.getToken();
  print('Refreshed FCM Token: $newToken');
  return newToken!;
}
// Future<String> getFirebaseMessagingTokenDoctor() async {
//   await messageDoctor.requestPermission();
//   print('Co vao get');
//   await messageDoctor.requestPermission();
//   String? fcmToken = '';
//   fcmToken = await messageDoctor.getToken();
//   print(fcmToken);
//   return fcmToken!;
// }

Future<void> sendPushNotificationByUser(
    String idUsers, String message, String idDoctor) async {
  Users users = await readBanThan(idUsers);
  Doctor doctor = await readIDDoctor(idDoctor);
  try {
    final body = {
      "to": doctor.pushToken,
      "notification": {"title": users.ten, "body": message}
    };
    var url = Uri.parse('https://fcm.googleapis.com/fcm/send');
    var res = await post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader:
              "key=AAAAL59ha9c:APA91bFzQ9qnbqeQlgvdOdak__Z56w3HhJUymYZYxxIU7_w1vBXkkoqgGeFDYf06jwIB9_upvQVmh92W1R_TZ1UwI7I2f_s0gDC95-bu5ZoI4TfJas5UKKfCn8JlTp0FegC-z6mMl1DM"
        },
        body: jsonEncode(body));
    print("Response status: ${res.statusCode}");
    print("Response body: ${res.body}");
  } catch (e) {
    // TODO
    print("Response error: $e");
  }
}

Future<void> sendPushNotificationByDoctor(
    String idDoctor, String message, String idUsers) async {
  Doctor doctor = await readIDDoctor(idDoctor);
  Users users = await readBanThan(idUsers);
  try {
    final body = {
      "to": users.pushToken,
      "notification": {"title": doctor.ten, "body": message}
    };
    var url = Uri.parse('https://fcm.googleapis.com/fcm/send');
    var res = await post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader:
              "key=AAAAL59ha9c:APA91bFzQ9qnbqeQlgvdOdak__Z56w3HhJUymYZYxxIU7_w1vBXkkoqgGeFDYf06jwIB9_upvQVmh92W1R_TZ1UwI7I2f_s0gDC95-bu5ZoI4TfJas5UKKfCn8JlTp0FegC-z6mMl1DM"
        },
        body: jsonEncode(body));
    print("Response status: ${res.statusCode}");
    print("Response body: ${res.body}");
  } catch (e) {
    // TODO
    print("Response error: $e");
  }
}
