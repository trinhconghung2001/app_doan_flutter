import 'package:cloud_firestore/cloud_firestore.dart';

import '../screens/homeDoctor/resultExamination/Result.dart';

Future<Result> readResult(String idUser, String idKetQua) async {
  DocumentReference documentReference =
      FirebaseFirestore.instance.collection('RegistrationSchedule').doc(idUser);
  DocumentSnapshot documentSnapshot = await documentReference.get();
  if (documentSnapshot.exists) {
    Map<String, dynamic> userData =
        documentSnapshot.data() as Map<String, dynamic>;
    try {
      List<Map<String, dynamic>> result = List<Map<String, dynamic>>.from(
          userData['resultExamination'] as List);
      List<Result> listResult = [];
      for (int i = 0; i < result.length; i++) {
        listResult.add(Result.fromMap(result[i]));
      }
      for (Result e in listResult) {
        if (e.id == idKetQua) {
          return e;
        }
      }
    } catch (e) {
      return Result.defaultContructor();
    }
  }

  return Result.defaultContructor();
}

Future<List<Result>> readListResult(String idUser) async {
  DocumentReference documentReference =
      FirebaseFirestore.instance.collection('RegistrationSchedule').doc(idUser);
  DocumentSnapshot documentSnapshot = await documentReference.get();
  if (documentSnapshot.exists) {
    Map<String, dynamic> userData =
        documentSnapshot.data() as Map<String, dynamic>;
    List<Map<String, dynamic>> result = [];
    try {
      //convert array data -> List
      result = List<Map<String, dynamic>>.from(
          userData['resultExamination'] as List);
      //reset lai list nguoi than
    } catch (e) {
      return [];
    }
    List<Result> listResult = [];
    for (int i = 0; i < result.length; i++) {
      listResult.add(Result.fromMap(result[i]));
    }
    return listResult;
  }
  return [];
}
