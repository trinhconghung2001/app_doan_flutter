import 'package:app_doan_flutter/screens/manage/data/DepartmentAdvise.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../screens/manage/data/Department.dart';
import '../screens/manage/data/Hospital.dart';
import '../screens/manage/data/Room.dart';
import 'FutureDepartment.dart';
import 'StorePreferences.dart';

// Future<List<Room>> readAllRoomVaccine_ByIdHospital(String idHosital) async {
//   Hospital hospital = await readIDHospital(idHosital);
//   List<Room> listRoom = [];
//   listRoom = hospital.phongTiem;
//   return listRoom;
// }

Future<List<Room>> readAllRoomExamination_ByIdHospital_ByIdKhoa(
    String idKhoa, String idHosital) async {
  List<Department> listDepartment =
      await readAllDepartment_ByIdHospital(idHosital);
  Department khoa = Department.defaultContructor();
  for (Department e in listDepartment) {
    if (e.id == idKhoa) {
      khoa = e;
      break;
    }
  }
  List<Room> listRoom = khoa.phongKham;
  return listRoom;
}

Future<List<Room>> readAllRoomAdvise_ByIdHospital_ByIdKhoa(
    String idKhoa, String idHosital) async {
  List<DepartmentAdvise> listDepartmentAdvise =
      await readAllDepartmentAdvise_ByIdHospital(idHosital);
  DepartmentAdvise khoaTuVan = DepartmentAdvise.defaultContructor();
  for (DepartmentAdvise e in listDepartmentAdvise) {
    if (e.id == idKhoa) {
      khoaTuVan = e;
      break;
    }
  }
  List<Room> listRoom = khoaTuVan.phongTuVan;
  return listRoom;
}

Future<Room> readIDPhongTiem(String id) async {
  if (StorePreferences.getIdHospital() != '') {
    CollectionReference collectionReference =
        FirebaseFirestore.instance.collection('Hospital');
    QuerySnapshot querySnapshot = await collectionReference.get();
    for (QueryDocumentSnapshot doc in querySnapshot.docs) {
      DocumentSnapshot documentSnapshot =
          await collectionReference.doc(doc.id).get();
      if (documentSnapshot.exists) {
        //lay du lieu
        Map<String, dynamic> userData =
            documentSnapshot.data() as Map<String, dynamic>;
        List<Room> listPhong = [];
        if (userData['id'] == StorePreferences.getIdHospital()) {
          List<Map<String, dynamic>> list =
              List<Map<String, dynamic>>.from(userData['phongTiem'] as List);
          for (int i = 0; i < list.length; i++) {
            listPhong.add(Room.fromMap(list[i]));
          }
        } else {
          listPhong = [];
        }
        for (Room i in listPhong) {
          if (i.id == id) {
            return i;
          }
        }
      } else {
        return Room.defaultContructor();
      }
    }
  }
  return Room.defaultContructor();
}

Future<Room> readIDPhongKham(String idKhoa, String idPhong) async {
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('Hospital');
  QuerySnapshot querySnapshot = await collectionReference.get();
  for (QueryDocumentSnapshot doc in querySnapshot.docs) {
    DocumentSnapshot documentSnapshot =
        await collectionReference.doc(doc.id).get();
    if (documentSnapshot.exists) {
      Map<String, dynamic> hospitalData =
          documentSnapshot.data() as Map<String, dynamic>;
      Hospital hospital = Hospital.fromMap(hospitalData);
      List<Department> listKhoa = hospital.khoa;
      Department khoaKham = Department.defaultContructor();
      for (Department i in listKhoa) {
        if (i.id == idKhoa) {
          khoaKham = i;
          break;
        }
      }
      List<Room> listPhong = khoaKham.phongKham;
      for (Room e in listPhong) {
        if (e.id == idPhong) {
          return e;
        }
      }
    }
  }
  return Room.defaultContructor();
}
