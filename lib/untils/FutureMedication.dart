import 'package:app_doan_flutter/screens/home/bookMedication/data/Medication.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

Future<List<Medication>> readListMedication(String idUser) async {
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('RegistrationSchedule');
  DocumentSnapshot documentSnapshot =
      await collectionReference.doc(idUser).get();
  if (documentSnapshot.exists) {
    //lay du lieu
    Map<String, dynamic> userData =
        documentSnapshot.data() as Map<String, dynamic>;
    List<Map<String, dynamic>> result = [];
    try {
      //convert array data -> List
      result = List<Map<String, dynamic>>.from(userData['medication'] as List);
      //reset lai list nguoi than
    } catch (e) {
      return [];
    }
    List<Medication> listMedication = [];
    for (int i = 0; i < result.length; i++) {
      listMedication.add(Medication.fromMap(result[i]));
    }
    return listMedication;
  } else {
    return [];
  }
}
