import 'package:app_doan_flutter/screens/manage/data/Doctor.dart';

import '../config/styles/KeyValue.dart';
import '../screens/login/data/User.dart';
import 'FutureManageDoctor.dart';
import 'FutureMessage.dart';
import 'FutureUser.dart';

Future<bool> checkCalendarAdvise(String idDoctor) async {
  List<Users> listUsers = await getAllUser();
  List<Users> listUser = [];
  listUser = listUsers;
  for (Users e in listUsers) {
    int soPhanTu = await readLengthMessage(idDoctor, e.id);
    if (soPhanTu < 0) {
      listUser.remove(e);
    }
  }
  if (listUser.length > 0) {
    return true;
  } else {
    return false;
  }
}

Future<List<Doctor>> readAllDoctor_Advise() async {
  List<Doctor> list = [];
  List<Doctor> listDoctor = await readAllDoctor();
  for (Doctor e in listDoctor) {
    if (e.role == key_role_doctor_advise) {
      list.add(e);
    }
  }
  return list;
}
