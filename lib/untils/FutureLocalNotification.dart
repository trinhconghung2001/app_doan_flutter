import 'dart:convert';
import 'dart:io';

import 'package:app_doan_flutter/untils/FutureUser.dart';
import 'package:app_doan_flutter/untils/StorePreferences.dart';
import 'package:http/http.dart';

import '../screens/login/data/User.dart';

Future<void> sendLocationNotification() async {
  Users users = await readBanThan(StorePreferences.getIdUser());
  try {
    final body = {
      "to": users.pushToken,
      "notification": {"title": 'Thông báo', "body": 'Đã đến giờ uống thuốc'}
    };
    var url = Uri.parse('https://fcm.googleapis.com/fcm/send');
    var res = await post(url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader:
              "key=AAAAL59ha9c:APA91bFzQ9qnbqeQlgvdOdak__Z56w3HhJUymYZYxxIU7_w1vBXkkoqgGeFDYf06jwIB9_upvQVmh92W1R_TZ1UwI7I2f_s0gDC95-bu5ZoI4TfJas5UKKfCn8JlTp0FegC-z6mMl1DM"
        },
        body: jsonEncode(body));
    print("Response status: ${res.statusCode}");
    print("Response body: ${res.body}");
  } catch (e) {
    // TODO
    print("Response error: $e");
  }
}