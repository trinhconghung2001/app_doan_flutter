import 'package:app_doan_flutter/screens/manage/data/Hospital.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

Future<Hospital> readIDHospital(String idBenhVien) async {
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('Hospital');
  DocumentSnapshot documentSnapshot =
      await collectionReference.doc(idBenhVien).get();
  if (documentSnapshot.exists) {
    Map<String, dynamic> result =
        documentSnapshot.data() as Map<String, dynamic>;
    return Hospital.fromMap(result);
  }
  return Hospital.defaultContructor();
}

Future<List<Hospital>> readAllHospital() async {
  List<Hospital> listHospital = [];
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('Hospital');
  QuerySnapshot querySnapshot = await collectionReference.get();
  for (QueryDocumentSnapshot doc in querySnapshot.docs) {
    DocumentSnapshot documentSnapshot =
        await collectionReference.doc(doc.id).get();
    if (documentSnapshot.exists) {
      //lay du lieu
      Map<String, dynamic> userData =
          documentSnapshot.data() as Map<String, dynamic>;
      listHospital.add(Hospital.fromMap(userData));
    }
  }
  return listHospital;
}
