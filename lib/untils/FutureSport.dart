import 'package:app_doan_flutter/screens/home/calcularCalo/data/Sport.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'FutureCalo.dart';

Future<List<Sport>> readCategorySport() async {
  DocumentReference documentReference = FirebaseFirestore.instance
      .collection('Categorys')
      .doc('8nDu9e5HN5HkdyhmO4Lz');
  DocumentSnapshot documentSnapshot = await documentReference.get();
  if (documentSnapshot.exists) {
    Map<String, dynamic> dataCategory =
        documentSnapshot.data() as Map<String, dynamic>;
    List<dynamic> list = dataCategory['sport'];
    List<Sport> listSport = list.map((e) => Sport.fromMap(e)).toList();
    return listSport;
  }
  return [];
}

Future<List<Sport>> readCategorySportUser(String idUser) async {
  DocumentReference documentReference =
      FirebaseFirestore.instance.collection('Sport').doc(idUser);
  DocumentSnapshot documentSnapshot = await documentReference.get();
  if (documentSnapshot.exists) {
    Map<String, dynamic> dataFood =
        documentSnapshot.data() as Map<String, dynamic>;
    try {
      List<dynamic> list = dataFood['sport'];
      List<Sport> listSport = list.map((e) => Sport.fromMap(e)).toList();
      return listSport;
    } catch (e) {
      // TODO
      return [];
    }
  }
  return [];
}

Future<List<Sport>> readListAllSport(String idUser) async {
  List<Sport> listUser = await readCategorySportUser(idUser);
  List<Sport> listCategory = await readCategorySport();
  List<Sport> listAll = [];
  listAll = listUser + listCategory;
  return listAll;
}

Future<List<Sport>> readListSportDay(String idUser, String date) async {
  Map<String, dynamic> resultCalo = await readCalo(idUser);
  Map<String, dynamic> mapCalo = resultCalo[date] as Map<String, dynamic>;
  List<dynamic> listDynamic = [];
  listDynamic = mapCalo['theThao'];
  List<Sport> listSport = [];
  listSport = listDynamic.map((e) => Sport.fromMap(e)).toList();
  return listSport;
}
