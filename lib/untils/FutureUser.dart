import 'package:cloud_firestore/cloud_firestore.dart';

import '../screens/login/data/FamilyMember.dart';
import '../screens/login/data/User.dart';

Future<FamilyMember> readIDBenhNhan(String idUser, String idBenhNhan) async {
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection("Users");
  DocumentReference documentReference = collectionReference.doc(idUser);
  DocumentSnapshot documentSnapshot = await documentReference.get();
  if (documentSnapshot.exists) {
    Map<String, dynamic> userData =
        documentSnapshot.data() as Map<String, dynamic>;
    Users banThan = Users.fromMap(userData);
    //neu la ban than
    if (idBenhNhan == idUser) {
      return FamilyMember(
          id: banThan.id,
          idUser: idUser,
          imgURL: banThan.imgURL,
          moiQH: banThan.moiQH,
          ten: banThan.ten,
          sdt: banThan.sdt,
          soTheBHYT: banThan.soTheBHYT,
          ngaySinh: banThan.ngaySinh,
          ngheNghiep: banThan.ngheNghiep,
          gioiTinh: banThan.gioiTinh,
          danToc: banThan.danToc,
          tinhThanhPho: banThan.tinhThanhPho,
          quanHuyen: banThan.quanHuyen,
          phuongXa: banThan.phuongXa,
          soNha: banThan.soNha);
    } else {
      List<FamilyMember> listHoSo = banThan.listMember;
      for (FamilyMember i in listHoSo) {
        if (i.id == idBenhNhan) {
          return i;
        }
      }
    }
  }
  return FamilyMember.defaultContructor();
}

Future<List<FamilyMember>> readListNguoiThan(String idUser) async {
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('Users');
  DocumentSnapshot documentSnapshot =
      await collectionReference.doc(idUser).get();
  if (documentSnapshot.exists) {
    //lay du lieu
    Map<String, dynamic> userData =
        documentSnapshot.data() as Map<String, dynamic>;
    //convert array data -> List
    List<Map<String, dynamic>> listHoSo =
        List<Map<String, dynamic>>.from(userData['listNguoiThan'] as List);
    //reset lai list nguoi than
    List<FamilyMember> listNguoiThan = [];
    for (int i = 0; i < listHoSo.length; i++) {
      listNguoiThan.add(FamilyMember.fromMap(listHoSo[i]));
    }
    return listNguoiThan;
  }
  return [];
}

Future<Users> readBanThan(String idUsers) async {
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('Users');
  DocumentSnapshot documentSnapshot =
      await collectionReference.doc(idUsers).get();
  if (documentSnapshot.exists) {
    //lay du lieu
    Map<String, dynamic> userData =
        documentSnapshot.data() as Map<String, dynamic>;
    return Users.fromMap(userData);
  }
  return Users.defaultContructor();
}

Future<List<Users>> getAllUser() async {
  List<Users> listUsers = [];
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('Users');
  QuerySnapshot querySnapshot = await collectionReference.get();
  for (QueryDocumentSnapshot doc in querySnapshot.docs) {
    DocumentSnapshot documentSnapshot =
        await collectionReference.doc(doc.id).get();
    if (documentSnapshot.exists) {
      //lay du lieu
      Map<String, dynamic> userData =
          documentSnapshot.data() as Map<String, dynamic>;
      listUsers.add(Users.fromMap(userData));
    }
  }
  return listUsers;
}
