import 'package:app_doan_flutter/screens/home/bookVaccine/data/Vaccination.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../screens/home/suggestVaccine/Vaccine.dart';

Future<Vaccine> readIDGoiVaccine(String idVaccine) async {
  DocumentReference documentReference = FirebaseFirestore.instance
      .collection("Categorys")
      .doc('8nDu9e5HN5HkdyhmO4Lz');
  DocumentSnapshot documentSnapshot = await documentReference.get();
  if (documentSnapshot.exists) {
    Map<String, dynamic> dataCategory =
        documentSnapshot.data() as Map<String, dynamic>;
    List<Map<String, dynamic>> result =
        List<Map<String, dynamic>>.from(dataCategory['goiVaccine'] as List);
    List<Vaccine> listVaccine = [];
    listVaccine = result.map((e) => Vaccine.fromMap(e)).toList();
    for (Vaccine i in listVaccine) {
      if (i.id == idVaccine) {
        return i;
      }
    }
  }
  return Vaccine.defaultContructor();
}

Future<List<Vaccination>> readAllVaccination(String idUser) async {
  DocumentReference documentReference =
      FirebaseFirestore.instance.collection('RegistrationSchedule').doc(idUser);
  DocumentSnapshot documentSnapshot = await documentReference.get();
  if (documentSnapshot.exists) {
    Map<String, dynamic> userData =
        documentSnapshot.data() as Map<String, dynamic>;
    List<Map<String, dynamic>> result = [];
    try {
      result = List<Map<String, dynamic>>.from(userData['vaccination'] as List);
    } catch (e) {
      return [];
    }
    List<Vaccination> listVaccination = [];
    for (int i = 0; i < result.length; i++) {
      listVaccination.add(Vaccination.fromMap(result[i]));
    }
    return listVaccination;
  }
  return [];
}

// Future<List<Vaccination>> readAllVaccination_byIDHosital(
//     String idBenhVien) async {
//   Hospital hospital = await readIDHospital(idBenhVien);
//   List<Vaccination> listVaccination = [];
//   List<Map<String, dynamic>> listTiem = hospital.lichTiem;
//   for (int i = 0; i < listTiem.length; i++) {
//     String idUser = listTiem[i]['idUser'];
//     String idLichTiem = listTiem[i]['idLichTiem'];
//     Vaccination vaccination =
//         await readVaccination_byIDUser_byIDVaccination(idUser, idLichTiem);
//     listVaccination.add(vaccination);
//   }
//   return listVaccination;
// }

Future<Vaccination> readVaccination_byIDUser_byIDVaccination(
    String idUser, String idLichTiem) async {
  List<Vaccination> list = await readAllVaccination(idUser);
  for (Vaccination i in list) {
    if (i.id == idLichTiem) {
      return i;
    }
  }
  return Vaccination.defaultContructor();
}
