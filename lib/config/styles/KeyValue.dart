const defaultHoSo = "1";
const defaultBenhVien = "2";
const defaultGioTuVan = "3";
const defaultTrieuChung = "4";
const defaultGioKham = "5";
const defaultLoaiKham = "6";
const defaultGioTiem = "7";
const defaultGoiVaccine = "8";
const defaultTienSuBenh = "9";
const defaultQuanHe = "10";
const defaultGioiTinh = "11";
const defaultDanToc = "12";
const defaultTinh = "13";
const defaultHuyen = "14";
const defaultXa = "15";
const defaultGioThongBao = "16";
const defaultStatusMedication = "17";
const defaultStatusBook = "18";
const defaultPhongTiem = "19";
const defaultKhoa = "20";
const defaultPhongKham = "21";
const defaultStatusResult = "22";
const defaultTypeMedication = "23";
const defaultKhoaTuVan = "24";
const defaultCachDung = '25';
const defaultTypeMedication2 = "26";
const defaultIntensity = "27";
const defaultUnit = '28';

const status_1 = "Mới đăng ký";
const status_2 = "Đã xác nhận";
const status_3 = "Đã hủy";
const status_4 = "Đang xác nhận";
const status_5 = "Bác sĩ từ chối";

const status_confirm_1 = "Xác nhận";
const status_confirm_2 = "Hủy";

const status_result_1 = "Đã trả kết quả";
const status_result_2 = "Chưa trả kết quả";

const status_medication_1 = "Đang uống";
const status_medication_2 = "Sắp uống";
const status_medication_3 = "Đã uống";

const key_account_reception = "reception";
const key_account_user = 'user';
const key_account_doctor = 'doctor';

const key_adults = "Người trưởng thành";
const key_woment_1 = "Phụ nữ chuẩn bị mang thai";
const key_woment_2 = "Phụ nữ mang thai và cho con bú";
const key_old_person = "Người cao tuổi";
const key_old_person_2 = "Người mắc bệnh nền";

const key_role_reception = "Tiếp nhận";
const key_role_doctor_examination = "Bác sĩ khám bệnh";
const key_role_doctor_vaccination = "Bác sĩ tiêm vaccine";
const key_role_doctor_advise = "Bác sĩ tư vấn";

const key_intensity_few = "Ít hoạt động, chỉ ăn đi làm về ngủ";
const key_intensity_little = "Có tập nhẹ nhàng, tuần 1-3 buổi";
const key_intensity_medium = "Có vận động vừa 4-5 buổi";
const key_intensity_high = "Vận động nhiều 6-7 buổi";
const key_intensity_very_high = "Vận động rất nhiều, ngày tập 2 lần";
const key_title_few = "Rất ít";
const key_title_little = "Ít";
const key_title_medium = "Trung bình";
const key_title_high = "Cao";
const key_title_very_high = "Rất cao";

const conclusion_underweight = "Nhẹ cân";
const conclusion_normal = "Bình thường";
const conclusion_overweight = "Thừa cân";
const conclusion_obesity_1 = "Béo phì độ 1";
const conclusion_obesity_2 = "Béo phì độ 2";
const conclusion_obesity_3 = "Béo phì độ 3";

const food_breakfase = "Bữa sáng";
const food_lunch = "Bữa trưa";
const food_dinner = "Bữa tối";
const food_snacks = "Bữa phụ";

const unit_gram = "gam";
const unit_teaspoon = "Muỗng cà phê (tsp)";
const unit_tablespoon = "Muỗng canh (tbsp)";
const unit_cup = "Cốc (cup)";
