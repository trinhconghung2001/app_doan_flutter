import 'package:flutter/material.dart';

const statusColor = Color(0xFF1C4983);
const primaryColor = Color(0xFF6F9BD4);
const filterColor = Color(0xFFBCD2EF);
const secondColor = Color(0xFF5CBBB8);
const warningColor = Color(0xFFCC3300);
const successColor = Color(0xFF4BB543);
const backgroundScreenColor = Color(0xFFF5F5F5);
const buttonTextColor = Color(0xffFDFDFD);
const backgroudSearch = Color(0xFF5CBBB8);
const huyColor = Color(0xFFE0E0E0);
const textColor = Color(0xFF292D34);
const buttonColor = Color(0xFF239FDB);
const MaterialColor primaryThemeColor =
    const MaterialColor(0xFF6F9BD4, const <int, Color>{
  50: Color(0xFF6F9BD4),
  100: Color(0xFF6F9BD4),
  200: Color(0xFF6F9BD4),
  300: Color(0xFF6F9BD4),
  400: Color(0xFF6F9BD4),
  500: Color(0xFF6F9BD4),
  600: Color(0xFF6F9BD4),
  700: Color(0xFF6F9BD4),
  800: Color(0xFF6F9BD4),
  900: Color(0xFF6F9BD4),
});
