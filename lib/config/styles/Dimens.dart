const defaultIcon = 14.0;
const defaultButtonHeight = 46.0;
const defaultButtonRadius = 23.0;
const defaultButtonText = 17.0;
const defaultPadding = 16.0;
const defaultPaddingMin = 8.0;
const defaultTextInsideBoxPadding = 10.0;
